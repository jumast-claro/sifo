﻿using System;

namespace Jumast.Claro.Vantive.DataAcces.Abstract
{
    public interface IVantiveDataModel
    {
        string Inbox { get; set; }
        string Clase { get; set; }
        string Cliente { get; set; }
        string Ejecutivo { get; set; }
        string Estado { get; set; }
        DateTime? FechaDeInstalacion { get; set; }
        DateTime? FechaDeRecibido { get; set; }
        string ID { get; set; }
        string Identificador { get; set; }
        string IdObjeto { get; set; }
        string Localidad { get; set; }
        string MotivoInterrupcion { get; set; }
        string MotivoOrden { get; set; }
        string Provincia { get; set; }
        string TomadoPor { get; set; }
        string UnidadDeNegocio { get; set; }
    }
}