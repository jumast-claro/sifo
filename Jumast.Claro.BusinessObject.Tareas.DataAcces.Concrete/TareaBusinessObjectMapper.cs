﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumast.Claro.BusinessObject.Tareas.DataAcces.Abstract;

namespace Jumast.Claro.BusinessObject.Tareas.DataAcces.Concrete
{
    public class TareaBusinessObjectMapper : ITareaBusinessObjectMapper
    {
        private const string DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
        private const string DATE_FORMAT = "dd/MM/yyyy";


        private DateTime? getNullableDateTime(string value, string stringFormat)
        {
            if (value == string.Empty)
            {
                return null;
            }
            return DateTime.ParseExact(value, stringFormat, CultureInfo.InvariantCulture);
        }

        private DateTime getDateTime(string value, string stringFormat)
        {
            return DateTime.ParseExact(value, stringFormat, CultureInfo.InvariantCulture);
        }

        private int getInt(string value)
        {
            return int.Parse(value);
        }

        public ITareaBusinessObjectDataModel MapDataTransferObjectToDataModel(ITareaBusinessObjectDataTransferObject dataTransferObject)
        {
            ITareaBusinessObjectDataModel dataModel = new TareaBusinessObjectDataModel();

            dataModel.UltimaNotaAutor = dataTransferObject.UltimaNotaAutor;
            dataModel.Calle = dataTransferObject.Calle;
            dataModel.Cliente = dataTransferObject.Cliente;
            dataModel.DiasDeInstalacion = null;
            dataModel.Enlace = dataTransferObject.Enlace;
            dataModel.FechaDeCompromiso = getNullableDateTime(dataTransferObject.FechaCompromisoOrden, DATE_FORMAT);
            dataModel.FechaFinTarea = getNullableDateTime(dataTransferObject.FechaFinTarea, DATE_TIME_FORMAT);
            dataModel.FechaInicioTarea = getDateTime(dataTransferObject.FechaInicioTarea, DATE_TIME_FORMAT);
            dataModel.FechaRenegociada = getNullableDateTime(dataTransferObject.FechaRenegociada, DATE_FORMAT);
            dataModel.UltimaNotaFecha = getDateTime(dataTransferObject.UltimaNotaFecha, DATE_TIME_FORMAT);
            dataModel.Identificador = dataTransferObject.Identificador;
            dataModel.Inbox = dataTransferObject.Inbox;
            dataModel.Localidad = dataTransferObject.Localidad;
            dataModel.Numero = dataTransferObject.Numero;
            dataModel.Orden = getInt(dataTransferObject.Orden);
            dataModel.Partido = dataTransferObject.Partido;
            dataModel.Producto = dataTransferObject.Producto;
            dataModel.Provincia = dataTransferObject.Provincia;
            dataModel.Resultado = dataTransferObject.Resultado;
            dataModel.TareaID = getInt(dataTransferObject.TareaId);
            dataModel.TipoDeMovimiento = dataTransferObject.TipoMovimiento;
            dataModel.TipoDeOrden = dataTransferObject.TipoDeOrden;
            dataModel.TipoDeSolucion = dataTransferObject.TipoDeSolucion;
            dataModel.TomadoPor = dataTransferObject.TomadoPor;
            dataModel.UltimaNota = dataTransferObject.UltimaNota;
            dataModel.UnidadDeNegocio = null;
            dataModel.WorkflowFechaInicio = getNullableDateTime(dataTransferObject.WorkflowFechaInicio, DATE_FORMAT);
            dataModel.WorkflowFechaFin = getNullableDateTime(dataTransferObject.WorkflowFechaFin, DATE_FORMAT);
            dataModel.WorkflowID = 0;

            return dataModel;
        }
    }
}
