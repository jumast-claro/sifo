﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumast.Claro.BusinessObject.Tareas.DataAcces.Abstract;

namespace Jumast.Claro.BusinessObject.Tareas.DataAcces.Concrete
{
    public class TareaBusinessObjectDataModel : ITareaBusinessObjectDataModel
    {
        public int TareaID { get; set; }
        public string Cliente { get; set; }
        public string Inbox { get; set; }
        public string TipoDeMovimiento { get; set; }
        public string Producto { get; set; }
        public string TipoDeOrden { get; set; }
        public string TipoDeSolucion { get; set; }
        public DateTime? FechaDeCompromiso { get; set; }
        public DateTime? FechaRenegociada { get; set; }
        public DateTime FechaInicioTarea { get; set; }
        public DateTime? FechaFinTarea { get; set; }
        public string Resultado { get; set; }
        public string TomadoPor { get; set; }
        public int Orden { get; set; }
        public string Enlace { get; set; }
        public string Identificador { get; set; }
        public DateTime UltimaNotaFecha { get; set; }
        public string UltimaNotaAutor { get; set; }
        public string UltimaNota { get; set; }
        public DateTime? WorkflowFechaInicio { get; set; }
        public DateTime? WorkflowFechaFin { get; set; }
        public string Provincia { get; set; }
        public string Partido { get; set; }
        public string Localidad { get; set; }
        public string Calle { get; set; }
        public string Numero { get; set; }
        public string Clase { get; set; }
        public string UnidadDeNegocio { get; set; }
        public int? DiasDeInstalacion { get; set; }
        public int WorkflowID { get; set; }
    }
}
