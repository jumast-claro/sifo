﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumast.Claro.BusinessObject.Tareas.DataAcces.Concrete
{
    public class TareaBusinessObjectDataTransferObject : ITareaBusinessObjectDataTransferObject
    {
        public string RegionFinal { get; set; } = "";
        public string Zona { get; set; } = "";
        public string Inbox { get; set; } = "";
        public string InboxFinal { get; set; } = "";
        public string TipoMovimiento { get; set; } = "";
        public string Tecnologia { get; set; } = "";
        public string TipoFibraOptica { get; set; } = "";
        public string Producto { get; set; }
        public string TipoDeOrden { get; set; } = ""; //
        public string TipoDeSolucion { get; set; } = "";
        //public string FechaInicioOrden { get; set; }
        public string FechaCompromisoOrden { get; set; } = "";
        public string FechaRenegociada { get; set; } = "";
        public string FechaInicioTarea { get; set; } = "";
        public string FechaFinTarea { get; set; } = ""; //
        //public string FechaCierreTareaAnterior { get; set; } = "";
        public string Resultado { get; set; } = "";
        public string TomadoPor { get; set; } = "";
        public string Cliente { get; set; } = "";
        public string Orden { get; set; } = "";
        public string Enlace { get; set; } = "";
        public string TareaId { get; set; } = "";
        public string Identificador { get; set; } = "";
        public string UltimaNotaFecha { get; set; } = "";
        public string UltimaNotaAutor { get; set; } = "";
        public string UltimaNota { get; set; } = "";
        public string WorkflowFechaInicio { get; set; } = "";
        public string WorkflowFechaFin { get; set; } = "";
        public string Provincia { get; set; } = "";
        public string Partido { get; set; } = "";
        public string Localidad { get; set; } = "";
        public string Calle { get; set; } = "";
        public string Numero { get; set; }
    }
}
