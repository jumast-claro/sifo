﻿
using System.Runtime.InteropServices;

namespace SIFO.DataAcces.TareasCerradas
{
    public class TareaAnalizadaDataTransferObject
    {
        public string TareaId { get; set; }
        public string Vantive_MalCargadaLaDireccion { get; set; }
        public string Vantive_FaltaDeContacto { get; set; }
        public string Vantive_MalTipificada { get; set; }
        public string Factibilidad_MalFactibilizado { get; set; }
        public string Factibilidad_CambioEnLasCondiciones { get; set; }
        public string Relevamiento_ProblemaDeContacto { get; set; }
        public string Relevamiento_ProblemaDeIngreso { get; set; }
        public string Relevamiento_SitioEnConstruccion { get; set; }
        public string InicioDePermisos_FaltaDeAutorizacionDelCliente { get; set; }
        public string InicioDePermisos_FaltaDeAutorizacionPorMotivosInternos { get; set; }
        public string ObraCivil_SitioEnConstruccion { get; set; }
        public string ObraCivil_ProblemaDeIngreso { get; set; }
        public string FaltaIngenieria { get; set; }
        public string FaltaAsignacion { get; set; }
        public string Tendido_SitioEnConstruccion { get; set; }
        public string Tendido_ProblemaDeIngreso { get; set; }
        public string Tendido_FaltaDeMateriales { get; set; }
        public string Odf_SitioEnConstruccion { get; set; }
        public string Odf_ProblemaDeIngreso { get; set; }
        public string Odf_FaltaDeMateriales { get; set; }
        public string SuspensionDeTareasAPedidoDelPM { get; set; }
        public string Fusiones_SoloInterior { get; set; }
        public string FreezingDeRed { get; set; }
        public string AdecuacionDeRed { get; set; }
        public string RadioBase_ProblemaDeIngreso { get; set; }
        public string Permiso_DemoraExtraordinaria { get; set; }
    }
}
