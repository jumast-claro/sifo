﻿using System;

namespace SIFO.DataAcces.TareasCerradas
{
    public class CvsAttribute : Attribute
    {
        public int Position { get; }

        public CvsAttribute(int position)
        {
            Position = position;
        }
    }
}