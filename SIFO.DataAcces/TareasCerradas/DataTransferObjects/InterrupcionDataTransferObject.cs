namespace SIFO.DataAcces.TareasCerradas
{
    public class InterrupcionDataTransferObject
    {
        public string TareaId { get; set; }
        public string Motivo { get; set; }
        public string TiempoEnDias { get; set; }
    }
}