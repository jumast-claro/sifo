using System.Collections.Generic;

namespace SIFO.DataAcces.TareasCerradas
{
    public interface ITareasAnalizadasRepository
    {
        IEnumerable<TareaAnalizadaDataTransferObject> SelectAll();
        TareaAnalizadaDataTransferObject SelectByTareaId(string tareaId);
        void SaveAll(IEnumerable<TareaAnalizadaDataTransferObject> data);
        void Save(TareaAnalizadaDataTransferObject dto);
    }
}