﻿using System.Linq;

namespace SIFO.DataAcces.TareasCerradas
{
    public class TareasAnalizadasCsvRepository : CsvRepository<TareaAnalizadaDataTransferObject>, ITareasAnalizadasRepository
    {
        public TareasAnalizadasCsvRepository(string path, string delimiter, bool hasFieldsEnclosedInQuotes) : base(path, delimiter, hasFieldsEnclosedInQuotes)
        {
        }

        public TareaAnalizadaDataTransferObject SelectByTareaId(string tareaId)
        {
            return this.SelectAll().FirstOrDefault(analizada => analizada.TareaId == tareaId); 
        }



        public bool Contains(string tareaId)
        {
            return this.SelectByTareaId(tareaId) != null;
        }

        public void Save(TareaAnalizadaDataTransferObject dto)
        {
            if (this.Contains(dto.TareaId))
            {
                var list = this.SelectAll().ToList();
                var record = list.Find(item => item.TareaId == dto.TareaId);
                list.Remove(record);
                list.Add(dto);
                SaveAll(list);
            }
            else
            {
                var list = SelectAll().ToList();
                list.Add(dto);
                SaveAll(list);
            }
        }
    }
}
