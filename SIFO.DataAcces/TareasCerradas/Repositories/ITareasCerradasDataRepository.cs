using System.Collections.Generic;

namespace SIFO.DataAcces.TareasCerradas
{

    public interface IBusinessObjectDataRepository
    {
        IEnumerable<TareaDataTransferObject> SelectAll();
        void SaveAll(IEnumerable<TareaDataTransferObject> data);
        TareaDataTransferObject SelectByTareaId(string tareaId);
    }
}