using System.Linq;

namespace SIFO.DataAcces.TareasCerradas.Repositories
{
    public class BusinessObjectDataRepository : CsvRepository<TareaDataTransferObject>, IBusinessObjectDataRepository
    {
        public BusinessObjectDataRepository(string path, string delimiter, bool hasFieldsEnclosedInQuotes) : base(path, delimiter, hasFieldsEnclosedInQuotes)
        {
        }

        public TareaDataTransferObject SelectByTareaId(string tareaId)
        {
            return SelectAll().FirstOrDefault(dataTransferObject => dataTransferObject.TareaId.Equals(tareaId));
        }
    }
}