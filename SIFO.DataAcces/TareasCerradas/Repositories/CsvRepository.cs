﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SIFO.DataAcces.TareasCerradas
{
    public class CsvRepository<TDataTransferObject>
    {
        private readonly string _path;
        private readonly string _delimiter;
        private readonly bool _hasFieldsEnclosedInQuotes;

        public CsvRepository(string path, string delimiter, bool hasFieldsEnclosedInQuotes)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException($"No se encontró el archivo {path}");
            }
            _path = path;
            _delimiter = delimiter;
            _hasFieldsEnclosedInQuotes = hasFieldsEnclosedInQuotes;
        }

        public IEnumerable<TDataTransferObject> SelectAll()
        {
            var resultSet = new List<TDataTransferObject>();

            var csvReader = new CsvFileReader(_path, _delimiter, _hasFieldsEnclosedInQuotes);
            IEnumerable<string[]> fieldsArray = csvReader.ReadAllFields();

            var csvHelper = new CsvHelper();

            foreach (var fields in fieldsArray)
            {
                var dataTransferObject = Activator.CreateInstance<TDataTransferObject>();
                csvHelper.SetValues(dataTransferObject, fields);
                resultSet.Add(dataTransferObject);
            }
            return resultSet;
        }

        public void SaveAll(IEnumerable<TDataTransferObject> data)
        {
            List<List<KeyValuePair<int, string>>> list = new List<List<KeyValuePair<int, string>>>();
            var csvHelper = new CsvHelper();


            StringBuilder stringBuilder = new StringBuilder();
            foreach (var dto in data)
            {
                csvHelper.AddQuotesToDataTransferObject(dto);
                string line = csvHelper.createLine(dto);
                stringBuilder.AppendLine(line);
            }


            File.WriteAllText(_path, stringBuilder.ToString());
        }
    }
}
