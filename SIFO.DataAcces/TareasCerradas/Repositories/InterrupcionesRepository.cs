﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;

namespace SIFO.DataAcces.TareasCerradas
{
    public class InterrupcionesRepository
    {
        private const string PATH = @"C:\Users\Jumast\Desktop\db_interrupciones.csv";
        private const string DELIMITER = ",";
        private const int TAREA_ID = 0;
        private const int MOTIVO_INTERRUPCION = 1;
        private const int TIEMPO_INTERRUMPIDO = 2;


        public IEnumerable<InterrupcionDataTransferObject> SelectAll()
        {
            var resultado = new List<InterrupcionDataTransferObject>();

            using (var parser = new TextFieldParser(PATH))
            {
                parser.SetDelimiters(DELIMITER);
                parser.HasFieldsEnclosedInQuotes = true;
                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();
                    var dataTransferObject = new InterrupcionDataTransferObject()
                    {
                        TareaId = fields[TAREA_ID],
                        Motivo = fields[MOTIVO_INTERRUPCION],
                        TiempoEnDias = fields[TIEMPO_INTERRUMPIDO]
                    };
                    resultado.Add(dataTransferObject);
                }

            }
            return resultado;
        }

        public IEnumerable<InterrupcionDataTransferObject> SelectByTareaId(string tareaId)
        {
            return SelectAll().Where(dto => dto.TareaId == tareaId);
        }
    }
}
