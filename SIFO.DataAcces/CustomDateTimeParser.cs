using System;
using System.Globalization;

namespace SIFO.DataAcces
{
    public class CustomDateTimeParser
    {


        public DateTime? TryParseDate(string value)
        {
            if (value == string.Empty)
            {
                return null;
            }

            return DateTime.ParseExact(value, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }

        public DateTime ParseDate(string value)
        {



            if (value == string.Empty)
            {
                throw new ArgumentException("La cadena no puede ser vac�a");
            }

            return DateTime.ParseExact(value, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }

        public DateTime ParseDateTime(string value)
        {
            // temp
            //if (value.Equals(string.Empty))
            //{
            //    return DateTime.MinValue;
            //}

            var ans = DateTime.ParseExact(value, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            return ans;

        }
    }
}