﻿using System.Collections.Generic;
using System.Text;

namespace SIFO.DataAcces
{
    public class CsvHelper
    {
        public List<object> getValues(object instance)
        {
            var result = new List<object>();

            var properties = instance.GetType().GetProperties();

            foreach (var property in properties)
            {
                var value = property.GetGetMethod().Invoke(instance, null);
                result.Add(value);
            }

            return result;
        }

        public string createLine(object instance)
        {
            var values = getValues(instance);
            var sb = new StringBuilder();
            foreach (var value in values)
            {
                sb.Append(value);
                sb.Append(',');
            }
            sb.Remove(sb.Length - 1, 1);

            return sb.ToString();
        }

        public string AddQuotes(string value)
        {
            var sb = new StringBuilder();
            sb.Append('"');
            sb.Append(value);
            sb.Append('"');
            return sb.ToString();
        }

        public void AddQuotesToDataTransferObject(object dataTransferObject)
        {
            var properties = dataTransferObject.GetType().GetProperties();

            foreach (var property in properties)
            {
                var stringValue = property.GetGetMethod().Invoke(dataTransferObject, null).ToString();
                property.GetSetMethod().Invoke(dataTransferObject, new object[] {AddQuotes(stringValue)});
            }
        }

        public void SetValues(object instance, string[] fields)
        {
            var properties = instance.GetType().GetProperties();

            int i = 0;
            foreach (var property in properties)
            {
                var stringValue = fields[i];
                object value = stringValue;

                var propertyType = property.PropertyType;
                if (propertyType == typeof (int))
                {
                    if (stringValue == "")
                    {
                        value = 0;
                    }
                    else
                    {
                        value = int.Parse(stringValue);
                    }
                   
                }

                property.GetSetMethod().Invoke(instance, new[]{value});
                i++;

            }
        }

    }
}
