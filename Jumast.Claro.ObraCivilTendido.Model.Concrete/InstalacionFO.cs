﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumas.Claro.ObraCivilTendido.Model;
using Jumast.Claro.BusinessObject;
using Jumast.Claro.ObraCivilTendido.Model.Abstract;

namespace Jumast.Claro.ObraCivilTendido.Model.Concrete
{
    public class InstalacionFO : IInstalacionFO
    {
        private  const  int DIAS_EN_OTRAS_BANDEJAS = 14;

        public string Cliente { get; set; }
        public string Link { get; set; }
        public string Direccion { get; set; }
        public string Partido { get; set; }
        public string Enlace { get; set; }
        public string Orden { get; set; }
        public DateTime? FechaDeRecibido { get; set; }
        public DateTime? FechaDeCompromisoVantive { get; set; }
        public string TipoDeOrden { get; set; }
        public string Tecnologia { get; set; }
        public string ContratistaEnCurso { get; set; }
        public DateTime? FechaDeAsignado { get; set; }
        public DateTime? FechaDeInformeRecibido { get; set; }
        public double? MetrosDeObraCivil { get; set; }
        public double? MetrosDeTendido { get; set; }
        public double? ValorRelevado { get; set; }
      
        public string DocumentadoWebGis { get; set; }
        public DateTime? FechaDeCierre { get; set; }
        public string Hipervisor { get; set; }

        public string Observaciones { get; set; }
        public string Estado { get; set; }


        public double? DiasHastaAsignarRelevamiento
        {
            get
            {
                if (FechaDeRecibido.HasValue && FechaDeAsignado.HasValue)
                {
                    return (FechaDeAsignado.Value - FechaDeRecibido.Value).TotalDays;
                }
                else if (FechaDeRecibido.HasValue && !FechaDeAsignado.HasValue)
                {
                    return (DateTime.Now - FechaDeRecibido.Value).TotalDays;
                }
                return null;
            }
        }

        public Zona Zona
        {
            get
            {
                if (string.IsNullOrEmpty(Partido))
                {
                    return Zona.Indefinido;
                }
                else if (Partido == "Cap. Fed.")
                {
                    return Zona.CABA;
                }
                else
                {
                    return Zona.AMBA;
                }
            }
        }

        public double? DiasDeRelevamiento
        {
            get
            {
                if (FechaDeAsignado.HasValue && FechaDeInformeRecibido.HasValue)
                {
                    return (FechaDeInformeRecibido.Value - FechaDeAsignado.Value).TotalDays;
                }
                if (FechaDeAsignado.HasValue && !FechaDeInformeRecibido.HasValue)
                {
                    return (DateTime.Now - FechaDeAsignado.Value).TotalDays;
                }
                return null;
            }
        }

        public double? DiasDeInstalacion
        {
            get
            {
                if (FechaDeRecibido.HasValue && FechaDeCompromisoVantive.HasValue)
                {
                    return (FechaDeCompromisoVantive.Value - FechaDeRecibido.Value).TotalDays;
                }
                return null;
                
            }
        }

        public Vencida VencidaInstalacionesFO
        {
            get
            {
                if (FechaDeCompromisoVantive.HasValue)
                {
                    return FechaDeCompromisoVantive.Value.AddDays(-1*DIAS_EN_OTRAS_BANDEJAS)  > DateTime.Now ? Vencida.No : Vencida.Si;
                }
                return Vencida.NoAplica;
            }
        }

        public double? DiasParaVencimientoInstalacionesFO
        {
            get
            {
                if (FechaDeCompromisoVantive.HasValue)
                {
                    return (FechaDeCompromisoVantive.Value - DateTime.Now).TotalDays - DIAS_EN_OTRAS_BANDEJAS;
                }
                return null;
            }
        }

        public double? DiasDeRecibido
        {
            get
            {
                if (FechaDeRecibido.HasValue)
                {
                    return (DateTime.Now - FechaDeRecibido.Value).TotalDays;
                }
                return null;
            }
        }

        public Vencida Vencida
        {
            get
            {
                if (FechaDeCompromisoVantive.HasValue)
                {
                    return FechaDeCompromisoVantive.Value > DateTime.Now ? Vencida.No : Vencida.Si;
                }
                return Vencida.NoAplica;
            }
        }

        public double? DiasParaVencimiento
        {
            get
            {
                if (FechaDeCompromisoVantive.HasValue)
                {
                    return (FechaDeCompromisoVantive.Value - DateTime.Now).TotalDays;
                }
                return null;
            }
            
        }

        public TipoDeTarea TipoDeTarea
        {
            get
            {
                if (!MetrosDeObraCivil.HasValue)
                {
                    return TipoDeTarea.Indefinido;
                }
                else
                {
                    if (MetrosDeObraCivil.Value > 0)
                    {
                        return TipoDeTarea.ObraCivil;
                    }
                    else if (MetrosDeObraCivil.Value < 0)
                    {
                        return TipoDeTarea.Indefinido;
                    }
                    return TipoDeTarea.Tendido;
                }
            }
        }

    }
}
