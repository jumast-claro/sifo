﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Jumast.Wpf.Mvvm.BindableProperties
{

    public class ReadOnlyProperty<T> : IBindableProperty<T>
    {
        public ReadOnlyProperty(T value)
        {
            _value = value;
        }

        private T _value;
        public T Value
        {
            get { return _value; }
            set
            {
                throw new InvalidOperationException();
            }
        }

        public string DisplayString
        {
            get { return _value.ToString(); }
           
        }

        //------------------------------------------------------ 
        //  INotifyPropertyChanged
        //------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ReadWriteProperty<T> : IBindableProperty<T>
    {
        public ReadWriteProperty(T initialVvalue)
        {
            _value = initialVvalue;
        }

        private T _value;
        public T Value
        {
            get { return _value; }
            set
            {
                if (value != null && value.Equals(_value))
                {
                    return;
                }
                _value = value;
                OnPropertyChanged();
            }
        }

        public string DisplayString
        {
            get { return _value.ToString(); }
            //set
            //{
            //    throw new InvalidOperationException();
            //}
        }

        //------------------------------------------------------ 
        //  INotifyPropertyChanged
        //------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
