using System.ComponentModel;

namespace Jumast.Wpf.Mvvm.BindableProperties
{
    public interface IBindableProperty<TValue> : INotifyPropertyChanged
    {
        TValue Value { get; set; }
        string DisplayString { get; }
    }
}