﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumast.Claro.BusinessObject.Abstract;

namespace Jumast.Claro.BusinessObject.Concrete
{
    public class FechaDeCompromiso : IFechaDeCompromiso
    {
        private readonly DateTime? _fechaDeCompromiso;

        public FechaDeCompromiso(DateTime? fechaDeCompromiso)
        {
            _fechaDeCompromiso = fechaDeCompromiso;
        }

        public bool TieneFecha => _fechaDeCompromiso.HasValue;

        public DateTime? Fecha => _fechaDeCompromiso;

        public int? TiempoRestanteEnDias
        {
            get
            {
                if (!_fechaDeCompromiso.HasValue)
                {
                    return null;
                }
                else
                {
                    return (_fechaDeCompromiso.Value - DateTime.Today).Days;
                }
            }
        }

        public Vencida EstaVencida
        {
            get
            {
                if (!_fechaDeCompromiso.HasValue)
                {
                    return Vencida.NoAplica;
                }
                else
                {
                    return _fechaDeCompromiso.Value <= DateTime.Today ? Vencida.Si : Vencida.No;
                }
            }
            
        }
    }
}
