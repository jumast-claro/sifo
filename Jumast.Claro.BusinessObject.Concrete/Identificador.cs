﻿using System;
using Jumast.Claro.BusinessObject.Abstract;

namespace Jumast.Claro.BusinessObject.Concrete
{
    public class Identificador : IIdentificador
    {
        private readonly string _identificador;
        private const char WHITE_SPACE = ' ';

        private string _producto;
        private string _enlace;

        public Identificador(string identificador)
        {
            _identificador = identificador;
            parse(identificador);
        }

        public string TextoCompleto => _identificador;
        public string Producto => _producto;
        public string Enlace => _enlace;

        private void parse(string identificador)
        {
            try
            {
                var split = identificador.Split('-');
                var producto = split[0].Trim(WHITE_SPACE);


                var enlace = identificador.Substring(identificador.IndexOf(':')).Split(WHITE_SPACE)[1].Trim();

                _producto = producto;
                _enlace = enlace;
            }
            catch (Exception)
            {

                throw;
            }

        }


    }
}
