﻿using System;
using Jumast.Claro.BusinessObject.Abstract;

namespace Jumast.Claro.BusinessObject.Concrete
{
    public class FechaDeInicio : IFechaDeInicio
    {
        private readonly DateTime _fechaDeRecibido;

        public FechaDeInicio(DateTime fechaDeRecibido)
        {
            _fechaDeRecibido = fechaDeRecibido;
        }

        public DateTime Fecha => _fechaDeRecibido;
        public int TiempoTranscurridoEnDias => (DateTime.Today - _fechaDeRecibido).Days;
    }
}
