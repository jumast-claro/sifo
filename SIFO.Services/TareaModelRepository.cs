﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SIFO.DataAcces.TareasCerradas;
using SIFO.DesktopUI.TareasIngresadas;
using SIFO.Model;
using SIFO.Services.DataService;

namespace SIFO.Services
{
    public abstract class TareaModelRepository<T> : ITareaModelRepository<T> where T : ITarea
    {

        private readonly IBusinessObjectDataRepository _repo;
        private readonly List<T> _modelObjects = new List<T>();
        private readonly IDataMapper<T, TareaDataTransferObject> _mapper;

        protected TareaModelRepository(IBusinessObjectDataRepository repo, IDataMapper<T, TareaDataTransferObject> mapper)
        {
            _repo = repo;
            _mapper = mapper;

            var dataTransferObjects = _repo.SelectAll();
            foreach (var dataTransferObject in dataTransferObjects)
            {
                var model = _mapper.MapDataTransferObjectToModel(dataTransferObject);
                _modelObjects.Add(model);
            }
        }

        public T SelectByTareaId(string tareaId)
        {
            return _modelObjects.FirstOrDefault(orden => orden.TareaId == tareaId);
        }

        public IEnumerable<T> SelectAll()
        {
            return _modelObjects;
        }

        public void SaveAll()
        {
            var dataTransferObjects = _modelObjects.Select(model => _mapper.MapModelToDataTransferObject(model)).ToList();
            _repo.SaveAll(dataTransferObjects);
        }
    }
}
