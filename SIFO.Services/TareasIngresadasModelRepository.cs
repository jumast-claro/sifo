﻿using SIFO.DataAcces.TareasCerradas;
using SIFO.DesktopUI.TareasIngresadas;
using SIFO.Model;
using SIFO.Services.DataService;

namespace SIFO.Services
{
    public class TareasIngresadasModelRepository : TareaModelRepository<ITarea>, ITareasIngresadasModelRepository
    {
        public TareasIngresadasModelRepository(IBusinessObjectDataRepository repo, IDataMapper<ITarea, TareaDataTransferObject> mapper) : base(repo, mapper)
        {
        }
    }
}