﻿using System.Collections.Generic;
using SIFO.DataAcces.TareasCerradas;
using SIFO.DesktopUI.TareasIngresadas;
using SIFO.Model;
using SIFO.Services.DataService;

namespace SIFO.Services
{
    public class TareasCerradasModelRepository : TareaModelRepository<ITarea>, ITareasCerradasModelRepository
    {
        public TareasCerradasModelRepository(IBusinessObjectDataRepository repo, IDataMapper<ITarea, TareaDataTransferObject> mapper) : base(repo, mapper)
        {
        }
    }

    public class TempCerradasRepository : TareaModelRepository<ITarea>, ITareasCerradasModelRepository
    {
        public TempCerradasRepository(IBusinessObjectDataRepository repo, IDataMapper<ITarea, TareaDataTransferObject> mapper) : base(repo, mapper)
        {
        }
    }
}