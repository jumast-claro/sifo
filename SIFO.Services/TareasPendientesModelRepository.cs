﻿using SIFO.DataAcces.TareasCerradas;
using SIFO.DesktopUI.TareasIngresadas;
using SIFO.Model;
using SIFO.Services.DataService;

namespace SIFO.Services
{
    public class TareasPendientesModelRepository : TareaModelRepository<ITarea>, ITareasPendientesModelRepository
    {
        public TareasPendientesModelRepository(IBusinessObjectDataRepository repo, IDataMapper<ITarea, TareaDataTransferObject> mapper) : base(repo, mapper)
        {
        }
    }


    public class TareasPendientesInicio2017ModelRepository : TareaModelRepository<ITarea>, ITareasPendientesInicio2017ModelRepository
    {
        public TareasPendientesInicio2017ModelRepository(IBusinessObjectDataRepository repo, IDataMapper<ITarea, TareaDataTransferObject> mapper) : base(repo, mapper)
        {
        }
    }
}