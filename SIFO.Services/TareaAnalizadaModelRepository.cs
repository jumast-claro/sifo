﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SIFO.DataAcces.TareasCerradas;
using SIFO.Model;
using SIFO.Services.DataService;

namespace SIFO.Services
{
    public class TareaAnalizadaModelRepository
    {
        private readonly ITareasAnalizadasRepository _repo;
        private readonly List<OrdenAnalizada> _modelObjects = new List<OrdenAnalizada>();
        private IDataMapper<OrdenAnalizada, TareaAnalizadaDataTransferObject> _mapper = new AnalizadaDataMapper();

        public TareaAnalizadaModelRepository(ITareasAnalizadasRepository repo)
        {
            _repo = repo;


            var dtos = repo.SelectAll();
            foreach (var dto in dtos)
            {
                var model = _mapper.MapDataTransferObjectToModel(dto);
                _modelObjects.Add(model);
            }

        }

        public OrdenAnalizada SelectByTareaId(string tareaId)
        {
            var model = _modelObjects.FirstOrDefault(o => o.TareaId == tareaId);
            return model;
        }

        public bool Contains(string tareaId)
        {
            return _modelObjects.Any(o => o.TareaId == tareaId);
        }

        public IEnumerable<OrdenAnalizada> SelectAll()
        {
            return _modelObjects;
        }

        public void SaveAll()
        {
            var dataTransferObjects = _modelObjects.Select(model => _mapper.MapModelToDataTransferObject(model)).ToList();
            _repo.SaveAll(dataTransferObjects);
        }

        public void Add(OrdenAnalizada ordenAnalizada)
        {
            _modelObjects.Add(ordenAnalizada);
        }

        //public void Save(OrdenAnalizada ordenAnalizada)
        //{
        //    var item = _modelObjects.Find(o => o.TareaId == ordenAnalizada.TareaId);
        //    if (item == null)
        //    {
        //        _modelObjects.Add(ordenAnalizada);
        //    }
        //    SaveAll();
        //}


    }
}
