﻿using System.Globalization;
using SIFO.DataAcces;
using SIFO.DataAcces.TareasCerradas;
using SIFO.Model;

namespace SIFO.Services.DataService
{
    public class TareaCerradaDataMapper : TareaDataMapper<ITarea>
    {

        public TareaCerradaDataMapper(IEnumMapper<Estado> resultadoEnumMapper, IEnumMapper<Zona> zonaEnumMapper, TareaFactory tareaCerradaFactory) : base(resultadoEnumMapper, zonaEnumMapper, tareaCerradaFactory)
        {
        }

        public override TareaDataTransferObject MapModelToDataTransferObject(ITarea model)
        {
            var dataTransferObject = base.MapModelToDataTransferObject(model);
            dataTransferObject.FechaFinTarea = model.FechaCierreTarea.Fecha.Value.ToString("dd/MM/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
            return dataTransferObject;
        }

    }
}
