﻿using System;
using SIFO.Model;

namespace SIFO.Services.DataService
{
    public class TareaIngresadaDataMapper : TareaDataMapper<ITarea>
    {

        public TareaIngresadaDataMapper(IEnumMapper<Estado> resultadoEnumMapper, IEnumMapper<Zona> zonaEnumMapper, TareaFactory tareaCerradaFactory) : base(resultadoEnumMapper, zonaEnumMapper, tareaCerradaFactory)
        {
        }

    }
}