using System;
using System.Globalization;
using System.Net.Http.Headers;
using SIFO.DataAcces;
using SIFO.DataAcces.TareasCerradas;
using SIFO.Model;

namespace SIFO.Services.DataService
{
    public abstract class TareaDataMapper<T> : IDataMapper<T, TareaDataTransferObject> where T: ITarea
    {
        private readonly IEnumMapper<Estado> _resultadoEnumMapper;
        private readonly IEnumMapper<Zona> _zonaEnumMapper;
        private readonly TareaFactory _tareaCerradaFactory;

        private long tryConvertToLong(string value)
        {
            long ans = -1;
            var conversionOk = long.TryParse(value, out ans);

            if (conversionOk)
            {
                return ans;
            }
            return 0;


            //if (value.Contains("-") || value.Contains("L"))
            //{
            //    return 0;
            //}

            //return value == string.Empty ? 0 : long.Parse(value);
        }

        private string format(string fieldValue)
        {
            return fieldValue.Replace("\"", "\'\'");
        }

        protected TareaDataMapper(IEnumMapper<Estado> resultadoEnumMapper, IEnumMapper<Zona> zonaEnumMapper, TareaFactory tareaCerradaFactory)
        {
            _resultadoEnumMapper = resultadoEnumMapper;
            _zonaEnumMapper = zonaEnumMapper;
            _tareaCerradaFactory = tareaCerradaFactory;
        }

        public T MapDataTransferObjectToModel(TareaDataTransferObject dataTRansferObject)
        {
            var parser = new CustomDateTimeParser();
            var inbox = dataTRansferObject.Inbox;
            var fechaInicioTarea = parser.ParseDateTime(dataTRansferObject.FechaInicioTarea);

            var str = dataTRansferObject.FechaFinTarea;

            IFecha v;
            if (str.Equals(string.Empty))
            {
                v = new NullFechaDeCierre();
            }
            else
            {
                var date = parser.ParseDateTime(str);
                v = new FechaDeCierre(date);
            }
            //var fechaCierreTarea = parser.ParseDateTime(dataTRansferObject.FechaFinTarea);


            //var fechaInicioOrden = parser.TryParseDate(dataTRansferObject.FechaInicioOrden);
            DateTime? fechaInicioOrden = null; 
            var fechaCompromisoOrden = parser.TryParseDate(dataTRansferObject.FechaCompromisoOrden);
            var tipoDeSolucion = dataTRansferObject.TipoDeSolucion;
            var enlace = tryConvertToLong(dataTRansferObject.PseudoEnlace);

            var pseudoEnlace = dataTRansferObject.PseudoEnlace + "-" + dataTRansferObject.Orden;

            var index = dataTRansferObject.Identificador.IndexOf('@');
            string identificador;
            if (index > 0)
            {
                identificador = dataTRansferObject.Identificador.Remove(index);
            }
            else
            {
                identificador = dataTRansferObject.Identificador;
            }



            var tipoDeOrden = dataTRansferObject.TipoDeOrden;
            var factory = _tareaCerradaFactory;
            var model = factory.Create(inbox, fechaInicioTarea, v, fechaInicioOrden, fechaCompromisoOrden, tipoDeSolucion, enlace, identificador, tipoDeOrden, dataTRansferObject.Partido);
            model.RegionFinal = dataTRansferObject.RegionFinal;
            model.Zona = _zonaEnumMapper.StringToEnum(dataTRansferObject.Zona);
            model.InboxFinal = dataTRansferObject.InboxFinal;
            model.Producto = dataTRansferObject.Producto;
            model.FechaCompromisoOrden = parser.TryParseDate(dataTRansferObject.FechaCompromisoOrden);
            model.FechaRenegociada = parser.TryParseDate(dataTRansferObject.FechaRenegociada);
            //model.FechaCierreTaraeAnterior = parser.TryParseDate(dataTRansferObject.FechaCierreTareaAnterior);
            model.FechaCierreTaraeAnterior = null;
            model.Estado = _resultadoEnumMapper.StringToEnum(dataTRansferObject.Estado);
            model.TomadoPor = dataTRansferObject.TomadoPor;
            model.Cliente = dataTRansferObject.Cliente;
            model.Orden = tryConvertToLong(dataTRansferObject.Orden);
            model.TareaId = dataTRansferObject.TareaId;
            model.UltimaNota = dataTRansferObject.UltimaNota;
            model.UltimaNotaAutor = dataTRansferObject.UltimaNotaAutor;
            model.UltimaNotaFecha = parser.ParseDateTime(dataTRansferObject.UltimaNotaFecha);

            model.TipoDeMovimiento = dataTRansferObject.TipoMovimiento;
            model.TipoDeFibraOptica = dataTRansferObject.TipoFibraOptica;
            model.Tecnologia = dataTRansferObject.Tecnologia;

            model.PseudoEnlace = pseudoEnlace;

            model.FechaInicioWorkflow = parser.TryParseDate(dataTRansferObject.WorkflowFechaInicio);
            model.FechaFinWorkflow = parser.TryParseDate(dataTRansferObject.WorkflowFechaFin);

            model.Calle = dataTRansferObject.Calle;
            model.Numero = dataTRansferObject.Numero;

            model.Partido = dataTRansferObject.Partido;



            return (T) model;
        }

        public virtual TareaDataTransferObject MapModelToDataTransferObject(T model)
        {
            var dataTransferObject = new TareaDataTransferObject()
            {
                RegionFinal = model.RegionFinal,
                Zona = _zonaEnumMapper.EnumToString(model.Zona),
                Inbox = format(model.Inbox),
                InboxFinal = format(model.InboxFinal),
                Producto = format(model.Producto),
                TipoDeOrden = format(model.TipoDeOrden),
                TipoDeSolucion = format(model.TipoSolucion),
                //FechaInicioOrden = model.FechaInicioOrden.HasValue ? model.FechaInicioOrden.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : "",
                FechaCompromisoOrden = model.FechaCompromisoOrden.HasValue ? model.FechaCompromisoOrden.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : "",
                FechaRenegociada = model.FechaRenegociada.HasValue ? model.FechaRenegociada.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : "",
                FechaInicioTarea = model.FechaInicioTarea.ToString("dd/MM/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture),
                //FechaCierreTareaAnterior = model.FechaCierreTaraeAnterior.HasValue ? model.FechaCierreTaraeAnterior.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : "",
                Estado = _resultadoEnumMapper.EnumToString(model.Estado),
                TomadoPor = model.TomadoPor,
                Cliente = format(model.Cliente),
                Orden = model.Orden.ToString(),
                PseudoEnlace = model.Enlace.ToString(),
                TareaId = format(model.TareaId),
                Identificador = format(model.Identificador)
            };

            return dataTransferObject;
        }
    }
}