﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SIFO.DataAcces.TareasCerradas;
using SIFO.Model;

namespace SIFO.Services.DataService
{
    public class InterrupcionDataMapper : IDataMapper<Interrupcion, InterrupcionDataTransferObject>
    {
        public Interrupcion MapDataTransferObjectToModel(InterrupcionDataTransferObject dataTRansferObject)
        {
            var motivo = dataTRansferObject.Motivo;
            var tiempoEnDias = double.Parse(dataTRansferObject.TiempoEnDias.Replace(",", "."), CultureInfo.InvariantCulture);
            var interrupcion = new Interrupcion(motivo, tiempoEnDias);
            return interrupcion;
        }

        public InterrupcionDataTransferObject MapModelToDataTransferObject(Interrupcion model)
        {
            throw new NotImplementedException();
        }
    }
}
