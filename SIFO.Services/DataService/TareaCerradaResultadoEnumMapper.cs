using System;
using SIFO.Model;

namespace SIFO.Services.DataService
{
    public class TareaCerradaResultadoEnumMapper : IEnumMapper<Estado>
    {
        public string EnumToString(Estado enumValue)
        {
            return enumValue.ToString();
        }

        public Estado StringToEnum(string stringValue)
        {
            switch (stringValue)
            {
                case "Exito":
                    return Estado.Exito;
                case "Fracaso":
                    return Estado.Fracaso;
                case "":
                    return Estado.Exito;
                default:
                    throw new ArgumentOutOfRangeException(nameof(stringValue), stringValue, null);
            }
        }
    }
}