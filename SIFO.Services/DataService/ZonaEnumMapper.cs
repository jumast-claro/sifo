using System;
using SIFO.Model;

namespace SIFO.Services.DataService
{
    public class ZonaEnumMapper : IEnumMapper<Zona>
    {
        public string EnumToString(Zona enumValue)
        {
            return enumValue.ToString();
        }

        public Zona StringToEnum(string stringValue)
        {
            switch (stringValue)
            {
                case "Amba":
                    return Zona.Amba;
                case "Interior":
                    return Zona.Interior;
                case "Sin Info":
                    return Zona.SinInfo;
                case "SinInfo":
                    return Zona.SinInfo;
                default:
                    throw new ArgumentOutOfRangeException(nameof(stringValue), stringValue, null);
            }
        }
    }
}