﻿using SIFO.Model;

namespace SIFO.Services.DataService
{
    public class TareaPendienteDataMapper : TareaDataMapper<ITarea>
    {
        public TareaPendienteDataMapper(IEnumMapper<Estado> resultadoEnumMapper, IEnumMapper<Zona> zonaEnumMapper, TareaFactory tareaCerradaFactory) : base(resultadoEnumMapper, zonaEnumMapper, tareaCerradaFactory)
        {
        }
    }
}