using System;
using SIFO.Model;

namespace SIFO.Services.DataService
{
    public class TareaIngresadaResultadoMapper : IEnumMapper<Estado>
    {
        public string EnumToString(Estado enumValue)
        {
            switch (enumValue)
            {
                case Estado.Exito:
                    return enumValue.ToString();
                case Estado.Fracaso:
                    return enumValue.ToString();
                case Estado.Pendiente:
                    return "";
                default:
                    throw new ArgumentOutOfRangeException(nameof(enumValue), enumValue, null);
            }
        }

        public Estado StringToEnum(string stringValue)
        {
            switch (stringValue)
            {
                case "Exito":
                    return Estado.Exito;
                case "Fracaso":
                    return Estado.Fracaso;
                case "":
                    return Estado.Pendiente;

                default:
                    throw new ArgumentOutOfRangeException(nameof(stringValue), stringValue, null);
            }
        }
    }
}