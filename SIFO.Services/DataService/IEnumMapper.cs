﻿namespace SIFO.Services.DataService
{
    public interface IEnumMapper<TEnum>
    {
        string EnumToString(TEnum enumValue);
        TEnum StringToEnum(string stringValue);
    }
}