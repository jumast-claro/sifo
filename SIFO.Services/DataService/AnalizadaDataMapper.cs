﻿using AutoMapper;
using SIFO.DataAcces.TareasCerradas;
using SIFO.Model;

namespace SIFO.Services.DataService
{
    public class AnalizadaDataMapper : IDataMapper<OrdenAnalizada, TareaAnalizadaDataTransferObject>
    {

        private int toInt(string value)
        {
            if (value == "")
            {
                return 0;
            }
            return int.Parse(value);
        }

        public OrdenAnalizada MapDataTransferObjectToModel(TareaAnalizadaDataTransferObject dataTRansferObject)
        {
            return new OrdenAnalizada(dataTRansferObject.TareaId)
            {

                Vantive_MalCargadaLaDireccion = toInt( dataTRansferObject.Vantive_MalCargadaLaDireccion),
                Vantive_FaltaDeContacto = toInt( dataTRansferObject.Vantive_FaltaDeContacto),
                Vantive_MalTipificada = toInt( dataTRansferObject.Vantive_MalTipificada),
                Factibilidad_MalFactibilizado = toInt( dataTRansferObject.Factibilidad_MalFactibilizado),
                Factibilidad_CambioEnLasCondiciones = toInt( dataTRansferObject.Factibilidad_CambioEnLasCondiciones),
                Relevamiento_ProblemaDeContacto = toInt( dataTRansferObject.Relevamiento_ProblemaDeContacto),
                Relevamiento_ProblemaDeIngreso = toInt( dataTRansferObject.Relevamiento_ProblemaDeIngreso),
                Relevamiento_SitioEnConstruccion = toInt( dataTRansferObject.Relevamiento_SitioEnConstruccion),
                InicioDePermisos_FaltaDeAutorizacionDelCliente = toInt( dataTRansferObject.InicioDePermisos_FaltaDeAutorizacionDelCliente),
                InicioDePermisos_FaltaDeAutorizacionPorMotivosInternos = toInt( dataTRansferObject.InicioDePermisos_FaltaDeAutorizacionPorMotivosInternos),
                ObraCivil_SitioEnConstruccion = toInt( dataTRansferObject.ObraCivil_SitioEnConstruccion),
                ObraCivil_ProblemaDeIngreso = toInt( dataTRansferObject.ObraCivil_ProblemaDeIngreso),
                FaltaIngenieria = toInt( dataTRansferObject.FaltaIngenieria),
                FaltaAsignacion = toInt( dataTRansferObject.FaltaAsignacion),
                Tendido_SitioEnConstruccion = toInt( dataTRansferObject.Tendido_SitioEnConstruccion),
                Tendido_ProblemaDeIngreso = toInt( dataTRansferObject.Tendido_ProblemaDeIngreso),
                Tendido_FaltaDeMateriales = toInt( dataTRansferObject.Tendido_FaltaDeMateriales),
                Odf_SitioEnConstruccion = toInt( dataTRansferObject.Odf_SitioEnConstruccion),
                Odf_ProblemaDeIngreso = toInt( dataTRansferObject.Odf_ProblemaDeIngreso),
                Odf_FaltaDeMateriales = toInt( dataTRansferObject.Odf_FaltaDeMateriales),
                SuspensionDeTareasAPedidoDelPM = toInt( dataTRansferObject.SuspensionDeTareasAPedidoDelPM),
                Fusiones_SoloInterior = toInt( dataTRansferObject.Fusiones_SoloInterior),
                FreezingDeRed = toInt( dataTRansferObject.FreezingDeRed),
                AdecuacionDeRed = toInt( dataTRansferObject.AdecuacionDeRed),
                RadioBase_ProblemaDeIngreso = toInt( dataTRansferObject.RadioBase_ProblemaDeIngreso),
                Permiso_DemoraExtraordinaria = toInt(dataTRansferObject.Permiso_DemoraExtraordinaria)
            };

        }

        public TareaAnalizadaDataTransferObject MapModelToDataTransferObject(OrdenAnalizada model)
        {
            return new TareaAnalizadaDataTransferObject()
            {
                TareaId = model.TareaId.ToString(),
                Vantive_MalCargadaLaDireccion = model.Vantive_MalCargadaLaDireccion.ToString(),
                Vantive_FaltaDeContacto = model.Vantive_FaltaDeContacto.ToString(),
                Vantive_MalTipificada = model.Vantive_MalTipificada.ToString(),
                Factibilidad_MalFactibilizado = model.Factibilidad_MalFactibilizado.ToString(),
                Factibilidad_CambioEnLasCondiciones = model.Factibilidad_CambioEnLasCondiciones.ToString(),
                Relevamiento_ProblemaDeContacto = model.Relevamiento_ProblemaDeContacto.ToString(),
                Relevamiento_ProblemaDeIngreso = model.Relevamiento_ProblemaDeIngreso.ToString(),
                Relevamiento_SitioEnConstruccion = model.Relevamiento_SitioEnConstruccion.ToString(),
                InicioDePermisos_FaltaDeAutorizacionDelCliente = model.InicioDePermisos_FaltaDeAutorizacionDelCliente.ToString(),
                InicioDePermisos_FaltaDeAutorizacionPorMotivosInternos = model.InicioDePermisos_FaltaDeAutorizacionPorMotivosInternos.ToString(),
                ObraCivil_SitioEnConstruccion = model.ObraCivil_SitioEnConstruccion.ToString(),
                ObraCivil_ProblemaDeIngreso = model.ObraCivil_ProblemaDeIngreso.ToString(),
                FaltaIngenieria = model.FaltaIngenieria.ToString(),
                FaltaAsignacion = model.FaltaAsignacion.ToString(),
                Tendido_SitioEnConstruccion = model.Tendido_SitioEnConstruccion.ToString(),
                Tendido_ProblemaDeIngreso = model.Tendido_ProblemaDeIngreso.ToString(),
                Tendido_FaltaDeMateriales = model.Tendido_FaltaDeMateriales.ToString(),
                Odf_SitioEnConstruccion = model.Odf_SitioEnConstruccion.ToString(),
                Odf_ProblemaDeIngreso = model.Odf_ProblemaDeIngreso.ToString(),
                Odf_FaltaDeMateriales = model.Odf_FaltaDeMateriales.ToString(),
                SuspensionDeTareasAPedidoDelPM = model.SuspensionDeTareasAPedidoDelPM.ToString(),
                Fusiones_SoloInterior = model.Fusiones_SoloInterior.ToString(),
                FreezingDeRed = model.FreezingDeRed.ToString(),
                AdecuacionDeRed = model.AdecuacionDeRed.ToString(),
                RadioBase_ProblemaDeIngreso = model.RadioBase_ProblemaDeIngreso.ToString(),
                Permiso_DemoraExtraordinaria = model.Permiso_DemoraExtraordinaria.ToString()
            };

        }
    }
}
