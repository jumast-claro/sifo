namespace SIFO.Services.DataService
{
    public interface IDataMapper<TModel, TDataTRansferObject>
    {
        TModel MapDataTransferObjectToModel(TDataTRansferObject dataTRansferObject);
        TDataTRansferObject MapModelToDataTransferObject(TModel model);
    }
}