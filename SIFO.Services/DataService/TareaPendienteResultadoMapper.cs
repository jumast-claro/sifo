﻿using System;
using SIFO.Model;

namespace SIFO.Services.DataService
{
    public class TareaPendienteResultadoMapper : IEnumMapper<Estado>
    {
        public string EnumToString(Estado enumValue)
        {
            if (enumValue == Estado.Pendiente)
            {
                return string.Empty;
            }
            throw new ArgumentOutOfRangeException(nameof(enumValue), enumValue, null);
        }

        public Estado StringToEnum(string stringValue)
        {
            if (stringValue.Equals(string.Empty))
            {
                return Estado.Pendiente;
            }
            throw new ArgumentOutOfRangeException(nameof(stringValue), stringValue, null);
        }
    }
}