using System.Collections.Generic;
using Vantive.DataAcces;
using Vantive.DataAcces.Abstractions;
using Vantive.DataService.Abstractions;
using Vantive.Model.Abstractions;

namespace Vantive.DataService.Implementations
{
    public class TareasVantiveModelRepository : ITareasVantiveModelRepository
    {
        private readonly List<ITareaVantive> _modelObjects = new List<ITareaVantive>();

        public TareasVantiveModelRepository(IVantiveDataRepository repo, IVantiveDataMapper dataMapper)
        {
            var dataTransferObjects = repo.SelectAll();
            foreach (var dataTrasferObject in dataTransferObjects)
            {
                var model = dataMapper.MapDataTransferObjectToModel(dataTrasferObject);
                _modelObjects.Add(model);
            }

        }

        public IEnumerable<ITareaVantive> SelectAll()
        {
            return _modelObjects;
        }
    }
}