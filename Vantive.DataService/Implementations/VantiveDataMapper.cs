﻿using System;
using System.Globalization;
using Vantive.DataAcces.Abstractions;
using Vantive.DataService.Abstractions;
using Vantive.Model.Abstractions;
using Vantive.Model.Implementations;

namespace Vantive.DataService.Implementations
{
    public class VantiveDataMapper : IVantiveDataMapper
    {
        private const string DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";

        public ITareaVantive MapDataTransferObjectToModel(IVantiveDataTransferObject dataTransferObject)
        {
            var tareaVantive = new TareaVantive()
            {
                FechaDeInstalacion = mapFechaDeInstalacion(dataTransferObject.FechaDeInstalacion),
                Identificador = dataTransferObject.Identificador,
                NumeroDeOrden = dataTransferObject.ID,
                Cliente = dataTransferObject.Cliente,
                Localidad = dataTransferObject.Localidad,
                Provincia = dataTransferObject.Provincia,
                TomadoPor = dataTransferObject.TomadoPor,
                Estado = dataTransferObject.Estado,
                MotivoDeInterrupcion = dataTransferObject.MotivoInterrupcion,
                MotivoDeOrden = dataTransferObject.MotivoOrden,
                FechaDeRecibido = DateTime.ParseExact(dataTransferObject.FechaDeRecibido, DATE_FORMAT,CultureInfo.InvariantCulture),
                Ejecutivo = dataTransferObject.Ejecutivo,
                UnidadDeNegocio = dataTransferObject.UnidadDeNegocio,
                IdTarea = dataTransferObject.IdObjecto,
                Clase = dataTransferObject.Clase
            };



            return tareaVantive;


        }


        private DateTime? mapFechaDeInstalacion(string fechaDeInstalacion)
        {
            if (string.IsNullOrEmpty(fechaDeInstalacion))
            {
                return null;
            }
            else
            {
                return DateTime.ParseExact(fechaDeInstalacion, DATE_FORMAT, CultureInfo.InvariantCulture);
            }
        }

    }
}
