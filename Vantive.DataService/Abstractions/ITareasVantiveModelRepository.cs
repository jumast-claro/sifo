﻿using System.Collections.Generic;
using Vantive.Model.Abstractions;

namespace Vantive.DataService.Abstractions
{
    public interface ITareasVantiveModelRepository
    {
        IEnumerable<ITareaVantive> SelectAll();
    }
}