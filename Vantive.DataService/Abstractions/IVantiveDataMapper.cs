﻿using Vantive.DataAcces.Abstractions;
using Vantive.Model.Abstractions;

namespace Vantive.DataService.Abstractions
{
    public interface IVantiveDataMapper
    {
        ITareaVantive MapDataTransferObjectToModel(IVantiveDataTransferObject dataTransferObject);
    }
}