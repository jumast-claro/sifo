﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Internal;
using NUnit.Framework;
using SIFO.DataAcces.TareasCerradas;

namespace SIFO.DataAcces.UnitTests.CsvHelperTests
{
    [TestFixture]
    public class CsvHelperTests
    {
        [Test]
        public void CreateLine()
        {
            var instance = new MockDataTransferObject()
            {
                Name = "Luke",
                LastName = "Skywalker",
                Age = 30
            };

            var repo = new CsvHelper();

            var line = repo.createLine(instance);
            Assert.AreEqual(line, "Luke,Skywalker,30");
        }

        [Test]
        public void AddQuotes()
        {
            var value = "LukeSkywalker";
            var repo = new CsvHelper();
            var quoted = repo.AddQuotes(value);

            var expected = "\"LukeSkywalker\"";
            Assert.AreEqual(expected, quoted);
        }

        [Test]
        public void SetValues()
        {
            var dataTransferObject = new MockDataTransferObject();

            var csvHelper = new CsvHelper();
            var fields = new List<string>();
            fields.Add("Luke");
            fields.Add("Skywalker");
            fields.Add("30");


            //var csvLine = "\"Luke\",\"Skywalker\", \"30\"";

           
            csvHelper.SetValues(dataTransferObject, fields.ToArray());

            Assert.AreEqual(dataTransferObject.Name, "Luke");
            Assert.AreEqual(dataTransferObject.LastName, "Skywalker");
            Assert.AreEqual(dataTransferObject.Age, 30);

        }
    }



    public class MockDataTransferObject
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
}
