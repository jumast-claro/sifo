﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Internal;
using SIFO.DataAcces;

namespace SIFO.DesktopUI.DataAcces.UnitTest
{
    [TestFixture]
    public class DateTimeParserTest
    {

        [Test]
        public void ParseDateTime_AM()
        {

            var stringRepresentation = @"27/01/2016 04:21:27 AM";

            var expected = new DateTime(2016, 1, 27, 4, 21, 27);

            var actual = new CustomDateTimeParser().ParseDateTime(stringRepresentation);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ParseDateTime_PM()
        {

            var stringRepresentation = @"27/01/2016 04:21:27 PM";

            var expected = new DateTime(2016, 1, 27, 16, 21, 27);

            var actual = new CustomDateTimeParser().ParseDateTime(stringRepresentation);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ParseDateTime_PM1()
        {

            var stringRepresentation = @"07/07/2015 01:24:43 PM";

            var expected = new DateTime(2015, 7, 7, 13, 24, 43);

            var actual = new CustomDateTimeParser().ParseDateTime(stringRepresentation);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ParseDateTime_PM2()
        {

            var stringRepresentation = @"29/02/2016 01:24:43 PM";

            var expected = new DateTime(2016, 2, 29, 13, 24, 43);

            var actual = new CustomDateTimeParser().ParseDateTime(stringRepresentation);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ParseDateTime_PM3()
        {

            var stringRepresentation = @"02/25/2016 01:24:43 PM";

            var excepcion = Assert.Throws(typeof (FormatException), delegate { new CustomDateTimeParser().ParseDateTime(stringRepresentation); });

        }




        [Test]
        public void ParseDate()
        {
            //arrange
            var stringRepresentation = "18/03/2016";
            var parser = new CustomDateTimeParser();
            var expected = new DateTime(2016,3,18);

            //act
            var actual = parser.ParseDate(stringRepresentation);

            //assert
            Assert.AreEqual(actual, expected);
        }

        [Test]
        public void ParseDate_2()
        {
            //arrange
            var stringRepresentation = "03/18/2016";
            var parser = new CustomDateTimeParser();

            //act - assert
            Assert.Throws(typeof (FormatException), delegate { parser.ParseDate(stringRepresentation); });

        }

        [Test]
        public void ParseDate_EmptyString_ThrowsException()
        {
            // arrange
            const string stringRepresentation = "";
            var parser = new CustomDateTimeParser();

            // act
            var exception = Assert.Throws<ArgumentException>(delegate { parser.ParseDate(stringRepresentation); });

            // assert
            Assert.AreEqual("La cadena no puede ser vacía", exception.Message);
        }

        [Test]
        public void TryParseDate_EmptyString_ReturnsNull()
        {
            // arrange
            const string stringRepresentation = "";
            var parser = new CustomDateTimeParser();
            DateTime? expected = null;

            // act
            var actual = parser.TryParseDate(stringRepresentation);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void test()
        {
            var stringRepresentation = @"27/01/2016 04:21:27 PM";

            var expected = new DateTime(2016, 1, 27, 16, 21, 27);

            var actual = new CustomDateTimeParser().ParseDateTime(stringRepresentation);

            Assert.AreEqual(expected, actual);
        }
    }
}
