﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Jumast.Claro.ObraCivilTendido.Mvvm
{
    public static class ApplicationResources
    {

        public static Brush TipoDeTarea_ObraCivil
        {
            get
            {
                var resource = Application.Current.FindResource("TipoDeTarea_ObraCivil");
                var brush = (Brush)resource;
                return brush;
            }

        }

        public static Brush TipoDeTarea_Tendido
        {
            get
            {
                var resource = Application.Current.FindResource("TipoDeTarea_Tendido");
                var brush = (Brush)resource;
                return brush;
            }

        }

        public static Brush TipoDeTarea_Indefinido
        {
            get
            {
                var resource = Application.Current.FindResource("TipoDeTarea_Indefinido");
                var brush = (Brush)resource;
                return brush;
            }

        }

        public static Brush VencidaSegunFechaDeCompromiso_Si
        {
            get
            {
                var resource = Application.Current.FindResource("VencidaSegunFechaDeCompromiso_Si");
                var color = (Color) resource;
                return new SolidColorBrush(color);
            }
           
        }

        public static Brush VencidaSegunFechaDeCompromiso_No
        {
            get
            {
                var resource = Application.Current.FindResource("VencidaSegunFechaDeCompromiso_No");
                var color = (Color)resource;
                return new SolidColorBrush(color);
            }

        }

        public static Brush VencidaSegunFechaDeCompromiso_NoAplica
        {
            get
            {
                var resource = Application.Current.FindResource("VencidaSegunFechaDeCompromiso_NoAplica");
                var color = (Color)resource;
                return new SolidColorBrush(color);
            }

        }

        public static Brush Estado_Definicion
        {
            get
            {
                var resource = Application.Current.FindResource("Estado_Definicion");
                var brush = (Brush)resource;
                return brush;
            }

        }

        public static Brush Estado_Relevamiento
        {
            get
            {
                var resource = Application.Current.FindResource("Estado_Relevamiento");
                var brush = (Brush)resource;
                return brush;
            }

        }

        public static Brush Estado_Sondeos
        {
            get
            {
                var resource = Application.Current.FindResource("Estado_Sondeos");
                var brush = (Brush)resource;
                return brush;
            }

        }

        public static Brush Estado_Permisos
        {
            get
            {
                var resource = Application.Current.FindResource("Estado_Permisos");
                var brush = (Brush)resource;
                return brush;
            }

        }

        public static Brush Estado_ObraCivil
        {
            get
            {
                var resource = Application.Current.FindResource("Estado_ObraCivil");
                var brush = (Brush)resource;
                return brush;
            }

        }

        public static Brush Estado_Tendido
        {
            get
            {
                var resource = Application.Current.FindResource("Estado_Tendido");
                var brush = (Brush)resource;
                return brush;
            }

        }

        public static Brush Estado_ODF
        {
            get
            {
                var resource = Application.Current.FindResource("Estado_ODF");
                var brush = (Brush)resource;
                return brush;
            }

        }

        public static Brush Estado_Terminada
        {
            get
            {
                var resource = Application.Current.FindResource("Estado_Terminada");
                var brush = (Brush)resource;
                return brush;
            }

        }
    }
}
