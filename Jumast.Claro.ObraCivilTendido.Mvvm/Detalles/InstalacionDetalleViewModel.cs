﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Jumast.Claro.ObraCivilTendido.DesktopUI;
using Jumast.Claro.ObraCivilTendido.Model.Abstract;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Claro.ObraCivilTendido.Mvvm
{
    public class InstalacionDetalleViewModel : InstalacionViewModel, INotifyPropertyChanged
    {
        public InstalacionDetalleViewModel(IInstalacionFO model) : base(model)
        {
            var descripcion = $"{model.Cliente} ({model.Direccion}, {model.Partido}) OS:{model.Orden}_Enl:{model.Enlace}";
            DescripcionProperty = new ReadWriteProperty<string>(descripcion);
        }


        public IBindableProperty<string> DescripcionProperty { get; private set; } 

        private NotasViewModel _notas = new NotasViewModel();
        public NotasViewModel Notas
        {
            get { return _notas; }
            set
            {
                _notas = value;
                OnPropertyChanged();
            }
        }

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
