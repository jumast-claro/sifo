﻿using System;
using System.Globalization;
using System.Windows.Data;
using Jumast.Claro.BusinessObject;

namespace Jumast.Claro.ObraCivilTendido.Mvvm.Converters.ColorConverters
{
    public class VencidaColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var valor = (Vencida)value;
            switch (valor)
            {
                case Vencida.Si:
                    return ApplicationResources.VencidaSegunFechaDeCompromiso_Si;
                case Vencida.No:
                    return ApplicationResources.VencidaSegunFechaDeCompromiso_No;
                case Vencida.NoAplica:
                    return ApplicationResources.VencidaSegunFechaDeCompromiso_NoAplica;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
