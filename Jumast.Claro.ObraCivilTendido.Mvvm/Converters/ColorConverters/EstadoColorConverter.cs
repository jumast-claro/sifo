using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Jumast.Claro.ObraCivilTendido.Mvvm.Converters.ColorConverters
{
    public class EstadoColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var estado = value as string;

            if (string.IsNullOrEmpty(estado))
            {
                return ApplicationResources.Estado_Definicion;
            }
            if (stringsAreEqualIgnoringCase(estado, "Relevamiento"))
            {
                return ApplicationResources.Estado_Relevamiento;
            }
            if (stringsAreEqualIgnoringCase(estado, "Sondeos"))
            {
                return ApplicationResources.Estado_Sondeos;
            }
            if (stringsAreEqualIgnoringCase(estado, "Permisos"))
            {
                return ApplicationResources.Estado_Permisos;
            }
            if (stringsAreEqualIgnoringCase(estado, "Obra Civil"))
            {
                var r = ApplicationResources.Estado_ObraCivil;
                return r;
            }
            if (stringsAreEqualIgnoringCase(estado, "Tendido"))
            {
                return ApplicationResources.Estado_Tendido;
            }
            if (stringsAreEqualIgnoringCase(estado, "ODF"))
            {
                return ApplicationResources.Estado_ODF;
            }
            if (stringsAreEqualIgnoringCase(estado, "Terminado"))
            {
                return ApplicationResources.Estado_Terminada;
            }
            return new SolidColorBrush(Colors.Transparent);

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private bool stringsAreEqualIgnoringCase(string a, string b)
        {
            return string.Compare(a, b, StringComparison.CurrentCultureIgnoreCase) == 0;
        }
    }
}