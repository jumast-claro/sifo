using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Jumast.Claro.ObraCivilTendido.Notas;
using Syncfusion.Data.Extensions;

namespace Jumast.Claro.ObraCivilTendido.DesktopUI
{
    public class NotasViewModel : INotifyPropertyChanged
    {
        public string Enlace { get; set; }


        private ObservableCollection<INota> _notas = new ObservableCollection<INota>();
        public ObservableCollection<INota> Notas
        {
            get
            {
                return _notas;
            }
            set
            {
                _notas = value.OrderByDescending(n => n.Fecha).ToObservableCollection();
                OnPropertyChanged();
            }
        }

        private NotaModel _nota;
        public NotaModel Nota
        {
            get
            {
                return _nota;
            }
            set
            {
                _nota = value;
                OnPropertyChanged();
            }
        }

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}