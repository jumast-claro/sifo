﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Media.Imaging;
using Jumas.Claro.ObraCivilTendido.Model;
using Jumast.Claro.BusinessObject;
using Jumast.Claro.BusinessObject.Mvvm;
using Jumast.Claro.ObraCivilTendido.Model.Abstract;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Claro.ObraCivilTendido.DesktopUI
{
    public interface IVencida
    {
        Vencida Vencida { get; }
    }

    public class InstalacionViewModel : IVencida
    {
        private readonly IInstalacionFO _model;

        public InstalacionViewModel(IInstalacionFO model)
        {
            _model = model;
            ClienteProperty = new ReadWriteProperty<string>(model.Cliente);
            DireccionProperty = new ReadWriteProperty<string>(model.Direccion);
            PartidoProperty = new ReadWriteProperty<string>(model.Partido);
            NumerosDeEnlaceProperty = new ReadWriteProperty<string>(model.Enlace);
            NumeroDeOrdenProperty = new ReadWriteProperty<string>(model.Orden);
            FechaDeRecibidoProperty = new ReadOnlyFechaProperty(model.FechaDeRecibido);
            FechaDeCompromisoVantiveProperty = new ReadWriteProperty<DateTime?>(model.FechaDeCompromisoVantive);
            TipoDeOrdenProperty = new ReadWriteProperty<string>(model.TipoDeOrden);
            TecnologiaProperty = new ReadWriteProperty<string>(model.Tecnologia);
            ContratistaEnCursoProperty = new ReadWriteProperty<string>(model.ContratistaEnCurso);
            FechaDeAsignadoProperty = new ReadWriteProperty<DateTime?>(model.FechaDeAsignado);
            FechaDeInformeRecibidoProperty = new ReadWriteProperty<DateTime?>(model.FechaDeInformeRecibido);
            MetrosDeObraCivilProperty = new ReadWriteProperty<double?>(model.MetrosDeObraCivil);
            MetrosDeTendidoProperty = new ReadWriteProperty<double?>(model.MetrosDeTendido);
            ValorRelevadoProperty = new ReadWriteProperty<double?>(model.ValorRelevado);
            DocumentadoWebGisProperty = new ReadWriteProperty<string>(model.DocumentadoWebGis);
            FechaDeCierreProperty = new ReadOnlyFechaProperty(model.FechaDeCierre);
            HipervisorProperty = new ReadWriteProperty<string>(model.Hipervisor);

            VencidaProperty = new ReadWriteProperty<Vencida>(model.Vencida);
            DiasDeRecibidoProperty = new ReadWriteProperty<double?>(model.DiasDeRecibido);
            DiasParaVencimientoProperty = new ReadWriteProperty<double?>(model.DiasParaVencimiento);
            TipoDeTareaProperty = new ReadWriteProperty<TipoDeTarea>(model.TipoDeTarea);

            DiasDeRelevamientoProperty = new ReadWriteProperty<double?>(model.DiasDeRelevamiento);

            ZonaProperty = new ReadWriteProperty<Zona>(model.Zona);

            DiasHastaAsignarRelevamientoProperty = new ReadWriteProperty<double?>(model.DiasHastaAsignarRelevamiento);

            EstadoProperty = new ReadWriteProperty<string>(model.Estado);
            ObservacionesProperty = new ReadWriteProperty<string>(model.Observaciones);

            HyperlinkProperty = new ReadWriteProperty<string>(model.Link);

            DiasDeInstalacionProperty = new ReadWriteProperty<double?>(model.DiasDeInstalacion);

            VencidaInstalacionesFOProperty = new ReadWriteProperty<Vencida>(model.VencidaInstalacionesFO);
            DiasParaVencimientoInstalacionesFOProperty = new ReadWriteProperty<double?>(model.DiasParaVencimientoInstalacionesFO);
        }

        public IBindableProperty<string> ClienteProperty { get; private set; }
        public IBindableProperty<string> DireccionProperty { get; private set; }
        public IBindableProperty<string> PartidoProperty { get; private set; }
        public IBindableProperty<string> NumerosDeEnlaceProperty { get; set; } 
        public IBindableProperty<string> NumeroDeOrdenProperty { get; set; } 
        public ReadOnlyFechaProperty FechaDeRecibidoProperty { get; set; }
        public IBindableProperty<DateTime?> FechaDeCompromisoVantiveProperty { get; set; }
        public IBindableProperty<string> TipoDeOrdenProperty { get; set; }
        public IBindableProperty<string> TecnologiaProperty { get; set; }
        public IBindableProperty<string> ContratistaEnCursoProperty { get; set; } 
        public IBindableProperty<DateTime?> FechaDeAsignadoProperty { get; set; }
        public IBindableProperty<DateTime?> FechaDeInformeRecibidoProperty { get; set; }
        public IBindableProperty<double?> MetrosDeObraCivilProperty { get; set; }
        public IBindableProperty<double?> MetrosDeTendidoProperty { get; set; }
        public IBindableProperty<double?> ValorRelevadoProperty { get; set; }
        public IBindableProperty<string> DocumentadoWebGisProperty { get; set; }
        public ReadOnlyFechaProperty FechaDeCierreProperty { get; set; }
        public IBindableProperty<string> HipervisorProperty { get; set; }

        public IBindableProperty<Vencida> VencidaProperty { get; set; }
        public IBindableProperty<double?> DiasDeRecibidoProperty { get; set; }
        public IBindableProperty<double?> DiasParaVencimientoProperty { get; set; }

        public IBindableProperty<TipoDeTarea> TipoDeTareaProperty { get; set; }

        public IBindableProperty<double?> DiasDeRelevamientoProperty { get; private set; }

        public IBindableProperty<double?> DiasDeInstalacionProperty { get; private set; }

        public IBindableProperty<Zona> ZonaProperty { get; private set; }

        public IBindableProperty<double?> DiasHastaAsignarRelevamientoProperty { get; private set; }


        public Vencida Vencida
        {
            get { return VencidaProperty.Value; }
            
        }

        public IBindableProperty<string> ObservacionesProperty { get; set; }
        public IBindableProperty<string> EstadoProperty { get; set; }

        public IBindableProperty<string> HyperlinkProperty { get; set; }

        public IInstalacionFO Model
        {
            get { return _model; }
        }

        public IBindableProperty<Vencida> VencidaInstalacionesFOProperty { get; set; }
        public IBindableProperty<double?> DiasParaVencimientoInstalacionesFOProperty { get; set; }
    }
}
