using Jumast.Claro.ObraCivilTendido.DataAcces.Abstract;
using Jumast.Claro.ObraCivilTendido.DataAcces.Concrete;
using Jumast.Claro.ObraCivilTendido.Mvvm.InstalacionesSeguimiento;
using Jumast.Claro.ObraCivilTendido.Notas;

namespace Jumast.Claro.ObraCivilTendido.Mvvm
{
    public class TerminadasDataGridViewModel : InstalacionesDataGridViewModel
    {
        public TerminadasDataGridViewModel(IInstalacionesFOExcelRepository excelRepository, INotasRepository notasRepository) : base(excelRepository, notasRepository, false)
        {
        }
    }
}