﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Jumast.Claro.ObraCivilTendido.DesktopUI;
using Syncfusion.UI.Xaml.Grid;

namespace Jumast.Claro.ObraCivilTendido.Mvvm.InstalacionesTerminadas
{
    /// <summary>
    /// Interaction logic for TerminadasDataGridView.xaml
    /// </summary>
    public partial class TerminadasDataGridView : UserControl
    {
        public static DependencyProperty SfDataGridProperty = DependencyProperty.Register("SfDataGrid", typeof(SfDataGrid), typeof(TerminadasDataGridView));

        public TerminadasDataGridView()
        {
            InitializeComponent();
            this.SfDataGrid = this._sfDataGridWrapper._sfDataGrid;
        }

        public SfDataGrid SfDataGrid
        {
            get { return (SfDataGrid)GetValue(SfDataGridProperty); }
            set
            {
                SetValue(SfDataGridProperty, value);
            }
        }
    }
}
