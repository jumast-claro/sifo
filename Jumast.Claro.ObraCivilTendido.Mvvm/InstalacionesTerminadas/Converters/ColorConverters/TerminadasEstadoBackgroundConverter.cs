﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;using System.Windows.Data;

namespace Jumast.Claro.ObraCivilTendido.Mvvm.InstalacionesTerminadas.Converters.ColorConverters
{
    public class TerminadasEstadoBackgroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var estado = value as string;
            switch (estado)
            {
                case "Terminada":
                    return Brushes.MediumSeaGreen;
                case "Fracasada":
                    return Brushes.Pink;
            }
            return Brushes.Transparent;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
