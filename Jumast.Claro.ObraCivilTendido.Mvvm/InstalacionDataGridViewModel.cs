﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Jumast.Claro.ObraCivilTendido.DataAcces.Abstract;
using Jumast.Claro.ObraCivilTendido.DataAcces.Concrete;
using Jumast.Claro.ObraCivilTendido.DesktopUI;
using Jumast.Claro.ObraCivilTendido.Notas;
using Jumast.Wpf.Mvvm;
using Syncfusion.Data.Extensions;

namespace Jumast.Claro.ObraCivilTendido.Mvvm
{
    public class InstalacionesDataGridViewModel : INotifyPropertyChanged
    {

        private bool _isBusy = false;

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        private readonly IInstalacionesFOExcelRepository _instalacionesRepository;
        private readonly INotasRepository _notasRepository;
        private readonly bool _readDataOnInit;

        //---------------------------------------------------------------------
        // Constructor
        //---------------------------------------------------------------------
        public InstalacionesDataGridViewModel(IInstalacionesFOExcelRepository instalacionesRepository, INotasRepository notasRepository, bool readDataOnInit)
        {
            _instalacionesRepository = instalacionesRepository;
            _notasRepository = notasRepository;
            _readDataOnInit = readDataOnInit;

            //if (readDataOnInit)
            //{
            //    var data = getDataAsync();
            //}

            Notas = new NotasViewModel();
            ActualizarCommand = new RelayCommand(_actualizarExecutd, o => true);
            AbrirCarpetaCommand = new RelayCommand(_abrirCarpetaExecuted, _abrirCarpetaCanExecute);
            AgregarNotaCommand = new RelayCommand(agregarNotaExecuted, agregarNotaCanExecute);
            EliminarNotaCommand = new RelayCommand(eleminarNotaExecuted, eliminarNotaCanExecute);
            GuardarNotasCommand = new RelayCommand(_guardarNotasExecuted, o => true);

            VerDetallesCommand = new RelayCommand(_verDetallesExecuted, _verDetallesCanExecute);
        }



        //---------------------------------------------------------------------
        // Commands
        //---------------------------------------------------------------------
        public ICommand ActualizarCommand { get; private set; }

        private void doIt()
        {
            var viewModels = new ObservableCollection<InstalacionViewModel>();
            _instalacionesRepository.Actualizar();
            var instalaciones = _instalacionesRepository.ObtenerTodasLasInstalaciones();
            foreach (var instalacion in instalaciones)
            {
                var instalacionViewModel = new InstalacionViewModel(instalacion);
                viewModels.Add(instalacionViewModel);
            }

            Instalaciones = viewModels;
            IsBusy = false;
        }

        private void _actualizarExecutd(object o)
        {

            try
            {
                Task.Run(() =>
                {
                    IsBusy = true;
                    _instalacionesRepository.Actualizar();
                    var instalaciones = _instalacionesRepository.ObtenerTodasLasInstalaciones();
                    foreach (var instalacion in instalaciones)
                    {
                        var instalacionViewModel = new InstalacionViewModel(instalacion);
                        Instalaciones.Add(instalacionViewModel);
                    }
                    IsBusy = false;
                }).ContinueWith(t =>
                {
                    var flattened = t.Exception.Flatten();
                    flattened.Handle(ex =>
                    {
                        MessageBox.Show("Error:" + ex.Message);
                        IsBusy = false;
                        return false;
                    });
                }, TaskContinuationOptions.OnlyOnFaulted);
            }

            catch (AggregateException e)
            {
                MessageBox.Show(e.Message);
            }


        }

        public ICommand AbrirCarpetaCommand { get; private set; }
        private void _abrirCarpetaExecuted(object o)
        {
            try
            {
                Process.Start(new ProcessStartInfo(InstalacionSeleccionada.HyperlinkProperty.Value));
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }
        }
        private bool _abrirCarpetaCanExecute(object o)
        {
            return InstalacionSeleccionada != null && Directory.Exists(InstalacionSeleccionada.HyperlinkProperty.Value);
        }

        public ICommand AgregarNotaCommand { get; set; }
        private void agregarNotaExecuted(object o)
        {
            var nuevaNota = new NotaModel() { Fecha = DateTime.Now, Enlace = InstalacionSeleccionada.NumerosDeEnlaceProperty.Value };
            Notas.Notas.Insert(0, nuevaNota);
            _notasRepository.AgregarNota(nuevaNota);
        }
        private bool agregarNotaCanExecute(object o)
        {
            return !string.IsNullOrEmpty(InstalacionSeleccionada?.NumerosDeEnlaceProperty.Value);
        }

        public ICommand EliminarNotaCommand { get; set; }
        private void eleminarNotaExecuted(object o)
        {
            _notasRepository.EliminarNota(Notas.Nota);
            Notas.Notas.Remove(Notas.Nota);
        }
        private bool eliminarNotaCanExecute(object o)
        {
            return Notas.Nota != null;
        }

        public ICommand GuardarNotasCommand { get; set; }
        private void _guardarNotasExecuted(object o)
        {
            try
            {
                _notasRepository.Guardar();
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }

        }

        public ICommand VerDetallesCommand { get; private set; }

        private void _verDetallesExecuted(object o)
        {
            var instalacionesViewModel = InstalacionSeleccionada.Model;
            var view = new InstalacionDetalleView();
            var viewModel = new InstalacionDetalleViewModel(instalacionesViewModel);
            viewModel.Notas = this.Notas;



            view.DataContext = viewModel;

            var window = new Window();
            window.Owner = Application.Current.MainWindow;
            window.Content = view;

            //window.Width = 1100;
            //window.Height = 950;
            window.WindowState = WindowState.Maximized;
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            window.Show();

        }
        private bool _verDetallesCanExecute(object o)
        {
            return InstalacionSeleccionada != null;
        }


        //---------------------------------------------------------------------
        // Properties
        //---------------------------------------------------------------------
        private ObservableCollection<InstalacionViewModel> _viewModels = new ObservableCollection<InstalacionViewModel>();
        public ObservableCollection<InstalacionViewModel> Instalaciones
        {
            get { return _viewModels; }
            set
            {
                if (value == _viewModels) return;
                _viewModels = value;
                OnPropertyChanged();
            }
        }

        private InstalacionViewModel _instalacionSeleccionada;
        public InstalacionViewModel InstalacionSeleccionada
        {
            get { return _instalacionSeleccionada; }
            set
            {
                if (value != _instalacionSeleccionada)
                {
                    _instalacionSeleccionada = value;
                    OnPropertyChanged();


                    if (InstalacionSeleccionada == null)
                    {
                        Notas.Notas.Clear();
                    }
                    else
                    {
                        var enlace = InstalacionSeleccionada.NumerosDeEnlaceProperty.Value;
                        var notas = _notasRepository.TodasLasNotas().Where(n => enlace.Contains(n.Enlace));
                        Notas.Notas = notas.ToObservableCollection();
                    }
                }
            }
        }

        private NotasViewModel _notas;
        public NotasViewModel Notas
        {
            get { return _notas; }
            set
            {
                _notas = value;
                OnPropertyChanged();
            }
        }

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
