﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using Jumast.Claro.BusinessObject.Tareas.DataAcces.Abstract;

namespace Jumast.Claro.BusinessObject.Tareas.DataAcces.Concrete.Csv
{
    public class TareasBusinessObjectCsvReader
    {
        private const int ZONA = 1;
        private const int INBOX = 2;
        private const int TIPO_DE_MOVIMIENTO = 4;
        private const int PRODUCTO = 7;
        private const int TIPO_DE_ORDEN = 8;
        private const int TIPO_DE_SOLUCION = 9;
        private const int FECHA_DE_COMPROMISO = 10;
        private const int FECHA_RENEGOCIADA = 11;
        private const int FECHA_INICIO_TAREA = 12;
        private const int FECHA_FIN_TAREA = 13;
        private const int RESULTADO = 14;
        private const int TOMADO_POR = 15;
        private const int CLIENTE = 16;
        private const int ORDEN = 17;
        private const int ENLACE = 18;
        private const int TAREA_ID = 19;
        private const int IDENTIFICADOR = 20;
        private const int FECHA_ULTIMA_NOTA = 21;
        private const int AUTOR_ULTIMA_NOTA = 22;
        private const int ULTIMA_NOTA = 23;
        private const int WORKFLOW_FECHA_INICIO = 24;
        private const int WORKFLOW_FECHA_FIN = 25;
        private const int PROVINCIA = 26;
        private const int PARTIDO = 27;
        private const int LOCALIDAD = 28;
        private const int CALLE = 29;
        private const int NUMERO = 30;

        //---------------------------------------------------------------------
        // Campos
        //---------------------------------------------------------------------
        private readonly string _fullFileNameWithExtension;
        private IEnumerable<ITareaBusinessObjectDataModel> _data;
        private readonly ITareaBusinessObjectMapper _mapper = new TareaBusinessObjectMapper();

        //---------------------------------------------------------------------
        // Constructor
        //---------------------------------------------------------------------
        public TareasBusinessObjectCsvReader(string fullFileNameWithExtension)
        {
            _fullFileNameWithExtension = fullFileNameWithExtension;
        }


        //---------------------------------------------------------------------
        // Métodos públicos
        //---------------------------------------------------------------------
        public IEnumerable<ITareaBusinessObjectDataModel> SelectAll()
        {
            if (_data == null)
            {
                _data = readCsvFile();
            }
            return _data;
        }

        private IEnumerable<ITareaBusinessObjectDataModel> readCsvFile()
        {
            var list = new List<ITareaBusinessObjectDataModel>();
            using (TextReader textReader = File.OpenText(_fullFileNameWithExtension))
            {
                var csvReader = new CsvReader(textReader);
                while (csvReader.Read())
                {
                    var dataTransferObject = readLine(csvReader);
                    var dataModel = _mapper.MapDataTransferObjectToDataModel(dataTransferObject);
                    list.Add(dataModel);
                }
            }
            return list;
        }

        private TareaBusinessObjectDataTransferObject readLine(CsvReader csv)
        {
            var dataTransferObject = new TareaBusinessObjectDataTransferObject()
            {
                Zona = csv.GetField<string>(ZONA),
                Inbox = csv.GetField<string>(INBOX),
                TipoMovimiento = csv.GetField<string>(TIPO_DE_MOVIMIENTO),
                Producto = csv.GetField<string>(PRODUCTO),
                TipoDeOrden = csv.GetField<string>(TIPO_DE_ORDEN),
                TipoDeSolucion = csv.GetField<string>(TIPO_DE_SOLUCION),
                FechaCompromisoOrden = csv.GetField<string>(FECHA_DE_COMPROMISO),
                FechaRenegociada = csv.GetField<string>(FECHA_RENEGOCIADA),
                FechaInicioTarea = csv.GetField<string>(FECHA_INICIO_TAREA),
                FechaFinTarea = csv.GetField<string>(FECHA_FIN_TAREA),
                Resultado = csv.GetField<string>(RESULTADO),
                TomadoPor = csv.GetField<string>(TOMADO_POR),
                Cliente = csv.GetField<string>(CLIENTE),
                Orden = csv.GetField<string>(ORDEN),
                Enlace = csv.GetField<string>(ENLACE),
                TareaId = csv.GetField<string>(TAREA_ID),
                Identificador = csv.GetField<string>(IDENTIFICADOR),
                UltimaNotaFecha = csv.GetField<string>(FECHA_ULTIMA_NOTA),
                UltimaNotaAutor = csv.GetField<string>(AUTOR_ULTIMA_NOTA),
                UltimaNota = csv.GetField<string>(ULTIMA_NOTA),
                WorkflowFechaInicio = csv.GetField<string>(WORKFLOW_FECHA_INICIO),
                WorkflowFechaFin = csv.GetField<string>(WORKFLOW_FECHA_FIN),
                Provincia = csv.GetField<string>(PROVINCIA),
                Partido = csv.GetField<string>(PARTIDO),
                Localidad = csv.GetField<string>(LOCALIDAD),
                Calle = csv.GetField<string>(CALLE),
                Numero = csv.GetField<string>(NUMERO),
            };

            return dataTransferObject;
        }
    }
}
