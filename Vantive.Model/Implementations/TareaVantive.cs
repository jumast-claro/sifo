﻿using System;
using Vantive.Model.Abstractions;
using Vantive.Model.Enums;

namespace Vantive.Model.Implementations
{
    public class TareaVantive : ITareaVantive
    {
        public DateTime? FechaDeInstalacion { get; set; }
        public string Identificador { get; set; }
        public string NumeroDeOrden { get; set; }
        public string Cliente { get; set; }
        public string Localidad { get; set; }
        public string Provincia { get; set; }
        public string TomadoPor { get; set; }
        public string Estado { get; set; }
        public string MotivoDeInterrupcion { get; set; }
        public string MotivoDeOrden { get; set; }
        public DateTime FechaDeRecibido { get; set; }
        public string Ejecutivo { get; set; }
        public string UnidadDeNegocio { get; set; }
      
        public string IdTarea { get; set; }
        public string Clase { get; set; }

        public VencidaEnum Vencida
        {
            get
            {
                if (!FechaDeInstalacion.HasValue)
                {
                    return VencidaEnum.NoAplica;
                }
                else
                {
                    return FechaDeInstalacion.Value <= DateTime.Now ? VencidaEnum.Si : VencidaEnum.No;
                }
            }
        }

        public int? DiasParaVencimiento
        {
            get
            {
                if (!FechaDeInstalacion.HasValue)
                {
                    return null;
                }
                else
                {
                    return (FechaDeInstalacion.Value - DateTime.Now).Days;
                }
            }
        }

        public int DiasDeRecibido
        {
            get { return (DateTime.Now - FechaDeRecibido).Days; }
        }

        public string Producto => Identificador.Split('-')[0];

    }
}
