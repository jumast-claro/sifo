﻿using System;
using Vantive.Model.Enums;

namespace Vantive.Model.Abstractions
{
    public interface ITareaVantive
    {
        DateTime? FechaDeInstalacion { get; set; }
        string Clase { get; set; }
        string Cliente { get; set; }
        string Ejecutivo { get; set; }
        string Estado { get; set; }
      
        DateTime FechaDeRecibido { get; set; }
        string Identificador { get; set; }
        string IdTarea { get; set; }
        string Localidad { get; set; }
        string MotivoDeInterrupcion { get; set; }
        string MotivoDeOrden { get; set; }
        string NumeroDeOrden { get; set; }
        string Provincia { get; set; }
        string TomadoPor { get; set; }
        string UnidadDeNegocio { get; set; }

        VencidaEnum Vencida { get; }
        int? DiasParaVencimiento { get; }

        int DiasDeRecibido { get; }

        string Producto { get; }
    }


}