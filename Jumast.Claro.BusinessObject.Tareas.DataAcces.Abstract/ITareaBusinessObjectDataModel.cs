﻿using System;

namespace Jumast.Claro.BusinessObject.Tareas.DataAcces.Abstract
{
    public interface ITareaBusinessObjectDataModel
    {
        string UltimaNotaAutor { get; set; }
        string Calle { get; set; }
        string Clase { get; set; }
        string Cliente { get; set; }
        int? DiasDeInstalacion { get; set; }
        string Enlace { get; set; }
        DateTime? FechaDeCompromiso { get; set; }
        DateTime? FechaFinTarea { get; set; } //
        DateTime FechaInicioTarea { get; set; }
        DateTime? FechaRenegociada { get; set; }
        DateTime UltimaNotaFecha { get; set; }
        string Identificador { get; set; }
        string Inbox { get; set; }
        string Localidad { get; set; }
        string Numero { get; set; }
        int Orden { get; set; }
        string Partido { get; set; }
        string Producto { get; set; }
        string Provincia { get; set; }
        string Resultado { get; set; }
        int TareaID { get; set; }
        string TipoDeMovimiento { get; set; }
        string TipoDeOrden { get; set; }
        string TipoDeSolucion { get; set; }
        string TomadoPor { get; set; }
        string UltimaNota { get; set; }
        string UnidadDeNegocio { get; set; }
        DateTime? WorkflowFechaFin { get; set; }
        DateTime? WorkflowFechaInicio { get; set; }
        int WorkflowID { get; set; }
    }
}