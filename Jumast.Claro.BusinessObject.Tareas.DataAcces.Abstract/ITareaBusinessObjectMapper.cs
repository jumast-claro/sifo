﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumast.Claro.BusinessObject.Tareas.DataAcces.Concrete;

namespace Jumast.Claro.BusinessObject.Tareas.DataAcces.Abstract
{
    public interface ITareaBusinessObjectMapper
    {
        ITareaBusinessObjectDataModel MapDataTransferObjectToDataModel(ITareaBusinessObjectDataTransferObject dataTransferObject);
    }
}
