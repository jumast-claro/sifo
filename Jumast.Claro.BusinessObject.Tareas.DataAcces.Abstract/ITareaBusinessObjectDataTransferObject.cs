﻿namespace Jumast.Claro.BusinessObject.Tareas.DataAcces.Concrete
{
    public interface ITareaBusinessObjectDataTransferObject
    {
        string Calle { get; set; }
        string Cliente { get; set; }
        string Enlace { get; set; }
        string FechaCompromisoOrden { get; set; }
        string FechaFinTarea { get; set; }
        string FechaInicioTarea { get; set; }
        string FechaRenegociada { get; set; }
        string Identificador { get; set; }
        string Inbox { get; set; }
        string InboxFinal { get; set; }
        string Localidad { get; set; }
        string Numero { get; set; }
        string Orden { get; set; }
        string Partido { get; set; }
        string Producto { get; set; }
        string Provincia { get; set; }
        string RegionFinal { get; set; }
        string Resultado { get; set; }
        string TareaId { get; set; }
        string Tecnologia { get; set; }
        string TipoDeOrden { get; set; }
        string TipoDeSolucion { get; set; }
        string TipoFibraOptica { get; set; }
        string TipoMovimiento { get; set; }
        string TomadoPor { get; set; }
        string UltimaNota { get; set; }
        string UltimaNotaAutor { get; set; }
        string UltimaNotaFecha { get; set; }
        string WorkflowFechaFin { get; set; }
        string WorkflowFechaInicio { get; set; }
        string Zona { get; set; }
    }
}