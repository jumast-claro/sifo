﻿namespace Vantive.DataAcces.Abstractions
{
    public interface IVantiveDataTransferObject
    {
        string FechaDeInstalacion { get; set; }
        string Identificador { get; set; }
        string ID { get; set; }
        string Cliente { get; set; }
        string Localidad { get; set; }
        string Provincia { get; set; }
        string TomadoPor { get; set; }
        string Estado { get; set; }
        string MotivoInterrupcion { get; set; }
        string MotivoOrden { get; set; }
        string FechaDeRecibido { get; set; }
        string Ejecutivo { get; set; }
        string UnidadDeNegocio { get; set; }
        string IdObjecto { get; set; }
        string Clase { get; set; }
    }
}
