using System.Collections.Generic;

namespace Vantive.DataAcces.Abstractions
{
    public interface IVantiveDataRepository
    {
        IEnumerable<VantiveDataTransferObject> SelectAll();
    }
}