﻿using System.Collections.Generic;
using SIFO.DataAcces.TareasCerradas;
using Vantive.DataAcces.Abstractions;

namespace Vantive.DataAcces.Implementations
{
    public class VantiveDataRepository : CsvRepository<VantiveDataTransferObject>, IVantiveDataRepository
    {
        public VantiveDataRepository(string path, string delimiter, bool hasFieldsEnclosedInQuotes) : base(path, delimiter, hasFieldsEnclosedInQuotes)
        {
        }
    }
}