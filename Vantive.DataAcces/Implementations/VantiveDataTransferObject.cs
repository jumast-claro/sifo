﻿using Vantive.DataAcces.Abstractions;

namespace Vantive.DataAcces
{
    public class VantiveDataTransferObject : IVantiveDataTransferObject
    {
        public string FechaDeInstalacion { get; set; }
        public string Identificador { get; set; }
        public string ID { get; set; }
        public string Cliente { get; set; }
        public string Localidad { get; set; }
        public string Provincia { get; set; }
        public string TomadoPor { get; set; }
        public string Estado { get; set; }
        public string MotivoInterrupcion { get; set; }
        public string MotivoOrden { get; set; }
        public string FechaDeRecibido { get; set; }
        public string Ejecutivo { get; set; }
        public string UnidadDeNegocio { get; set; }
        public string IdObjecto { get; set; }
        public string Clase { get; set; }
    }
}
