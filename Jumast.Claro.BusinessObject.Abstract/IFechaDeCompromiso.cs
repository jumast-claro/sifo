﻿using System;

namespace Jumast.Claro.BusinessObject.Abstract
{
    public interface IFechaDeCompromiso
    {
        bool TieneFecha { get; }
        DateTime? Fecha { get; }
        int? TiempoRestanteEnDias { get; }
        Vencida EstaVencida { get; }
    }
}