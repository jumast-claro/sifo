﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumast.Claro.BusinessObject.Abstract
{
    public interface IIdentificador
    {
        string TextoCompleto { get; }
        string Enlace { get; }
        string Producto { get; }
    }
}
