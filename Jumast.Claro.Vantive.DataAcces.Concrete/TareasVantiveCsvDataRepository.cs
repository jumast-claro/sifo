﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumast.Claro.Vantive.DataAcces.Abstract;
using Jumast.IO.Csv;
using Microsoft.VisualBasic.FileIO;

namespace Jumast.Claro.Vantive.DataAcces.Concrete
{
    public class TareasVantiveCsvDataRepository : ITareasVantiveDataRepository
    {

        public bool PermitirTareasDuplicadas { get; set; } = true;

        public void ReadFile(string filePath, string inbox)
        {
            var lines = readFile(filePath);
            foreach (var line in lines)
            {
                var dataModel = createDataModelFromCsvLine(line);
                dataModel.Inbox = inbox;

                if (PermitirTareasDuplicadas)
                {
                    _data.Add(dataModel);
                }
                else
                {
                    if (_data.All(t => t.Identificador != dataModel.Identificador))
                    {
                        _data.Add(dataModel);
                    }
                }
              
            }
        }


        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        private const string DATE_TIME_FORMAT = "yyyy/MM/dd HH:mm:ss";
        private const int FECHA_DE_INSTALACION = 0;
        private const int IDENTIFICADOR = 1;
        private const int ID = 2;
        private const int CLIENTE = 3;
        private const int LOCALIDAD = 4;
        private const int PROVINCIA = 5;
        private const int TOMADO_POR = 6;
        private const int ESTADO = 7;
        private const int MOTIVO_INTERRUPCION = 8;
        private const int MOTIVO_ORDEN = 9;
        private const int RECIBIDO = 10;
        private const int EJECUTIVO = 11;
        private const int UNIDAD_DE_NEGOCIO = 12;
        private const int ID_OBJECTO = 13;
        private const int CLASE = 14;

        private List<IVantiveDataModel> _data = new List<IVantiveDataModel>(); 
        //---------------------------------------------------------------------
        // ITareasVantiveDataRepository
        //---------------------------------------------------------------------
        public IEnumerable<IVantiveDataModel> SelectAll()
        {
            //return getData();
            return _data;
        }

        //---------------------------------------------------------------------
        // Private Methods
        //---------------------------------------------------------------------

        private IEnumerable<string[]> readFile(string fullFilePathWithExtension)
        {
            if (!File.Exists(fullFilePathWithExtension))
            {
                throw new FileNotFoundException($"No se encontró el archivo {fullFilePathWithExtension}");
            }

            var result = new List<string[]>();
            using (var parser = new TextFieldParser(fullFilePathWithExtension))
            {
                parser.SetDelimiters("\t");
                parser.HasFieldsEnclosedInQuotes = false;
                parser.ReadLine();
                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();
                    result.Add(fields);
                }
            }
            return result;
        }

        private IVantiveDataModel createDataModelFromCsvLine(string[] line)
        {
            IVantiveDataModel dataModel = new VantiveDataModel()
            {
                FechaDeInstalacion = getDate(getValueForColumn(line, FECHA_DE_INSTALACION)),
                Identificador = getValueForColumn(line, IDENTIFICADOR),
                ID = getValueForColumn(line, ID),
                Cliente = getValueForColumn(line, CLIENTE),
                Localidad = getValueForColumn(line, LOCALIDAD),
                Provincia = getValueForColumn(line, PROVINCIA),
                TomadoPor = getValueForColumn(line, TOMADO_POR),
                Estado = getValueForColumn(line, ESTADO),
                MotivoInterrupcion = getValueForColumn(line, MOTIVO_INTERRUPCION),
                MotivoOrden = getValueForColumn(line, MOTIVO_ORDEN),
                FechaDeRecibido = getDate(getValueForColumn(line, RECIBIDO)),
                Ejecutivo = getValueForColumn(line, EJECUTIVO),
                UnidadDeNegocio = getValueForColumn(line, UNIDAD_DE_NEGOCIO),
                IdObjeto = getValueForColumn(line, ID_OBJECTO),
                Clase = getValueForColumn(line, CLASE)

            };

            return dataModel;
        }

        private string getValueForColumn(string[] line, int column)
        {
            return line[column];
        }

        private DateTime? getDate(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            else
            {
                return DateTime.ParseExact(value, DATE_TIME_FORMAT, CultureInfo.InvariantCulture);
            }
        }
    }
}
