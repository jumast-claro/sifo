﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;

namespace Jumast.IO.Csv
{
    public class CsvFileReader
    {
        private readonly string _path;
        private readonly string _delimiter;
        private readonly bool _hasFieldsEnclosedInQuotes;

        public CsvFileReader(string path, string delimiter, bool hasFieldsEnclosedInQuotes)
        {
            _path = path;
            _delimiter = delimiter;
            _hasFieldsEnclosedInQuotes = hasFieldsEnclosedInQuotes;
        }

        public IEnumerable<string[]> ReadAllFields()
        {
            var result = new List<string[]>();
            using (var parser = new TextFieldParser(_path))
            {
                parser.SetDelimiters(_delimiter);
                parser.HasFieldsEnclosedInQuotes = _hasFieldsEnclosedInQuotes;
                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();
                    result.Add(fields);
                }
            }
            return result;
        }
    }
}
