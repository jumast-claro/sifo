﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumast.Claro.BusinessObject;
using Jumast.Claro.BusinessObject.Abstract;
using Jumast.Claro.BusinessObject.Concrete;
using Jumast.Claro.Vantive.Model.Abstract;

namespace Jumast.Claro.Vantive.Model.Concrete
{
    public class TareaVantive : ITareaVantive
    {
        private readonly Identificador _identificador;
        private readonly IFechaDeInicio _fechaInicioTarea;
        private readonly IFechaDeCompromiso _fechaInstalacion;

        public TareaVantive(DateTime fechaInicioTarea, DateTime? fechaInstalacion,string identificador)
        {
            _identificador = new Identificador(identificador);
            _fechaInstalacion = new FechaDeCompromiso(fechaInstalacion);
            _fechaInicioTarea = new FechaDeInicio(fechaInicioTarea);
        }

        public string Inbox { get; set; }
        public DateTime? FechaDeInstalacion { get; set; }
        public string Identificador { get; set; }
        public string NumeroDeOrden { get; set; }
        public string Cliente { get; set; }
        public string Localidad { get; set; }
        public string Provincia { get; set; }
        public string TomadoPor { get; set; }
        public string Estado { get; set; }
        public string MotivoInterrupcion { get; set; }
        public string MotivoOrden { get; set; }
        //public DateTime FechaDeRecibido { get; set; }
        public string Ejecutivo { get; set; }
        public string UnidadDeNegocio { get; set; }
        public string IdTarea { get; set; }
        public string Clase { get; set; }

        public string Producto => _identificador.Producto;
        public string Enlace => _identificador.Enlace;

        public IFechaDeInicio FechaInicioTarea => _fechaInicioTarea;
        public IFechaDeCompromiso FechaInstalacion => _fechaInstalacion;
    }


   
}
