﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.Properties;
using SIFO.DesktopUI.Utilities.Reflection;
using SIFO.Model;

namespace SIFO.DesktopUI.TareasPendientes
{
    public class TareaPendienteViewModel
    {
        private readonly ITarea _model;

        public TareaPendienteViewModel(ITarea tareaPendiente)
        {
            _model = tareaPendiente;

            _inicializarPropiedades();
        }

        public ITarea Model => _model;

        private void _inicializarPropiedades()
        {

            MesDeInicioProperty = new ReadOnlyProperty<int>(_model.FechaInicioTarea.Month);

            PlantaProperty = new WrappingViewModelProperty<string>(() => _model.Planta == Planta.PlantaExterna ? "PE" : (_model.Planta == Planta.PlantaInterna ? "PI" : ""));
            RegionFinalProperty = new WrappingViewModelProperty<string>(() => _model.RegionFinal);
            ZonaProperty = new WrappingViewModelProperty<string>(() => _model.Zona.ToString());
            InboxProperty = new ReadOnlyProperty<string>(_model.Inbox);
            InboxFinalProperty = new ReadOnlyProperty<string>(_model.InboxFinal);
            TecnologiaProperty = new ReadOnlyProperty<string>(_model.TecnologiaBis == TecnologiaEnum.FibraOptica ? "FO" : _model.TecnologiaBis == TecnologiaEnum.Inalambrico ? "Inal." : _model.TecnologiaBis == TecnologiaEnum.NoAplica ? "N/A" : "?");
            ProductoProperty = new WrappingViewModelProperty<string>(() => _model.Producto);
            TipoDeOrdenProperty = new ReadOnlyProperty<string>(_model.TipoDeOrden);

            TipoDeSolucionProperty = new ReadOnlyProperty<string>(_model.TipoSolucion, _model.TipoSolucion == String.Empty ? "?" : _model.CodigoTipificacion);

            FechaInicioOrdenProperty = new WrappingViewModelProperty<string>(() => _model.FechaInicioOrden.HasValue ? _model.FechaInicioOrden.Value.ToString("dd/MM/yyyy") : "?");
            FechaCompromisoOrdenProperty = new ReadOnlyProperty<DateTime?>(_model.FechaCompromisoOrden, _model.FechaCompromisoOrden.HasValue ? _model.FechaCompromisoOrden.Value.ToString("dd/MM/yyyy") : "");
            FechaRenegociadaProperty = new WrappingViewModelProperty<string>(() => _model.FechaRenegociada.HasValue ? _model.FechaRenegociada.Value.ToString("dd/MM/yyyy") : "");
            FechaInstalacionProperty = new WrappingViewModelProperty<string>(() => _model.FechaDeInstalacion.HasValue ? _model.FechaDeInstalacion.Value.ToString("dd/MM/yyy") : "");


            TiempoMaximoDePermanenciaEnTodasLasBandejasProperty = new WrappingViewModelProperty<int>(() => _model.DiasDeInstalacion ?? 0, (value) => value == 0 ? "?" : value.ToString());

            FechaInicioProperty = new ReadOnlyProperty<DateTime>(_model.FechaInicioTarea, _model.FechaInicioTarea.ToString("dd/MM/yyyy"));
            TomadoPorProperty = new ReadOnlyProperty<string>(_model.TomadoPor);
            ClienteProperty = new ReadOnlyProperty<string>(_model.Cliente);
            OrdenProperty = new ReadOnlyProperty<long>(_model.Orden);
            NumeroEnlaceProperty = new WrappingViewModelProperty<long>(() => _model.Enlace);
            TareaIdProperty = new ReadOnlyProperty<string>(_model.TareaId);
            IdentificadorProperty = new WrappingViewModelProperty<string>(() => _model.Identificador);

            EsProyectoEspecialProperty = new WrappingViewModelProperty<bool>(() => _model.EsProyectoEspecial, (value) => value ? "Sí" : "No");

            IngresoOffTimeProperty = new ReadOnlyProperty<bool>(_model.IngresaOffTime, _model.IngresaOffTime ? "Sí" : "No");
            ZonaSinInfo = new WrappingViewModelProperty<bool>(() => _model.Zona == Zona.SinInfo);
            SinEnlace = new WrappingViewModelProperty<bool>(() => _model.Enlace == 0);


            EsAltaPuraProperty = new WrappingViewModelProperty<bool>(() => _model.EsAltaPura, (value) => value ? "Sí" : "No");

        }

        //[Filter("Está cerrada")]
        //public IBindableProperty<bool> EstaCerradaProperty { get; private set; }

        [Filter("Mes de inicio")]
        public IBindableProperty<int> MesDeInicioProperty { get; set; }
        [Filter("Tarea ID")]
        public IBindableProperty<string> TareaIdProperty { get; private set; }
        [Filter("Orden")]
        public IBindableProperty<long> OrdenProperty { get; private set; }
        [Filter("Enlace")]
        public IBindableProperty<long> NumeroEnlaceProperty { get; private set; }
        [Filter("Tomado por")]
        public IBindableProperty<string> TomadoPorProperty { get; set; }

        [Filter("Planta")]
        public IBindableProperty<string> PlantaProperty { get; private set; }
        [Filter("Zona")]
        public IBindableProperty<string> ZonaProperty { get; private set; }
        [Filter("Region final")]
        public IBindableProperty<string> RegionFinalProperty { get; private set; }
        [Filter("Inbox")]
        public IBindableProperty<string> InboxProperty { get; private set; }
        [Filter("Inbox final")]
        public IBindableProperty<string> InboxFinalProperty { get; set; }
        [Filter("Tecnología")]
        public IBindableProperty<string> TecnologiaProperty { get; set; }
        [Filter("Tipo de orden")]
        public IBindableProperty<string> TipoDeOrdenProperty { get; set; }
        [Filter("Es alta pura?")]
        public IBindableProperty<bool> EsAltaPuraProperty { get; set; }
        [Filter("Es proyecto especial?")]
        public IBindableProperty<bool> EsProyectoEspecialProperty { get; private set; }
        [Filter("Fecha inicio orden")]
        public IBindableProperty<string> FechaInicioOrdenProperty { get; private set; }
        [Filter("Fecha compromiso orden")]
        public IBindableProperty<DateTime?> FechaCompromisoOrdenProperty { get; set; }
        [Filter("Fecha renegociada")]
        public IBindableProperty<string> FechaRenegociadaProperty { get; set; }
        [Filter("Fecha instalación")]
        public IBindableProperty<string> FechaInstalacionProperty { get; set; }
        // Fecha instalacion
        [Filter("Tipo de solución")]
        public IBindableProperty<string> TipoDeSolucionProperty { get; set; }
        [Filter("Días de instalación")]
        public IBindableProperty<int> TiempoMaximoDePermanenciaEnTodasLasBandejasProperty { get; set; }
        [Filter("Fecha inicio tarea")]
        public IBindableProperty<DateTime> FechaInicioProperty { get; private set; }

        [Filter("Cliente")]
        public IBindableProperty<string> ClienteProperty { get; private set; }
        [Filter("Identificador")]
        public IBindableProperty<string> IdentificadorProperty { get; private set; }

        [Filter("Producto")]
        public IBindableProperty<string> ProductoProperty { get; set; }

        [Filter("Ingreso off time?")]
        public IBindableProperty<bool> IngresoOffTimeProperty { get; set; }
        [Filter("Zona sin info?")]
        public IBindableProperty<bool> ZonaSinInfo { get; set; }
        [Filter("Sin enlace?")]
        public IBindableProperty<bool> SinEnlace { get; set; }
    }
}
