﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using SIFO.DesktopUI.CollectionView;
using SIFO.DesktopUI.Framework;
using SIFO.DesktopUI.TareasCerradas;
using SIFO.DesktopUI.TareasIngresadas;
using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.CollectionView;
using SIFO.DesktopUI.Utilities.Reflection;
using SIFO.Model;
using SIFO.Services;

namespace SIFO.DesktopUI.TareasPendientes
{

   

    //public class TareasPendientesViewModel : INotifyPropertyChanged
    //{

    //    private readonly CustomCollectionView<TareaPendienteViewModel> _customCollectionView;
    //    private readonly List<TareaPendienteViewModel> _tareas = new List<TareaPendienteViewModel>();
    //    private TareaPendienteViewModel _selectedItem;


    //    private readonly ITareasPendientesModelRepository _tareasPendientesModelRepository;




    //    [InitializeCommandsAspect]
    //    public TareasPendientesViewModel(ITareasPendientesModelRepository tareasPendientesModelRepository,ITareasIngresadasModelRepository tareasIngresadasModelRepository, TareasCerradasModelRepository tareasCerradasModelRepository)
    //    {

    //        _tareasPendientesModelRepository = tareasPendientesModelRepository;

    //        var pendientes = tareasPendientesModelRepository.SelectAll();

    //        foreach (var tareaPendienteModel in tareasPendientesModelRepository.SelectAll())
    //        {
    //            var tareaPendienteViewModel = new TareaPendienteViewModel(tareaPendienteModel);
    //            _tareas.Add(tareaPendienteViewModel);
    //        }


    //        _customCollectionView = new CustomCollectionView<TareaPendienteViewModel>(_tareas);

    //        GroupService = new GroupService(ItemsSource);
    //        SummaryViewModel = new NewSummaryViewModel()
    //        {
    //            CantidadTotal = _customCollectionView.AllCount,
    //            CantidadEnVistaActual = _customCollectionView.FilteredCount

    //        };
    //    }


    //    public ICollectionView ItemsSource => _customCollectionView.View;

    //    public TareaPendienteViewModel SelectedItem
    //    {
    //        get { return _selectedItem; }
    //        set
    //        {
    //            if (_selectedItem == value) return;
    //            _selectedItem = value;
    //            OnPropertyChanged();
    //        }
    //    }

    //    public FilterServiceViewModel<TareaPendienteViewModel> FilterServiceViewModel => _customCollectionView.Filter;

    //    public GroupService GroupService { get; private set; }
    //    public NewSummaryViewModel SummaryViewModel { get; private set; }

    //    //public TareasIngresadasAnalisisViewModel TareasIngresadasEstadisticasViewModel
    //    //{
    //    //    get { return _ingresadasAnalisisViewModel; }
    //    //    set
    //    //    {
    //    //        _ingresadasAnalisisViewModel = value;
    //    //        OnPropertyChanged();
    //    //    }
    //    //}

    //    //---------------------------------------------------------------------
    //    // Commands
    //    //---------------------------------------------------------------------
    //    [Command(nameof(_removeAllGroupsExecuted), nameof(_removeAllGroupsCanExecute))]
    //    public ICommand RemoveAllGroupsCommand { get; private set; }

    //    private void _removeAllGroupsExecuted(object o)
    //    {
    //        GroupService.RemoveAllGroupsCommand.Execute(o);


    //    }

    //    private bool _removeAllGroupsCanExecute(object o)
    //    {
    //        return GroupService.RemoveAllGroupsCommand.CanExecute(o);
    //    }


    //    public ICommand RemoveAllFiltersCommand { get; private set; }
    //    private void _removeAllFiltersExecuted(object o)
    //    {
    //        FilterServiceViewModel.RemoveAllFilters();
    //        var ordenesFiltradas = new ObservableCollection<TareaPendienteViewModel>();
    //        foreach (var tareaIngresada in _tareas)
    //        {
    //            ordenesFiltradas.Add(tareaIngresada);
    //        }


    //        _customCollectionView.RemoveAllFilters();
    //        SummaryViewModel.CantidadEnVistaActual = SummaryViewModel.CantidadTotal;
    //        //updateCharts();
    //    }
    //    private bool _removeAllFiltersCanExecute(object o)
    //    {
    //        return _customCollectionView.IsFiltered;
    //    }



    //    public ICommand ShowFilterOptionsCommand { get; private set; }
    //    private void _showFilterOptionsExecuted(object o)
    //    {
    //        var propertyName = (string)o;
    //        var view = new FilterOptionsView();
    //        view.AcceptCommand = new RelayCommand(o1 => _customCollectionView.ApplyFilters(), o1 => true);
    //        view.GroupCommand = GroupService.GroupCommand;

    //        var window = new Window
    //        {
    //            Content = view,
    //            HorizontalContentAlignment = HorizontalAlignment.Stretch
    //        };


    //        var filterDialogViewModel = FilterServiceViewModel.GetFilterDialogViewModelForProperty(propertyName);
    //        foreach (var a in filterDialogViewModel)
    //        {
    //            a.IsSelected = BuscadorDeTareas.IsCurrentFilter;
    //        }

    //        _customCollectionView.SetCurrentViewCountForProperty(propertyName);
    //        window.DataContext = filterDialogViewModel;

    //        window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
    //        window.WindowStyle = WindowStyle.ToolWindow;
    //        window.Width = 300;
    //        window.Height = 600;
    //        window.ShowDialog();


    //        SummaryViewModel.CantidadEnVistaActual = _customCollectionView.FilteredCount;

    //        //updateCharts();

    //    }



    //    private bool _showFilterOptionsCanExecute(object o)
    //    {
    //        return true;
    //    }

    //    //private void updateCharts()
    //    //{
    //    //    List<ITarea> universe = new List<ITarea>();
    //    //    var e = ItemsSource.GetEnumerator();
    //    //    while (e.MoveNext())
    //    //    {
    //    //        var ingresadaViewModel = e.Current as TareaIngresadaViewModel;
    //    //        if (ingresadaViewModel != null)
    //    //        {
    //    //            universe.Add(ingresadaViewModel.Model);
    //    //        }
    //    //    }
    //    //    this.TareasIngresadasEstadisticasViewModel = new TareasIngresadasAnalisisViewModel(universe);
    //    //}









    //    //---------------------------------------------------------------------
    //    // INotifyPropertyChanged
    //    //---------------------------------------------------------------------
    //    public event PropertyChangedEventHandler PropertyChanged;

    //    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    //    {
    //        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    //    }



    //}
}
