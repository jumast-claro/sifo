﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using SIFO.DesktopUI.Annotations;

namespace SIFO.DesktopUI.Login
{
    public class LoginViewModel : INotifyPropertyChanged
    {


        private string _user = "Jumas";
        private string _password;



        public string User
        {
            get { return _user; }
            set
            {
                if (value == _user)
                {
                    return;
                }
                _user = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                if (value == _password)
                {
                    return;
                }
                _password = value;
                OnPropertyChanged();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
