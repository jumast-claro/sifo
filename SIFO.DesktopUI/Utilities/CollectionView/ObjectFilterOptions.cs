using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using SIFO.DesktopUI.Framework;

namespace SIFO.DesktopUI.CollectionView
{
    public class ObjectFilterOptions
    {

        public FilterDialogViewModel this[string propertyName]
        {
            get { return _sourceViewOptions.First(o => o.PropertyName == propertyName); }
        }

        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        private readonly ObservableCollection<FilterDialogViewModel> _sourceViewOptions = new ObservableCollection<FilterDialogViewModel>();
        private bool _toogleAll;


        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------
        public ObjectFilterOptions()
        {

        }

        //---------------------------------------------------------------------
        // Properties
        //---------------------------------------------------------------------
        public ObservableCollection<FilterDialogViewModel> SourceViewOptions
        {
            get { return _sourceViewOptions; }
        }

        public FilterDialogViewModel GetValuesForProperty(string propertyName)
        {
            return SourceViewOptions.First(o => o.PropertyName == propertyName);
        }


        //---------------------------------------------------------------------
        // Methods
        //---------------------------------------------------------------------
        public IEnumerable<object> GetSelectedValuesForProperty(string propertyName)
        {
            var propertyOptions = _sourceViewOptions.First(po => po.PropertyName.Equals(propertyName));
            return propertyOptions.SelectedValues;
        }

        public void SelectAllValuesForAllProperties()
        {
            foreach (var propertyValuesViewModel in _sourceViewOptions)
            {
                propertyValuesViewModel.ToogleAll = true;
            }
        }
    }
}