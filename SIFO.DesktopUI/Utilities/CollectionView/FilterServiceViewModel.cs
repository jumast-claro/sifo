﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using SIFO.DesktopUI.CollectionView;
using SIFO.DesktopUI.TareasCerradas;
using SIFO.DesktopUI.Utilities.Reflection;

namespace SIFO.DesktopUI.Utilities.CollectionView
{


    public class CustomCollectionView<T>
    {
        private T[] _allArray;
        private T[] _filteredArray;
        private ObservableCollection<T> _observable;
        private ICollectionView _collectionView;


        private FilterServiceViewModel<T> _filter;

        public CustomCollectionView(IEnumerable<T> sourceCollection)
        {
            var observable = new ObservableCollection<T>();

            var initialCollectionCount = sourceCollection.Count();
            _allArray = new T[initialCollectionCount];
            _filteredArray = new T[initialCollectionCount];

            var i = 0;
            foreach (var item in sourceCollection)
            {
                observable.Add(item);

                _allArray[i] = item;
                _filteredArray[i] = item;
                i++;
            }

            _observable = observable;
            _collectionView = CollectionViewSource.GetDefaultView(_observable);

            _filter = new FilterServiceViewModel<T>(_collectionView);
        }


        public ICollectionView View => _collectionView;
        public int AllCount => _allArray.Length;
        public int FilteredCount => _observable.Count;

        public FilterServiceViewModel<T> Filter => _filter;


        public void RemoveAllFilters()
        {
            _observable.Clear();
            foreach (var item in _allArray)
            {
                _observable.Add(item);
            }
        }

        public void ApplyFilters()
        {
            var filtradas = _filter.Filter(_allArray);
            _observable.Clear();

            foreach (var item in filtradas)
            {
                _observable.Add(item);
            }
        }

        public bool IsFiltered => AllCount != FilteredCount;


        public void SetCurrentViewCountForProperty(string propertyName)
        {
            _filter.SetCurrentViewCountForProperty(propertyName, _observable);
        }
    }

    public class FilterServiceViewModel<T> : INotifyPropertyChanged
    {
        public FilterDialogViewModel GetFilterDialogViewModelForProperty(string propertyName)
        {
            var filterDialogViewModel = _objectFilterOptions.SourceViewOptions.First(o => o.PropertyName == propertyName);
            //foreach (var a in filterDialogViewModel)
            //{
            //    a.IsSelected = BuscadorDeTareas.IsCurrentFilter;
            //}
            return filterDialogViewModel;
        }

        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        private readonly ICollectionView _viewToBeFiltered;

        private ObjectFilterOptions _objectFilterOptions = new ObjectFilterOptions();


        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------
        [InitializeCommandsAspect]
        public FilterServiceViewModel(ICollectionView viewToBeFiltered)
        {
            _viewToBeFiltered = viewToBeFiltered;
            initializeOptions();
            setOptions();
        }

        public void Refresh()
        {
            initializeOptions();
            setOptions();
        }

        public ObservableCollection<T> Filter<T>(T[] collection)
        {
            _filteredProperties.Clear();

            var filteredCollection = new ObservableCollection<T>();

            var reflector = new FilterAttributeReflector(typeof(T));
            var propertyValues = reflector.GetDistinctValues(_viewToBeFiltered.SourceCollection);

            var dicFilterDialogViewModel = new Dictionary<string, FilterDialogViewModel>();
            var dicPropertyInfo = new Dictionary<string, PropertyInfo>();
            foreach (var property in propertyValues)
            {
                var propertyName = property.PropertyName;
                var acceptedValues = _objectFilterOptions.GetSelectedValuesForProperty(propertyName);
                var filterDialogViewModel = GetFilterDialogViewModelForProperty(propertyName);
                //var selectedValues = filterDialogViewModel.SelectedValues;
                //dic.Add(propertyName, selectedValues);
                dicFilterDialogViewModel.Add(propertyName, filterDialogViewModel);

                var propertyInfo = typeof(T).GetProperty(propertyName);
                dicPropertyInfo.Add(propertyName, propertyInfo);


                StringBuilder sb = new StringBuilder();
                sb.Append($"[{filterDialogViewModel.PropertyDisplayName}] = ");
                int selectedCount = 0;
                foreach (var b in filterDialogViewModel)
                {
                    b.IsCurrentFilter = b.IsSelected;
                    if (b.IsCurrentFilter)
                    {
                        if (selectedCount != 0)
                        {
                            sb.Append(" ó ");
                        }
                        //sb.Append($"{b.Value} ");
                        sb.Append($"{b.Value}");
                        selectedCount++;
                    }
                }

                var a = filterDialogViewModel;
                if (!a.AreAllSelected())
                {
                    if (selectedCount <= 5)
                    {
                        _filteredProperties.Add(new { Name = a.PropertyName, DisplayName = sb.ToString() });
                    }
                    else
                    {
                        _filteredProperties.Add(new { Name = a.PropertyName, DisplayName = $"{a.PropertyDisplayName} = (+ 5 valores)" });
                    }


                }

            }

            var notSelectedproperties = new List<PropertyInformationViewModel>();
            foreach (var property in propertyValues)
            {
                var propertyName = property.PropertyName;
                var dialogViewModel = dicFilterDialogViewModel[propertyName];
                if (!dialogViewModel.AreAllSelected())
                {
                    notSelectedproperties.Add(property);
                }
            }

            var dicOpciones = new HashSet<Tuple<string, object>>();

          

            for (var i = 0; i < collection.Length; i++)
            {
                var item = collection[i];
                var accepted = true;
                foreach (var property in notSelectedproperties)
                {

                    var propertyName = property.PropertyName;
                    var dialogViewModel = dicFilterDialogViewModel[propertyName];
                    var acceptedValues = dicFilterDialogViewModel[propertyName].SelectedValues;

                    var instanceProperty = dicPropertyInfo[propertyName].GetMethod.Invoke(item, null);
                    var instancePropertyValue = instanceProperty.GetType().GetProperty("Value").GetMethod.Invoke(instanceProperty, null);
                    var value = instancePropertyValue;

                    if (!acceptedValues.Contains(value))
                    {
                        {
                            accepted = false;
                        }
                    }


                    if (!accepted)
                    {
                        break;
                    }
                }

                if (accepted)
                {
                    filteredCollection.Add(item);
                }
            }



            return filteredCollection;


        }

        public void SetCurrentViewCountForProperty(string propertyName, IEnumerable filtered)
        {
            var propertyInfo = typeof (T).GetProperty(propertyName);
            var filterDialogViewModel = GetFilterDialogViewModelForProperty(propertyName);
            foreach (var v in filterDialogViewModel.Values)
            {
                filterDialogViewModel.GetOptiosForValue(v).CurrentViewCount = 0;
            }


            foreach (var item in filtered)
            {
                var instanceProperty = propertyInfo.GetMethod.Invoke(item, null);
                var instancePropertyValue = instanceProperty.GetType().GetProperty("Value").GetMethod.Invoke(instanceProperty, null);
                var value = instancePropertyValue;
                var opt = filterDialogViewModel.GetOptiosForValue(value);
                opt.CurrentViewCount += 1;
            }
        }

        private void initializeOptions()
        {
            //_objectFilterOptions.SourceViewOptions.Clear();

            var newObjectFilterOptions = new ObjectFilterOptions();

            //var reflector = new FilterAttributeReflector(typeof(TareaViewModel));
            var reflector = new FilterAttributeReflector(typeof(T));
            var propertyValues = reflector.GetDistinctValues(_viewToBeFiltered.SourceCollection);

            foreach (var propertyValuesSet in propertyValues)
            {
                var filterOptions = new FilterDialogViewModel(propertyValuesSet.PropertyName);
                filterOptions.PropertyDisplayName = propertyValuesSet.PropertyDisplayName;
                foreach (var value in propertyValuesSet.DistinctValues)
                {
                    filterOptions.AddOption(value, true, propertyValuesSet.GetNumberOfOcurrencesForValue(value));
                }
                newObjectFilterOptions.SourceViewOptions.Add(filterOptions);
            }
            _objectFilterOptions = newObjectFilterOptions; ;
        }

        private void setOptions()
        {
            var reflector = new FilterAttributeReflector(typeof(T));
            //var reflector = new FilterAttributeReflector(typeof(TareaViewModel));
            var propertyValuesInView = reflector.GetDistinctValues(_viewToBeFiltered);
            var propertyValuesInSource = reflector.GetDistinctValues(_viewToBeFiltered.SourceCollection);

            foreach (var propertyValuesSet in propertyValuesInSource)
            {
                foreach (var value in propertyValuesSet.DistinctValues)
                {
                    var pvvm = _objectFilterOptions.GetValuesForProperty(propertyValuesSet.PropertyName);
                    var option = pvvm.GetOptiosForValue(value);

                    var propertyValueSetInView = propertyValuesInView.First(o => o.PropertyName == propertyValuesSet.PropertyName);
                    if (propertyValueSetInView.HasValue(value))
                    {
                        option.CurrentViewCount = propertyValueSetInView.GetNumberOfOcurrencesForValue(value);
                    }
                    else
                    {
                        option.CurrentViewCount = 0;
                    }
                }
            }
        }

        //---------------------------------------------------------------------
        // Properties
        //---------------------------------------------------------------------

        public IEnumerable<object> Properties
        {
            get { return _objectFilterOptions.SourceViewOptions.Select(o => new { Name = o.PropertyName, DisplayName = o.PropertyDisplayName }); }
        }

        //---------------------------------------------------------------------
        // Commands
        //---------------------------------------------------------------------

        public delegate void FilterStartingDelegate(object sender);
        public event FilterStartingDelegate FilterStarting;
        protected virtual void OnFilterStarting()
        {
            FilterStarting?.Invoke(this);
        }

        public delegate void FilterFinishingDelegate(object sender);
        public event FilterFinishingDelegate FilterFinishing;
        protected virtual void OnFilterFinishing()
        {
            FilterFinishing?.Invoke(this);
        }

        public ICommand ApplyFiltersCommand { get; private set; }

        private void _applyFiltersExecuted(object o)
        {
            OnFilterStarting();
            _filteredProperties.Clear();
            foreach (var a in _objectFilterOptions.SourceViewOptions)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append($"[{a.PropertyDisplayName}] = ");
                int selectedCount = 0;
                foreach (var b in a)
                {
                    b.IsCurrentFilter = b.IsSelected;
                    if (b.IsCurrentFilter)
                    {
                        if (selectedCount != 0)
                        {
                            sb.Append(" ó ");
                        }
                        //sb.Append($"{b.Value} ");
                        sb.Append($"{b.Value}");
                        selectedCount++;
                    }
                }

                if (!a.AreAllSelected())
                {
                    if (selectedCount <= 5)
                    {
                        _filteredProperties.Add(new { Name = a.PropertyName, DisplayName = sb.ToString() });
                    }
                    else
                    {
                        _filteredProperties.Add(new { Name = a.PropertyName, DisplayName = $"{a.PropertyDisplayName} = (+ 5 valores)" });
                    }


                }
            }
            _viewToBeFiltered.Filter = accepted;
            _viewToBeFiltered.Refresh();
            setOptions();


            OnFilterFinishing();
        }


        private ObservableCollection<object> _filteredProperties = new ObservableCollection<object>();

        public ObservableCollection<object> FilteredProperties
        {
            get { return _filteredProperties; }
            set
            {
                throw new NotImplementedException();
            }
        }



        private bool _applyFiltersCanExecute(object o)
        {
            return true;
        }

        private bool accepted(object o)
        {
            var item = (T)o;
            //var properties = item.GetType().GetProperties();

            foreach (var propertyName in _objectFilterOptions.SourceViewOptions.Select(op => op.PropertyName))
            {
                var acceptedValues = _objectFilterOptions.GetSelectedValuesForProperty(propertyName);
                if (acceptedValues == null)
                {
                    return false;
                }
                var instanceProperty = item.GetType().GetProperty(propertyName).GetMethod.Invoke(item, null);
                var instancePropertyValue = instanceProperty.GetType().GetProperty("Value").GetMethod.Invoke(instanceProperty, null);
                if (!acceptedValues.Contains(instancePropertyValue))
                {
                    return false;
                }
            }
            return true;
        }

        public ICommand RemoveAllFiltersCommand { get; private set; }

        public void RemoveAllFilters()
        {
           
            foreach (var a in _objectFilterOptions.SourceViewOptions)
            {
                foreach (var b in a)
                {
                    //b.IsCurrentFilter = b.IsSelected;
                    b.IsSelected = true;
                    b.IsCurrentFilter = true;
                }
            }
            _filteredProperties.Clear();
            //_viewToBeFiltered.Filter = o => true;

        }



        private void _removeAllFiltersExecuted(object o)
        {
            _objectFilterOptions.SelectAllValuesForAllProperties();
            foreach (var a in _objectFilterOptions.SourceViewOptions)
            {
                foreach (var b in a)
                {
                    //b.IsCurrentFilter = b.IsSelected;
                    b.IsCurrentFilter = false;
                    b.IsSelected = true;
                }
            }
            _viewToBeFiltered.Filter = item => true;

            setOptions();
            _filteredProperties.Clear();
        }
        private bool _removeAllFiltersCanExecute(object o)
        {
            return true;
        }

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
