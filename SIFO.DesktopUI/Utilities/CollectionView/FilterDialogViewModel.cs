using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Data;
using System.Windows.Input;
using SIFO.DesktopUI.Annotations;
using SIFO.DesktopUI.Framework;
using SIFO.DesktopUI.Utilities;

namespace SIFO.DesktopUI.CollectionView
{
    public class FilterDialogViewModel : INotifyPropertyChanged, IEnumerable<PropertyValueViewModel>
    {

        public Type ValueType => Values.FirstOrDefault().GetType();

        private string _metodoOrdenamiento = "Por valor";

        public string MetodoOrdenamiento
        {
            get
            {
                return _metodoOrdenamiento;
            }
            set
            {
                _metodoOrdenamiento = value;

                if (_metodoOrdenamiento == "Por cantidad en filtro actual")
                {
                    _view.SortDescriptions.Clear();
                    _view.SortDescriptions.Add(new SortDescription("CurrentViewCount", ListSortDirection.Descending));
                }
                else if(_metodoOrdenamiento == "Por valor")
                {
                    _view.SortDescriptions.Clear();
                    _view.SortDescriptions.Add(new SortDescription("Value", ListSortDirection.Descending));
                }

                else if (_metodoOrdenamiento == "Por cantidad total")
                {
                    _view.SortDescriptions.Clear();
                    _view.SortDescriptions.Add(new SortDescription("Count", ListSortDirection.Descending));
                }

                OnPropertyChanged(_metodoOrdenamiento);
                OnPropertyChanged(nameof(View));
            }
        }
        public List<string> OpcionesOrdenamiento { get; } = new List<string>(){"Por valor", "Por cantidad en filtro actual","Por cantidad total"}; 

        private string _selectedValue = "";

        public bool IsNotFilteringOptions
        {
            get
            {
                return _selectedValue == "";
            }
        }

        public string SelectedValue
        {
            get
            {
                return _selectedValue;
            }
            set
            {
                if (value.Equals(_selectedValue)) return;
                _selectedValue = value;


                if (_selectedValue == "")
                {
                    ToogleAll = true;
                }
                else
                {
                    ToogleAll = false;
                }


                OnPropertyChanged();
                OnPropertyChanged(nameof(IsNotFilteringOptions));
                OnPropertyChanged(nameof(View));
            }
        }

        public bool ContainsSelectedValue
        {
            get { return Values.Contains(_selectedValue); }
            set
            {
                throw new NotImplementedException();
            }
        }

        private ICollectionView _view;


        /// <summary>
        /// Representa el conjunto de todas las opciones para el valor de una determinada propiedad.
        /// </summary>

        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        private readonly string _propertyName;
        private string _propertyDisplayName = "";
        private readonly ObservableCollection<PropertyValueViewModel> _options = new ObservableCollection<PropertyValueViewModel>();
        private bool _toogleAllIsChecked = true;


        public bool AreAllSelected()
        {
            return _options.Count(o => o.IsSelected == false) == 0;
        }
        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------
        [InitializeCommandsAspect]
        public FilterDialogViewModel(string propertyName)
        {
            _propertyName = propertyName;
            _view = CollectionViewSource.GetDefaultView(_options);
            _view.SortDescriptions.Add(new SortDescription("Value", ListSortDirection.Ascending));
            ShowEmptyOption = false;

            _predicates.Add(filterBySearchValuePredicate);

        }



        private List<Predicate<object>> _predicates = new List<Predicate<object>>(); 

        private bool dontShowEmptyOptionsPredicate(object o)
        {
            var propertyValueViewModel = o as PropertyValueViewModel;
            return propertyValueViewModel != null && propertyValueViewModel.CurrentViewCount != 0;

        }

        private bool filterBySearchValuePredicate(object o)
        {

            if (_selectedValue.Equals(""))
            {
                return true;
            }

            var propertyValueViewModel = o as PropertyValueViewModel;
            var value = propertyValueViewModel.Value;

            if (value is string)
            {
                var typedValue = value as string;
                return typedValue.Contains(_selectedValue);

            }

            if (value is int)
            {
                var typedValue = (int)value;

                int pretendedInt;
                var carParse = int.TryParse(_selectedValue, out pretendedInt);
                if (!carParse)
                {
                    return false;
                }

                return typedValue == pretendedInt;

            }

            else
            {
                var stringValue = value.ToString();
                return stringValue.Contains(_selectedValue);
            }

        }

        private bool predicate(object o)
        {
            foreach (var p in _predicates)
            {
                if (!p(o))
                {
                    return false;
                }
            }
            return true;
        }


        //---------------------------------------------------------------------
        // Methods
        //---------------------------------------------------------------------
        public bool ContainsOptionWithValue(object value)
        {
            return _options.Select(o => o.Value).Contains(value);
            //return _options.ContainsValue(value);
        }

        public void UnselectAll()
        {
            foreach (var option in _options)
            {
                option.IsSelected = false;
            }
            OnPropertyChanged(nameof(ToogleAll));
        }

        public void SelectAll()
        {
            foreach (var option in _options)
            {
                option.IsSelected = true;
            }
            OnPropertyChanged(nameof(ToogleAll));
        }

        public void ToogleAllOptions()
        {
            foreach (var option in _options)
            {
                option.IsSelected = !option.IsSelected;
            }
        }

        private bool _showEmptyOption = false;
        public bool ShowEmptyOption
        {
            get { return _showEmptyOption; }
            set
            {
                _showEmptyOption = value;

                if (_showEmptyOption == false && !_predicates.Contains(dontShowEmptyOptionsPredicate))
                {
                    _predicates.Add(dontShowEmptyOptionsPredicate);
                }
                if (_showEmptyOption == true && _predicates.Contains(dontShowEmptyOptionsPredicate))
                {
                    _predicates.Remove(dontShowEmptyOptionsPredicate);
                }
  
                OnPropertyChanged();
                OnPropertyChanged(nameof(View));
            }
        }

        //---------------------------------------------------------------------
        // Properties
        //---------------------------------------------------------------------
        public PropertyValueViewModel GetOptiosForValue(object value)
        {
            return _options.First(o => o.Value.Equals(value));
        }

        public IEnumerable<object> SelectedValues => _options.Where(o => o.IsSelected).Select(o => o.Value);
        public IEnumerable<object> Values => _options.Select(o => o.Value);

        public ICollectionView View
        {
            get
            {
                _view.Filter = predicate;
                return _view;
            }
        }

        public string PropertyName => _propertyName;

    
        public string PropertyDisplayName
        {
            get
            {
                if (_propertyDisplayName == "")
                {
                    return _propertyName;
                }
                return _propertyDisplayName;
            }
            set { _propertyDisplayName = value; }
        }


        public void AddOption(object value, bool isSelected, int count)
        {
            _options.Add(new PropertyValueViewModel(value, count, isSelected));
        }

        public void AddOption(PropertyValueViewModel option)
        {
            _options.Add(option);
        }


        private bool _showOptions = true;
        public bool ShowOptions
        {
            get { return _showOptions; }
            set
            {
                if (_showOptions.Equals(value)) return;
                _showOptions = value;
                OnPropertyChanged();
            }
        }



        public bool ToogleAll
        {
            get { return _toogleAllIsChecked; }
            set
            {
                if (value)
                {
                    SelectAll();
                }
                else
                {
                    UnselectAll();
                }
                _toogleAllIsChecked = value;
                OnPropertyChanged();
            }
        }



        //---------------------------------------------------------------------
        // Commands
        //---------------------------------------------------------------------
        public ICommand SelectAllCommand { get; private set; }
        private void _selectAllExecuted(object o)
        {
            foreach (var option in _options)
            {
                option.IsSelected = true;
            }
        }
        private bool _selectAllCanExecute(object o)
        {
            return true;
        }

        public ICommand UnselectAllCommand { get; private set; }
        private void _unselectAllExecuted(object o)
        {
            UnselectAll();
        }
        private bool _unselectAllCanExecute(object o)
        {
            return true;
        }

        public ICommand ToogleAllCommand { get; private set; }
        private void _toogleAllExecuted(object o)
        {
            //_options.ToogleAllOptions();
            ToogleAllOptions();
        }
        private bool _toogleAllCanExecute(object o)
        {
            return true;
        }

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        //---------------------------------------------------------------------
        // IEnumerable
        //---------------------------------------------------------------------
        public IEnumerator<PropertyValueViewModel> GetEnumerator()
        {
            return _options.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}