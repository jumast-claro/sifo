﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SIFO.DesktopUI.CollectionView
{
    /// <summary>
    /// Interaction logic for FilterOptionsView.xaml
    /// </summary>
    public partial class FilterOptionsView : UserControl
    {


        public static readonly DependencyProperty AcceptCommandProperty = DependencyProperty.Register("AcceptCommand", typeof(ICommand), typeof(FilterOptionsView));
        public static readonly DependencyProperty GroupCommandProperty = DependencyProperty.Register("GroupCommand", typeof(ICommand), typeof(FilterOptionsView));
        public static readonly DependencyProperty CloseOnAcceptProperty = DependencyProperty.Register("CloseOnAccept", typeof(bool), typeof(FilterOptionsView), new PropertyMetadata(true));
        public static readonly DependencyProperty CloseOnGroupProperty = DependencyProperty.Register("CloseOnGroup", typeof(bool), typeof(FilterOptionsView), new PropertyMetadata(true));
        //public static readonly DependencyProperty ShowEmptyOptionProperty = DependencyProperty.Register("ShowEmptyOption", typeof(bool), typeof(FilterOptionsView), new PropertyMetadata(true));

        public FilterOptionsView()
        {
            InitializeComponent();
        }


        public ICommand AcceptCommand
        {
            get { return (ICommand) GetValue(AcceptCommandProperty); }
            set { SetValue(AcceptCommandProperty, value);}
        }

        public ICommand GroupCommand
        {
            get { return (ICommand)GetValue(GroupCommandProperty); }
            set { SetValue(GroupCommandProperty, value); }
        }

        public bool CloseOnAccept
        {
            get { return (bool)GetValue(CloseOnAcceptProperty); }
            set { SetValue(CloseOnAcceptProperty, value); }
        }

        public bool CloseOnGroup
        {
            get { return (bool)GetValue(CloseOnGroupProperty); }
            set { SetValue(CloseOnGroupProperty, value); }
        }

        //public bool ShowEmptyOption
        //{
        //    get { return (bool)GetValue(ShowEmptyOptionProperty); }
        //    set { SetValue(ShowEmptyOptionProperty, value); }
        //}

        private void AcceptButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (!CloseOnAccept) return;
            var window = Window.GetWindow(this);
            window?.Close();
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            var window = Window.GetWindow(this);
            window?.Close(); ;
        }

        private void Group_OnClick(object sender, RoutedEventArgs e)
        {
            var window = Window.GetWindow(this);
            window?.Close(); ; ;
        }

        public Action OnAccept { get; set; }
    }
}
