using System.ComponentModel;
using System.Threading;
using System.Windows.Data;
using System.Windows.Input;
using SIFO.DesktopUI.Framework;

namespace SIFO.DesktopUI.Utilities.CollectionView
{
    public class GroupService
    {

        private ICollectionView _view;

        public GroupService(ICollectionView view)
        {
            _view = view;
            GroupCommand = new RelayCommand(_groupExecuted, _groupCanExecute);
            RemoveAllGroupsCommand = new RelayCommand(_removeAllGroupsExecuted, _removeAllGroupsCanExecute);
        }

        public ICommand GroupCommand { get; private set; }
        private bool _groupCanExecute(object o)
        {
            return true;
        }
        private void _groupExecuted(object o)
        {
            var propertyName = o as string;
            groupByProperty(propertyName);
        }
        private void groupByProperty(string propertyName)
        {
            var propertyPath = propertyName + ".Value";
            if (isGroupingBy(propertyPath)) return;

            var groupDescription = new PropertyGroupDescription(propertyPath);
            _view.GroupDescriptions.Add(groupDescription);

        }

        private bool isGroupingBy(string propertyPath)
        {
            var groups = _view.GroupDescriptions;

            foreach (var group in groups)
            {
                var g = group as PropertyGroupDescription;
                if (g.PropertyName.Equals(propertyPath))
                {
                    return true;
                }
            }
            return false;
        }

        public ICommand RemoveAllGroupsCommand { get; private set; }

        private bool _removeAllGroupsCanExecute(object o)
        {
            return true;
        }

        private void _removeAllGroupsExecuted(object o)
        {
            ////for (long i = 0; i < 1000000; i++)
            ////{
            ////    var j = 1;


            ////}
            _view.GroupDescriptions.Clear();
            //Thread.Sleep(2000);
        }


    }
}