using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SIFO.DesktopUI.Utilities.Reflection
{
    /// <summary>
    /// Representa el conjunto de todos los valores posibles para una determinada propiedad.
    /// Expone el nombre de la propiedad, y el conjunto de todos los valores posibles.
    /// </summary>
    public class PropertyInformationViewModel
    {
        //private readonly List<object> _values = new List<object>(); 
        private readonly Dictionary<object, int> _values = new Dictionary<object, int>();

        /// <summary>
        /// Inicializa la instancia, almacenando el nombre de la propiedad inspeccionada.
        /// </summary>
        /// <param name="propertyName">NombreInbox de la propiedad inspeccionada</param>
        public PropertyInformationViewModel(string propertyName)
        {
            PropertyName = propertyName;
        }

        /// <summary>
        /// Nombre de la propiedad.
        /// </summary>
        public string PropertyName { get; }
        public string PropertyDisplayName { get; set; }
        public int DisplayIndex { get; set; } = 0;

        public void AddValueDistinct(object value)
        {
            var theValue = value;
            if (!_values.ContainsKey(theValue))
            {
                _values.Add(theValue, 1);
            }
            else
            {
                _values[theValue] += 1;
            }
        }

        /// <summary>
        /// Conjunto de todos los valores posibles.
        /// </summary>
        public List<object> DistinctValues => _values.Keys.ToList();

        public int GetNumberOfOcurrencesForValue(object value)
        {
            return _values[value];
        }

        public bool HasValue(object value)
        {
            return _values.ContainsKey(value);
        }
    }
}