using System.ComponentModel;
using System.Runtime.CompilerServices;
using SIFO.DesktopUI.Annotations;

namespace SIFO.DesktopUI.CollectionView
{
    public class PropertyValueViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Representa una opci�n para el valor de una determinada propiedad. 
        /// </summary>

        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        private readonly object _value;
        private bool _isSelected;
        private bool _isCurrentFilter;
        private int _count;
        private int _currentViewCount;

        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------
        public PropertyValueViewModel(object value, int count,  bool isSelected)
        {
            _value = value;
            _isSelected = isSelected;
            _isCurrentFilter = isSelected;
            _count = count;
        }

        //---------------------------------------------------------------------
        // Properties
        //---------------------------------------------------------------------

        /// <summary>
        /// Devuelve el valor de la opci�n.
        /// </summary>
        public object Value => _value;

        /// <summary>
        /// Devuelve true si la opci�n est� seleccionada, y false si no.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                if (value.Equals(_isSelected)) return;
                _isSelected = value;
                OnPropertyChanged();
            }
        }

        public bool IsCurrentFilter
        {
            get
            {
                return _isCurrentFilter;
            }
            set
            {
                if (value.Equals(_isCurrentFilter)) return;
                _isCurrentFilter = value;
                OnPropertyChanged();
            }
        }



        public int Count
        {
            get
            {
                return _count;
            }
            set
            {
                if(value == _count) return;
                _count = value;
                OnPropertyChanged();
            }
        }

        public int CurrentViewCount
        {
            get { return _currentViewCount; }
            set
            {
                if (value == _currentViewCount) return;
                _currentViewCount = value;
                OnPropertyChanged();
            }
        }

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}