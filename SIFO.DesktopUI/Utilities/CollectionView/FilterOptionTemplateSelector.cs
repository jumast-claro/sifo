﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SIFO.DesktopUI.CollectionView
{
    public class FilterOptionTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            FrameworkElement element = container as FrameworkElement;
            if (element != null && item != null)
            {
                if (item is PropertyValueViewModel)
                {
                    var i = item as PropertyValueViewModel;
                    var type = i.Value.GetType();
                    if (type == typeof (string))
                    {
                        return element.FindResource("StringFilterOptionDataTemplate") as DataTemplate;
                    }
                    else if(type == typeof(bool))
                    {
                       return element.FindResource("BoolFilterOptionDataTemplate") as DataTemplate;
                    }
                    else if (type == typeof(DateTime))
                    {
                        return element.FindResource("DateTimeFilterOptionDataTemplate") as DataTemplate;
                    }
                    else
                    {
                        return element.FindResource("StringFilterOptionDataTemplate") as DataTemplate;
                    }

                }
            }
            return base.SelectTemplate(item, container);
        }
    }
}
