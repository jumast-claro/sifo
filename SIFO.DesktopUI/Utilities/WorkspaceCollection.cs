﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace SIFO.DesktopUI.Utilities
{
    public class WorkspaceCollection : INotifyPropertyChanged
    {
        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        private readonly ObservableCollection<object> _workspaces = new ObservableCollection<object>();
        private object _selectedWorkspace;
        private readonly IDataTemplateSelector _templateSelector;
        private object _defaultWorkspace;


        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------
        public WorkspaceCollection(IDataTemplateSelector workspaceTemplateSelector)
        {
            _templateSelector = workspaceTemplateSelector;
        }

        //---------------------------------------------------------------------
        // Propiedades
        //---------------------------------------------------------------------
        public ObservableCollection<object> WorkspacesSource => _workspaces;

        public object SelectedWorkspace
        {
            get { return _selectedWorkspace; }
            set
            {
                if (_selectedWorkspace == value) return;
                _selectedWorkspace = value;
                OnPropertyChanged();
            }
        }

        public object DefaultWorkspace
        {
            get { return _defaultWorkspace;}
            set { _defaultWorkspace = value; }

        }

        public IDataTemplateSelector TemplateSelector => _templateSelector;


        //---------------------------------------------------------------------
        // Methods
        //---------------------------------------------------------------------

        /// <summary>
        /// Agrega el objeto pasado por parámetro al conjunto de espacios de trabajo, y lo establece como el espacio de
        /// trabajo actual. 
        /// Pre: Si el parámetro es null, lanza una excepción de tipo ArgumentException.           
        /// </summary>
        /// <param name="workspace"></param>
        public void AddWorkspace(object workspace)
        {
            if (workspace == null)
            {
                throw new ArgumentException();
            }
            _workspaces.Add(workspace);
            SelectedWorkspace = workspace;
        }

        /// <summary>
        /// Elimina el objeto pasado por parámetro del conjunto de espacios de trabajo, y establece al espacio de trabajo 
        /// por defecto como el espacio de trabajo actual. 
        /// Pre: Si el parámetro es null, lanza una excepción de tipo ArgumentException. 
        /// Pre: Si el parámetro no está incluido en el conjunto de espacios de trabajo, lanza una excepción de tipo ArgumentException. 
        /// Post: SelectedWorkspace == DefaultWorkspace.
        /// </summary>
        /// <param name="workspace"></param>
        public void RemoveWorkspace(object workspace)
        {
            if (workspace == null || !_workspaces.Contains(workspace))
            {
                throw new ArgumentException();
            }
            _workspaces.Remove(workspace);
            SelectedWorkspace = _defaultWorkspace;
        }

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}