﻿using System;

namespace SIFO.DesktopUI.Utilities.Reflection
{
    [AttributeUsage(AttributeTargets.Property)]
    public class IncludePropertyAttribute : Attribute
    {
        public bool Include { get; }
        public int Position { get; }
        public string DisplayName { get; }



        public IncludePropertyAttribute(string displayName)
        {
            DisplayName = displayName;
            Position = 0;
            Include = true;
        }

        public IncludePropertyAttribute() : this("") { }

    }
}