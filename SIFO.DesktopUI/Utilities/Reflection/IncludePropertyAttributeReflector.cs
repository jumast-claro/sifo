using System;
using System.Linq;
using System.Reflection;
using SIFO.DesktopUI.Utilities.Properties;

namespace SIFO.DesktopUI.Utilities.Reflection
{
    public class IncludePropertyAttributeReflector
    {
        public void Configure(object result)
        {
            var fields = result.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic);

            var field = fields.FirstOrDefault(info => info.FieldType.Name == typeof (IPropertyCollection).Name);
            if (field == null) throw new NullReferenceException();

            var member = field.GetValue(result).GetType().GetMembers().FirstOrDefault(info => info.Name.Contains("AddProperty"));
            if (member == null) throw new NullReferenceException();

            var method = member as MethodInfo;
            var properties = result.GetType().GetProperties();

            foreach (var property in properties)
            {
                var att = Attribute.GetCustomAttribute(property, typeof (IncludePropertyAttribute)) as IncludePropertyAttribute;


                if (att?.Include == true)
                {
                    var param = property.GetValue(result);
                    var types = param.GetType().GetGenericArguments();
                    var type = types[0];

                    var bindable = param as IBindableProperty;
                    if (att.DisplayName != "")
                    {
                        bindable.DisplayName = att.DisplayName;
                    }
                    else
                    {
                        string propertyName = property.Name;
                        int index = propertyName.IndexOf("Property");
                        bindable.DisplayName = property.Name.Remove(index);
                    }


                    var args = method.GetGenericArguments();
                    var generic = method.MakeGenericMethod(type);
                    generic.Invoke(field.GetValue(result), new[] {param, 0});
                }
            }
        }
    }
}