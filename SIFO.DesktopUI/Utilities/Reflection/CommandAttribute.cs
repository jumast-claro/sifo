﻿using System;

namespace SIFO.DesktopUI.Utilities.Reflection
{
    public class CommandAttribute : Attribute
    {
        public CommandAttribute(string executeMethodName, string canExecuteMethodName)
        {
            ExecuteMethodName = executeMethodName;
            CanExecuteMethodName = canExecuteMethodName;
        }

        public CommandAttribute() : this("", "")
        {
            
        }

        public string ExecuteMethodName { get; private set; }
        public string CanExecuteMethodName { get; private set; }
    }
}