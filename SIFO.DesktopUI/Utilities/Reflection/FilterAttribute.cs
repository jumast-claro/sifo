﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIFO.DesktopUI.Utilities.Reflection
{
    public class FilterAttribute : Attribute
    {
        public string PropertyDisplayName { get; set; }
        public int DisplayIndex { get; set; } = 0;

        public FilterAttribute(string propertyDisplayName = "", int displayIndex = 0)
        {
            PropertyDisplayName = propertyDisplayName;
            DisplayIndex = displayIndex;
        }
    }
}
