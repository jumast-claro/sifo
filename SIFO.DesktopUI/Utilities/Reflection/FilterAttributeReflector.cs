﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SIFO.DesktopUI.TareasCerradas;
using SIFO.DesktopUI.Utilities.CollectionView;

namespace SIFO.DesktopUI.Utilities.Reflection
{
    public class FilterReflectionResult : IEnumerable<PropertyInformationViewModel>
    {
        private readonly Dictionary<string, PropertyInformationViewModel> _dic = new Dictionary<string, PropertyInformationViewModel>();

        public PropertyInformationViewModel this[string propertyName]
        {
            get { return _dic[propertyName]; }
        }

        public bool HasProperty(string propertyName)
        {
            return _dic.ContainsKey(propertyName);
        }

        public void Add(PropertyInformationViewModel info)
        {
            if (_dic.ContainsKey(info.PropertyName))
            {
                throw new ArgumentException();
            }

            _dic.Add(info.PropertyName, info);
        }

        //public List<PropertyInformationViewModel> Temp => _dic.Values.ToList();
        public IEnumerator<PropertyInformationViewModel> GetEnumerator()
        {
            return _dic.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class FilterAttributeReflector
    {
        private readonly Type _type;

        public FilterAttributeReflector(Type type)
        {
            _type = type;
        }


        public IEnumerable<PropertyInformationViewModel> GetDistinctValues(IEnumerable view)
        {
            var result = new FilterReflectionResult();
            var properties = getPropertiesWithAttribute();

            foreach (var property in properties)
            {
                var attribute = property.GetAttributeInstance<FilterAttribute>();
                    var propertyValues = new PropertyInformationViewModel(property.PropertyName)
                    {
                        PropertyDisplayName = attribute.PropertyDisplayName,
                        DisplayIndex = attribute.DisplayIndex
                    };
                    result.Add(propertyValues);
                processItems(property, view, result);
            }

            return result;
        }

        private void processItems(IPropertyInfoWrapper property, IEnumerable view, FilterReflectionResult result)
        {
            var e = view.GetEnumerator();
            while (e.MoveNext())
            {
                //if(property.PropertyName == "TareaIdProperty") continue;
                var item = e.Current;

                var propInstance = property.WrappedPropertyInfo.GetValue(item);
                var prop = propInstance as IBindableProperty;
                //var x = property.GetValue(item, "Value");
                var x = prop.Value;
                result[property.PropertyName].AddValueDistinct(x);
            }
        }

        private IEnumerable<IPropertyInfoWrapper> getPropertiesWithAttribute()
        {
            var result = new List<IPropertyInfoWrapper>();
            var properties = _type.GetProperties();
            foreach (var propertyInfo in properties)
            {
                var property = new PropertyInfoWrapper(propertyInfo);
                if (property.HasAttribute<FilterAttribute>())
                {
                    result.Add(property);
                }
            }
            return result;
        }
    }

    public class PropertyInfoWrapper : IPropertyInfoWrapper
    {
        private readonly PropertyInfo _propertyInfo;

        public PropertyInfoWrapper(PropertyInfo propertyInfo)
        {
            _propertyInfo = propertyInfo;
        }

        public object GetValue(object instance, string path)
        {
            var property = _propertyInfo.GetValue(instance);
            var propertyValue = property.GetType().GetProperties().First(p => p.Name == path).GetValue(property);
            return propertyValue;
        }

        public TAttribute GetAttributeInstance<TAttribute>() where TAttribute : Attribute
        {
            if (!_propertyInfo.IsDefined(typeof(TAttribute)))
            {
                throw new ArgumentException();
            }

            return _propertyInfo.GetCustomAttribute<TAttribute>();
        }

        public bool HasAttribute<TAttribute>()
        {
            return _propertyInfo.IsDefined(typeof(TAttribute));
        }

        public PropertyInfo WrappedPropertyInfo => _propertyInfo;

        public string PropertyName => _propertyInfo.Name;


    }

    public interface IPropertyInfoWrapper
    {
        string PropertyName { get; }

        TAttribute GetAttributeInstance<TAttribute>() where TAttribute : Attribute;
        object GetValue(object instance, string path);
        bool HasAttribute<TAttribute>();
        PropertyInfo WrappedPropertyInfo { get; }
    }


}
