﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using SIFO.DesktopUI.Framework;
using SIFO.DesktopUI.Utilities.Properties;

namespace SIFO.DesktopUI.Utilities.Reflection
{

    public class CommandNameParser
    {
        private readonly string _commandName;

        public CommandNameParser(string commandName)
        {
            _commandName = commandName;
        }

        public string GetExecuteMethodName()
        {
            var name = _commandName;
            name = name.Remove(name.IndexOf("Command"));

            var first = name[0].ToString().ToLower();
            var other = name.Remove(0, 1);

            name = name.Remove(0).Insert(0, "_" + first + other);

            name = name + "Executed";

            return name;
        }

        public string GetCanExecuteMethodName()
        {
            var name = _commandName;
            name = name.Remove(name.IndexOf("Command"));

            var first = name[0].ToString().ToLower();
            var other = name.Remove(0, 1);

            name = name.Remove(0).Insert(0, "_" + first + other);
            ;

            name = name + "CanExecute";

            return name;
        }
    }

    public class CommandReflector
    {
        public void Configure(object target)
        {
            var properties = target.GetType().GetProperties();
            var commandProperties = properties.Where(p => p.PropertyType == typeof(ICommand));
            foreach (var property in commandProperties)
            {
                var commandName = property.Name;
                var executeMethodName = "";
                var canExecuteMethodName = "";


                var commandAttribute = Attribute.GetCustomAttribute(property, typeof(CommandAttribute)) as CommandAttribute;
                if (commandAttribute != null)
                {
                    executeMethodName = commandAttribute.ExecuteMethodName;
                    canExecuteMethodName = commandAttribute.CanExecuteMethodName;
                }


                if (executeMethodName == "" || canExecuteMethodName == "")
                {
                    var parser = new CommandNameParser(commandName);

                    if (executeMethodName == "")
                    {
                        executeMethodName = parser.GetExecuteMethodName();
                    }
                    if (canExecuteMethodName == "")
                    {
                        canExecuteMethodName = parser.GetCanExecuteMethodName();
                    }



                }

                var executeMethod = GetMethodInfo(target, executeMethodName);
                var canExecuteMethod = GetMethodInfo(target, canExecuteMethodName);

                var execute = CreateExecuteMethod(target, executeMethod);
                var canExecute = CreateCanExecuteMethod(target, canExecuteMethod);

                ICommand command = createCommand(execute, canExecute);
                property.SetValue(target, command);
            }
        }

        private MethodInfo GetMethodInfo(object target, string methodName)
        {
            return target.GetType().GetMethod(methodName, BindingFlags.Instance | BindingFlags.NonPublic);
        }

        private Action<object> CreateExecuteMethod(object target, MethodInfo executeMethodInfo)
        {
            return Delegate.CreateDelegate(typeof(Action<object>), target, executeMethodInfo) as Action<object>;
        }

        private Predicate<object> CreateCanExecuteMethod(object target, MethodInfo canExecuteMethodInfo)
        {
            return Delegate.CreateDelegate(typeof(Predicate<object>), target, canExecuteMethodInfo) as Predicate<object>;
        }

        private ICommand createCommand(Action<object> execute, Predicate<object> canExecute)
        {
            return typeof(RelayCommand).GetConstructor(new Type[] { typeof(Action<object>), typeof(Predicate<object>) }).Invoke(new object[] { execute, canExecute }) as RelayCommand;
        }
    }

    public class CommandAttributeReflector
    {
        public void Configure(object target)
        {
            var properties = target.GetType().GetProperties();

            foreach (var property in properties)
            {
                var att = Attribute.GetCustomAttribute(property, typeof(CommandAttribute)) as CommandAttribute;
                if (att != null)
                {
                    var executeMethodName = att.ExecuteMethodName;
                    var canExecuteMethodName = att.CanExecuteMethodName;
                    var commandName = property.Name;


                    if (executeMethodName == "")
                    {
                        var name = commandName;
                        name = name.Remove(name.IndexOf("Command"));

                        var first = name[0].ToString().ToLower();
                        var other = name.Remove(0, 1);

                        name = name.Remove(0).Insert(0, "_" + first + other);

                        name = name + "Executed";

                        executeMethodName = name;

                    }
                    if (canExecuteMethodName == "")
                    {
                        var name = commandName;
                        name = name.Remove(name.IndexOf("Command"));

                        var first = name[0].ToString().ToLower();
                        var other = name.Remove(0, 1);

                        name = name.Remove(0).Insert(0, "_" + first + other); ;

                        name = name + "CanExecute";

                        canExecuteMethodName = name;
                    }


                    var executeMethod = target.GetType().GetMethod(executeMethodName, BindingFlags.Instance | BindingFlags.NonPublic);
                    var canExecuteMethod = target.GetType().GetMethod(canExecuteMethodName, BindingFlags.Instance | BindingFlags.NonPublic);

                    var execute = Delegate.CreateDelegate(typeof(Action<object>), target, executeMethod) as Action<object>;
                    var canExecute = Delegate.CreateDelegate(typeof(Predicate<object>), target, canExecuteMethod) as Predicate<object>;

                    ICommand command = typeof(RelayCommand).GetConstructor(new Type[] { typeof(Action<object>), typeof(Predicate<object>) }).Invoke(new object[] { execute, canExecute }) as RelayCommand;
                    property.SetValue(target, command);

                }
            }
        }
    }
}
