using System.Windows.Controls;

namespace SIFO.DesktopUI.Utilities
{
    public class TabbedWorkspaceCollection : WorkspaceCollection
    {
        public TabbedWorkspaceCollection(IDataTemplateSelector workspaceTemplateSelector) : base(workspaceTemplateSelector)
        {
        }
    }
}