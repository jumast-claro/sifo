﻿namespace SIFO.DesktopUI.Utilities.Validation
{
    public interface IValidationResult
    {
        bool IsValid { get; }
        string ErrorMessage { get; }
    }
}