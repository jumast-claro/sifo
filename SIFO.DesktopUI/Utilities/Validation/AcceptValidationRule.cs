﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SIFO.DesktopUI.Utilities.Validation
{
    public class RejectValidationRule<T> : IValidationRule<T>
    {
        private readonly Func<T, bool> _rejectFunc;
        private readonly string _errorMessage;

        public RejectValidationRule(Func<T, bool> rejectFunc, string errorMessage)
        {
            _rejectFunc = rejectFunc;
            _errorMessage = errorMessage;
        }


        public ValidationResult Validate(T value)
        {
            if (_rejectFunc(value))
            {
                return new ValidationResult(false, _errorMessage);
            }
            return new ValidationResult(true);
        }
    }

    public class AcceptValidationRule<T> : IValidationRule<T>
    {
        private readonly Func<T, bool> _accepts;
        private readonly string _errorMessage;

        public AcceptValidationRule(Func<T, bool> accepts, string errorMessage)
        {
            _accepts = accepts;
            _errorMessage = errorMessage;
        }


        public ValidationResult Validate(T value)
        {
            if (!_accepts(value))
            {
                return new ValidationResult(false, _errorMessage);
            }
            return new ValidationResult(true);
        }
    }

    public class RangeValidationRule<T> : IValidationRule<T>
    {
        private readonly List<T> _validValues = new List<T>();

        public RangeValidationRule(params T[] validValues)
        {
            foreach (var value in validValues)
            {
                _validValues.Add(value);
            }
        }

        public ValidationResult Validate(T value)
        {
            if (_validValues.Contains(value))
            {
                return new ValidationResult(true);
            }

            StringBuilder sb = new StringBuilder();
            //sb.Append('[');
            foreach (var validValue in _validValues)
            {
                sb.Append(validValue);
                sb.Append(", o ");
            }
            //sb.Length--;
            sb.Length = sb.Length - 4;
            //sb.Append(']');
            return new ValidationResult(false, $"Valor no permitido. Se esperaba {sb}.");
        }
    }
}