﻿using System;

namespace SIFO.DesktopUI.Utilities.Validation
{
    public class ConvertionResult
    {
        private readonly bool _hasValue = false;
        private readonly int _parsedValue = 0;

        public ConvertionResult(bool hasValue, int parsedValue)
        {
            _hasValue = hasValue;
            _parsedValue = parsedValue;
        }


        public bool HasValue => _hasValue;

        public int Value
        {
            get
            {
                if (!_hasValue)
                {
                    throw new InvalidOperationException();
                }
                return _parsedValue;
            }
        }
    }

    public class ConvertionResult<T>
    {
        private readonly bool _succes = false;
        private readonly T _parsedValue = default(T);
        private readonly string _errorMeesage;

        public ConvertionResult(bool succes, T parsedValue)
        {
            _succes = succes;
            _parsedValue = parsedValue;
            //_errorMeesage = errorMessage;
        }


        public bool Succes => _succes;

        public string ErrorMessage
        {
            get
            {
                if (_succes)
                {
                    throw  new InvalidOperationException();
                }
                return _errorMeesage;
            }
        }

        public T Value
        {
            get
            {
                if (!_succes)
                {
                    throw new InvalidOperationException();
                }
                return _parsedValue;
            }
        }

    }
}