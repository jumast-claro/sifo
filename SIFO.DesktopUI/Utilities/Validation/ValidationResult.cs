using System;

namespace SIFO.DesktopUI.Utilities.Validation
{
    public class ValidationResult : IValidationResult
    {
        readonly bool _isValid;
        readonly string _errorMessage;

        public ValidationResult(bool isValid, string errorMessage = "")
        {
            _isValid = isValid;
            _errorMessage = errorMessage;
        }



        public bool IsValid
        {
            get { return _isValid; }
        }

        public string ErrorMessage
        {
            get
            {
                if (_isValid)
                {
                    throw new InvalidOperationException();
                }
                return _errorMessage;
            }
        }
    }
}