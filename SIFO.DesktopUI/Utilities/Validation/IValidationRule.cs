﻿namespace SIFO.DesktopUI.Utilities.Validation
{
    public interface IValidationRule<T>
    {
        ValidationResult Validate(T value);
    }
}