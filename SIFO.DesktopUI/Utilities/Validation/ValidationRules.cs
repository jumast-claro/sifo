﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace SIFO.DesktopUI.Utilities.Validation
{
    public class ValidationRules<T> : IEnumerable<IValidationRule<T>>
    {
        private readonly List<IValidationRule<T>> _validationRules = new List<IValidationRule<T>>();

        public ValidationRules()
        {
        }


        public void AddRule(Func<T, bool> acceptFunc, string errorMessage)
        {
            _validationRules.Add(new AcceptValidationRule<T>(acceptFunc, errorMessage));
        }

        public void AddValidationRule(IValidationRule<T> validationRule)
        {
            _validationRules.Add(validationRule);
        }

        public void RemoveAllRules()
        {
            _validationRules.Clear();
        }

        public  IValidationResult Validate(T value)
        {
            foreach (var rule in _validationRules)
            {
                var validation = rule.Validate(value);
                if (!validation.IsValid)
                {
                    return new ValidationResult(false, validation.ErrorMessage);
                }
               
            }
            return new ValidationResult(true);
        }


        public IEnumerator<IValidationRule<T>> GetEnumerator()
        {
            return _validationRules.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
