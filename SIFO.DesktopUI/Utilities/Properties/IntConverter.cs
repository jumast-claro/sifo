using SIFO.DesktopUI.Utilities.Validation;

namespace SIFO.DesktopUI.Utilities.Properties
{
    public class IntConverter : IConverter<int>
    {

        public ConvertionResult<int> Convert(string stringValue)
        {
            if (stringValue == "")
            {
                return new ConvertionResult<int>(true, 0);
            }



            int intValue;
            var canParseToInt = int.TryParse(stringValue, out intValue);
            return new ConvertionResult<int>(canParseToInt, intValue);
        }

        public string ToBindingString(int value)
        {
            return value.ToString();
        }
    }
}