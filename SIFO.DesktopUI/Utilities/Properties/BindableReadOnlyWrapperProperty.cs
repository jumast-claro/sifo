﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIFO.DesktopUI.Utilities.Properties
{
    public class BindableReadOnlyWrapperProperty<T>: ViewModelPropertyBase<T>, IBindableProperty<T>
    {
        //------------------------------------------------------ 
        //  Fields
        //------------------------------------------------------
        private readonly Func<string> _getValue;


        //------------------------------------------------------ 
        //  Constructors
        //------------------------------------------------------
        public BindableReadOnlyWrapperProperty(Func<string> getValue)
        {
            this._getValue = getValue;
            IsReadOnly = true;

        }

        //------------------------------------------------------ 
        //  Properties
        //------------------------------------------------------
        public string BindingString
        {
            get { return _getValue(); }
            set
            {
                throw  new InvalidOperationException();
            }
        }

        public string DisplayString => _getValue();


        public override void RaisePropertyChanged()
        {
            OnPropertyChanged(nameof(BindingString));
            OnPropertyChanged(nameof(Value));
            OnPropertyChanged(nameof(DisplayString));
        }

        public override T Value { get; set; }

        public List<IBindableProperty> RelatedProperties { get; } = new List<IBindableProperty>();
        object IBindableProperty.Value => Value;

        public bool IsReadOnly { get; private set; } = false;

        public string DisplayName { get; set; }

    }
}
