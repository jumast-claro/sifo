﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace SIFO.DesktopUI.Utilities
{
    public class TrackableViewModelProperty<T> : IBindableProperty<T>, ITrackableViewModelProperty, INotifyPropertyChanged
    {
        //------------------------------------------------------ 
        //  Fields
        //------------------------------------------------------
        protected T _value;
        private T _lastValue;

        //------------------------------------------------------ 
        //  Constructors
        //------------------------------------------------------
        public TrackableViewModelProperty(T initialValue)
        {
            _value = initialValue;
            _lastValue = initialValue;
            DisplayName = "";
        }

        public TrackableViewModelProperty(T initialValue, bool isReadOnly) : this(initialValue)
        {
            IsReadOnly = isReadOnly;
        }

        //------------------------------------------------------ 
        //  Properties
        //------------------------------------------------------
        public virtual T Value
        {
            get { return _value; }
            set
            {
                if (value.Equals(_value)) return;
                _value = value;
                OnPropertyChanged();
                foreach (var p in RelatedProperties)
                {
                    p.RaisePropertyChanged();
                }
            }
        }


        public T LastValue => _lastValue;


        public bool IsReadOnly { get; }

        public string DisplayName { get; set; }

        //------------------------------------------------------ 
        //  Methods
        //------------------------------------------------------
        public void SaveValue()
        {
            _lastValue = Value;
            OnPropertyChanged("LastValue");
            foreach (var p in RelatedProperties)
            {
                p.RaisePropertyChanged();
            }
        }


        //------------------------------------------------------ 
        //  ITrackableViewModelProperty
        //------------------------------------------------------
        public void RestoreOriginalValue()
        {
            Value = _lastValue;
        }

        //------------------------------------------------------ 
        //  INotifyPropertyChanged
        //------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged()
        {
            OnPropertyChanged(nameof(Value));
        }

        public List<IBindableProperty> RelatedProperties { get; }
        object IBindableProperty.Value => Value;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
