using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;

namespace SIFO.DesktopUI.Utilities.Properties
{
    public class ReadOnlyProperty<T> : ViewModelPropertyBase<T>, IBindableProperty<T>
    {

        public List<IBindableProperty> RelatedProperties { get; } = new List<IBindableProperty>();
        object IBindableProperty.Value => Value;

        private readonly T _value;
        private readonly string _displayString;

        public ReadOnlyProperty(T value, string displayString)
        {
            _value = value;
            _displayString = displayString;
        }

        public ReadOnlyProperty(T value) : this(value, value.ToString())
        {
            
        }

        public override T Value
        {
            get { return _value; }
            set { throw new InvalidOperationException(); }
        }

        public string DisplayString
        {
            get { return _displayString; }
            set
            {
                throw new InvalidOperationException();
            }
        }

        public string DisplayName { get; set; } = "";
        public bool IsReadOnly => true;
    }

    [TypeConverter(typeof(ReadOnlyStringPropertyConverter))]
    public interface IBindableStringProperty
    {
        string Value { get; set; }
    }

    [TypeConverter(typeof(ReadOnlyStringPropertyConverter))]
    public class ReadOnlyStringProperty : ReadOnlyProperty<string>, IBindableStringProperty
    {
        public ReadOnlyStringProperty(string value, string displayString) : base(value, displayString)
        {
        }

        public ReadOnlyStringProperty(string value) : base(value)
        {
        }
    }

    public class ReadOnlyStringPropertyConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            return base.ConvertFrom(context, culture, value);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }


}