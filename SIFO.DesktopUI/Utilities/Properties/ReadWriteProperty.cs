﻿using System;
using System.Collections.Generic;

namespace SIFO.DesktopUI.Utilities
{
    public class ReadWriteProperty<T> : ViewModelPropertyBase<T>, IBindableProperty<T>
    {


        private T _value;
        //private readonly string _displayString;

        public ReadWriteProperty(T initialVvalue)
        {
            _value = initialVvalue;
        }



        public override T Value
        {
            get { return _value; }
            set
            {
                if (value!= null && value.Equals(_value))
                {
                    return;
                }
                _value = value;
                OnPropertyChanged();
                foreach (var p in RelatedProperties)
                {
                    p.RaisePropertyChanged();
                }
            }
        }

        public string DisplayString
        {
            get { return _value.ToString(); }
            set
            {
                throw new InvalidOperationException();
            }
        }

        public string DisplayName { get; set; } = "";
        public bool IsReadOnly => true;
        public List<IBindableProperty> RelatedProperties { get; } = new List<IBindableProperty>();
        object IBindableProperty.Value => Value;

        public Action ValueChangedCallBack { get; set; }
    }
}