﻿using System;
using System.Collections.Generic;
using SIFO.DesktopUI.Utilities.Converters;

namespace SIFO.DesktopUI.Utilities.Properties
{
    public class WrappingViewModelProperty<T> : ViewModelPropertyBase<T>, IBindableProperty<T>
    {
        //------------------------------------------------------ 
        //  Fields
        //------------------------------------------------------
        private readonly Action<T> _setValue;
        private readonly Func<T> _getValue;
        private readonly Func<T, string> _getDisplayString;


        //------------------------------------------------------ 
        //  Constructors
        //------------------------------------------------------
        public WrappingViewModelProperty(Action<T> setValue, Func<T> getValue, Func<T, string> getDisplayString)
        {
            this._setValue = setValue;
            this._getValue = getValue;
            this._getDisplayString = getDisplayString;

        }

        public WrappingViewModelProperty(Action<T> setValue, Func<T> getValue) : this(setValue, getValue, value => value.ToString())
        {
            
        }
        public WrappingViewModelProperty(Func<T> getValue) : this(value => { throw new NotImplementedException(); }, getValue, value => value.ToString())
        {
            IsReadOnly = true;
        }

        public WrappingViewModelProperty(Func<T> getValue, Func<T, string> getDisplayString) : this(value => { throw new NotImplementedException();}, getValue, getDisplayString)
        {
            
        }



        //------------------------------------------------------ 
        //  Properties
        //------------------------------------------------------
        public override T Value
        {
            get
            {
                return this._getValue();
            }
            set
            {
                if (value.Equals(Value)) return;
                this._setValue(value);
                base.RaisePropertyChanged();
            }
        }

        public string DisplayString => _getDisplayString(Value);


        public override void RaisePropertyChanged()
        {
            OnPropertyChanged(nameof(Value));
            OnPropertyChanged(nameof(DisplayString));
            foreach (var p in RelatedProperties)
            {
                p.RaisePropertyChanged();
            }
        }

        public List<IBindableProperty> RelatedProperties { get; } = new List<IBindableProperty>();
        object IBindableProperty.Value => Value;

        public bool IsReadOnly { get; private set; } = false;

        public string DisplayName { get; set; }

        public BaseConverter Converter
        {
            get
            {
                return new FechaDeInstalacionConverter();
            }
        }

    }
}


