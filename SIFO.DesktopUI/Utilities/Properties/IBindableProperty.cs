﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using SIFO.DesktopUI.Utilities.Properties;

namespace SIFO.DesktopUI.Utilities
{
    public interface IBindableProperty
    {
        string DisplayName { get; set; }
        bool IsReadOnly { get; }
        event PropertyChangedEventHandler PropertyChanged;
        void RaisePropertyChanged();
        
        List<IBindableProperty> RelatedProperties { get; }

        object Value { get; }

    }

    public interface IBindableProperty<TValue> : IBindableProperty
    {
        TValue Value { get; set; }
        //event PropertyChangedEventHandler PropertyChanged;
        void RaisePropertyChanged();
    }
}
