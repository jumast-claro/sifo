﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SIFO.DesktopUI.Utilities.Properties
{
    /// <summary>
    /// Interaction logic for DateTimePropertyView.xaml
    /// </summary>
    public partial class DateTimePropertyView : UserControl
    {
        public static DependencyProperty DescriptionProperty = DependencyProperty.Register("Description", typeof(string), typeof(PropertyTextView));
        public static DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(string), typeof(PropertyTextView));

        public DateTimePropertyView()
        {
            InitializeComponent();
        }

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public string Value
        {
            get { return (string)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
    }
}
