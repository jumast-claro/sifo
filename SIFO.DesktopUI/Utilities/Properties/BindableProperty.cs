using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using System.Windows.Input;
using SIFO.DesktopUI.Framework;
using SIFO.DesktopUI.Utilities.Validation;

namespace SIFO.DesktopUI.Utilities.Properties
{
    public class DefaultLongConverter : IConverter<long>
    {
        public ConvertionResult<long> Convert(string stringValue)
        {
            try
            {
                return new ConvertionResult<long>(true, long.Parse(stringValue));
            }
            catch (Exception)
            {
                
               return new ConvertionResult<long>(false, long.MinValue);
            }
        }

        public string ToBindingString(long value)
        {
            return value.ToString();
        }
    }

    public class BindablePropertyFactory
    {
        public BindableProperty<T> CreateBindableProperty<T>(T initialValue, IConverter<T> converter)
        {
            return new BindableProperty<T>(initialValue, converter);
        }

        public BindableProperty<T> CreateBindableProperty<T>(T initialValue, Func<string, T> converterFunc, Func<T, string> toBindingString)
        {
            return new BindableProperty<T>(initialValue, converterFunc, toBindingString);
        }

        public BindableProperty<string> CreateBindableProperty(string initialValue, bool isReadOnly)
        {
            var prop = new BindableProperty<string>(initialValue, s => s, s => s) {IsReadOnly = isReadOnly};
            return prop;
        }

        public BindableProperty<long> CreateBindableProperty(long initialValue, bool isReadOnly)
        {
            var prop = new BindableProperty<long>(initialValue, new DefaultLongConverter()) {IsReadOnly = isReadOnly};
            return prop;
        }
    }

    public class FuncConverter<T> : IConverter<T>
    {
        private Func<string, T> _convert;
        private Func<T, string> _toBindingString;

        public FuncConverter(Func<string, T> converter, Func<T, string> toBindingString)
        {
            _convert = converter;
            _toBindingString = toBindingString;
        }

        public ConvertionResult<T> Convert(string stringValue)
        {
            try
            {
                return new ConvertionResult<T>(true, _convert(stringValue));
            }
            catch (Exception)
            {
                
                return new ConvertionResult<T>(false, default(T));
            }
        }

        public string ToBindingString(T value)
        {
            return _toBindingString(value);
        }
    }

    public class BindableProperty<T> : INotifyPropertyChanged, INotifyDataErrorInfo, ITrackableViewModelProperty, IValidatable<T>, IBindableProperty, IBindableProperty<T>
    {
        //------------------------------------------------------ 
        //  Fields
        //------------------------------------------------------
        private readonly Stack<string> _textValues = new Stack<string>();
        private readonly T _initialValue;
        private string _bindingString;
        private T _value;
        private readonly IConverter<T> _converter;

        private readonly List<string> _errors = new List<string>();
        private readonly ValidationRules<T> _validationRules = new ValidationRules<T>();

        //------------------------------------------------------ 
        //  Constructors
        //------------------------------------------------------
        public BindableProperty(T initialValue, IConverter<T> converter)
        {
            IsReadOnly = false;
            _initialValue = initialValue;
            _value = initialValue;
            _converter = converter;
            _bindingString = _converter.ToBindingString(_initialValue);

            _textValues.Push(BindingString);

            UndoCommand = new RelayCommand(_undoExecuted, _undoCanExecute);
            EvalFormulaCommand = new RelayCommand(_evalFormulaExecuted, _evalFormulaCanExecute);
        }

        public BindableProperty(T initialValue, Func<string, T> converterFunc, Func<T, string> toBindingString) : this(initialValue, new FuncConverter<T>(converterFunc, toBindingString))
        {
        }

        private void clearErrors()
        {
            _errors.Clear();
        }

        private void setError(string error)
        {
            _errors.Clear();
            _errors.Add(error);
        }

        private void notifyChange()
        {
            OnPropertyChanged(nameof(BindingString));
            OnPropertyChanged(nameof(Value));
            OnErrorChanged();
        }


        //------------------------------------------------------ 
        //  Properties
        //------------------------------------------------------
        public string DisplayName { get; set; }
        public bool IsReadOnly { get; set; }

        public string BindingString
        {
            get { return _bindingString; }
            set
            {
                if (value.Equals(_bindingString))
                {
                    return;
                };

                _bindingString = value;
                _textValues.Push(value);
            
    

                var convertionResult = _converter.Convert(value);
                if (!convertionResult.Succes)
                {
                    setError("No se reconoce el formato del valor ingresado.");
                }
                else
                {
                    var convertedValue = convertionResult.Value;
                    var validationResult = _validationRules.Validate(convertedValue);
                    if (validationResult.IsValid)
                    {
                        _value = convertedValue;
                        clearErrors();
                    }
                    else
                    {
                        setError(validationResult.ErrorMessage);
                    }
                }
                notifyChange();
            }
        }

        public T Value
        {
            get { return _value; }
            set
            {
                if (value.Equals(_value)) return;
                _value = value;
                BindingString = _converter.ToBindingString(value);
                notifyChange();
            }
        }

        public void ValidationDependsOn(BindableProperty<T> dependency)
        {
            dependency.PropertyChanged += (sender, args) => ApplyValidationRules();

        }


        public void ApplyValidationRules()
        {
            var convertionResult = _converter.Convert(BindingString);
            if (!convertionResult.Succes)
            {
                setError("No se reconoce el formato del valor ingresado.");
            }
            else
            {
                var convertedValue = convertionResult.Value;
                var validationResult = _validationRules.Validate(convertedValue);
                if (validationResult.IsValid)
                {
                    _value = convertedValue;
                    clearErrors();
                }
                else
                {
                    setError(validationResult.ErrorMessage);
                
                }
            }
            notifyChange();
           
        }

        //------------------------------------------------------ 
        //  Commands
        //------------------------------------------------------
        public ICommand UndoCommand { get; set; }
        private void _undoExecuted(object o)
        {
            var tb = o as TextBox;

            if (_textValues.Count > 1)
            {
                _textValues.Pop();
                BindingString = _textValues.Pop();
            }
            else if (_textValues.Count == 1)
            {
                _textValues.Pop();
                BindingString = _initialValue.ToString();
            }

            tb.CaretIndex = BindingString.Length;
        }

        private bool _undoCanExecute(object o)
        {
            return true;
        }

        public ICommand EvalFormulaCommand { get; set; }
        private void _evalFormulaExecuted(object o)
        {
            if (BindingString.IndexOf('=') == 0)
            {
                try
                {
                    var datesPart = BindingString.Remove(0, 1);
                    var parts = datesPart.Split('-');
                    var date2 = parts[0];
                    var date1 = parts[1];
                    var dias = (DateTime.ParseExact(date2, "dd/MM/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact(date1, "dd/MM/yyyy", CultureInfo.InvariantCulture)).Days;
                    dias = Math.Abs(dias);
                    BindingString = dias.ToString();
                }
                catch (Exception)
                {
                    setError("F�rmula inv�lida");
                    notifyChange();
                }
            }
        }
        private bool _evalFormulaCanExecute(object o)
        {
            return true;
        }
        //------------------------------------------------------ 
        //  INotifyPropertyChanged
        //------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged()
        {
            //OnPropertyChanged(nameof(Value));
            OnPropertyChanged(nameof(BindingString));
        }

        public List<IBindableProperty> RelatedProperties { get; } = new List<IBindableProperty>();

        object IBindableProperty.Value
        {
            get { return Value; }
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        //------------------------------------------------------ 
        //  INotifyDataErrorInfo
        //------------------------------------------------------
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        public bool HasErrors
        {
            get { return _errors.Count > 0; }
        }
        public IEnumerable GetErrors(string propertyName)
        {
            return _errors;
        }

        protected virtual void OnErrorChanged([CallerMemberName] string propertyName = "")
        {
            ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }


        //------------------------------------------------------ 
        //  ITrackable
        //------------------------------------------------------
        public void RestoreOriginalValue()
        {
            BindingString = _converter.ToBindingString(_initialValue);
            _value = _initialValue;
        }


        //------------------------------------------------------ 
        //  IValidatable
        //------------------------------------------------------
        public void AddValidationRule(IValidationRule<T> validationRule)
        {
            _validationRules.AddValidationRule(validationRule);
        }


        public void Restore()
        {
            if (BindingString == "")
            {
                BindingString = _converter.ToBindingString(Value);
            }
        }
    }
}