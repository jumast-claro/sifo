using System;
using SIFO.DesktopUI.Utilities.Validation;

namespace SIFO.DesktopUI.Utilities.Properties
{
    public interface IValidatable<T>
    {
        void AddValidationRule(IValidationRule<T> validationRule);
    }
}