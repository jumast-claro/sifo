﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace SIFO.DesktopUI.Utilities.Properties
{
    public abstract class BaseConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}