﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SIFO.DesktopUI.Utilities
{
    public abstract class ViewModelPropertyBase<T> : INotifyPropertyChanged
    {
        public virtual void RaisePropertyChanged()
        {
            OnPropertyChanged(nameof(Value));
//            OnValueChanged();
        }

        //------------------------------------------------------ 
        //  Propiedades
        //------------------------------------------------------
        public abstract T Value { get; set; }
        //public virtual string DisplayString => this.Value.ToString();

        //------------------------------------------------------ 
        //  INotifyValueChanged
        //------------------------------------------------------
//        public event ValueChangedEventHandler ValueChanged;
//        protected virtual void OnValueChanged()
//        {
//            ValueChanged?.Invoke(this, new ValueChangedEventArgs(this.Value));
//        }

        //------------------------------------------------------ 
        //  INotifyPropertyChanged
        //------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }



    }
}