﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace SIFO.DesktopUI.Utilities.Properties
{

    public class Aux
    {
        public object Property { get; private set; }
        public int Position { get; private set; }

        public Aux(object property, int position)
        {
            Property = property;
            Position = position;
        }
    }


    public interface IPropertyCollection
    {
        IEnumerable<object> Properties { get; }
        void AddProperty<TValue>(IBindableProperty<TValue> property, int position = 0);
    }

    public class PropertyBag : IPropertyCollection
    {
        private readonly ObservableCollection<Aux> _viewModelProperties = new ObservableCollection<Aux>();


        public void AddProperty<TValue>(IBindableProperty<TValue> viewModelProperty, int position = 0)
        {
            _viewModelProperties.Add(new Aux(viewModelProperty, position)); 
        }

        public IEnumerable<object> Properties => new ObservableCollection<object>(_viewModelProperties.OrderBy(aux => aux.Position).Select(aux => aux.Property));

    }
}