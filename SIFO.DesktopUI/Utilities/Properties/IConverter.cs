using SIFO.DesktopUI.Utilities.Validation;

namespace SIFO.DesktopUI.Utilities.Properties
{
    public interface IConverter<T>
    {
        ConvertionResult<T> Convert(string stringValue);
        string ToBindingString(T value);
    }
}