namespace SIFO.DesktopUI.Utilities
{
    public interface ITrackableViewModelProperty
    {
        void RestoreOriginalValue();
    }
}