using System;
using System.Globalization;
using SIFO.DesktopUI.Utilities.Validation;

namespace SIFO.DesktopUI.Utilities.Properties
{
    public class NullableDateTimeConverter : IConverter<DateTime?>
    {
        public ConvertionResult<DateTime?> Convert(string stringValue)
        {

            if (stringValue == "")
            {
                return new ConvertionResult<DateTime?>(true, null);
            }

            DateTime? result = null;
            try
            {
                result = DateTime.ParseExact(stringValue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                
                return new ConvertionResult<DateTime?>(false, null);
            }
            return new ConvertionResult<DateTime?>(true, result);
        }

        public string ToBindingString(DateTime? value)
        {
            return value?.ToString("dd/MM/yyyy") ?? "";
        }
    }
}