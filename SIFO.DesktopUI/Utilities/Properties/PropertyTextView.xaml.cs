﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SIFO.DesktopUI.Utilities.Properties
{
    /// <summary>
    /// Interaction logic for PropertyTextView.xaml
    /// </summary>
    public partial class PropertyTextView : UserControl
    {
        public static DependencyProperty DescriptionProperty = DependencyProperty.Register("Description", typeof(string), typeof(PropertyTextView));
        public static DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(PropertyTextView));
        public static DependencyProperty IsFirstProperty = DependencyProperty.Register("IsFirst", typeof(bool), typeof(PropertyTextView), new FrameworkPropertyMetadata(false));
        public static DependencyProperty IsLastProperty = DependencyProperty.Register("IsLast", typeof(bool), typeof(PropertyTextView), new FrameworkPropertyMetadata(false));

        //public static DependencyProperty DescriptionTextBlockStyleProperty = DependencyProperty.Register("DescriptionTextBlockStyle", typeof(Style), typeof(PropertyTextView));



        public PropertyTextView()
        {
            InitializeComponent();
        }

        public string Description
        {
            get { return (string) GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value);}
        }

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public bool IsFirst
        {
            get { return (bool)GetValue(IsFirstProperty); }
            set { SetValue(IsFirstProperty, value);}
        }

        public bool IsLast
        {
            get { return (bool)GetValue(IsLastProperty); }
            set { SetValue(IsLastProperty, value); }
        }

        //public Style DescriptionTextBlockStyle
        //{
        //    get
        //    {
        //        return (Style) GetValue((DescriptionTextBlockStyleProperty));
        //    }
        //    set
        //    {
        //        SetValue(DescriptionTextBlockStyleProperty, value);
        //    }
        //}

    }
}
