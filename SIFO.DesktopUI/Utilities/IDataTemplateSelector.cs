using System.Windows;

namespace SIFO.DesktopUI.Utilities
{
    public interface IDataTemplateSelector
    {
        DataTemplate SelectTemplate(object item, DependencyObject container);
    }
}