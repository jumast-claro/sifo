﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PostSharp.Aspects;
using SIFO.DesktopUI.Utilities.Reflection;

namespace SIFO.DesktopUI.Utilities
{
    [Serializable]
    public class InitializeCommandsAspect : OnMethodBoundaryAspect
    {
        public override void OnExit(MethodExecutionArgs args)
        {
            //base.OnExit(args);
            var reflector = new CommandReflector();
            reflector.Configure(args.Instance);
        }
    }
}
