﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace SIFO.DesktopUI.Utilities.Converters
{
    [ValueConversion(typeof(object), typeof(string))]
    public class DateTimeToStringConverter : MarkupExtension, IValueConverter
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var theValue = (DateTime)value;
            return theValue.ToString("dd/MM/yyy HH:mm:ss");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}