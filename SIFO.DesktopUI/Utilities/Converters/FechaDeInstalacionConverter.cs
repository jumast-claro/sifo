using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;
using SIFO.DesktopUI.Utilities.Properties;

namespace SIFO.DesktopUI.Utilities.Converters
{
    [ValueConversion(typeof(object), typeof(string))]
    public class FechaDeInstalacionConverter : BaseConverter
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var theValue = (DateTime)value;
            return theValue == DateTime.MaxValue ? "[no tiene]" : theValue.ToString("dd/MM/yyy");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}