﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace SIFO.DesktopUI.Utilities.Converters
{
    public class IntSoStringConverter : MarkupExtension, IValueConverter
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((int)value).ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = (string) value;



            if (str == "")
            {
                return 0;
            }
            if (str.Equals("0"))
            {
                return 0;
            }
            if (str[0] == '0' && str[1] == '-')
            {
                int re;
                string s = str.Remove(0,1);
                var valid = int.TryParse(s, out re);
                if (valid)
                {
                    return re;
                }
            }
            //if (str[0] == '-')
            //{
                
            //}

            int result;
            var validInt = int.TryParse(str, out result);
            if (validInt)
            {
                return result;
            }
            return DependencyProperty.UnsetValue;
            //if (str.Last() == '-')
            //{
            //    return DependencyProperty.UnsetValue;
            //}
            //if (str.Equals("-0") || str.Equals("-"))
            //{
            //    return -1;
            //}
            //else
            //{
            //    return int.Parse(str);
            //}
        }
    }
}
