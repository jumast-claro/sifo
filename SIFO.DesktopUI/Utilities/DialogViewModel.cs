﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SIFO.DesktopUI.Utilities
{
    public delegate void DialogAcceptedEventHandler(object sender, EventArgs e);
    public delegate void DialogCanceledEventHandler(object sender, EventArgs e);
    public delegate void DialogClosedEventHandler(object sender, EventArgs e);



    public enum ButtonType
    {
        Accept,
        Cancel,
        RestoreValues,
    }


    public class ButtonPressedEventArgs : EventArgs
    {
        
        public ButtonType ButtonType { get; private set; }

        public ButtonPressedEventArgs(ButtonType buttonType)
        {
            ButtonType = buttonType;
        }

    }

    public delegate void ButtonPressedEventHandler(object sender, ButtonPressedEventArgs e);

    public interface INotifyDialogAccepted
    {
        event DialogAcceptedEventHandler DialogAccepted;
    }

    public interface INotifyDialogCanceled
    {
        event DialogCanceledEventHandler DialogCanceled;
    }

    public interface INotifyDialogClosed
    {
        event DialogClosedEventHandler DialogClosed;
    }

}
