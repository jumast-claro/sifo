﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using LiveCharts;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Wpf;
using OxyPlot.Axes;
using SIFO.DesktopUI.TareasCerradas.Indicadores;
using SIFO.DesktopUI.TareasCerradas.Indicadores.PlantaExterna;
using SIFO.DesktopUI.TareasIngresadas;
using SIFO.DesktopUI.Utilities;
using SIFO.Model;
using SIFO.Services;
using CategoryAxis = OxyPlot.Axes.CategoryAxis;
using ColumnSeries = OxyPlot.Series.ColumnSeries;
using LinearAxis = OxyPlot.Axes.LinearAxis;

namespace SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento
{
    public class CumplimientoViewModel : INotifyPropertyChanged
    {

        //---------------------------------------------------------------------
        // Private
        //---------------------------------------------------------------------
        private SortedDictionary<string, BothViewModel> _dic = new SortedDictionary<string, BothViewModel>();
        private readonly TareasCerradasModelRepository _tareasCerradasModelRepository;
        private readonly ITareasIngresadasModelRepository _tareasIngresadasModelRepository;

        public QueryOptionsViewModel QueryOptionsViewModel { get; private set; }


        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------
        [InitializeCommandsAspect] 
        public CumplimientoViewModel(TareasCerradasModelRepository tareasCerradasModelRepository, ITareasIngresadasModelRepository tareasIngresadasModelRepository)
        {

            var repo = new EnlacesCerradosModelRepository(tareasCerradasModelRepository);

            _tareasCerradasModelRepository = tareasCerradasModelRepository;
            _tareasIngresadasModelRepository = tareasIngresadasModelRepository;
            doIt(_tareasCerradasModelRepository.SelectAll());
            doItIngresadas(_tareasIngresadasModelRepository.SelectAll());
            var cerradasQueryOptions = new QueryOptions(_tareasCerradasModelRepository.SelectAll(), Planta.PlantaExterna);
            cerradasQueryOptions.IncluirProyectosEspeciales = true;
            QueryOptionsViewModel = new QueryOptionsViewModel(cerradasQueryOptions);
            updateCharts();
        }
        private void doItIngresadas(IEnumerable<ITarea> ingresadas)
        {
            //var cerradas = _tareasCerradasModelRepository.SelectAll();

            var dic = _dic;

            foreach (var tarea in ingresadas)
            {
                var inbox = tarea.Inbox;


                var mes = tarea.FechaInicioTarea.Month;


                var vm = dic[inbox].Analisis[mes];
                vm.Mes = mes;
                vm.CantIngresadasProperty.Value += 1;


            }

            //BothViewModel = dic;
            updateCharts();

        }

        private void doIt(IEnumerable<ITarea> cerradas)
        {
            //var cerradas = _tareasCerradasModelRepository.SelectAll();

            var dic = new SortedDictionary<string, BothViewModel>();

            foreach (var tarea in cerradas)
            {

                if ((tarea.Inbox == "Obra Civil Tend AMBA" || tarea.Inbox == "Obra Civil Tend INTE") && tarea.OnTimeInbox == false)
                {
                    var pseudoEnlace = tarea.PseudoEnlace;
                    var cerradasPermisos = cerradas
                        .Where(t => t.Inbox == "Permisos Municipales")
                        .Where(t => t.PseudoEnlace == pseudoEnlace);

                    if (cerradasPermisos.Count() > 0)
                    {
                        tarea.TieneObraCivil = true;
                    }

                    foreach (var cerradaPermisos in cerradasPermisos)
                    {
                        var inicioP = cerradaPermisos.FechaInicioTarea;
                        var finP = cerradaPermisos.FechaCierreTarea.Fecha;
                        if (inicioP > tarea.FechaInicioTarea && finP < tarea.FechaCierreTarea.Fecha)
                        {
                            tarea.TiempoEnOtrasBandejas = cerradaPermisos.TiempoEfectivoPermanenciaTareaEnEstaBandeja;
                        }
                    }
                }




                var inbox = tarea.Inbox;
                if (!dic.ContainsKey(inbox))
                {
                    dic[inbox] = new BothViewModel();

                    for (int i = 1; i <= 12; i++)
                    {
                        dic[inbox].Analisis[i] = new MovimientoMensualDeEnlacesViewModel();
                    }
                }

                var mes = tarea.FechaCierreTarea.Mes;
                var resultado = tarea.Estado;


                var vm = dic[inbox].Analisis[mes];
                vm.Mes = mes;

                //AnalisisInboxViewModel semestreVm;
                //semestreVm = mes <= 6 ? dic[inbox].Analisis[13] : dic[inbox].Analisis[14];


                if (resultado == Estado.Exito)
                {
                    vm.CantExitoProperty.Value += 1;

                    if (tarea.OnTimeInbox)
                    {
                        vm.CantOnTimeProperty.Value += 1;
                    }
                    else
                    {
                        vm.CantOffTimeProperty.Value += 1;

                        if (tarea.OnTimeGeneral)
                        {
                            vm.CantOffTimeOnTimeProperty.Value += 1;
                        }
                        else
                        {
                            vm.CantOffTimeOffTimeProperty.Value += 1;
                        }
                    }


                }
                else if (resultado == Estado.Fracaso)
                {
                    vm.CantFracasoProperty.Value += 1;
                }

                vm.CantCerradasProperty.Value += 1;

               
            }
         
            BothViewModel = dic;
            updateCharts();

        }

        private void updateCharts()
        {
            foreach (var key in _dic.Keys)
            {
                var inbox = key;
                var values = _dic[key];

                var plot = new PlotModel();
                plot.Axes.Add(new LinearAxis()
                {
                    AxislineThickness = 3,
                    AxislineColor = OxyColors.Black,
                    IsZoomEnabled = false,
                    IsPanEnabled = false,

                });






                var width = 40;
                var ingresadas = new ColumnSeries();
                ingresadas.IsStacked = true;
                ingresadas.LabelPlacement = LabelPlacement.Middle;
                ingresadas.Title = "Ingresadas";
                ingresadas.FillColor = OxyColors.Yellow;
                ingresadas.LabelFormatString = "{0}";
                ingresadas.ColumnWidth = width;
                ingresadas.StackGroup = "0";

                var onTime = new ColumnSeries();
                onTime.IsStacked = true;
                onTime.LabelPlacement = LabelPlacement.Middle;
                onTime.Title = "OnTime";
                onTime.LabelFormatString = "{0}";
                onTime.FillColor = OxyColors.Green;
                onTime.ColumnWidth = width;
                onTime.StackGroup = "1";


                var offTime = new ColumnSeries();
                offTime.IsStacked = true;
                offTime.LabelPlacement = LabelPlacement.Middle;
                offTime.Title = "OffTime";
                offTime.FillColor = OxyColors.Red;
                offTime.LabelFormatString = "{0}";
                offTime.ColumnWidth = width;
                offTime.StackGroup = "1";

                var fracasadas = new ColumnSeries();

                fracasadas.IsStacked = true;
                //fracasadas.LabelPlacement = LabelPlacement.Middle;
                fracasadas.Title = "Fracasada";
                fracasadas.FillColor = OxyColors.Black;
                //fracasadas.LabelFormatString = "{0}";
                fracasadas.ColumnWidth = width;
                fracasadas.StackGroup = "1";

                var backlog = new ColumnSeries();
                backlog.IsStacked = true;
                backlog.LabelPlacement = LabelPlacement.Middle;
                backlog.Title = "Backlog";
                backlog.FillColor = OxyColors.Blue;
                backlog.LabelFormatString = "{0}";
                backlog.ColumnWidth = width;
                backlog.StackGroup = "2";



                //cerradas.IsStacked = true;

                for (int i = 1; i <= 12; i++)
                {

                    var index = i - 1;
                    ingresadas.Items.Add(new ColumnItem()
                    {
                        CategoryIndex = index,
                        Value = values.Analisis[i].CantIngresadasProperty.Value,

                    });

                    onTime.Items.Add(new ColumnItem()
                    {

                        //Color = OxyColors.Green,
                        CategoryIndex = index,
                        Value = values.Analisis[i].CantOnTimeProperty.Value,
                    });




                    offTime.Items.Add(new ColumnItem()
                    {
                        //Color = OxyColors.Red,
                        CategoryIndex = index,
                        Value = values.Analisis[i].CantOffTimeProperty.Value,
                    });

                    fracasadas.Items.Add(new ColumnItem()
                    {

                        //Color = OxyColors.Green,
                        CategoryIndex = index,
                        Value = values.Analisis[i].CantFracasoProperty.Value,
                    });

                    backlog.Items.Add(new ColumnItem()
                    {
                        CategoryIndex = index,
                        Value = values.Analisis[i].CantPendientesFinalProperty.Value
                    });





                }

                var cat = new CategoryAxis();
                cat.Labels.Add("Ene-16");
                cat.Labels.Add("Feb-16");
                cat.Labels.Add("Mar-16");
                cat.Labels.Add("Abr-16");
                cat.Labels.Add("May-16");
                cat.Labels.Add("Jun-16");
                cat.Labels.Add("Jul-16");
                cat.Labels.Add("Ago-16");
                cat.Labels.Add("Sep-16");
                cat.Labels.Add("Oct-16");
                cat.Labels.Add("Nov-16");
                cat.Labels.Add("Dic-16");

                cat.MajorGridlineStyle = LineStyle.Solid;
                cat.MinorGridlineStyle = LineStyle.Solid;

                cat.GapWidth = 0.3;

                cat.IsZoomEnabled = false;
                cat.IsPanEnabled = false;

                plot.Series.Add(ingresadas);
                plot.Series.Add(onTime);
                plot.Series.Add(offTime);
                plot.Series.Add(fracasadas);
                plot.Series.Add(backlog);

                //var l = new LinearAxis();
                //l.Position = AxisPosition.Left;
                ////l.PositionAtZeroCrossing = true;
                //l.MajorGridlineStyle = LineStyle.Solid;
                //l.MinorGridlineStyle = LineStyle.None;
                //l.AxislineColor = OxyColors.DarkRed;

                plot.Axes.Add(cat);
                //plot.Axes.Add(l);


                _dic[inbox].PlotModel = plot;
            }


        }
        //---------------------------------------------------------------------
        // Properties
        //---------------------------------------------------------------------
        public SortedDictionary<string, BothViewModel> BothViewModel
        {
            get
            {
                return _dic;
            }
            set
            {
                _dic = value;
                OnPropertyChanged();
            }
        }


        //---------------------------------------------------------------------
        // Commands
        //---------------------------------------------------------------------
        public ICommand AplicarCommand { get; private set; }
        private void _aplicarExecuted(object o)
        {
            _dic.Clear();

            QueryOptions queryOptions = new QueryOptions()
            {
                Planta = Planta.PlantaExterna,
                IncluirProyectosEspeciales = QueryOptionsViewModel.IncluirProyectosEspecialesProperty.Value,

            };

            foreach (var option in QueryOptionsViewModel.Zona.SelectedOptions)
            {
                queryOptions.Zona.Add((Zona)Enum.Parse(typeof(Zona), option.DescriptionProperty.Value));
            }

            foreach (var option in QueryOptionsViewModel.Productos.SelectedOptions)
            {
                queryOptions.Productos.Add(option.DescriptionProperty.Value);
            }

            foreach (var option in QueryOptionsViewModel.TipoDeOrden.SelectedOptions)
            {
                queryOptions.TipoDeOrden.Add(option.DescriptionProperty.Value);
            }

            foreach (var option in QueryOptionsViewModel.TipoDeSolucion.SelectedOptions)
            {
                queryOptions.TipoDeSolucion.Add(option.DescriptionProperty.Value);
            }
            foreach (var option in QueryOptionsViewModel.Inbox.SelectedOptions)
            {
                queryOptions.Inbox.Add(option.DescriptionProperty.Value);
            }
            foreach (var option in QueryOptionsViewModel.RegionFinal.SelectedOptions)
            {
                queryOptions.RegionFinal.Add(option.DescriptionProperty.Value);
            }
            foreach (var option in QueryOptionsViewModel.Cliente.SelectedOptions)
            {
                queryOptions.Cliente.Add(option.DescriptionProperty.Value);
            }

            foreach (var option in QueryOptionsViewModel.TipoDeFibraOptica.SelectedOptions)
            {
                queryOptions.TipoDeFibraOptica.Add(option.DescriptionProperty.Value);
            }
            foreach (var option in QueryOptionsViewModel.TipoDeMovimiento.SelectedOptions)
            {
                queryOptions.TipoDeMovimiento.Add(option.DescriptionProperty.Value);
            }
            foreach (var option in QueryOptionsViewModel.Tecnologia.SelectedOptions)
            {
                queryOptions.Tecnologia.Add(option.DescriptionProperty.Value);
            }


            var cerradas = new UniversoDeTareas<ITarea>(_tareasCerradasModelRepository.SelectAll());
            var c = cerradas.Where(queryOptions);

            var ingresadas = new UniversoDeTareas<ITarea>(_tareasIngresadasModelRepository.SelectAll());
            var i = ingresadas.Where(queryOptions);
            doIt(c);
            doItIngresadas(i);

            //OnPropertyChanged(nameof(BothViewModel));
        }
        private bool _aplicarCanExecute(object o)
        {
            return true;
        }

          /// INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
