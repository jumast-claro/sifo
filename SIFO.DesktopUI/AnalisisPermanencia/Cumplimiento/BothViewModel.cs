using OxyPlot;

namespace SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento
{
    public class BothViewModel
    {

        public BothViewModel()
        {
            Analisis = new MovimientoMensualDeEnlacesViewModel[14];
            PlotModel = new PlotModel();
        }

        public MovimientoMensualDeEnlacesViewModel[] Analisis { get; }

        public PlotModel PlotModel { get; set; }
    }
}