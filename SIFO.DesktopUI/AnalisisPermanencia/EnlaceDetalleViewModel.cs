using System.Collections.ObjectModel;

namespace SIFO.DesktopUI.AnalisisPermanencia
{
    public class EnlaceDetalleViewModel
    {
        public ObservableCollection<TareaDetalleViewModel> Tareas { get; set; } = new ObservableCollection<TareaDetalleViewModel>();
    }
}