﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using SIFO.DesktopUI.CollectionView;
using SIFO.DesktopUI.Framework;
using SIFO.DesktopUI.TareasCerradas;
using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.CollectionView;
using SIFO.DesktopUI.Utilities.Properties;
using SIFO.Model;
using SIFO.Services;

namespace SIFO.DesktopUI.AnalisisPermanencia.Interior
{
    public class AnalisisPermanenciaInteriorViewModel : INotifyPropertyChanged
    {

        private TareaDetalleViewModel _s;

        public TareaDetalleViewModel SelectedDetalle
        {
            get { return _s; }
            set
            {
                _s = value;
                OnPropertyChanged();
            }
        }

        //pu;blic  ReadWriteProperty<TareaDetalleViewModel> SelectedDetalleProperty { get; set; } = new ReadWriteProperty<TareaDetalleViewModel>(null);

        public EnlaceDetalleViewModel EnlaceDetalleViewModel { get; set; } = new EnlaceDetalleViewModel();

        private PermanenciaInteriorViewModel _selectedItem;

        public PermanenciaInteriorViewModel SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                if (value == _selectedItem) return;
                _selectedItem = value;

                EnlaceDetalleViewModel.Tareas.Clear();

                if (value != null)
                {
                    var e = value.PseudoEnlaceProperty.Value;
                    var cerradas = _tareaCerradaModelRepository.SelectAll().Where(t => t.PseudoEnlace == e);

                    foreach (var t in cerradas)
                    {
                        //var vm = new TareaDetalleViewModel();
                        //vm.InboxProperty.Value = t.Inbox;
                        //vm.PermanenciaProperty.Value = t.TiempoEfectivoPermanenciaTareaEnEstaBandeja;
                        //vm.FechaInicioProperty.Value = t.FechaInicioTarea;
                        //vm.FechaCierreProperty.Value = t.FechaCierreTarea.Fecha;
                        //vm.ResultadoProperty.Value = t.Estado.ToString();
                        //vm.UltimaNotaProperty.Value = t.UltimaNota;
                        //vm.UltimaNotaAutorProperty.Value = t.UltimaNotaAutor;
                        //vm.UltimaNotaFechaProperty.Value = t.UltimaNotaFecha;
                        var vm = new TareaDetalleViewModel(t);


                        EnlaceDetalleViewModel.Tareas.Add(vm);
                    }
                }

                SelectedDetalle = EnlaceDetalleViewModel.Tareas.FirstOrDefault();
                OnPropertyChanged();
            }
        }

        public OptionsViewModel<int> Meses { get; } = new OptionsViewModel<int>();
        public OptionsViewModel<string> TiposDeOrden { get; } = new OptionsViewModel<string>();

        private readonly TareasCerradasModelRepository _tareaCerradaModelRepository;

        private CustomCollectionView<PermanenciaInteriorViewModel> _collectionView;

        public IBindableProperty<int> MesProperty { get; private set; } = new ReadWriteProperty<int>(10);

        public ICommand AplicarCommand { get; set; }

        private void _aplicarExecuted(object o)
        {
            var cerradas = _tareaCerradaModelRepository.SelectAll().Where(t => t.Zona == Zona.Interior &&
            (t.Inbox == "Administracion FO" ||
            t.Inbox == "Gestion de Ingresos" ||
            t.Inbox == "Ingenieria de FO" ||
            t.Inbox == "Inserciones Interior" ||
            t.Inbox == "Instalacion GPON" ||
            t.Inbox == "Obra Civil Tend INTE" ||
            t.Inbox == "Operac. - Start Up" ||
            t.Inbox == "Permisos Municipales" ||
            t.Inbox == "Start UP - FO"))
                .Where(t => t.Estado == Estado.Exito);

            var mes = Meses.SelectedOption.Value.Value;
            if (mes != 0)
            {
                cerradas = cerradas.Where(t => t.FechaCierreTarea.Mes == mes);
            }

            var tipoDeOrden = TiposDeOrden.SelectedOption.Value.Value;
            if (tipoDeOrden != "Todos")
            {
                cerradas = cerradas.Where(t => t.TipoDeOrden == tipoDeOrden);
            }

            doIt(cerradas);

            OnPropertyChanged(nameof(CollectionView));

            SummaryViewModel.CantidadTotal = _collectionView.AllCount;
            SummaryViewModel.CantidadEnVistaActual = _collectionView.FilteredCount;
            OnPropertyChanged(nameof(SummaryViewModel));

        }

        private bool _aplicarCanExecute(object o)
        {
            return true;
        }

        private GroupService _groupService;

        [InitializeCommandsAspect]
        public AnalisisPermanenciaInteriorViewModel(TareasCerradasModelRepository tareasCerradasModelRepository)
        {
            _tareaCerradaModelRepository = tareasCerradasModelRepository;

            var cerradas = _tareaCerradaModelRepository.SelectAll().Where(t => t.Zona == Zona.Interior &&
            (t.Inbox == "Administracion FO" ||
            t.Inbox == "Gestion de Ingresos" ||
            t.Inbox == "Ingenieria de FO" ||
            t.Inbox == "Inserciones Interior" ||
            t.Inbox == "Instalacion GPON" ||
            t.Inbox == "Obra Civil Tend INTE" ||
            t.Inbox == "Operac. - Start Up" ||
            t.Inbox == "Permisos Municipales" ||
            t.Inbox == "Start UP - FO"))
                .Where(t => t.Estado == Estado.Exito);


            for (int i = 0; i <= DateTime.Now.Month; i++)
            {

                var o = new OptionViewModel<int>(DateTime.Now.Month - 1 == i, i);
                Meses.Options.Add(o);
            }



            foreach (var option in Meses.Options)
            {
                if (option.Value.Value == DateTime.Now.Month)
                {
                    Meses.SelectedOption = option;
                }
            }

            var tipos = cerradas.Select(t => t.TipoDeOrden).Distinct();


            TiposDeOrden.Options.Add(new OptionViewModel<string>(false, "Todos"));
            foreach (var t in tipos)
            {
                var o = new OptionViewModel<string>(false, t);
                TiposDeOrden.Options.Add(o);
            }



            foreach (var o in TiposDeOrden.Options)
            {
                if (o.Value.Value == "Todos")
                {
                    TiposDeOrden.SelectedOption = o;
                }
            }

            TiposDeOrden.SelectedValueChangedCallback = () =>
            {
                _aplicarExecuted(null);
            };

            Meses.SelectedValueChangedCallback = () =>
            {
                _aplicarExecuted(null);
            };

            var mes = Meses.SelectedOption.Value.Value;
            if (mes != 0)
            {
                cerradas = cerradas.Where(t => t.FechaCierreTarea.Mes == mes);
            }
            var tipoDeOrden = TiposDeOrden.SelectedOption.Value.Value;
            if (tipoDeOrden != "Todos")
            {
                cerradas = cerradas.Where(t => t.TipoDeOrden == tipoDeOrden);
            }
            doIt(cerradas);


        }

        private void doIt(IEnumerable<ITarea> cerradas)
        {

            var pseudoEnlaces = cerradas.Select(t => t.PseudoEnlace).Distinct();

            var a = new List<PermanenciaInteriorViewModel>();
            foreach (var pseudoEnlace in pseudoEnlaces)
            {
                var tareas = cerradas.Where(t => t.PseudoEnlace == pseudoEnlace);

                var firstOrDefault = tareas.FirstOrDefault();
                var vm = new PermanenciaInteriorViewModel(pseudoEnlace, firstOrDefault.Partido, firstOrDefault.Producto, firstOrDefault.TipoSolucion)
                {

                    PseudoEnlaceProperty = new ReadOnlyProperty<string>(pseudoEnlace)

                };
                foreach (var tarea in tareas)
                {
                    vm.AgregarTarea(tarea);
                }
                a.Add(vm);
            }

            _collectionView = new CustomCollectionView<PermanenciaInteriorViewModel>(a);
            _groupService = new GroupService(CollectionView);

            SummaryViewModel = new NewSummaryViewModel()
            {
                CantidadTotal = _collectionView.AllCount,
                CantidadEnVistaActual = _collectionView.FilteredCount

            };
        }

        public NewSummaryViewModel SummaryViewModel { get; private set; }

        public ICommand ShowFilterOptionsCommand { get; private set; }
        private void _showFilterOptionsExecuted(object o)
        {
            var propertyName = (string)o;
            var view = new FilterOptionsView();
            view.AcceptCommand = new RelayCommand(o1 => _collectionView.ApplyFilters(), o1 => true);
            view.GroupCommand = _groupService.GroupCommand;

            var window = new Window
            {
                Content = view,
                HorizontalContentAlignment = HorizontalAlignment.Stretch
            };


            var filterDialogViewModel = _collectionView.Filter.GetFilterDialogViewModelForProperty(propertyName);
            foreach (var a in filterDialogViewModel)
            {
                a.IsSelected = a.IsCurrentFilter;
            }

            _collectionView.SetCurrentViewCountForProperty(propertyName);
            window.DataContext = filterDialogViewModel;

            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            window.WindowStyle = WindowStyle.ToolWindow;
            window.Width = 300;
            window.Height = 600;
            window.ShowDialog();


            SummaryViewModel.CantidadEnVistaActual = _collectionView.FilteredCount;

            //updateCharts();
        }
        private bool _showFilterOptionsCanExecute(object o)
        {
            return true;
        }

        public ICommand RemoveAllGroupsCommand { get; private set; }

        private void _removeAllGroupsExecuted(object o)
        {

            _groupService.RemoveAllGroupsCommand.Execute(o);

        }

        private bool _removeAllGroupsCanExecute(object o)
        {
            return _groupService.RemoveAllGroupsCommand.CanExecute(o);
        }

        public ICommand RemoveAllFiltersCommand { get; private set; }
        private void _removeAllFiltersExecuted(object o)
        {
            _collectionView.Filter.RemoveAllFilters();
            _collectionView.RemoveAllFilters();
            SummaryViewModel.CantidadEnVistaActual = SummaryViewModel.CantidadTotal;
        }
        private bool _removeAllFiltersCanExecute(object o)
        {
            return _collectionView.IsFiltered;
        }

        public ICollectionView CollectionView => _collectionView.View;

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
