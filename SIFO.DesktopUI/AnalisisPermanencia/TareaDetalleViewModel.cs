using System;
using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.Properties;
using SIFO.Model;

namespace SIFO.DesktopUI.AnalisisPermanencia
{
    public class TareaDetalleViewModel
    {
        private readonly ITarea _tarea;


        public TareaDetalleViewModel(ITarea tarea)
        {
            _tarea = tarea;
            TareaIdProperty = new ReadWriteProperty<string>(tarea.TareaId);
            InboxProperty = new ReadWriteProperty<string>(tarea.Inbox);
            PermanenciaProperty = new ReadWriteProperty<double>(tarea.TiempoEfectivoPermanenciaTareaEnEstaBandeja);
            FechaInicioProperty = new ReadWriteProperty<DateTime>(tarea.FechaInicioTarea);
            FechaCierreProperty = new ReadOnlyProperty<DateTime?>(tarea.FechaCierreTarea.Fecha, tarea.Estado == Estado.Pendiente ? "" : tarea.FechaCierreTarea.Fecha.Value.ToString("dd/MM/yyyy HH:mm:ss"));
            ResultadoProperty = new ReadWriteProperty<string>(tarea.Estado.ToString());
            UltimaNotaProperty = new ReadWriteProperty<string>(tarea.UltimaNota);
            UltimaNotaFechaProperty = new ReadWriteProperty<DateTime>(tarea.UltimaNotaFecha);
            UltimaNotaAutorProperty = new ReadWriteProperty<string>(tarea.UltimaNotaAutor);
            MesDeCierreProperty = new ReadWriteProperty<int>(tarea.FechaCierreTarea.Mes);
        }

        public ReadWriteProperty<string> TareaIdProperty { get;  }
        public ReadWriteProperty<string> InboxProperty { get;  }
        public ReadWriteProperty<double> PermanenciaProperty { get;  }
        public ReadWriteProperty<DateTime> FechaInicioProperty { get;  }
        public IBindableProperty<DateTime?> FechaCierreProperty { get;  }

        public ReadWriteProperty<int> MesDeCierreProperty { get;  }

        public ReadWriteProperty<string> ResultadoProperty { get;  }
        public ReadWriteProperty<string> UltimaNotaProperty { get;  }
        public ReadWriteProperty<string> UltimaNotaAutorProperty { get;  }
        public ReadWriteProperty<DateTime> UltimaNotaFechaProperty { get;  }

        public ITarea Tarea => _tarea;
    }
}