using SIFO.DesktopUI.Utilities;

namespace SIFO.DesktopUI.AnalisisPermanencia
{
    public class OptionViewModel<T>
    {
        public OptionViewModel(bool isSelected, T value)
        {
            IsSelected = new ReadWriteProperty<bool>(isSelected);
            Value = new ReadWriteProperty<T>(value);
        }

        public ReadWriteProperty<bool> IsSelected { get; private set; }
        public ReadWriteProperty<T> Value { get; private set; } 

    }
}