using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.Properties;
using SIFO.DesktopUI.Utilities.Reflection;
using SIFO.Model;

namespace SIFO.DesktopUI.AnalisisPermanencia
{
    public class PermanenciaAmbaViewModel
    {
        public PermanenciaAmbaViewModel(string pseudoEnlace, string partido, string producto, string tipificacion)
        {
            PseudoEnlaceProperty = new ReadOnlyProperty<string>(pseudoEnlace);
            PartidoProperty = new ReadOnlyProperty<string>(partido);
            ProductoProperty = new ReadWriteProperty<string>(producto);
            TipificacionProperty = new ReadWriteProperty<string>(tipificacion);

            //PermanenciaEnPermisosProperty = new WrappingViewModelProperty<double>(0, () => CantidadEnPermisosProperty.Value);
        }

        [Filter] public IBindableProperty<string> PseudoEnlaceProperty { get; set; }
        [Filter] public ReadOnlyProperty<string> PartidoProperty { get; set; }
        [Filter] public ReadWriteProperty<double> PermanenciaEnIngenieriaDeFoProperty { get; set; } = new ReadWriteProperty<double>(0);
        [Filter] public ReadWriteProperty<double> PermanenciaEnAdministracionFoProperty { get; set; } = new ReadWriteProperty<double>(0);
        [Filter] public ReadWriteProperty<double> PermanenciaEnPermisosProperty { get; set; } = new ReadWriteProperty<double>(0);
        [Filter] public ReadWriteProperty<double> PermanenciaEnObraCivilTendidoProperty { get; set; } = new ReadWriteProperty<double>(0);
        [Filter] public ReadWriteProperty<double> PermanenciaEnInstalacionGponProperty { get; set; } = new ReadWriteProperty<double>(0);
        [Filter] public ReadWriteProperty<double> PermanenciaEnInsercionesProperty { get; set; } = new ReadWriteProperty<double>(0);
        [Filter] public ReadWriteProperty<double> PermanenciaEnOperacStartUpProperty { get; set; } = new ReadWriteProperty<double>(0);
        [Filter] public ReadWriteProperty<double> PermanenciaEnStartUpFoProperty { get; set; } = new ReadWriteProperty<double>(0);
        [Filter] public ReadWriteProperty<double> PermanenciaEnGestionDeIngresosProperty { get; set; } = new ReadWriteProperty<double>(0);


        [Filter] public ReadWriteProperty<int> CantidadEnIngenieriaDeFoProperty { get; set; } = new ReadWriteProperty<int>(0);
        [Filter] public ReadWriteProperty<int> CantidadEnAdministracionFoProperty { get; set; } = new ReadWriteProperty<int>(0);
        [Filter] public ReadWriteProperty<int> CantidadEnPermisosProperty { get; set; } = new ReadWriteProperty<int>(0);
        [Filter] public ReadWriteProperty<int> CantidadEnObraCivilTendidoProperty { get; set; } = new ReadWriteProperty<int>(0);
        [Filter] public ReadWriteProperty<int> CantidadEnInstalacionGponProperty { get; set; } = new ReadWriteProperty<int>(0);
        [Filter] public ReadWriteProperty<int> CantidadEnInsercionesProperty { get; set; } = new ReadWriteProperty<int>(0);
        [Filter] public ReadWriteProperty<int> CantidadEnOperacStartUpProperty { get; set; } = new ReadWriteProperty<int>(0);
        [Filter] public ReadWriteProperty<int> CantidadEnStartUpProperty { get; set; } = new ReadWriteProperty<int>(0);
        [Filter] public ReadWriteProperty<int> CantidadEnGestionDeIngresosProperty { get; set; } = new ReadWriteProperty<int>(0);

        [Filter] public ReadWriteProperty<string> ProductoProperty { get; set; } = new ReadWriteProperty<string>("");
        [Filter] public ReadWriteProperty<string> TipificacionProperty { get; set; } = new ReadWriteProperty<string>("");




        public void AgregarTarea(ITarea tarea)
        {
            ReadWriteProperty<double> prop = null;

            if (tarea.Inbox == "Inserciones AMBA")
            {
                prop = PermanenciaEnInsercionesProperty;
                CantidadEnInsercionesProperty.Value += 1;
            }

            if (tarea.Inbox == "Gestion de Ingresos")
            {
                prop = PermanenciaEnGestionDeIngresosProperty;
                CantidadEnGestionDeIngresosProperty.Value += 1;
            }
            if (tarea.Inbox == "Obra Civil Tend AMBA")
            {
                prop = PermanenciaEnObraCivilTendidoProperty;
                CantidadEnObraCivilTendidoProperty.Value += 1;
            }
            if (tarea.Inbox == "Permisos Municipales")
            {
                prop = PermanenciaEnPermisosProperty;
                CantidadEnPermisosProperty.Value += 1;
            }
            if (tarea.Inbox == "Instalacion GPON")
            {
                prop = PermanenciaEnInstalacionGponProperty;
                CantidadEnInstalacionGponProperty.Value += 1;
            }
            if (tarea.Inbox == "Administracion FO")
            {
                prop = PermanenciaEnAdministracionFoProperty;
                CantidadEnAdministracionFoProperty.Value += 1;
            }
            if (tarea.Inbox == "Ingenieria de FO")
            {
                prop = PermanenciaEnIngenieriaDeFoProperty;
                CantidadEnIngenieriaDeFoProperty.Value += 1;
            }
            if (tarea.Inbox == "Operac. - Start Up")
            {
                prop = PermanenciaEnOperacStartUpProperty;
                CantidadEnOperacStartUpProperty.Value += 1;
            }
            if (tarea.Inbox == "Start UP - FO")
            {
                prop = PermanenciaEnStartUpFoProperty;
                CantidadEnStartUpProperty.Value += 1;
            }


            if (prop != null)
            {
                prop.Value += tarea.TiempoEfectivoPermanenciaTareaEnEstaBandeja;
            }
        }

    }
}