using System;
using System.Collections.Generic;

namespace SIFO.DesktopUI.AnalisisPermanencia
{
    public class OptionsViewModel<T>
    {

        private OptionViewModel<T> _selectedOption;

        public OptionViewModel<T> SelectedOption
        {
            get
            {
                return _selectedOption;
            }
            set
            {
                _selectedOption = value;
                SelectedValueChangedCallback();
            }
            
        }
        public List<OptionViewModel<T>> Options { get; } = new List<OptionViewModel<T>>();

        public Action SelectedValueChangedCallback { get; set; } = () => { };
    }
}