using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using SIFO.DesktopUI.Framework;

namespace SIFO.DesktopUI.SyncfusionControls.PseudoTareas._3_MovimientoDeEnlaces.TablaAnual
{
    public class WpfComboxBoxViewModel : INotifyPropertyChanged
    {
        //---------------------------------------------------------------------
        // Field
        //---------------------------------------------------------------------
        private readonly Action<object> _selectedItemChangedAction;

        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------
        public WpfComboxBoxViewModel()
        {
        }

        public WpfComboxBoxViewModel(Action<object> selectedItemChangedAction)
        {
            _selectedItemChangedAction = selectedItemChangedAction;
            //SelectedItemChangedCommand = new RelayCommand(selectedItemChangedAction, o => true);
        }

        public ObservableCollection<int> Items { get; set; } = new ObservableCollection<int>();

        private int _selectedItem = 0;

        public int SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (value != _selectedItem)
                {
                    _selectedItem = value;
                    _selectedItemChangedAction(null);
                    OnPropertyChanged();
                }
            }
        }

        //public ICommand SelectedItemChangedCommand { get; private set; }


        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}