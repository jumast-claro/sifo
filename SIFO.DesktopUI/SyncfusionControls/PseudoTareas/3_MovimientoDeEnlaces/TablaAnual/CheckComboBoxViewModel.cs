﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using SIFO.DesktopUI.Framework;
using Syncfusion.Data.Extensions;

namespace SIFO.DesktopUI.SyncfusionControls.PseudoTareas._3_MovimientoDeEnlaces.TablaAnual
{
    public class CheckComboBoxViewModel : INotifyPropertyChanged
    {
        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        private Action<object> itemCheckedAction;

        //---------------------------------------------------------------------
        // Constructor
        //---------------------------------------------------------------------
        public CheckComboBoxViewModel()
        {
            
        }

        public CheckComboBoxViewModel(Action<object> itemCheckedExecuted)
        {
            itemCheckedAction = itemCheckedExecuted;
            ItemCheckedCommand = new RelayCommand(itemCheckedDecorated, o => true);
        }

        //---------------------------------------------------------------------
        // Commands
        //---------------------------------------------------------------------
        public ICommand ItemCheckedCommand { get; private set; }

        //---------------------------------------------------------------------
        // Properties
        //---------------------------------------------------------------------
        private ObservableCollection<string> _selectedItems = new ObservableCollection<string>();
        public ObservableCollection<string> SelectedItems
        {
            get
            {
                return _selectedItems;
            }
            set
            {
                _selectedItems = value.OrderBy(v => v).ToObservableCollection();
                OnPropertyChanged();
            }
        }

        private ObservableCollection<string> _allItems = new ObservableCollection<string>();
        public ObservableCollection<string> AllItems
        {
            get
            {
                return _allItems;
            }
            set
            {
                _allItems = value;
                OnPropertyChanged();
            }
        }

        private bool _isChecked = false;
        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                _isChecked = value;

                if (value == true)
                {
                    SelectAll();
                }
                else
                {
                    SelectedItems.Clear();
                }
                OnPropertyChanged();
            }
        }

        //---------------------------------------------------------------------
        // Methods
        //---------------------------------------------------------------------
        public void AddOption(string option)
        {
            _allItems.Add(option);
            _allItems =_allItems.OrderBy(o => o).ToObservableCollection();
            OnPropertyChanged(nameof(AllItems));
        }

        public void AddDistinctOptions(IEnumerable<string> options)
        {
            foreach (var option in options)
            {
                if (!_allItems.Contains(option))
                {
                    _allItems.Add(option);
                }
            }
            _allItems = _allItems.OrderBy(o => o).ToObservableCollection();
            OnPropertyChanged(nameof(AllItems));
        }

        public void SelectAll()
        {
            SelectedItems.Clear();
            foreach (var option in AllItems)
            {
                SelectedItems.Add(option);
            }
            _isChecked = true;
            OnPropertyChanged(nameof(IsChecked));
        }

        //---------------------------------------------------------------------
        // Private
        //---------------------------------------------------------------------
        private bool isSelected(string option)
        {
            var ans =  SelectedItems.Contains(option);
            return ans;
        }

        private void itemCheckedDecorated(object o)
        {
            itemCheckedAction(o);
            if (AllItems.All(isSelected))
            {
                _isChecked = true;
            }
            else
            {
                _isChecked = false;
            }
            OnPropertyChanged(nameof(IsChecked));
        }

        private string _literalText = "";
        private string _displayText = "prueba";

        public string Text
        {
            get
            {

                if (SelectedItems.Count == 1)
                {
                    return SelectedItems.FirstOrDefault();
                }
                else
                {
                    var allCount = AllItems.Count;
                    var selectedCount = SelectedItems.Count;
                    return $"{selectedCount} de {allCount}";
                }
                
                //var sb = new StringBuilder();
                //foreach (var option in SelectedItems)
                //{
                //    if (option == "") { sb.Append(""); continue;}
                //    sb.Append(option.Substring(0,2));
                //    sb.Append(" - ");
                //}
                //sb.Remove(sb.Length - 3, 3);

                //var s = sb.ToString();
                //return s;
            }
            set
            {
                _literalText = value;
                OnPropertyChanged();
            }
        }

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
