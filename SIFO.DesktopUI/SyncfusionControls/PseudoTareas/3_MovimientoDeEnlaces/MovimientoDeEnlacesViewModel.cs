﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Schema;
using Ninject.Infrastructure.Language;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using SIFO.DesktopUI.AnalisisPermanencia;
using SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento;
using SIFO.DesktopUI.App_Enlaces;
using SIFO.DesktopUI.Framework;
using SIFO.DesktopUI.SyncfusionControls.PseudoTareas._3_MovimientoDeEnlaces.TablaAnual;
using SIFO.DesktopUI.TareasIngresadas;
using SIFO.Model;
using Syncfusion.Data.Extensions;
using Syncfusion.Windows.Controls.Gantt;

namespace SIFO.DesktopUI.SyncfusionControls.PseudoTareas
{
    public class Model
    {
        public string Name { get; set; }
        public double Count { get; set; }
    }

    public class MonthModel
    {
        public int Mes { get; set; } = 0;
        public int A { get; set; }
        public int B { get; set; }
        public int C { get; set; }
        public int Z { get; set; }
        public int Otros { get; set; }

    }

    public class MovimientoDeEnlacesViewModel : INotifyPropertyChanged
    {
        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        private readonly EnlacesCerradosModelRepository _enlacesCerradosModelRepository;
        private readonly EnlacesIngreadosModelRepository _enlacesIngreadosModel;
        private readonly ITareasCerradasModelRepository _tareasCerradasModelRepository;
        private readonly ITareasIngresadasModelRepository _tareasIngresadasModelRepository;
        private readonly Dictionary<string, MovimientoMensualDeEnlacesViewModel[]> _dic = new Dictionary<string, MovimientoMensualDeEnlacesViewModel[]>();
        private readonly Dictionary<string, PlotModel> _plotDic = new Dictionary<string, PlotModel>();
        private readonly MovimientoDeEnlacesChartViewModel _chartViewModel = new MovimientoDeEnlacesChartViewModel();
        private readonly ViewModelStrategy _viewModelStrategy;

        //---------------------------------------------------------------------
        // Constructor
        //---------------------------------------------------------------------
        public MovimientoDeEnlacesViewModel(EnlacesPendientesInicio2017ModelRepository enlacesPendientesInicio2017ModelRepository, EnlacesCerradosModelRepository enlacesCerradosModelRepository, EnlacesIngreadosModelRepository enlacesIngreadosModel, ITareasCerradasModelRepository tareasCerradasModelRepository, ITareasIngresadasModelRepository tareasIngresadasModelRepository)
        {
            _enlacesCerradosModelRepository = enlacesCerradosModelRepository;
            _enlacesIngreadosModel = enlacesIngreadosModel;
            _tareasCerradasModelRepository = tareasCerradasModelRepository;
            _tareasIngresadasModelRepository = tareasIngresadasModelRepository;

           
            // Cerradas
            var cerrados = _enlacesCerradosModelRepository.SelectAll();


            // Ingresadas
            var ingresados = enlacesIngreadosModel.SelectAll();

            var pendientes = enlacesPendientesInicio2017ModelRepository.SelectAll();

            _viewModelStrategy = new ViewModelStrategy(ingresados, cerrados, pendientes);
            _viewModelStrategy.Strategy.Predicates.Add(e => TipoDeMovimientoComboBoxViewModel.SelectedItems.Contains(e.TipoDeMovimiento));
            _viewModelStrategy.Strategy.Predicates.Add(e => ZonaComboBoxViewModel.SelectedItems.Contains(e.Zona));


            TipoDeMovimientoComboBoxViewModel = new CheckComboBoxViewModel(_movimientoChekcedCommand);
            TipoDeMovimientoComboBoxViewModel.AddDistinctOptions(ingresados.Select(t => t.TipoDeMovimiento).Distinct());
            TipoDeMovimientoComboBoxViewModel.SelectedItems.Add("Administrativo");

            TipoDeSolucionComboBoxViewModel = new CheckComboBoxViewModel(_tipoDeSolucionCheckedExecuted);
            TipoDeSolucionComboBoxViewModel.AddDistinctOptions(_enlacesCerradosModelRepository.SelectAll().Select(e => e.TipoDeSolucion).Distinct());
            TipoDeSolucionComboBoxViewModel.SelectAll();

            ZonaComboBoxViewModel = new CheckComboBoxViewModel(_zonaCheckedCommand);
            ZonaComboBoxViewModel.AddDistinctOptions(_enlacesCerradosModelRepository.SelectAll().Select(e => e.Zona).Distinct());
            ZonaComboBoxViewModel.SelectAll();



            AnnioComboBoxViewModel = new WpfComboxBoxViewModel(selectedAnnioChangedAction);
            AnnioComboBoxViewModel.Items.Add(2016);
            AnnioComboBoxViewModel.Items.Add(2017);
            AnnioComboBoxViewModel.Items.Add(2018);
            AnnioComboBoxViewModel.Items.Add(2019);
            AnnioComboBoxViewModel.SelectedItem = 2019;



            // Lista de inbox
            var inboxes = cerrados.Select(t => t.Inbox).Distinct();
            foreach (var i in inboxes)
            {
                InboxList.Add(i);
            }
            SelectedInbox = InboxList.FirstOrDefault();
            

            _viewModelStrategy.Procesar(AnnioComboBoxViewModel.SelectedItem);
            updatePlotModels();

            // Lista de movimientos
            var mov1 = cerrados.Select(t => t.TipoDeMovimiento).Distinct();
            foreach (var m in mov1)
            {
                Movimientos.Add(m);
            }

           


            updateGanttTasks();


            SelectedInbox = "Obra Civil Tend AMBA";

            updateItemsSource();
            updateHistogramItemsSource();
            updatePieChart();
            updatePiesChart2();
            updateTestItemsSource();

            foreach (var mes in MonthHelper.Enumerate)
            {
                HistogramMonthOptions.Add(mes);
            }
         


            for (var i = 1; i <= 15; i++)
            {
                HistogramIntervalOptions.Add(i);
            }

            MonthChecked = new RelayCommand(_monthChekedExecuted, o => true);


            DetailsViewModel = new EnlaceDataGridDetailsViewModel(tareasCerradasModelRepository, tareasIngresadasModelRepository);
        }

        private void selectedAnnioChangedAction(object o)
        {
            updateVistaAnual();
        }

        private WpfComboxBoxViewModel _annioComboxBoxViewModel;
        public WpfComboxBoxViewModel AnnioComboBoxViewModel
        {
            get { return _annioComboxBoxViewModel; }
            set
            {
                _annioComboxBoxViewModel = value;
                OnPropertyChanged();
            }
        }

      

        private void updateVistaAnual()
        {
            var predicates = _viewModelStrategy.Strategy.Predicates;
            predicates.Clear();

            predicates.Add(e => TipoDeMovimientoComboBoxViewModel.SelectedItems.Contains(e.TipoDeMovimiento));
            predicates.Add(e => TipoDeSolucionComboBoxViewModel.SelectedItems.Contains(e.TipoDeSolucion));
            predicates.Add(e => ZonaComboBoxViewModel.SelectedItems.Contains(e.Zona));
            _viewModelStrategy.Procesar(AnnioComboBoxViewModel.SelectedItem);
            OnPropertyChanged();
            OnPropertyChanged(nameof(ViewModel));


            updatePlotModels();
            OnPropertyChanged(nameof(PlotViewModel));

            updateItemsSource();
            updateHistogramItemsSource();
            updatePieChart();
            updatePiesChart2();
            updateTestItemsSource();
        }

        //---------------------------------------------------------------------
        // Properties
        //---------------------------------------------------------------------
        private int _histogramInterval = 5;
        public int HistogramInterval
        {
            get { return _histogramInterval; }
            set
            {
                if (value != _histogramInterval)
                {
                    _histogramInterval = value;
                    OnPropertyChanged();
                }
            }
        }

        public List<int> HistogramIntervalOptions { get; private set; } = new List<int>();

        
      

        private void _tipoDeSolucionCheckedExecuted(object o)
        {
                TipoDeSolucionComboBoxViewModel.SelectedItems = TipoDeSolucionComboBoxViewModel.SelectedItems.OrderBy(tt => tt).ToObservableCollection();
                updateVistaAnual();
        }

        public CheckComboBoxViewModel TipoDeMovimientoComboBoxViewModel { get; private set; }
        public CheckComboBoxViewModel TipoDeSolucionComboBoxViewModel { get; private set; }
        public CheckComboBoxViewModel ZonaComboBoxViewModel { get; private set; }



        private void _movimientoChekcedCommand(object o)
        {
            TipoDeMovimientoComboBoxViewModel.SelectedItems = TipoDeMovimientoComboBoxViewModel.SelectedItems.OrderBy(m => m).ToObservableCollection();
            updateVistaAnual();
        }

        private void _zonaCheckedCommand(object o)
        {
            ZonaComboBoxViewModel.SelectedItems = ZonaComboBoxViewModel.SelectedItems.OrderBy(m => m).ToObservableCollection();
            updateVistaAnual();
        }

        public ICommand MonthChecked { get; set; }

        private void _monthChekedExecuted(object o)
        {
            Selected = Selected.OrderBy(mes => mes).ToObservableCollection();
            //Selected.Clear();
            updateHistogramItemsSource();
            updatePieChart();
        }

        public List<string> TipoDeSolucionList { get; private set; } = new List<string>();

        private ObservableCollection<int> _selectedMonths = new ObservableCollection<int>() {1,2,3,4,5,6,7,8,9,10,11,12};

        public ObservableCollection<int> Selected
        {
            get
            {
                return _selectedMonths;
            }
            set
            {
                _selectedMonths = value;
                updateHistogramItemsSource();
                OnPropertyChanged();
            }
        }

        private List<int> _histogramMonthOptions = new List<int>();
        public List<int> HistogramMonthOptions
        {
            get { return _histogramMonthOptions; }
            set
            {
                _histogramMonthOptions = value; 
                OnPropertyChanged();
            }
        }


        public ObservableCollection<Model> TipoDeSolucionItemsSource { get; set; } = new ObservableCollection<Model>();
        public ObservableCollection<Model> TecnologiaItemsSource { get; set; } = new ObservableCollection<Model>();
        public ObservableCollection<MonthModel> TestItemsSource { get; set; } = new ObservableCollection<MonthModel>();
        public List<string> Movimientos { get; private set; } = new List<string>();


        public List<MovimientoMensualDeEnlacesViewModel[]> ViewModels => _dic.Values.ToList();
        private ObservableCollection<TareaDetalleViewModel> _tareas = new ObservableCollection<TareaDetalleViewModel>();


        private ObservableCollection<EnlaceCerradoViewModel> _enlacesCerrados = new ObservableCollection<EnlaceCerradoViewModel>();
        public ObservableCollection<EnlaceCerradoViewModel> PseudoTareas
        {
            get { return _enlacesCerrados; }
            set
            {
                _enlacesCerrados = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<TareaDetalleViewModel> Tareas
        {
            get { return _tareas; }
            set
            {
                _tareas = value;
                OnPropertyChanged();
            }
        }

        private TareaDetalleViewModel _tareasSeleccionada;
        public TareaDetalleViewModel TareasSeleccionada
        {
            get { return _tareasSeleccionada; }
            set
            {
                _tareasSeleccionada = value;
                OnPropertyChanged();
            }
        }


        private ObservableCollection<EnlaceCerradoViewModel> _itemsSource = new ObservableCollection<EnlaceCerradoViewModel>();

        public ObservableCollection<EnlaceCerradoViewModel> ItemsSource
        {
            get
            {
                return _itemsSource;
            }
            set
            {
                if (value != _itemsSource)
                {
                    _itemsSource = value;
                    OnPropertyChanged();
                }
            }
        }


        private ObservableCollection<EnlaceCerradoViewModel> _histogramItemsSource = new ObservableCollection<EnlaceCerradoViewModel>();
        public ObservableCollection<EnlaceCerradoViewModel> HistogramItemsSource
        {
            get { return _histogramItemsSource; }
            set
            {
                _histogramItemsSource = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<TaskDetails> _taskCollection;
        public ObservableCollection<TaskDetails> TaskCollection
        {
            get { return _taskCollection; }
            set { _taskCollection = value; }
        }

        private EnlaceCerradoViewModel _selectedEnlaceCerrado;
        public EnlaceCerradoViewModel SelectedEnlaceCerrado
        {
            get { return _selectedEnlaceCerrado; }
            set
            {
                if (value == _selectedEnlaceCerrado) return;
                _selectedEnlaceCerrado = value;
                updateTareas(value);

                DetailsViewModel.Enlace = value;
                OnPropertyChanged();
            }
        }

        public MovimientoMensualDeEnlacesViewModel[] ViewModel
        {
            get
            {

                var result = _viewModelStrategy[SelectedInbox];
                return result;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public PlotModel PlotViewModel
        {
            //get { return _plotDic[SelectedInbox]; }
            get { return _chartViewModel.GetPlotModelForInbox(SelectedInbox); }
        }

        private List<string> _inboxList = new List<string>();
        public List<string> InboxList
        {
            get
            {
                _inboxList.Sort();
                return _inboxList;
            }
        }


        private string _selectedInbox;
        public string SelectedInbox
        {
            get { return _selectedInbox; }
            set
            {
                _selectedInbox = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(ViewModel));
                OnPropertyChanged(nameof(PlotViewModel));
                obtenerEnlacesParaInboxSeleccionado();
                updateItemsSource();
                updateHistogramItemsSource();
                updatePieChart();
                updatePiesChart2();
                updateTestItemsSource();

            }
        }

        //---------------------------------------------------------------------
        // Private
        //---------------------------------------------------------------------
        private void obtenerEnlacesParaInboxSeleccionado()
        {
            var o = new ObservableCollection<EnlaceCerradoViewModel>();

            //PseudoTareas.Clear();
            if (SelectedInbox == null) return;


            var cerrados = _enlacesCerradosModelRepository.SelectAll().Where(e => e.Inbox == SelectedInbox);

            foreach (var e in cerrados)
            {
                //PseudoTareas.Add(new EnlaceCerradoViewModel(e));
                o.Add(new EnlaceCerradoViewModel(e));
            }

            PseudoTareas = o;
        }


        private void updateTareas(EnlaceCerradoViewModel ecvm)
        {
            var temp = new ObservableCollection<TareaDetalleViewModel>();
            Tareas.Clear();

            if (ecvm != null)
            {
                var pseudoEnlace = ecvm.PseudoEnlaceProperty.Value;
                var cerradas = _tareasCerradasModelRepository.SelectAll().Where(t => t.PseudoEnlace == pseudoEnlace);
                var ingresadas = _tareasIngresadasModelRepository.SelectAll().Where(t => t.PseudoEnlace == pseudoEnlace);

                foreach (var tareaCerrada in cerradas)
                {
                    var vm = new TareaDetalleViewModel(tareaCerrada);
                    temp.Add(vm);
                }


                foreach (var ingresada in ingresadas)
                {
                    if (ingresada.Estado == Estado.Pendiente)
                    {
                        temp.Add(new TareaDetalleViewModel(ingresada));
                    }
                }

                foreach (var item in temp.OrderBy(t => t.FechaInicioProperty.Value))
                {
                    Tareas.Add(item);
                }

                TareasSeleccionada = Tareas.FirstOrDefault();
                updateGanttTasks();

                OnPropertyChanged(nameof(SelectedEnlaceCerrado));
                OnPropertyChanged(nameof(TaskCollection));
            }



        }


        private void updateItemsSource()
        {
            var itemsSource = PseudoTareas.Where(e => TipoDeMovimientoComboBoxViewModel.SelectedItems.Contains(e.TipoDeMovimientoProperty.Value)).ToObservableCollection();
            itemsSource = itemsSource.Where(e => TipoDeSolucionComboBoxViewModel.SelectedItems.Contains(e.TipoDeSolucionProperty.Value)).ToObservableCollection();
            itemsSource = itemsSource.Where(e => ZonaComboBoxViewModel.SelectedItems.Contains(e.ZonaProperty.Value)).ToObservableCollection();
            ItemsSource = itemsSource;
        }

        private void updateHistogramItemsSource()
        {
            //var newHistogramItemsSource = PseudoTareas.Where(e => SelectedMovimientos.Contains(e.TipoDeMovimientoProperty.Value)).ToObservableCollection();
            var newHistogramItemsSource = ItemsSource.Where(e => true).ToObservableCollection();
            newHistogramItemsSource = newHistogramItemsSource.Where(e => e.FechaDeCierreProperty.Value.Year == AnnioComboBoxViewModel.SelectedItem).ToObservableCollection();
            newHistogramItemsSource = newHistogramItemsSource.Where(e => Selected.Contains(e.FechaDeCierreProperty.Value.Month)).ToObservableCollection();
            HistogramItemsSource = newHistogramItemsSource;
        }


        private void updatePlotModels()
        {
            if (_viewModelStrategy == null) return;

            foreach (var i in InboxList)
            {
                _chartViewModel.CreatePlotModel(_viewModelStrategy.Strategy.ObtenerMovimientoAnual(i, AnnioComboBoxViewModel.SelectedItem));
            }
        }

        private void updatePieChart()
        {
            TipoDeSolucionItemsSource.Clear();

            foreach (var tipo in HistogramItemsSource.Select(e => e.TipoDeSolucionProperty.Value).Distinct())
            {
                this.TipoDeSolucionItemsSource.Add(new Model()
                {
                    Name = tipo,
                    Count = HistogramItemsSource.Count(e => e.TipoDeSolucionProperty.Value == tipo)
                });
            }
        }

        private void updatePiesChart2()
        {
            TecnologiaItemsSource.Clear();
            foreach (var tipo in HistogramItemsSource.Select(e => e.TecnologiaProperty.Value).Distinct())
            {
                this.TecnologiaItemsSource.Add(new Model()
                {
                    Name = tipo,
                    Count = HistogramItemsSource.Count(e => e.TecnologiaProperty.Value == tipo)
                });
            }
        }

        private void updateTestItemsSource()
        {
            TestItemsSource.Clear();
            foreach (var mes in MonthHelper.Enumerate)
            {
                var m = new MonthModel() { Mes = mes };
                var m1 = ItemsSource.Where(e => e.FechaDeCierreProperty.Value.Month == mes);

                foreach (var e in m1)
                {
                    var tipo = e.TipoDeSolucionProperty.Value;
                    string tipo2 = "";


                    if (tipo.StartsWith("A"))
                    {
                        m.A += 1;
                    }
                    else if (tipo.StartsWith("B"))
                    {
                        m.B += 1;
                    }
                    else if (tipo.StartsWith("C"))
                    {
                        m.C += 1;
                    }
                    else if (tipo.StartsWith("Z"))
                    {
                        m.Z += 1;
                    }
                    else
                    {
                        m.Otros += 1;
                    }

                }
                TestItemsSource.Add(m);
            }

        }

        private void updateGanttTasks()
        {
            _taskCollection = new ObservableCollection<TaskDetails>();
            var tareas = Tareas.OrderBy(t => t.FechaInicioProperty.Value);
            foreach (var tarea in tareas)
            {
                if (tarea.ResultadoProperty.Value == "Pendiente") continue;

                var start = tarea.FechaInicioProperty.Value;
                var end = tarea.FechaCierreProperty.Value ?? DateTime.Now;
                var duration = (end - start);

                var task = (new TaskDetails()
                {
                    TaskId = int.Parse(tarea.TareaIdProperty.Value),
                    StartDate = start,
                    FinishDate = end,
                    TaskName = tarea.InboxProperty.Value,
                    Duration = duration
                });


                task.Resources.Add(new Resource()
                {
                    Name = task.TaskName,

                });

                _taskCollection.Add(task);

            }
        }

        private EnlaceDataGridDetailsViewModel _enlaceDataGridDetailsViewModel;
        public EnlaceDataGridDetailsViewModel DetailsViewModel
        {
            get
            {
                return _enlaceDataGridDetailsViewModel;
            }
            private set
            {
                _enlaceDataGridDetailsViewModel = value;

            }
        }

        private EnlaceCerradoViewModel _selectedEnlace;
        public EnlaceCerradoViewModel SelectedEnlace
        {
            get
            {
                return _selectedEnlace;
            }
            set
            {
                if (value != _selectedEnlace)
                {
                    _selectedEnlace = value;
                    DetailsViewModel.Enlace = value;
                    OnPropertyChanged();
                }

            }
        }

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
