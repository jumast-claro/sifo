﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SIFO.DesktopUI.SyncfusionControls.PseudoTareas.Details
{
    /// <summary>
    /// Interaction logic for DetalleDeTareasMainView.xaml
    /// </summary>
    public partial class DetalleDeTareasMainView : UserControl
    {
        public static DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(object), typeof(DetalleDeTareasMainView));
        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register("SelectedItem", typeof(object), typeof(DetalleDeTareasMainView), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty TareasGanttProperty = DependencyProperty.Register("TareasGantt", typeof(object), typeof(DetalleDeTareasMainView));
        public static DependencyProperty HighlightedTareasGanttProperty = DependencyProperty.Register("HighlightedTareasGantt", typeof(object), typeof(DetalleDeTareasMainView));

        public DetalleDeTareasMainView()
        {
            InitializeComponent();
        }

        public object ItemsSource
        {
            get
            {
                return GetValue(ItemsSourceProperty);
            }
            set
            {
                SetValue(ItemsSourceProperty, value);
            }
        }

        public object SelectedItem
        {
            get { return GetValue(SelectedItemProperty); }
            set
            {
                SetValue(SelectedItemProperty, value);
            }
        }

        public object TareasGantt
        {
            get
            {
                return GetValue(TareasGanttProperty);
            }
            set
            {
                SetValue(TareasGanttProperty, value);
            }
        }

        public object HighlightedTareasGantt
        {
            get
            {
                return GetValue(HighlightedTareasGanttProperty);
            }
            set
            {
                SetValue(HighlightedTareasGanttProperty, value);
            }
        }
    }
}
