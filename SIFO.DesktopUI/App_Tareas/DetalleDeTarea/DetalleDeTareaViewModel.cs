﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SIFO.Model;

namespace SIFO.DesktopUI.App_Tareas
{
    public class DetalleDeTareaViewModel
    {
        public DetalleDeTareaViewModel(ITarea tarea)
        {
            TareaId = tarea.TareaId;
        }

        public string TareaId { get; private set; }
    }
}
