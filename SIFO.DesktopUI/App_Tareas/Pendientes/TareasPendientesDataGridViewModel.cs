﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using SIFO.DesktopUI.App_Enlaces;
using SIFO.DesktopUI.App_Vantive;
using SIFO.DesktopUI.App_Vantive.SyncfusionDataGrid;
using SIFO.DesktopUI.Commands;
using SIFO.DesktopUI.TareasIngresadas;
using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.Properties;

namespace SIFO.DesktopUI.App_Tareas.Pendientes
{
    public class TareasPendientesDataGridViewModel : INotifyPropertyChanged
    {
        private readonly List<SfTareaPendienteViewModel> _all = new List<SfTareaPendienteViewModel>();

        public TareasPendientesDataGridViewModel(ITareasPendientesModelRepository modelRepository, ITareasIngresadasModelRepository tareasIngresadasModelRepository, ITareasCerradasModelRepository tareasCerradasModelRepository)
        {
            var tareasPendientes = modelRepository.SelectAll();

            foreach (var tarePendiente in tareasPendientes)
            {
                var viewModel = new SfTareaPendienteViewModel(tarePendiente);
                _all.Add(viewModel);
            }


            var fechaDeBacklog = tareasPendientes.Select(t => t.FechaInicioTarea).Max();
            HeaderTextProperty = new ReadOnlyProperty<string>($"Tareas pendientes al {fechaDeBacklog.ToString("dd/MM/yyyy")}");

            DetailsViewModel = new EnlaceDataGridDetailsViewModel(tareasCerradasModelRepository, tareasIngresadasModelRepository);

            MostrarDetalleDeTareasCommand = new MostrarDetallesDeTareaCommand(tareasIngresadasModelRepository, tareasCerradasModelRepository);

        }


        public List<SfTareaPendienteViewModel> TareasPendientes => _all;

        public IBindableProperty<string> HeaderTextProperty { get; set; }

        private SfTareaPendienteViewModel _selectedTareaPendiente;
        public SfTareaPendienteViewModel SelectedTareaPendiente
        {
            get { return _selectedTareaPendiente; }
            set
            {
                _selectedTareaPendiente = value;
                DetailsViewModel.Enlace = value;
                OnPropertyChanged();
            }
        }

        private EnlaceDataGridDetailsViewModel _enlaceDataGridDetailsViewModel;
        public EnlaceDataGridDetailsViewModel DetailsViewModel
        {
            get
            {
                return _enlaceDataGridDetailsViewModel;
            }
            private set
            {
                _enlaceDataGridDetailsViewModel = value;

            }
        }

        public ICommand MostrarDetalleDeTareasCommand { get; private set; }

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }



    }
}
