﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento;
using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.Properties;
using SIFO.Model;

namespace SIFO.DesktopUI.App_Tareas.Pendientes
{
    public class SfTareaPendienteViewModel : IEnlaceViewModel
    {
        private readonly ITarea _model;

        public SfTareaPendienteViewModel(ITarea tareaPendiente)
        {
            _model = tareaPendiente;

            _inicializarPropiedades();
        }

        public ITarea Model => _model;

        private void _inicializarPropiedades()
        {

            if (!Model.FechaDeInstalacion.HasValue)
            {
                var x = 5;
            }


            AnnioDeInicioProperty = new ReadOnlyProperty<int>(_model.FechaInicioTarea.Year);
            MesDeInicioProperty = new ReadOnlyProperty<int>(_model.FechaInicioTarea.Month); 
            PlantaProperty = new WrappingViewModelProperty<string>(() => _model.Planta == Planta.PlantaExterna ? "PE" : (_model.Planta == Planta.PlantaInterna ? "PI" : ""));



            RegionFinalProperty = new WrappingViewModelProperty<string>(() => _model.RegionFinal);
            ZonaProperty = new WrappingViewModelProperty<string>(() => _model.Zona.ToString());
            InboxProperty = new ReadOnlyProperty<string>(_model.Inbox);
            InboxFinalProperty = new ReadOnlyProperty<string>(_model.InboxFinal);
            TecnologiaProperty = new ReadOnlyProperty<string>(_model.TecnologiaBis == TecnologiaEnum.FibraOptica ? "FO" : _model.TecnologiaBis == TecnologiaEnum.Inalambrico ? "Inal." : _model.TecnologiaBis == TecnologiaEnum.NoAplica ? "N/A" : "?");
            ProductoProperty = new WrappingViewModelProperty<string>(() => _model.Producto);
            TipoDeOrdenProperty = new ReadOnlyProperty<string>(_model.TipoDeOrden);

            TipoDeSolucionProperty = new ReadOnlyProperty<string>(_model.TipoSolucion, _model.TipoSolucion == String.Empty ? "?" : _model.CodigoTipificacion);

            FechaInicioOrdenProperty = new WrappingViewModelProperty<string>(() => _model.FechaInicioOrden.HasValue ? _model.FechaInicioOrden.Value.ToString("dd/MM/yyyy") : "?");
            FechaCompromisoOrdenProperty = new ReadOnlyProperty<DateTime?>(_model.FechaDeInstalacion, _model.FechaDeInstalacion.HasValue ? _model.FechaDeInstalacion.Value.ToString("dd/MM/yyyy") : "");
            FechaRenegociadaProperty = new WrappingViewModelProperty<string>(() => _model.FechaRenegociada.HasValue ? _model.FechaRenegociada.Value.ToString("dd/MM/yyyy") : "");
            FechaInstalacionProperty = new WrappingViewModelProperty<string>(() => _model.FechaDeInstalacion.HasValue ? _model.FechaDeInstalacion.Value.ToString("dd/MM/yyy") : "");


            TiempoMaximoDePermanenciaEnTodasLasBandejasProperty = new WrappingViewModelProperty<int>(() => _model.DiasDeInstalacion ?? 0, (value) => value == 0 ? "?" : value.ToString());

            FechaInicioProperty = new ReadOnlyProperty<DateTime>(_model.FechaInicioTarea, _model.FechaInicioTarea.ToString("dd/MM/yyyy"));
            TomadoPorProperty = new ReadOnlyProperty<string>(_model.TomadoPor);
            ClienteProperty = new ReadOnlyProperty<string>(_model.Cliente);
            OrdenProperty = new ReadOnlyProperty<long>(_model.Orden);
            NumeroEnlaceProperty = new WrappingViewModelProperty<string>(() => _model.PseudoEnlace);
            TareaIdProperty = new ReadOnlyProperty<string>(_model.TareaId);
            IdentificadorProperty = new WrappingViewModelProperty<string>(() => _model.Identificador);

            EsProyectoEspecialProperty = new WrappingViewModelProperty<bool>(() => _model.EsProyectoEspecial, (value) => value ? "Sí" : "No");

            IngresoOffTimeProperty = new ReadOnlyProperty<bool>(_model.IngresaOffTime, _model.IngresaOffTime ? "Sí" : "No");
            ZonaSinInfo = new WrappingViewModelProperty<bool>(() => _model.Zona == Zona.SinInfo);
            SinEnlace = new WrappingViewModelProperty<bool>(() => _model.Enlace == 0);


            EsAltaPuraProperty = new WrappingViewModelProperty<bool>(() => _model.EsAltaPura, (value) => value ? "Sí" : "No");

            TipoDeMovimientoProperty = new ReadOnlyProperty<string>(_model.TipoDeMovimiento);
            WorkflowFechaDeInicioProperty = new ReadWriteProperty<DateTime?>(Model.FechaInicioWorkflow);

            DueDateProperty = new ReadWriteProperty<DateTime?>(_model.FechaDeInstalacion);

            string vencida;

            if (!_model.FechaDeInstalacion.HasValue)
            {
                vencida = "No aplica";
            }
            else
            {
                if (_model.FechaDeInstalacion.Value > DateTime.Now)
                {
                    vencida = "No";
                }
                else
                {
                    vencida = "Sí";
                }
            }

            VencidaProperty = new ReadOnlyProperty<string>(vencida);


            bool? igualFechas = null;

            if (_model.FechaInicioWorkflow.HasValue)
            {
                var inicioWf = _model.FechaInicioWorkflow.Value;
                var inicioTarea = _model.FechaInicioTarea;

                if(inicioTarea.Year == inicioWf.Year && inicioTarea.Month == inicioWf.Month && inicioTarea.Day == inicioWf.Day)
                {
                    igualFechas = true;
                }
                else
                {
                    igualFechas = false;
                }
            }

            InicioWorkFlowIgualInicioTareaProperty = new ReadOnlyProperty<bool?>(igualFechas);

            if (!_model.FechaDeInstalacion.HasValue)
            {
                DiasParaVencimientoProperty = new ReadOnlyProperty<TimeSpan?>(null);
            }
            else
            {
                DiasParaVencimientoProperty = new ReadOnlyProperty<TimeSpan?>(_model.FechaDeInstalacion - DateTime.Now);
            }


            DiasDesdeInicioProperty = new ReadOnlyProperty<TimeSpan>(DateTime.Now - _model.FechaInicioTarea);

            PseudoEnlaceProperty = new ReadOnlyProperty<string>(_model.PseudoEnlace);

            DireccionProperty = new ReadOnlyProperty<string>(_model.Calle + " " + _model.Numero);





        }

        //[Filter("Está cerrada")]
        //public IBindableProperty<bool> EstaCerradaProperty { get; private set; }

        public IBindableProperty<int> AnnioDeInicioProperty { get; set; }
        public IBindableProperty<int> MesDeInicioProperty { get; set; }
        public IBindableProperty<string> TareaIdProperty { get; private set; }
        public IBindableProperty<long> OrdenProperty { get; private set; }
        public IBindableProperty<string> NumeroEnlaceProperty { get; private set; }
        public IBindableProperty<string> TomadoPorProperty { get; set; }

        public IBindableProperty<string> PlantaProperty { get; private set; }
        public IBindableProperty<string> ZonaProperty { get; private set; }
        public IBindableProperty<string> RegionFinalProperty { get; private set; }
        public IBindableProperty<string> InboxProperty { get; private set; }
        public IBindableProperty<string> PseudoEnlaceProperty { get; private set; }
        public IBindableProperty<string> InboxFinalProperty { get; set; }
        public IBindableProperty<string> TecnologiaProperty { get; set; }
        public IBindableProperty<string> TipoDeOrdenProperty { get; set; }
        public IBindableProperty<string> TipoDeMovimientoProperty { get; set; }
        public IBindableProperty<bool> EsAltaPuraProperty { get; set; }
        public IBindableProperty<bool> EsProyectoEspecialProperty { get; private set; }
        public IBindableProperty<string> FechaInicioOrdenProperty { get; private set; }
        public IBindableProperty<DateTime?> FechaCompromisoOrdenProperty { get; set; }
        public IBindableProperty<string> FechaRenegociadaProperty { get; set; }
        public IBindableProperty<string> FechaInstalacionProperty { get; set; }
        // Fecha instalacion
        public IBindableProperty<string> TipoDeSolucionProperty { get; set; }
        public IBindableProperty<int> TiempoMaximoDePermanenciaEnTodasLasBandejasProperty { get; set; }
        public IBindableProperty<DateTime> FechaInicioProperty { get; private set; }

        public IBindableProperty<string> ClienteProperty { get; private set; }
        public IBindableProperty<string> IdentificadorProperty { get; private set; }

        public IBindableProperty<string> ProductoProperty { get; set; }

        public IBindableProperty<bool> IngresoOffTimeProperty { get; set; }
        public IBindableProperty<bool> ZonaSinInfo { get; set; }
        public IBindableProperty<bool> SinEnlace { get; set; }

        public IBindableProperty<DateTime?> WorkflowFechaDeInicioProperty { get; private set; }

        public IBindableProperty<DateTime?> DueDateProperty { get; set; }

        public IBindableProperty<string> VencidaProperty { get; set; }

        public IBindableProperty<TimeSpan?> DiasParaVencimientoProperty { get; set; }

        public IBindableProperty<bool?> InicioWorkFlowIgualInicioTareaProperty { get; set; }

        public IBindableProperty<TimeSpan> DiasDesdeInicioProperty { get; set; }

        public IBindableProperty<string> DireccionProperty { get; set; }
    }
}
