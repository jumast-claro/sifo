﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Syncfusion.UI.Xaml.Grid;

namespace SIFO.DesktopUI.App_Tareas.Pendientes.SyncfusionDataGrid
{
    /// <summary>
    /// Interaction logic for TareasPendientesSyncfusionDataGrid.xaml
    /// </summary>
    public partial class TareasPendientesSyncfusionDataGrid : UserControl
    {
        public static DependencyProperty SfDataGridProperty = DependencyProperty.Register("SfDataGrid", typeof(SfDataGrid), typeof(TareasPendientesSyncfusionDataGrid));

        public TareasPendientesSyncfusionDataGrid()
        {
            InitializeComponent();
            SfDataGrid = this.sfDataGrid;
        }


        public SfDataGrid SfDataGrid
        {
            get { return (SfDataGrid)GetValue(SfDataGridProperty); }
            set
            {
                SetValue(SfDataGridProperty, value);
            }
        }
    }
}
