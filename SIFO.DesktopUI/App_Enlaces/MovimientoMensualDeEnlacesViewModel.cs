using System.Collections;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.Properties;
using Syncfusion.UI.Xaml.Charts;

namespace SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento
{
    public class MovimientoMensualDeEnlacesViewModel : INotifyPropertyChanged
    {
        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------
        public MovimientoMensualDeEnlacesViewModel(MovimientoMensualDeEnlaces model)
        {

            if (model.Ingresadas == 0 && model.Cerradas == 0)
            {
                CantCerradasProperty = new ReadOnlyProperty<int>(0, "");
                CantPendientesInicialProperty = new ReadOnlyProperty<int>(0, "");
                CantPendientesFinalProperty = new ReadOnlyProperty<int>(0, "");
            }
            else
            {
                CantCerradasProperty = createBindableProperty(model.Cerradas);
                CantPendientesInicialProperty = createBindableProperty(model.PendientesInicial);
                CantPendientesFinalProperty = createBindableProperty(model.PendientesFinal);
            }

            if (model.Ingresadas == 0)
            {
                CantIngresadasProperty = new ReadOnlyProperty<int>(0, "");
            }
            else
            {
                CantIngresadasProperty = createBindableProperty(model.Ingresadas);
            }

            if (model.CerradasExito == 0)
            {
                CantExitoProperty = createNullBindableProperty();
                CantFracasoProperty = createNullBindableProperty();
                CantOnTimeProperty = createNullBindableProperty();
                CantOffTimeProperty = createNullBindableProperty();
                CantIndefinidasProperty = createNullBindableProperty();
                CantOffTimeOnTimeProperty = createNullBindableProperty();
                CantOffTimeOffTimeProperty = createNullBindableProperty();
                CantOffTimeIndefinidoProperty = createNullBindableProperty();

            }

            else
            {
                CantExitoProperty = createBindableProperty(model.CerradasExito);
                CantFracasoProperty = createBindableProperty(model.CerradasFracaso);
                CantOnTimeProperty = new ReadOnlyProperty<int>(model.OnTime, model.OnTime.ToString() + $" ({model.OnTimePermanenciaEnPorcentaje.ToString("F1")}%)");
                CantOffTimeProperty = new ReadOnlyProperty<int>(model.OffTime, model.OffTime.ToString() + $" ({model.OffTimePermanenciaEnPorcentaje.ToString("F1")}%)");
                CantIndefinidasProperty = new ReadOnlyProperty<int>(model.OnTimeIndefinido);
                CantNoAplicaProperty = createBindableProperty(model.OnTimeNoAplica);
                CantOffTimeOnTimeProperty = createBindableProperty(model.OffTimePermanenciaOnTimeGeneral);
                CantOffTimeOffTimeProperty = createBindableProperty(model.OffTimePermanenciaOffTimeGeneral);
                CantOffTimeIndefinidoProperty = createBindableProperty(model.OffTimePermanenciaIndefinidoTimeGeneral);
            }


            if (model.CerradasExito == 0)
            {
                OnTimePermanenciaEnPorcentaje = new ReadOnlyProperty<double>(0, "");
                OffTimePermanenciaEnPorcentaje = new ReadOnlyProperty<double>(0, "");
            }
            else
            {
                OnTimePermanenciaEnPorcentaje = new ReadOnlyProperty<double>(model.OnTimePermanenciaEnPorcentaje, model.OnTimePermanenciaEnPorcentaje.ToString("F1") + "%");
                OffTimePermanenciaEnPorcentaje = new ReadOnlyProperty<double>(model.OffTimePermanenciaEnPorcentaje, model.OffTimePermanenciaEnPorcentaje.ToString("F1") + "%");
            }

        }

        public MovimientoMensualDeEnlacesViewModel()
        {
            CantCerradasProperty = createBindableProperty();
            CantExitoProperty = createBindableProperty();
            CantFracasoProperty = createBindableProperty();
            CantIndefinidasProperty = createBindableProperty();
            CantOffTimeOffTimeProperty = createBindableProperty();
            CantIngresadasProperty = createBindableProperty();
            CantNoAplicaProperty = createBindableProperty();
            CantOffTimeOffTimeProperty = createBindableProperty();
            CantOffTimeOnTimeProperty = createBindableProperty();
            CantOffTimeIndefinidoProperty = createBindableProperty();
            CantOnTimeProperty = createBindableProperty();
            CantPendientesFinalProperty = createBindableProperty();
            CantPendientesInicialProperty = createBindableProperty();

            
        }

        //---------------------------------------------------------------------
        // Properties
        //---------------------------------------------------------------------
        public int Mes { get; set; }
        public IBindableProperty<int> CantCerradasProperty { get; set; }
        public IBindableProperty<int> CantPendientesInicialProperty { get; set; }
        public IBindableProperty<int> CantIngresadasProperty { get; set; }
        public IBindableProperty<int> CantExitoProperty { get; set; }
        public IBindableProperty<int> CantFracasoProperty { get; set; }
        public IBindableProperty<int> CantOnTimeProperty { get; set; }
        public IBindableProperty<int> CantOffTimeProperty { get; set; }
        public IBindableProperty<int> CantOffTimeOnTimeProperty { get; set; }
        public IBindableProperty<int> CantOffTimeOffTimeProperty { get; set; }
        public IBindableProperty<int> CantOffTimeIndefinidoProperty { get; set; }
        public IBindableProperty<int> CantPendientesFinalProperty { get; set; } 
        public IBindableProperty<int> CantIndefinidasProperty { get; set; } 
        public IBindableProperty<int> CantNoAplicaProperty { get; set; }

        public IBindableProperty<double> OnTimePermanenciaEnPorcentaje { get; set; }
        public IBindableProperty<double> OffTimePermanenciaEnPorcentaje { get; set; }


        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        private IBindableProperty<int> createBindableProperty(int value)
        {
            return new ReadWriteProperty<int>(value);
        }

        private IBindableProperty<int> createBindableProperty()
        {
            return new ReadWriteProperty<int>(0);
        }

        private IBindableProperty<int> createNullBindableProperty()
        {
            return new ReadOnlyProperty<int>(0, "");
        }
    }
}