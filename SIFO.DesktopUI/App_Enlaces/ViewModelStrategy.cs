using System.Collections.Generic;
using System.Linq;
using SIFO.Model;

namespace SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento
{
    public class ViewModelStrategy
    {
        private readonly IEnumerable<Enlace> _ingreados;
        private readonly IEnumerable<IEnlaceCerrado> _cerrados;
        private readonly IEnumerable<IEnlace> _pendientesAlInicio2017;
        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        private readonly MovimientoMensualDeEnlacesStrategy _strategy;
        private readonly Dictionary<string, MovimientoMensualDeEnlacesViewModel[]> _dic = new Dictionary<string, MovimientoMensualDeEnlacesViewModel[]>();

        //---------------------------------------------------------------------
        // Constructor
        //---------------------------------------------------------------------
        public ViewModelStrategy(IEnumerable<Enlace> ingreados, IEnumerable<IEnlaceCerrado> cerrados, IEnumerable<IEnlace> pendientesAlInicio2017)
        {
            _ingreados = ingreados;
            _cerrados = cerrados;
            _pendientesAlInicio2017 = pendientesAlInicio2017;
            _strategy = new MovimientoMensualDeEnlacesStrategy(_ingreados, _cerrados, _pendientesAlInicio2017);
        }

        //---------------------------------------------------------------------
        // Public
        //---------------------------------------------------------------------
        public MovimientoMensualDeEnlacesViewModel[] this[string inbox]
        {
            get
            {
                if (!_dic.ContainsKey(inbox)) return null;
                return _dic[inbox];
            }
        }

        public MovimientoMensualDeEnlacesStrategy Strategy
        {
            get { return _strategy; }
        }

        public void Procesar(int annio)
        {
            _strategy.Procesar();

            var inboxs = _strategy.MovimientosMensuales.Select(m => m.Inbox).Distinct();


            foreach (var inbox in inboxs)
            {
                var movimientosMensualesParaInbox = _strategy.MovimientosMensuales.Where(m => m.Inbox == inbox && m.Annio == annio);
                var movimientoMensualArray = new MovimientoMensualDeEnlacesViewModel[13];
                for (int i = 1; i <= 12; i++)
                {
                    //var movimientoMensualViewModel = movimientosMensualesParaInbox.Where(m => m.Mes == i).Select(Map).FirstOrDefault();
                    var movimientoMensualViewModel = movimientosMensualesParaInbox.Where(m => m.Mes == i).Select(m => new MovimientoMensualDeEnlacesViewModel(m)).FirstOrDefault();
                    movimientoMensualArray[i] = movimientoMensualViewModel;
                }

                _dic[inbox] = movimientoMensualArray;
            }
        }

        public MovimientoMensualDeEnlaces[] GetModelForInbox(string inbox, int annio)
        {
            var movimientosMensualesParaInbox = _strategy.MovimientosMensuales.Where(m => m.Inbox == inbox && m.Annio == annio);
            var movimientoMensualArray = new MovimientoMensualDeEnlaces[13];
            for (int i = 1; i <= 12; i++)
            {
                var movimientoMensualViewModel = movimientosMensualesParaInbox.Where(m => m.Mes == i).FirstOrDefault();
                if (movimientoMensualViewModel == null)
                {
                    movimientoMensualViewModel = new MovimientoMensualDeEnlaces(inbox, i, annio);
                }

                movimientoMensualArray[i] = movimientoMensualViewModel;
            }
            return movimientoMensualArray;
        }
    }
}