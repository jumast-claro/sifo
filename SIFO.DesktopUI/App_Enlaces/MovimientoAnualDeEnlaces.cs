namespace SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento
{
    public class MovimientoAnualDeEnlaces
    {
        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        private readonly string _inbox;
        private readonly int _annio;
        private readonly MovimientoMensualDeEnlaces[] _array;

        //---------------------------------------------------------------------
        // Constructor
        //---------------------------------------------------------------------
        public MovimientoAnualDeEnlaces(string inbox, int annio)
        {
            _inbox = inbox;
            _annio = annio;
            _array = new MovimientoMensualDeEnlaces[13];
            for (int i = 1; i <= 12; i++)
            {
                _array[i] = new MovimientoMensualDeEnlaces(inbox, i, annio);
            }
        }

        //---------------------------------------------------------------------
        // Public
        //---------------------------------------------------------------------
        public MovimientoMensualDeEnlaces this[int mes]
        {
            get { return _array[mes]; }
            set { _array[mes] = value; }
        }
        public int Annio => _annio;
        public string Inbox => _inbox;
    }
}