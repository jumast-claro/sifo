﻿using System.Collections;
using System.Collections.Generic;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento;
using SIFO.DesktopUI.Utilities;

namespace SIFO.DesktopUI
{
    public class MonthHelper
    {
        private static readonly List<int> _months = new List<int> {1,2,3,4,5,6,7,8,9,10,11,12};
        public static IEnumerable<int> Enumerate => _months; 
    }

    //public enum Meses
    //{
    //    Enero = 1,
    //    Febrero = 2,
    //    Marzo = 3,
    //    Abril = 4,
    //    Mayo = 5,
    //    Junio = 6,
    //    Julio = 7,
    //    Agosto = 8,
    //    Septiembre = 9,
    //    Octubre = 10,
    //    Noviembre = 11,
    //    Diciembre = 12

    //}

    public sealed class MovimientoDeEnlacesChartViewModel
    {
        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        private readonly Dictionary<string, PlotModel> _plotDic = new Dictionary<string, PlotModel>();


        //---------------------------------------------------------------------
        // Methods
        //---------------------------------------------------------------------
        public PlotModel GetPlotModelForInbox(string inbox)
        {
            if (!_plotDic.ContainsKey(inbox)) return null;
            return _plotDic[inbox];
        }

        public void CreatePlotModel(MovimientoAnualDeEnlaces movimientoAnual)
        {
            var plot = new PlotModel();

            plot.Axes.Add(new LinearAxis()
            {
                IsPanEnabled = false,
                IsZoomEnabled = false
            });


            var ingresadas = createColumnSeries("Ingresadas", OxyColors.SaddleBrown, "0");
            ingresadas.TextColor = OxyColors.WhiteSmoke;

            var onTime = createColumnSeries("OnTime", OxyColors.MediumSeaGreen, "1");
            var offTime = createColumnSeries("OffTime", OxyColors.PaleVioletRed, "1");
            var indefinido = createColumnSeries("Indefinido", OxyColors.Orange, "1");
            var fracasadas = createColumnSeries("Fracasadas", OxyColors.DarkGray, "1");
            var backlog = createColumnSeries("Backlog", OxyColors.LightBlue, "2");


            foreach (var mes in MonthHelper.Enumerate)
            {
                var movimientoMensual = movimientoAnual[mes];
                var index = mes - 1;

                addColumnItem(ingresadas, index, movimientoMensual.Ingresadas);
                addColumnItem(onTime, index, movimientoMensual.OnTime);
                addColumnItem(offTime, index, movimientoMensual.OffTime);
                addColumnItem(indefinido, index, movimientoMensual.OnTimeIndefinido);
                addColumnItem(fracasadas, index, movimientoMensual.CerradasFracaso);
                addColumnItem(backlog, index, movimientoMensual.PendientesFinal);
            }


            plot.Series.Add(ingresadas);
            plot.Series.Add(onTime);
            plot.Series.Add(offTime);
            plot.Series.Add(indefinido);
            plot.Series.Add(fracasadas);
            plot.Series.Add(backlog);
            plot.Axes.Add(crearEjeX(""));
            agregarPlotModel(movimientoAnual.Inbox, plot);
        }

        //---------------------------------------------------------------------
        // Private
        //---------------------------------------------------------------------
        private void addColumnItem(ColumnSeries columnSeries, int index, int value)
        {
            columnSeries.Items.Add(new ColumnItem()
            {
                CategoryIndex = index,
                Value = value
            });
        }

        private ColumnSeries createColumnSeries(string title, OxyColor fillColor, string stackGroup)
        {
            var columnSeries = new ColumnSeries();
            columnSeries.IsStacked = true;
            columnSeries.LabelPlacement = LabelPlacement.Middle;
            columnSeries.LabelFormatString = "{0}";
            columnSeries.ColumnWidth = 120;

            columnSeries.Title = title;
            columnSeries.FillColor = fillColor;
            columnSeries.StackGroup = stackGroup;
            return columnSeries;
        }

        private CategoryAxis crearEjeX(string annio)
        {
            var categoryAxis = new CategoryAxis();
            categoryAxis.Labels.Add($"Ene-{annio}");
            categoryAxis.Labels.Add($"Feb-{annio}");
            categoryAxis.Labels.Add($"Mar-{annio}");
            categoryAxis.Labels.Add($"Abr-{annio}");
            categoryAxis.Labels.Add($"May-{annio}");
            categoryAxis.Labels.Add($"Jun-{annio}");
            categoryAxis.Labels.Add($"Jul-{annio}");
            categoryAxis.Labels.Add($"Ago-{annio}");
            categoryAxis.Labels.Add($"Sep-{annio}");
            categoryAxis.Labels.Add($"Oct-{annio}");
            categoryAxis.Labels.Add($"Nov-{annio}");
            categoryAxis.Labels.Add($"Dic-{annio}");

            categoryAxis.IsZoomEnabled = false;
            categoryAxis.IsPanEnabled = false;
            return categoryAxis;
        }

        private void agregarPlotModel(string inbox, PlotModel plotModel)
        {
            _plotDic[inbox] = plotModel;
        }
    }
}
