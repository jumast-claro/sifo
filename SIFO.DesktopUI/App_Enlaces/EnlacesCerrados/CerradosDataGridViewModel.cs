using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using SIFO.DesktopUI.App_Enlaces;
using SIFO.DesktopUI.CollectionView;
using SIFO.DesktopUI.Framework;
using SIFO.DesktopUI.TareasCerradas;
using SIFO.DesktopUI.TareasIngresadas;
using SIFO.DesktopUI.Utilities.CollectionView;
using SIFO.DesktopUI.Utilities.Reflection;
using SIFO.Model;

namespace SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento
{
   

    public class CerradosDataGridViewModel : INotifyPropertyChanged
    {
        //---------------------------------------------------------------------
        // Private
        //---------------------------------------------------------------------
        private readonly ITareasCerradasModelRepository _tareasCerradasModelRepository;
        private readonly ITareasIngresadasModelRepository _tareasIngresadasModelRepository;
        private readonly ObservableCollection<EnlaceCerradoViewModel> _listViewModel = new ObservableCollection<EnlaceCerradoViewModel>();

        private readonly ICollectionView _pseudoTareas;


        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------
        public CerradosDataGridViewModel(ITareasCerradasModelRepository tareasCerradasModelRepository, ITareasIngresadasModelRepository tareasIngresadasModelRepository)
        {
            _tareasCerradasModelRepository = tareasCerradasModelRepository;
            _tareasIngresadasModelRepository = tareasIngresadasModelRepository;
            var repo = new EnlacesCerradosModelRepository(tareasCerradasModelRepository);


            foreach (var pseudoTarea in repo.SelectAll())
            {
                var viewModel = new EnlaceCerradoViewModel(pseudoTarea);
                _listViewModel.Add(viewModel);

            }

            _pseudoTareas = CollectionViewSource.GetDefaultView(_listViewModel);

            DetailsViewModel = new EnlaceDataGridDetailsViewModel(tareasCerradasModelRepository, tareasIngresadasModelRepository);

        }

        //---------------------------------------------------------------------
        // Properties
        //---------------------------------------------------------------------
        public ICollectionView PseudoTareas => _pseudoTareas;



        private EnlaceDataGridDetailsViewModel _enlaceDataGridDetailsViewModel;
        public EnlaceDataGridDetailsViewModel DetailsViewModel
        {
            get
            {
                return _enlaceDataGridDetailsViewModel;
            }
            private set
            {
                _enlaceDataGridDetailsViewModel = value;

            }
        }

        private EnlaceCerradoViewModel _selectedEnlace;
        public EnlaceCerradoViewModel SelectedEnlace
        {
            get
            {
                return _selectedEnlace;
            }
            set
            {
                if (value != _selectedEnlace)
                {
                    _selectedEnlace = value;
                    DetailsViewModel.Enlace = value;
                    OnPropertyChanged();
                }
                
            }
        }


        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}