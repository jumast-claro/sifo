﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SIFO.DesktopUI.SyncfusionControls.PseudoTareas.Details;

namespace SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento
{
    public partial class EnlacesCerradosDataGrid_VistaReducida : UserControl
    {
        public static DependencyProperty EnlacesItemsSourceProperty = DependencyProperty.Register("EnlacesItemsSource", typeof(object), typeof(EnlacesCerradosDataGrid_VistaReducida));
        public static readonly DependencyProperty EnlaceSeleccionadoProperty = DependencyProperty.Register("EnlaceSeleccionado", typeof(object), typeof(EnlacesCerradosDataGrid_VistaReducida), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty TareasItemsSourceProperty = DependencyProperty.Register("TareasItemsSource", typeof(object), typeof(EnlacesCerradosDataGrid_VistaReducida));
        public static readonly DependencyProperty TareaSeleccionadaProperty = DependencyProperty.Register("TareaSeleccionada", typeof(object), typeof(EnlacesCerradosDataGrid_VistaReducida), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty TareasGanttProperty = DependencyProperty.Register("TareasGantt", typeof(object), typeof(EnlacesCerradosDataGrid_VistaReducida));
        public static DependencyProperty HighlightedTareasGanttProperty = DependencyProperty.Register("HighlightedTareasGantt", typeof(object), typeof(EnlacesCerradosDataGrid_VistaReducida));




        public EnlacesCerradosDataGrid_VistaReducida()
        {
            InitializeComponent();
        }

        public object EnlacesItemsSource
        {
            get
            {
                return GetValue(EnlacesItemsSourceProperty);
            }
            set
            {
                SetValue(EnlacesItemsSourceProperty, value);
            }
        }

        public object EnlaceSeleccionado 
        {
            get { return GetValue(EnlaceSeleccionadoProperty); }
            set
            {
                SetValue(EnlaceSeleccionadoProperty, value);
            }
        }

        public object TareasItemsSource
        {
            get
            {
                return GetValue(TareasItemsSourceProperty);
            }
            set
            {
                SetValue(TareasItemsSourceProperty, value);
            }
        }

        public object TareaSeleccionada
        {
            get { return GetValue(TareaSeleccionadaProperty); }
            set
            {
                SetValue(TareaSeleccionadaProperty, value);
            }
        }

        public object TareasGantt
        {
            get
            {
                return GetValue(TareasGanttProperty);
            }
            set
            {
                SetValue(TareasGanttProperty, value);
            }
        }

        public object HighlightedTareasGantt
        {
            get
            {
                return GetValue(HighlightedTareasGanttProperty);
            }
            set
            {
                SetValue(HighlightedTareasGanttProperty, value);
            }
        }
    }


}
