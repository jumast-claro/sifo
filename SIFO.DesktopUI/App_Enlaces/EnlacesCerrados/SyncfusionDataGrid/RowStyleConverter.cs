﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento;
using SIFO.Model;
using Syncfusion.UI.Xaml.Grid;

namespace SIFO.DesktopUI.SyncfusionControls.PseudoTareasDataGridView
{
    public class RowStyleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            var vm = (EnlaceCerradoViewModel)value;
            if (vm == null) return Brushes.AliceBlue;

            var onTime = vm.OnTimeProperty.Value;

            switch (onTime)
            {
                case OnTimeEnlace.Si:
                    return Brushes.MediumSeaGreen;
                case OnTimeEnlace.No:
                    return Brushes.PaleVioletRed;
                case OnTimeEnlace.Indefinido:
                    return Brushes.LightYellow;
                case OnTimeEnlace.NoAplica:
                    return Brushes.Black;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class RowForegroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            var vm = (EnlaceCerradoViewModel)value;
            if (vm == null) return Brushes.AliceBlue;

            var onTime = vm.OnTimeProperty.Value;

            switch (onTime)
            {
                case OnTimeEnlace.NoAplica:
                    return Brushes.WhiteSmoke;
                default:
                    return Brushes.Black;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
