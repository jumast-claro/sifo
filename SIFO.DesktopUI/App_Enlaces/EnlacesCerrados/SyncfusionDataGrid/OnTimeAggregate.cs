﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento;
using SIFO.Model;
using Syncfusion.Data;
using Syncfusion.Windows.Tools.Controls;

namespace SIFO.DesktopUI.SyncfusionControls.PseudoTareasDataGridView
{
    public class OnTimeAggregate : ISummaryAggregate
    {

        public int OnTimeSi { get; set; } = 0;
        public int OnTimeNo { get; set; } = 0;

        public Action<IEnumerable, string, PropertyDescriptor> CalculateAggregateFunc()
        {
            return (items, property, pd) =>
            {
                OnTimeSi = 0;
                OnTimeNo = 0;
                var enumerable = items as IEnumerable<EnlaceCerradoViewModel>;
                doIt(enumerable);
            };
        }


        private void doIt(IEnumerable<EnlaceCerradoViewModel> items)
        {
            foreach (var pseudoTarea in items)
            {
                var onTime = pseudoTarea.OnTimeProperty.Value;

                switch (onTime)
                {
                    case OnTimeEnlace.Si:
                        OnTimeSi += 1;
                        break;
                    case OnTimeEnlace.No:
                        OnTimeNo += 1;
                        break;

                }

                //if (pseudoTarea.OnTimeProperty.Value == OnTimeEnlace.Si)
                //{
                //    OnTimeSi += 1;
                //}
                //else if()
                //{
                //    OffTimeNo += 1;
                //}
            }
        }
    }
}
