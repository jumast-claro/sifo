﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento;
using Syncfusion.UI.Xaml.Grid;

namespace SIFO.DesktopUI.SyncfusionControls
{
    /// <summary>
    /// Interaction logic for EnlacesCerradosSfDataGridView.xaml
    /// </summary>
    public partial class EnlacesCerradosSfDataGridView : UserControl
    {
        public static DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(object), typeof(EnlacesCerradosSfDataGridView));

        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register("SelectedItem", typeof(EnlaceCerradoViewModel), typeof(EnlacesCerradosSfDataGridView), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static readonly DependencyProperty SfDataGridProperty = DependencyProperty.Register("SfDataGrid", typeof(SfDataGrid), typeof(EnlacesCerradosSfDataGridView));

        public static DependencyProperty IsHiddenProperty = DependencyProperty.Register("IsHidden", typeof(bool), typeof(EnlacesCerradosSfDataGridView), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, t));

        private static void t(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var me = d as EnlacesCerradosSfDataGridView;
            var sfDataGrid = me.SfDataGrid;
            sfDataGrid.Columns[0].IsHidden = (bool)e.NewValue;
        }

        public EnlacesCerradosSfDataGridView()
        {
            InitializeComponent();
            SfDataGrid = @this.sfDataGrid;
        }


        public bool IsHiden
        {
            get { return (bool) GetValue(IsHiddenProperty); }
            set
            {
                SetValue(IsHiddenProperty, value);
            }
        }

        public object ItemsSource
        {
            get
            {
                return GetValue(ItemsSourceProperty);
            }
            set
            {
                SetValue(ItemsSourceProperty, value);
            }
        }

        public EnlaceCerradoViewModel SelectedItem
        {
            get { return (EnlaceCerradoViewModel)GetValue(SelectedItemProperty); }
            set
            {
                SetValue(SelectedItemProperty, value);
            }
        }

        public SfDataGrid SfDataGrid
        {
            get { return (SfDataGrid) GetValue(SfDataGridProperty); }
            set
            {
                SetValue(SfDataGridProperty, value);
            }
        }

      
    }
}
