﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento;
using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.Properties;
using SIFO.Model;

namespace SIFO.DesktopUI.App_Enlaces.MainView
{
    public class EnlaceIngresadoViewModel : IEnlaceViewModel
    {
        private readonly IEnlace _model;

        public EnlaceIngresadoViewModel(IEnlace model)
        {
            _model = model;
            _initializaBindableProperties();
        }

        private void _initializaBindableProperties()
        {
            PseudoEnlaceProperty = new ReadOnlyProperty<string>(_model.PseudoEnlace);
            OrdenProperty = new ReadOnlyProperty<long>(_model.Orden);
            InboxProperty = new ReadOnlyProperty<string>(_model.Inbox);
            CantidadDeTareasProperty = new ReadOnlyProperty<int>(_model.CanTareas);

            TipoDeMovimientoProperty = new ReadOnlyProperty<string>(_model.TipoDeMovimiento);
            TecnologiaProperty = new ReadOnlyProperty<string>(_model.Tecnologia);
            TipoDeFibraOpticaProperty = new ReadOnlyProperty<string>(_model.TipoDeFibraOptica);

            ProductoProperty = new ReadOnlyProperty<string>(_model.Producto);
            TipoDeOrdenProperty = new ReadOnlyProperty<string>(_model.TipoDeOrden);
            TipoDeSolucionProperty = new ReadOnlyProperty<string>(_model.TipoDeSolucion);
            ClienteProperty = new ReadOnlyProperty<string>(_model.Cliente);
            IdentificadorProperty = new ReadOnlyProperty<string>(_model.Identificador);

            //FechaDeCompromisoProperty = new ReadOnlyProperty<DateTime>(_model.FechaDeCompromiso.Fecha, _model.FechaDeCompromiso.IsNull ? "" : _model.FechaDeCompromiso.Fecha.ToString("dd/MM/yyyy"));
            AnnioProperty = new ReadOnlyProperty<int>(_model.Annio);
            MesProperty = new ReadWriteProperty<int>(_model.Mes);

            WorkflowFechaInicioProperty = new ReadOnlyProperty<DateTime?>(_model.WorkFlowFechaInicio);
            WorkflowFechaFinProperty = new ReadOnlyProperty<DateTime?>(_model.WorkFlowFechaFin);
            ZonaProperty = new ReadOnlyProperty<string>(_model.Zona);

        }

        public IBindableProperty<int> AnnioProperty { get; set; }
        public IBindableProperty<int> MesProperty { get; set; }
        public IBindableProperty<string> PseudoEnlaceProperty { get; private set; }
        public IBindableProperty<long> OrdenProperty { get; private set; }
        public IBindableProperty<string> InboxProperty { get; private set; }
        public IBindableProperty<int> CantidadDeTareasProperty { get; private set; }
        public IBindableProperty<string> TipoDeMovimientoProperty { get; set; }
        public IBindableProperty<string> TecnologiaProperty { get; set; }
        public IBindableProperty<string> TipoDeFibraOpticaProperty { get; set; }
        public IBindableProperty<string> ProductoProperty { get; set; }
        public IBindableProperty<string> TipoDeOrdenProperty { get; set; }
        public IBindableProperty<string> TipoDeSolucionProperty { get; set; }
        public IBindableProperty<string> ClienteProperty { get; set; }
        public IBindableProperty<string> IdentificadorProperty { get; set; }
        public IBindableProperty<string> ResultadoProperty { get; set; }

        public IBindableProperty<DateTime?> WorkflowFechaInicioProperty { get; set; }
        public IBindableProperty<DateTime?> WorkflowFechaFinProperty { get; set; }
        public IBindableProperty<string> ZonaProperty { get; set; }
        //public IBindableProperty<DateTime> FechaDeCompromisoProperty { get; set; }

    }
}
