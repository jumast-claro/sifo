﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento;
using SIFO.DesktopUI.SyncfusionControls;
using Syncfusion.UI.Xaml.Grid;

namespace SIFO.DesktopUI.App_Enlaces
{
    /// <summary>
    /// Interaction logic for IngresadosSfDataGridView.xaml
    /// </summary>
    public partial class IngresadosSfDataGridView : UserControl
    {
        public static readonly DependencyProperty SfDataGridProperty = DependencyProperty.Register("SfDataGrid", typeof(SfDataGrid), typeof(IngresadosSfDataGridView));

        public static DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(object), typeof(IngresadosSfDataGridView));

        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register("SelectedItem", typeof(object), typeof(IngresadosSfDataGridView), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public IngresadosSfDataGridView()
        {
            InitializeComponent();
            SfDataGrid = @this.Sfgrid;
        }

        public SfDataGrid SfDataGrid
        {
            get { return (SfDataGrid)GetValue(SfDataGridProperty); }
            set
            {
                SetValue(SfDataGridProperty, value);
            }
        }


        public object ItemsSource
        {
            get
            {
                return GetValue(ItemsSourceProperty);
            }
            set
            {
                SetValue(ItemsSourceProperty, value);
            }
        }

        public object SelectedItem
        {
            get { return GetValue(SelectedItemProperty); }
            set
            {
                SetValue(SelectedItemProperty, value);
            }
        }



    }


}
