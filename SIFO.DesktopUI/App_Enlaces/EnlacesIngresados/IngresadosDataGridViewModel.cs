using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Data;
using SIFO.DesktopUI.TareasIngresadas;
using SIFO.Model;

namespace SIFO.DesktopUI.App_Enlaces.MainView
{
    public class IngresadosDataGridViewModel : INotifyPropertyChanged
    {
        //---------------------------------------------------------------------
        // Private
        //---------------------------------------------------------------------
        private readonly ObservableCollection<EnlaceIngresadoViewModel> _list = new ObservableCollection<EnlaceIngresadoViewModel>();
        private readonly ICollectionView _enlacesCollectionView;

        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------
        public IngresadosDataGridViewModel(ITareasIngresadasModelRepository tareasIngresadasModelRepository, ITareasCerradasModelRepository tareasCerradasModelRepository)
        {
            var enlacesIngresadosRepo = new EnlacesIngreadosModelRepository(tareasIngresadasModelRepository);

            foreach (var enlaceIngresado in enlacesIngresadosRepo.SelectAll())
            {
                var viewModel = new EnlaceIngresadoViewModel(enlaceIngresado);
                _list.Add(viewModel);
            }

            _enlacesCollectionView = CollectionViewSource.GetDefaultView(_list);

            DetailsViewModel = new EnlaceDataGridDetailsViewModel(tareasCerradasModelRepository, tareasIngresadasModelRepository);
        }

        //---------------------------------------------------------------------
        // Properties
        //---------------------------------------------------------------------
        public ICollectionView Enlaces => _enlacesCollectionView;

        private EnlaceDataGridDetailsViewModel _enlaceDataGridDetailsViewModel;
        public EnlaceDataGridDetailsViewModel DetailsViewModel
        {
            get
            {
                return _enlaceDataGridDetailsViewModel;
            }
            private set
            {
                _enlaceDataGridDetailsViewModel = value; 
                
            }
        }

        private EnlaceIngresadoViewModel _selectedEnlace;

        public EnlaceIngresadoViewModel SelectedEnlace
        {
            get { return _selectedEnlace; }
            set
            {
                if (value != _selectedEnlace)
                {
                    _selectedEnlace = value;
                    DetailsViewModel.Enlace = value;
                    OnPropertyChanged();
                }
            }
        }

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}