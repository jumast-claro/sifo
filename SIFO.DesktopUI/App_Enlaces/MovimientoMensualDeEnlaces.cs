namespace SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento
{



    public sealed class MovimientoMensualDeEnlaces 
    {
        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        private readonly string _inbox;
        private readonly int _annio;
        private readonly int _mes;

        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------
        public MovimientoMensualDeEnlaces(string inbox, int mes, int annio)
        {
            _inbox = inbox;
            _annio = annio;
            _mes = mes;
        }

        //---------------------------------------------------------------------
        // Public
        //---------------------------------------------------------------------
        public string Inbox => _inbox;
        public int Annio => _annio;
        public int Mes => _mes;
        public int PendientesInicial { get; set; }
        public int Ingresadas { get; set; }
        public int Cerradas { get; set; }
        public int CerradasExito { get; set; }
        public int CerradasFracaso { get; set; }
        public int OnTime { get; set; }
        public int OffTime { get; set; }
        public int OnTimeNoAplica { get; set; }
        public int OnTimeIndefinido { get; set; }
        public int OffTimePermanenciaOnTimeGeneral { get; set; }
        public int OffTimePermanenciaOffTimeGeneral { get; set; }
        public int OffTimePermanenciaIndefinidoTimeGeneral { get; set; }

        public double OnTimePermanenciaEnPorcentaje
        {
            get
            {
                if (CerradasExito == 0)
                {
                    return 0;
                }
                else
                {
                    return (OnTime / (double)CerradasExito) * 100;
                }
            }
        }

        public double OffTimePermanenciaEnPorcentaje
        {
            get
            {
                if (CerradasExito == 0)
                {
                    return 0;
                }
                else
                {
                    return (OffTime/(double) CerradasExito)*100;
                }
            }
        }


        //private int _pendientesFinal;

        public int PendientesFinal
        {
            get { return PendientesInicial + Ingresadas - Cerradas; }

        }

        public void Reset()
        {
            PendientesInicial = 0;
            Ingresadas = 0;
            Cerradas = 0;
            CerradasExito = 0;
            CerradasFracaso = 0;
            OnTime = 0;
            OffTime = 0;
            OnTimeNoAplica = 0;
            OnTimeIndefinido = 0;
            OffTimePermanenciaOnTimeGeneral = 0;
            OffTimePermanenciaOffTimeGeneral = 0;
            OffTimePermanenciaIndefinidoTimeGeneral = 0;
            //PendientesFinal = 0;
        }
    }
}