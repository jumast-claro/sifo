﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SIFO.DesktopUI.SyncfusionControls.PseudoTareas
{


    /// <summary>
    /// Interaction logic for EnlacesPorInboxMainView.xaml
    /// </summary>
    public partial class EnlacesPorInboxMainView : UserControl, IMovimientoDePseudoTareas
    {
        public static DependencyProperty TareasGanttProperty = DependencyProperty.Register("TareasGantt", typeof(object), typeof(EnlacesPorInboxMainView));
        public static DependencyProperty EnlacesItemsSourceProperty = DependencyProperty.Register("EnlacesItemsSource", typeof(object), typeof(EnlacesPorInboxMainView));
        public static readonly DependencyProperty EnlaceSeleccionadoProperty = DependencyProperty.Register("EnlaceSeleccionado", typeof(object), typeof(EnlacesPorInboxMainView), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty TareasItemsSourceProperty = DependencyProperty.Register("TareasItemsSource", typeof(object), typeof(EnlacesPorInboxMainView));
        public static readonly DependencyProperty TareaSeleccionadaProperty = DependencyProperty.Register("TareaSeleccionada", typeof(object), typeof(EnlacesPorInboxMainView), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty TableViewModelProperty = DependencyProperty.Register("TableViewModel", typeof(object), typeof(EnlacesPorInboxMainView));
        public EnlacesPorInboxMainView()
        {
            InitializeComponent();
        }

        public object EnlacesItemsSource
        {
            get
            {
                return GetValue(EnlacesItemsSourceProperty);
            }
            set
            {
                SetValue(EnlacesItemsSourceProperty, value);
            }
        }

        public object EnlaceSeleccionado
        {
            get { return GetValue(EnlaceSeleccionadoProperty); }
            set
            {
                SetValue(EnlaceSeleccionadoProperty, value);
            }
        }

        public object TareasItemsSource
        {
            get
            {
                return GetValue(TareasItemsSourceProperty);
            }
            set
            {
                SetValue(TareasItemsSourceProperty, value);
            }
        }

        public object TareaSeleccionada
        {
            get { return GetValue(TareaSeleccionadaProperty); }
            set
            {
                SetValue(TareaSeleccionadaProperty, value);
            }
        }

        public object TableViewModel
        {
            get { return GetValue(TableViewModelProperty); }
            set
            {
                SetValue(TableViewModelProperty, value);
            }
        }

        public object TareasGantt
        {
            get
            {
                return GetValue(TareasGanttProperty);
            }
            set
            {
                SetValue(TareasGanttProperty, value);
            }
        }
    }
}
