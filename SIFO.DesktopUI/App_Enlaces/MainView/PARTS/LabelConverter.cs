﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Syncfusion.UI.Xaml.Charts;

namespace SIFO.DesktopUI.App_Enlaces.MainView.PARTS
{
    public class Labelconvertor : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ChartAdornment pieAdornment = value as ChartAdornment;
            return String.Format((pieAdornment.Item as SyncfusionControls.PseudoTareas.Model).Name+ " : " + pieAdornment.YData);
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
