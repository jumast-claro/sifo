﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento;
using SIFO.DesktopUI.Utilities;

namespace SIFO.DesktopUI.App_Enlaces.MainView.PARTS
{
  


    /// <summary>
    /// Interaction logic for PieChart.xaml
    /// </summary>
    public partial class PieChart : UserControl
    {

        public static DependencyProperty SeriesItemsSourceProperty = DependencyProperty.Register("SeriesItemsSource", typeof (ObservableCollection<SyncfusionControls.PseudoTareas.Model>), typeof (PieChart), new FrameworkPropertyMetadata(new ObservableCollection<SyncfusionControls.PseudoTareas.Model>(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public PieChart()
        {
            InitializeComponent();
        }

       

        public ObservableCollection<SyncfusionControls.PseudoTareas.Model> SeriesItemsSource
        {
            get
            {
                return (ObservableCollection<SyncfusionControls.PseudoTareas.Model>)GetValue(SeriesItemsSourceProperty);
            }
            set
            {
                SetValue(SeriesItemsSourceProperty, value);
            }
        }

    }
}
