﻿namespace SIFO.DesktopUI.SyncfusionControls.PseudoTareas
{
    public interface IMovimientoDePseudoTareas
    {
        object EnlaceSeleccionado { get; set; }
        object EnlacesItemsSource { get; set; }

        object TableViewModel { get; set; }
        object TareaSeleccionada { get; set; }
        object TareasItemsSource { get; set; }
    }
}