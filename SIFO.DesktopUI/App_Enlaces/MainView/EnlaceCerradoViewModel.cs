using System;
using SIFO.DesktopUI.App_Enlaces.MainView;
using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.Properties;
using SIFO.DesktopUI.Utilities.Reflection;
using SIFO.Model;

namespace SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento
{

    public interface IEnlaceViewModel
    {
        IBindableProperty<string> InboxProperty { get;}
        IBindableProperty<string> PseudoEnlaceProperty { get; }
    }

    public class EnlaceCerradoViewModel : IEnlaceViewModel
    {

        private readonly IEnlaceCerrado _model;

        public EnlaceCerradoViewModel(IEnlaceCerrado model)
        {
            _model = model;
            _initializaBindableProperties();
        }

        private void _initializaBindableProperties()

        {
            PseudoEnlaceProperty = new ReadOnlyProperty<string>(_model.PseudoEnlace);
            OrdenProperty = new ReadOnlyProperty<long>(_model.Orden);
            InboxProperty = new ReadOnlyProperty<string>(_model.Inbox);
            TiempoEnBandejaBrutoProperty = new ReadOnlyProperty<double>(_model.TiempoEnBandejaBruto);
            TiempoEnOtrasBandejasProperty = new ReadOnlyProperty<double>(_model.TiempoEnOtrasBandejas);
            TiempoEnBandejaNetoProperty = new ReadOnlyProperty<double>(_model.TiempoEnBandejaNeto);
            MesDeCierreProperty = new ReadOnlyProperty<int>(_model.MesCierre);
            CantidadDeTareasProperty = new ReadOnlyProperty<int>(_model.CanTareas);
            OnTimeProperty = new ReadOnlyProperty<OnTimeEnlace>(_model.OnTime);

            TipoDeMovimientoProperty = new ReadOnlyProperty<string>(_model.TipoDeMovimiento);
            TecnologiaProperty = new ReadOnlyProperty<string>(_model.Tecnologia);
            TipoDeFibraOpticaProperty = new ReadOnlyProperty<string>(_model.TipoDeFibraOptica);

            ProductoProperty = new ReadOnlyProperty<string>(_model.Producto);
            TipoDeOrdenProperty = new ReadOnlyProperty<string>(_model.TipoDeOrden);
            TipoDeSolucionProperty = new ReadOnlyProperty<string>(_model.TipoDeSolucion);
            ClienteProperty = new ReadOnlyProperty<string>(_model.Cliente);
            IdentificadorProperty = new ReadOnlyProperty<string>(_model.Identificador);

            ResultadoProperty = new ReadOnlyProperty<string>(_model.Resultado.ToString());

            FechaDeCompromisoProperty = new ReadOnlyProperty<DateTime?>(_model.FechaDeCompromiso.Fecha, _model.FechaDeCompromiso.IsNull ? "" : _model.FechaDeCompromiso.Fecha.Value.ToString("dd/MM/yyyy"));
            OnTimeGeneralProperty = new ReadOnlyProperty<OnTimeEnlace>(_model.OnTimeGeneral);

            FechaDeCierreProperty = new ReadOnlyProperty<DateTime>(_model.FechaDeCierre.Fecha.Value, _model.FechaDeCierre.Fecha.Value.ToString("dd/MM/yyyy"));

            AnnioProperty = new ReadOnlyProperty<int>(_model.Annio);

            WorkflowFechaInicioProperty = new ReadOnlyProperty<DateTime?>(_model.WorkFlowFechaInicio);
            WorkflowFechaFinProperty = new ReadOnlyProperty<DateTime?>(_model.WorkFlowFechaFin);
            ZonaProperty = new ReadOnlyProperty<string>(_model.Zona);

            PartidoProperty = new ReadOnlyProperty<string>(_model.Partido);

            PermanenciaMaximaPermitidaEnDiasProperty = new ReadOnlyProperty<int>(_model.TiempoMaximoPermitidoEnBandeja);

            TomadoPorProperty = new ReadOnlyProperty<string>(_model.TomadoPor);
            DireccionProperty = new ReadOnlyProperty<string>(_model.Direccion);


        }

        public IBindableProperty<int> AnnioProperty { get; set; }
        public IBindableProperty<string> PseudoEnlaceProperty { get; private set; }
        public IBindableProperty<long> OrdenProperty { get; private set; }
        public IBindableProperty<string> InboxProperty { get; private set; } 
        public IBindableProperty<double> TiempoEnBandejaBrutoProperty { get; private set; }
        public IBindableProperty<double> TiempoEnOtrasBandejasProperty { get; private set; }
        public IBindableProperty<double> TiempoEnBandejaNetoProperty { get; private set; }
        public IBindableProperty<int> MesDeCierreProperty { get; private set; }
        public IBindableProperty<int> CantidadDeTareasProperty { get; private set; }
        public IBindableProperty<OnTimeEnlace> OnTimeProperty { get; private set; }
        public IBindableProperty<string> TipoDeMovimientoProperty { get; set; }
        public IBindableProperty<string> TecnologiaProperty { get; set; }
        public IBindableProperty<string> TipoDeFibraOpticaProperty { get; set; }
        public IBindableProperty<string> ProductoProperty { get; set; }
        public IBindableProperty<string> TipoDeOrdenProperty { get; set; }
        public IBindableProperty<string> TipoDeSolucionProperty { get; set; }
        public IBindableProperty<string> ClienteProperty { get; set; }
        public IBindableProperty<string> IdentificadorProperty { get; set; }
        public IBindableProperty<string> ResultadoProperty { get; set; }

        public IBindableProperty<DateTime?> FechaDeCompromisoProperty { get; set; }

        public IBindableProperty<OnTimeEnlace> OnTimeGeneralProperty { get; set; }
        public IBindableProperty<DateTime> FechaDeCierreProperty { get; set; }

        public IBindableProperty<DateTime?> WorkflowFechaInicioProperty { get; set; }
        public IBindableProperty<DateTime?> WorkflowFechaFinProperty { get; set; }
        public IBindableProperty<string> ZonaProperty { get; set; }

        public IBindableProperty<string> PartidoProperty { get; set; }

        public IBindableProperty<int> PermanenciaMaximaPermitidaEnDiasProperty { get; set; }

        public IBindableProperty<string> TomadoPorProperty { get; set; }
        public IBindableProperty<string> DireccionProperty { get; set; }
    }
}