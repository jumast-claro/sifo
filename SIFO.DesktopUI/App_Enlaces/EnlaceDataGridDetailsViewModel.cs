﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using SIFO.DesktopUI.AnalisisPermanencia;
using SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento;
using SIFO.DesktopUI.App_Enlaces.MainView;
using SIFO.DesktopUI.TareasIngresadas;
using SIFO.Model;
using Syncfusion.Data.Extensions;
using Syncfusion.Windows.Controls.Gantt;

namespace SIFO.DesktopUI.App_Enlaces
{
    public class EnlaceDataGridDetailsViewModel : INotifyPropertyChanged
    {
        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        private readonly ITareasCerradasModelRepository _tareasCerradasModelRepository;
        private readonly ITareasIngresadasModelRepository _tareasIngresadasModelRepository;

        //---------------------------------------------------------------------
        // Constructor
        //---------------------------------------------------------------------
        public EnlaceDataGridDetailsViewModel(ITareasCerradasModelRepository tareasCerradasModelRepository, ITareasIngresadasModelRepository tareasIngresadasModelRepository)
        {
            _tareasCerradasModelRepository = tareasCerradasModelRepository;
            _tareasIngresadasModelRepository = tareasIngresadasModelRepository;
        }


        //---------------------------------------------------------------------
        // Properties
        //---------------------------------------------------------------------
        private IEnlaceViewModel _enlace;
        public IEnlaceViewModel Enlace
        {
            get
            {
                return _enlace;
            }
            set
            {
                if (value != null && value != _enlace)
                {
                    obtenerTareas(value.PseudoEnlaceProperty.Value);
                    seleccionarTarea(value.InboxProperty.Value);
                    obtenerTareasGantt();
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(TareasGantt));
                }
            }
        }


        private ObservableCollection<TareaDetalleViewModel> _tareas = new ObservableCollection<TareaDetalleViewModel>();
        public ObservableCollection<TareaDetalleViewModel> Tareas
        {
            get
            {
                return _tareas;
            }
            set
            {
                _tareas = value;
                OnPropertyChanged();
            }
        }

        private TareaDetalleViewModel _selectedTarea;
        public TareaDetalleViewModel SelectedTarea
        {
            get { return _selectedTarea; }
            set
            {
                _selectedTarea = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<TaskDetails> _tareasGantt;
        public ObservableCollection<TaskDetails> TareasGantt
        {
            get { return _tareasGantt; }
            set
            {
                if (value != _tareasGantt)
                {
                    _tareasGantt = value;
                    OnPropertyChanged();
                }
            }
        }  

        //---------------------------------------------------------------------
        // Private
        //---------------------------------------------------------------------
        private void obtenerTareas(string enlace)
        {
            var temp = new ObservableCollection<TareaDetalleViewModel>();
            var cerradas = _tareasCerradasModelRepository.SelectAll().Where(t => t.PseudoEnlace == enlace);

            foreach (var tareaCerrada in cerradas)
            {
                var vm = new TareaDetalleViewModel(tareaCerrada);
                temp.Add(vm);
            }

            var ingresadasPendientes = _tareasIngresadasModelRepository.SelectAll()
                .Where(t => t.PseudoEnlace == enlace)
                .Where(t => t.Estado == Estado.Pendiente);
            foreach (var ingresada in ingresadasPendientes)
            {
                var vm = new TareaDetalleViewModel(ingresada);
                temp.Add(vm);
            }

            Tareas = temp.OrderBy(t => t.FechaInicioProperty.Value).ToObservableCollection();


        }

        private void seleccionarTarea(string inbox)
        {
            SelectedTarea = Tareas.Where(t => t.InboxProperty.Value == inbox).FirstOrDefault();
        }

        private ObservableCollection<TaskDetails> _tareasGanttPendientes = new ObservableCollection<TaskDetails>();

        public ObservableCollection<TaskDetails> TareasGanttPendientes
        {
            get { return _tareasGanttPendientes; }
            set
            {
                if (value != _tareasGanttPendientes)
                {
                    _tareasGanttPendientes = value;
                    OnPropertyChanged();
                }
            }
        } 

        private void obtenerTareasGantt()
        {
            _tareasGantt= new ObservableCollection<TaskDetails>();
            var tareas = Tareas.OrderBy(t => t.FechaInicioProperty.Value);

            TareasGanttPendientes.Clear();
            foreach (var tarea in tareas)
            {
                var start = tarea.FechaInicioProperty.Value;
                var end = tarea.FechaCierreProperty.Value ?? DateTime.Now;
                var duration = (end - start);

                var task = (new TaskDetails()
                {
                    TaskId = int.Parse(tarea.TareaIdProperty.Value),
                    StartDate = start,
                    FinishDate = end,
                    TaskName = tarea.InboxProperty.Value,
                    Duration = duration
                });

                if (tarea.ResultadoProperty.Value == "Pendiente")
                {
                    task.FinishDate = DateTime.Now;
                    task.Duration = task.FinishDate - task.StartDate;
                    TareasGanttPendientes.Add(task);
                }

                _tareasGantt.Add(task);


                task.Resources.Add(new Resource()
                {
                    Name = task.TaskName,

                });

               

               

            }
        }

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
