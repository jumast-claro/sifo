using System;
using System.Collections.Generic;
using System.Linq;
using SIFO.Model;
using SIFO.Services;

namespace SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento
{
    public class MovimientoMensualDeEnlacesStrategy
    {
        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        private readonly IEnumerable<IEnlace> _ingresados;
        private readonly IEnumerable<IEnlaceCerrado> _cerrados;
        private readonly IEnumerable<IEnlace> _pendientesAlInicio2017;
        private readonly List<MovimientoMensualDeEnlaces> _list = new List<MovimientoMensualDeEnlaces>();
        private readonly List<Predicate<IEnlace>> _predicates = new List<Predicate<IEnlace>>(); 

        //---------------------------------------------------------------------
        // Constructor
        //---------------------------------------------------------------------
        public MovimientoMensualDeEnlacesStrategy(IEnumerable<IEnlace> ingresados, IEnumerable<IEnlaceCerrado> cerrados, IEnumerable<IEnlace> pendientesAlInicio2017)
        {
            _ingresados = ingresados;
            _cerrados = cerrados;
            _pendientesAlInicio2017 = pendientesAlInicio2017;
        }


        //---------------------------------------------------------------------
        // Public
        //---------------------------------------------------------------------
        public List<MovimientoMensualDeEnlaces> MovimientosMensuales => _list;

        public MovimientoAnualDeEnlaces ObtenerMovimientoAnual(string inbox, int annio)
        {
            var movimientoAnual = new MovimientoAnualDeEnlaces(inbox, annio);

            foreach (var mes in MonthHelper.Enumerate)
            {
                movimientoAnual[mes] = obtenerMovimientoMensual(inbox, mes, annio);
            }

            return movimientoAnual;
        }

        public List<Predicate<IEnlace>> Predicates
        {
            get { return _predicates; }
        }

        public void Procesar()
        {

            foreach (var movimientoMensual in _list)
            {
                movimientoMensual.Reset();
            }

            procesarIngresados(_ingresados);
            procesarCerrados(_cerrados);
            procesarPendientes();
        }

        //---------------------------------------------------------------------
        // Private
        //---------------------------------------------------------------------
        private void procesarPendientes()
        {
            // Procesar enero de 2017
            foreach (var enlace in _pendientesAlInicio2017)
            {
                var movimientoMensual = obtenerMovimientoMensual(enlace.Inbox, enlace.Mes, enlace.Annio);
                {
                    bool incluirEnlace = _predicates.All(predicate => predicate(enlace));

                    if (incluirEnlace)
                    {
                        movimientoMensual.PendientesInicial += 1;
                    }
                }
            }



            var listaDeInbox = _cerrados.Select(e => e.Inbox).Distinct();
            foreach(var inbox in listaDeInbox)
            {
                // Procesar 2017, desde febrero hasta diciembre
                for (int i = 2; i <= 12; i++)
                {
                    var movimientoMensualAnterior = obtenerMovimientoMensual(inbox, i - 1, 2017);
                    var movimientoMensualActual = obtenerMovimientoMensual(inbox, i, 2017);
                    movimientoMensualActual.PendientesInicial = movimientoMensualAnterior.PendientesFinal;

                }

                // Procesar 2016
                for (int i = 9; i <= 12; i++)
                {
                    var movimientoMensualAnterior = obtenerMovimientoMensual(inbox, i - 1, 2016);
                    var movimientoMensualActual = obtenerMovimientoMensual(inbox, i, 2016);
                    movimientoMensualActual.PendientesInicial = movimientoMensualAnterior.PendientesFinal;

                }

                // Procesar enero de 2018
                var movimientoDiciembre2017 = obtenerMovimientoMensual(inbox, 12, 2017);
                var movimientoEnero2018 = obtenerMovimientoMensual(inbox, 1, 2018);
                movimientoEnero2018.PendientesInicial = movimientoDiciembre2017.PendientesFinal;
            }

            

        }


        private void procesarIngresados(IEnumerable<IEnlace> ingresados)
        {
            foreach (var enlace in ingresados)
            {
                var movimientoMensual = obtenerMovimientoMensual(enlace.Inbox, enlace.Mes, enlace.Annio);
                bool incluirEnlace = _predicates.All(predicate => predicate(enlace));

                if (incluirEnlace)
                {
                    movimientoMensual.Ingresadas += 1;
                }
            }
        }

        private void procesarCerrados(IEnumerable<IEnlaceCerrado> cerrados)
        {
            foreach (var enlace in cerrados)
            {
                var movimientoMensual = obtenerMovimientoMensual(enlace.Inbox, enlace.Mes, enlace.Annio);
                bool incluirEnlace = _predicates.All(predicate => predicate(enlace));

                if (incluirEnlace)
                {
                    movimientoMensual.Cerradas += 1;

                    switch (enlace.OnTime)
                    {
                        case OnTimeEnlace.Si:
                            movimientoMensual.OnTime += 1;
                            break;
                        case OnTimeEnlace.No:
                            movimientoMensual.OffTime += 1;
                            break;
                        case OnTimeEnlace.Indefinido:
                            movimientoMensual.OnTimeIndefinido += 1;
                            break;
                        case OnTimeEnlace.NoAplica:
                            movimientoMensual.OnTimeNoAplica += 1;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    switch (enlace.Resultado)
                    {
                        case Estado.Exito:
                            movimientoMensual.CerradasExito += 1;
                            break;
                        case Estado.Fracaso:
                            movimientoMensual.CerradasFracaso += 1;
                            break;
                        case Estado.Pendiente:
                            throw new ArgumentOutOfRangeException();
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    if (enlace.OnTime == OnTimeEnlace.No)
                    {
                        switch (enlace.OnTimeGeneral)
                        {
                            case OnTimeEnlace.Si:
                                movimientoMensual.OffTimePermanenciaOnTimeGeneral += 1;
                                break;
                            case OnTimeEnlace.No:
                                movimientoMensual.OffTimePermanenciaOffTimeGeneral += 1;
                                break;
                            case OnTimeEnlace.Indefinido:
                                movimientoMensual.OffTimePermanenciaIndefinidoTimeGeneral += 1;
                                break;
                            case OnTimeEnlace.NoAplica:
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }
                }
            }
        }

        private MovimientoMensualDeEnlaces obtenerMovimientoMensual(string inbox, int mes, int annio)
        {
            var movimientoMensual = _list.Where(e => e.Inbox == inbox).Where(e => e.Mes == mes).Where(e => e.Annio == annio).FirstOrDefault();
            if (movimientoMensual == null)
            {
                movimientoMensual = new MovimientoMensualDeEnlaces(inbox, mes, annio);
                _list.Add(movimientoMensual);
            }

            return movimientoMensual;
        }
    }
}