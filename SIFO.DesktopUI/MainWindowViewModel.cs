﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using SIFO.DesktopUI.AnalisisPermanencia;
using SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento;
using SIFO.DesktopUI.AnalisisPermanencia.Interior;
using SIFO.DesktopUI.App_Enlaces.MainView;
using SIFO.DesktopUI.App_Seguimiento;
using SIFO.DesktopUI.App_Tareas.Pendientes;
using SIFO.DesktopUI.App_Vantive;
using SIFO.DesktopUI.SyncfusionControls.PseudoTareas;
using SIFO.DesktopUI.TareasCerradas;
using SIFO.DesktopUI.TareasCerradas.Indicadores.PlantaExterna;
using SIFO.DesktopUI.TareasCerradas.Indicadores.PlantaInterna;
using SIFO.DesktopUI.TareasIngresadas;
using SIFO.DesktopUI.TareasPendientes;
using SIFO.DesktopUI.Utilities;


namespace SIFO.DesktopUI
{
    public class MainWindowViewModel
    {
        //---------------------------------------------------------------------
        // Campos
        //---------------------------------------------------------------------
        private readonly SeguimientoDataGridViewModel _seguimientoDataGridViewModel;
        private readonly MovimientoDeEnlacesViewModel _movimientoDeEnlacesViewModel;
        private readonly CerradosDataGridViewModel _pseudoTareasViewModel;
        private readonly IngresadosDataGridViewModel _ingresadosDataGridViewModel;
        private readonly PendientesVantiveDataGridViewModel _pendientesVantiveDataGridViewModel;
        private readonly TareasPendientesDataGridViewModel _tareasPendientesDataGridViewModel;

        //---------------------------------------------------------------------
        // Constructor
        //---------------------------------------------------------------------
        public MainWindowViewModel(MovimientoDeEnlacesViewModel movimientoDeEnlacesViewModel, CerradosDataGridViewModel pseudoTareasViewModel, IngresadosDataGridViewModel ingresadosDataGridViewModel, TareasPendientesDataGridViewModel tareasPendientesDataGridViewModel)
        {
            //_seguimientoDataGridViewModel = seguimientoDataGridViewModel;
            _movimientoDeEnlacesViewModel = movimientoDeEnlacesViewModel;
            _pseudoTareasViewModel = pseudoTareasViewModel;
            _ingresadosDataGridViewModel = ingresadosDataGridViewModel;
            //_pendientesVantiveDataGridViewModel = pendientesVantiveDataGridViewModel;
            _tareasPendientesDataGridViewModel = tareasPendientesDataGridViewModel;
        }

        //---------------------------------------------------------------------
        // Propiedades
        //---------------------------------------------------------------------
        public CerradosDataGridViewModel CerradosDataGridViewModel => _pseudoTareasViewModel;
        public IngresadosDataGridViewModel IngresadosDataGridViewModel => _ingresadosDataGridViewModel;
        public MovimientoDeEnlacesViewModel MovimientoDePseudoTareasViewModel => _movimientoDeEnlacesViewModel;
        public PendientesVantiveDataGridViewModel PendientesVantiveDataGridViewModel => _pendientesVantiveDataGridViewModel;
        public TareasPendientesDataGridViewModel TareasPendientesBODataGridViewModel => _tareasPendientesDataGridViewModel;
        public SeguimientoDataGridViewModel SeguimientoObraCivilTendidoAMBAViewModel => _seguimientoDataGridViewModel;
    }
}
