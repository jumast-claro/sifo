﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SIFO.DesktopUI.AttachedProperties
{
    public class FilterDataGridColumn
    {
        public static readonly DependencyProperty FilterPathProperty = DependencyProperty.RegisterAttached("FilterPath", typeof(string), typeof(FilterDataGridColumn), new PropertyMetadata(null));

        public static void SetFilterPath(DataGridColumn column, string value)
        {
           column.SetValue(FilterPathProperty, value); 
        }

        public static string GetFilterPath(DataGridColumn column)
        {
            return (string) column.GetValue(FilterPathProperty);
        }
        
    }



}
