﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.VisualBasic.ApplicationServices;
using Ninject.Modules;
using Ninject;
using SIFO.DesktopUI.DependencyInjection.Ninject;
using SIFO.DesktopUI.Login;

namespace SIFO.DesktopUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            //System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es");

            INinjectModule[] modules = { new TareasCerradasDataModule(), new TareasCerradasModelModule(), new VantiveNinjectModule(), };
            NinjectInjector injector = new NinjectInjector(modules);
            var mainWindowViewModel = injector.Resolve<MainWindowViewModel>();
            this.DataContext = mainWindowViewModel;

            InitializeComponent();


        }

    }
}
