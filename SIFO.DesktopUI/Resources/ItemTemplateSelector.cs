﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.Properties;

namespace SIFO.DesktopUI.Resources
{
    public class ItemTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            FrameworkElement element = container as FrameworkElement;
            if (element != null && item != null)
            {

                var property = item as IBindableProperty;
                if (property.IsReadOnly)
                {
                    return element.FindResource("ReadOnlyViewModelPropertyDataTemplate") as DataTemplate;
                }

                if (item is BindableProperty<DateTime?>)
                {
                    return element.FindResource("BindablePropertyDataTemplate") as DataTemplate;
                }

                if (item is IBindableProperty<string>)
                {
                    return element.FindResource("StringViewModelPropertyDataTemplate") as DataTemplate;
                }
                if (item is IBindableProperty<DateTime> || item is IBindableProperty<DateTime?>)
                {
                     return element.FindResource("DateTimeViewModelPropertyDataTemplate") as DataTemplate;                   
                }
                if (item is IBindableProperty<int> || item is IBindableProperty<long>)
                {
                    return element.FindResource("IntViewModelPropertyDataTemplate") as DataTemplate;
                }
            }
            return base.SelectTemplate(item, container);
        }
    }
}
