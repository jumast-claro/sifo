﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SIFO.DesktopUI.Resources
{
    /// <summary>
    /// Interaction logic for EditView.xaml
    /// </summary>
    public partial class EditView : UserControl
    {
        //public static DependencyProperty AcceptCommandProperty = DependencyProperty.Register("AcceptCommand", typeof(ICommand), typeof(EditView));
        //public static DependencyProperty CancelCommandProperty = DependencyProperty.Register("CancelCommand", typeof(ICommand), typeof(EditView));
        //public static DependencyProperty RestoreValuesCommandProperty = DependencyProperty.Register("RestoreValuesCommand", typeof(ICommand), typeof(EditView));

        public EditView()
        {
            InitializeComponent();
        }


        //public ICommand AcceptCommand
        //{
        //    get { return (ICommand) GetValue(AcceptCommandProperty); }
        //    set
        //    {
        //        SetValue(AcceptCommandProperty, value);
        //    }
        //}

        //public ICommand CancelCommand
        //{
        //    get { return (ICommand)GetValue(CancelCommandProperty); }
        //    set
        //    {
        //        SetValue(CancelCommandProperty, value);
        //    }
        //}

        //public ICommand RestoreValuesCommand
        //{
        //    get { return (ICommand)GetValue(RestoreValuesCommandProperty); }
        //    set
        //    {
        //        SetValue(RestoreValuesCommandProperty, value);
        //    }
        //}


        private void AcceptButton_OnClick(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }

        private void CancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }
    }
}
