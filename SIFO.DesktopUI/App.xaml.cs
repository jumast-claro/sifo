﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using AutoMapper;
using SIFO.DataAcces.TareasCerradas;
using SIFO.Model;

namespace SIFO.DesktopUI
{
    //public class PathProvider
    //{
    //    public string GetPath()
    //    {
    //        var user = Environment.UserName;
    //        if (user == "exb09805")
    //        {
    //            return @"S:\Implantacion\Servicios Fijos\Servicios Fijos\Reportes\SIFO\data";
    //        }
    //        else if(user == "Jumast")
    //        {
    //            return @"C:\Users\Jumast\Desktop";
    //        }
    //        throw new InvalidOperationException();

    //    }
    //}

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {


        private readonly string PATH = ConfigurationManager.AppSettings.Get("RUTA");
        private readonly string FILE_NAME = ConfigurationManager.AppSettings.Get("LogFileName");

        private string getPath()
        {
            return Path.Combine(PATH, FILE_NAME);
        }

        private void App_OnExit(object sender, ExitEventArgs e)
        {
            var user = Environment.UserName;


            try
            {
                using (FileStream fs = new FileStream(getPath(), FileMode.Append))
                {
                    using (TextWriter tw = new StreamWriter(fs))
                    {
                        tw.WriteLine(user + "," + "FIN   " + "," + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }

          
        }

        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            var user = Environment.UserName;

            try
            {
                using (FileStream fs = new FileStream(getPath(), FileMode.Append))
                {
                    using (TextWriter tw = new StreamWriter(fs))
                    {
                        tw.WriteLine(user + "," + "INICIO" + "," + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }

            
        }
    }
}
