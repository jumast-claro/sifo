using SIFO.DesktopUI.TareasIngresadas;
using SIFO.DesktopUI.Utilities.Reflection;
using SIFO.Model;
using SIFO.Services;

namespace SIFO.DesktopUI.TareasCerradas
{
    public class EditViewModelFactory
    {
        private readonly IncludePropertyAttributeReflector _configurator;
        private readonly CommandAttributeReflector _commandAttributeReflector;
        private readonly ITareaModelRepository<ITarea>_repo;

        public EditViewModelFactory(IncludePropertyAttributeReflector configurator, CommandAttributeReflector commandAttributeReflector, ITareaModelRepository<ITarea> repo)
        {
            _repo = repo;
            _configurator = configurator;
            _commandAttributeReflector = commandAttributeReflector;
        }

        public TareaCerradaDetalleViewModel CreateViewModel(string tareaId)
        {
            var viewModel =  new TareaCerradaDetalleViewModel(_repo, tareaId);
            _configurator.Configure(viewModel);
            _commandAttributeReflector.Configure(viewModel);
            return viewModel;
        }
    }
}