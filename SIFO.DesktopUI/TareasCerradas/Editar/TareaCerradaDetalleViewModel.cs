﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Configuration;
using System.Windows.Input;
using SIFO.DesktopUI.TareasIngresadas;
using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.Properties;
using SIFO.DesktopUI.Utilities.Reflection;
using SIFO.DesktopUI.Utilities.Validation;
using SIFO.Model;
using SIFO.Services;

namespace SIFO.DesktopUI.TareasCerradas
{
    public class TareaCerradaDetalleViewModel
    {
        private BindablePropertyFactory _propertyFactory = new BindablePropertyFactory();

        private readonly List<IBindableProperty> _properties = new List<IBindableProperty>();
        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        private readonly ITarea _model; // Orden que se va a editar.
        private readonly IPropertyCollection _propertyBag = new PropertyBag();
        private readonly ITareaModelRepository<ITarea> _repository;

        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------
        /// <param name="tareaId">TareaId de la orden que se quiere editar.</param>
        public TareaCerradaDetalleViewModel(ITareaModelRepository<ITarea> repo ,string tareaId)
        {
            _repository = repo;
            var model = _repository.SelectByTareaId(tareaId);
            if (model == null)
            {
                throw new ArgumentException();
            }

            _model = model;
            _inicializarPropiedades(model);
        }
        private void _inicializarPropiedades(ITarea model)
        {
            InboxProperty = _propertyFactory.CreateBindableProperty(model.Inbox, true);

            ZonaProperty = _propertyFactory.CreateBindableProperty(model.Zona.ToString(), false);
            ZonaProperty.AddValidationRule(new RangeValidationRule<string>("Amba", "Interior", "SinInfo"));

            ClienteProperty = _propertyFactory.CreateBindableProperty(model.Cliente, false);
            TipoDeOrdenProperty = _propertyFactory.CreateBindableProperty(model.TipoDeOrden, true);
            OrdenProperty = _propertyFactory.CreateBindableProperty(model.Orden, true);

            bool enlaceReadOnly = model.Enlace != 0;
            NumeroEnlaceProperty = _propertyFactory.CreateBindableProperty(model.Enlace, enlaceReadOnly);
            NumeroEnlaceProperty.AddValidationRule(new AcceptValidationRule<long>(enlace => enlace.ToString().Length == 7 || enlace == 0, "El valor ingresado no parece ser un número de enlace válido."));

            IdentificadorProperty = _propertyFactory.CreateBindableProperty(model.Identificador, true);

            FechaInicioOrdenProperty = new BindableProperty<DateTime?>(model.FechaInicioOrden, new NullableDateTimeConverter());
            FechaInicioOrdenProperty.AddValidationRule(new RejectValidationRule<DateTime?>(fechaInicioOrden => fechaInicioOrden.HasValue && fechaInicioOrden.Value > model.FechaInicioTarea, "La fecha de inicio de la orden no puede ser mayor a la fecha de inicio de la tarea."));
            FechaInicioOrdenProperty.AddValidationRule(new RejectValidationRule<DateTime?>(fechaInicioOrden => fechaInicioOrden.HasValue && fechaInicioOrden.Value >= model.FechaCompromisoOrden, "La fecha de inicio no puede ser menor a la fecha de compromiso."));

            FechaDeCompromisoProperty = new BindableReadOnlyWrapperProperty<string>(() => model.FechaCompromisoOrden.HasValue ? model.FechaCompromisoOrden.Value.ToString("dd/MM/yyyy") : "");

            FechaRenegociadaProperty = new BindableProperty<DateTime?>(model.FechaRenegociada, new NullableDateTimeConverter());
            FechaInstalacionProperty = new BindableReadOnlyWrapperProperty<string>(() => model.FechaDeInstalacion.HasValue ? model.FechaDeInstalacion.Value.ToString("dd/MM/yyyy") : "");



            TipoDeSolucionProperty = _propertyFactory.CreateBindableProperty(model.TipoSolucion == "" ? "?" : model.TipoSolucion, true);
            DiasDeInstalacionProperty = new BindableReadOnlyWrapperProperty<string>(() => model.DiasDeInstalacion.HasValue ? model.DiasDeInstalacion.ToString() : "?");

            TareaIdProperty = _propertyFactory.CreateBindableProperty(model.TareaId, true);
            TomadoPorProperty = _propertyFactory.CreateBindableProperty(model.TomadoPor, true);
            FechaInicioProperty = _propertyFactory.CreateBindableProperty(model.FechaInicioTarea.ToString("dd/MM/yyy HH:mm:ss"), true);
            FechaFinProperty = _propertyFactory.CreateBindableProperty(model.FechaCierreTarea.Fecha.Value.ToString("dd/MM/yyyy HH:mm:ss"), true);

            ResultadoProperty = _propertyFactory.CreateBindableProperty(model.Estado.ToString(), true);
            TiempoEnBandejaProperty = _propertyFactory.CreateBindableProperty(model.TiempoEfectivoPermanenciaTareaEnEstaBandeja.ToString("F2"), true);

            FechaCierreTareaAnteriorProperty = new BindableProperty<DateTime?>(model.FechaCierreTaraeAnterior, new NullableDateTimeConverter());
            FechaCierreTareaAnteriorProperty.AddValidationRule(new RejectValidationRule<DateTime?>(fechaCierreAnterior => FechaInicioOrdenProperty.Value.HasValue && fechaCierreAnterior.HasValue && fechaCierreAnterior.Value < FechaInicioOrdenProperty.Value.Value, "La fecha de cierre de la tarea no puede ser menor a la fecha de inicio de la orden."));
            FechaCierreTareaAnteriorProperty.ValidationDependsOn(FechaInicioOrdenProperty);
            FechaCierreTareaAnteriorProperty.AddValidationRule(new RejectValidationRule<DateTime?>(fechaCierreAnterior => fechaCierreAnterior.HasValue && fechaCierreAnterior.Value > model.FechaInicioTarea, "La fecha de cierre de la tarea anterior no puede ser mayor a la fecha de inicio de esta tarea."));

            //OnTimeAPrioriProperty = new BindableReadOnlyWrapperProperty<string>(() => _model.OnTimeAPriori.ToString());
            //PermanenciaTeoricaAPrioriProperty = new BindableReadOnlyWrapperProperty<string>(() => _model.PermanenciaTeoricaApriori.ToString("F2"));
            //DemorasProperty = new BindableReadOnlyWrapperProperty<string>(() => !_model.Demoras.HasValue ? "0" : _model.Demoras.ToString());
            //PermanenciaTeoricaAPosterioriProperty = new BindableReadOnlyWrapperProperty<string>(() => model.PermanenciaTeoricaAPosteriori.ToString(("F2")));

            //OnTimeAPosterioriProperty = new BindableReadOnlyWrapperProperty<string>(() => _model.OnTimeAPosteriori.ToString());
            //AnalizarProperty = new BindableReadOnlyWrapperProperty<string>(() => _model.Analizar ? "Sí" : "No");
            //AnalizadaProperty = _propertyFactory.CreateBindableProperty(_model.Analizada ? "Sí" : "No", true);


            _properties.Add(ZonaProperty);
            _properties.Add(ClienteProperty);
            _properties.Add(TipoDeOrdenProperty);
            _properties.Add(OrdenProperty);
            _properties.Add(NumeroEnlaceProperty);
            _properties.Add(IdentificadorProperty);
            _properties.Add(FechaInicioOrdenProperty);
            _properties.Add(FechaDeCompromisoProperty);
            _properties.Add(FechaRenegociadaProperty);
            _properties.Add(FechaInstalacionProperty);
            _properties.Add(TipoDeOrdenProperty);
            _properties.Add(DiasDeInstalacionProperty);
            _properties.Add(TareaIdProperty);
            _properties.Add(TomadoPorProperty);
            _properties.Add(FechaInicioOrdenProperty);
            _properties.Add(FechaFinProperty);
            _properties.Add(ResultadoProperty);
            _properties.Add(TiempoEnBandejaProperty);
            _properties.Add(FechaCierreTareaAnteriorProperty);
            //_properties.Add(OnTimeAPrioriProperty);
            //_properties.Add(DemorasProperty);
            //_properties.Add(OnTimeAPosterioriProperty);
            //_properties.Add(AnalizarProperty);
        }

        //---------------------------------------------------------------------
        // Properties
        //---------------------------------------------------------------------
        public ObservableCollection<object> Properties => new ObservableCollection<object>(_propertyBag.Properties);
        [IncludeProperty] public IBindableProperty<string> InboxProperty { get; set; }
        [IncludeProperty] public BindableProperty<string> ZonaProperty { get; set; }
        [IncludeProperty] public IBindableProperty<string> ClienteProperty { get; set; }
        [IncludeProperty("Tipo de orden")] public IBindableProperty<string> TipoDeOrdenProperty { get; set; }
        [IncludeProperty("N° orden")] public IBindableProperty<long> OrdenProperty { get; set; }
        [IncludeProperty("N° enlace")] public BindableProperty<long> NumeroEnlaceProperty { get; set; }
        [IncludeProperty] public IBindableProperty<string> IdentificadorProperty { get; set; }
        [IncludeProperty("Inicio orden")] public BindableProperty<DateTime?> FechaInicioOrdenProperty { get; set; }
        [IncludeProperty("Compromiso orden")] public IBindableProperty<string> FechaDeCompromisoProperty { get; set; }

        [IncludeProperty("Fecha renegociada")] public  BindableProperty<DateTime?> FechaRenegociadaProperty { get; set; }
        [IncludeProperty("Fecha de instalación")] public IBindableProperty<string> FechaInstalacionProperty { get; set; }

        [IncludeProperty("Tipo de solución")] public BindableProperty<string> TipoDeSolucionProperty { get; set; }
        [IncludeProperty("Días de instalación")] public IBindableProperty<string> DiasDeInstalacionProperty { get; set; }
        [IncludeProperty("Tarea ID")] public IBindableProperty<string> TareaIdProperty { get; set; }
        [IncludeProperty("Tomado por")] public IBindableProperty<string> TomadoPorProperty { get; set; }
        [IncludeProperty("Inicio tarea")] public IBindableProperty<string> FechaInicioProperty { get; set; }
        [IncludeProperty("Cierre tarea")] public IBindableProperty<string> FechaFinProperty { get; set; }
        [IncludeProperty] public IBindableProperty<string> ResultadoProperty { get; set; }
        [IncludeProperty("Permanencia")] public IBindableProperty<string> TiempoEnBandejaProperty { get; set; }
        [IncludeProperty("Cierre tarea anterior")] public BindableProperty<DateTime?> FechaCierreTareaAnteriorProperty { get; set; }
        //[IncludeProperty("On time a priori")] public IBindableProperty<string> OnTimeAPrioriProperty { get; set; }
        //[IncludeProperty("Permanencia teórica a priori")] public IBindableProperty<string> PermanenciaTeoricaAPrioriProperty { get; set; } 
        //[IncludeProperty("Demoras")] public IBindableProperty<string> DemorasProperty { get; set; }
        //[IncludeProperty("Permanencia teórica a posteriori")] public IBindableProperty<string> PermanenciaTeoricaAPosterioriProperty { get; set; }
        //[IncludeProperty("On time a posteriori")] public IBindableProperty<string> OnTimeAPosterioriProperty { get; set; }
        //[IncludeProperty("Analizar?")] public IBindableProperty<string> AnalizarProperty { get; set; }
        //[IncludeProperty("Analizada?")] public IBindableProperty<string> AnalizadaProperty { get; set; }

        //---------------------------------------------------------------------
        // Comandos
        //---------------------------------------------------------------------
        [Command]
        public ICommand GuardarCommand { get; private set; }

        private void _guardarExecuted(object o)
        {
            _updateModel();
            _repository.SaveAll();
            foreach (var p in _properties)
            {
                p.RaisePropertyChanged();
            }
        }

        private bool _guardarCanExecute(object o)
        {
            return _properties.OfType<INotifyDataErrorInfo>().All(p => !p.HasErrors);
        }

        [Command]
        public ICommand AceptarCommand { get; private set; }
        private void _aceptarExecuted(object o)
        {
            _updateModel();
            _repository.SaveAll();
            foreach (var p in _properties)
            {
                p.RaisePropertyChanged();
            }
            OnAcceptExecuted();
            OnButtonPressed(ButtonType.Accept);
        }
        private void _updateModel()
        {
            _model.Cliente = ClienteProperty.Value;
            _model.Enlace = NumeroEnlaceProperty.Value;
            _model.FechaInicioOrden = FechaInicioOrdenProperty.Value;
            _model.FechaRenegociada = FechaRenegociadaProperty.Value;
            _model.TomadoPor = TomadoPorProperty.Value;
            _model.FechaCierreTaraeAnterior = FechaCierreTareaAnteriorProperty.Value;
        }
        private bool _aceptarCanExecute(object o) => true;

        [Command]
        public ICommand CancelarCommand { get; private set;}
        private void _cancelarExecuted(object o)
        {
            OnCancelExecuted();
            OnButtonPressed(ButtonType.Cancel);
        }
        private bool _cancelarCanExecute(object o) => true;

        [Command]
        public ICommand RestablecerCommand { get; private set; }
        private void _restablecerExecuted(object o)
        {
            foreach (var viewModelProperty in _propertyBag.Properties)
            {
                (viewModelProperty as ITrackableViewModelProperty)?.RestoreOriginalValue();
            }
            OnButtonPressed(ButtonType.RestoreValues);
        }
        private bool _restablecerCanExecute(object o) => true;



        //---------------------------------------------------------------------
        // IDialog
        //---------------------------------------------------------------------
        public event ButtonPressedEventHandler ButtonPressed;
        protected virtual void OnButtonPressed(ButtonType buttonType)
        {
            ButtonPressed?.Invoke(this, new ButtonPressedEventArgs(buttonType));
        }

        //---------------------------------------------------------------------
        // INotifyDialogAccepted
        //---------------------------------------------------------------------
        public event DialogAcceptedEventHandler DialogAccepted;
        protected virtual void OnAcceptExecuted()
        {
            DialogAccepted?.Invoke(this, new EventArgs());
        }

        //---------------------------------------------------------------------
        // INotifyDialogClosed
        //---------------------------------------------------------------------
        public event DialogCanceledEventHandler DialogCanceled;
        protected virtual void OnCancelExecuted()
        {
            DialogCanceled?.Invoke(this, new EventArgs());
        }
    }
}
