﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Forms.Design;
using System.Windows.Input;
using System.Windows.Threading;
using PostSharp.Extensibility;
using SIFO.DesktopUI.CollectionView;
using SIFO.DesktopUI.Framework;
using SIFO.DesktopUI.Resources;
using SIFO.DesktopUI.TareasCerradas.Analizar;
using SIFO.DesktopUI.TareasCerradas.Editar;
using SIFO.DesktopUI.TareasCerradas.Estadisticas;
using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.CollectionView;
using SIFO.DesktopUI.Utilities.Properties;
using SIFO.DesktopUI.Utilities.Reflection;
using SIFO.Model;
using SIFO.Services;
using SIFO.DesktopUI.TareasIngresadas;

namespace SIFO.DesktopUI.TareasCerradas
{


    public class b
    {
        public bool Ingresada { get; set; }
        public bool Cerrada { get; set; }
        public bool Pendiente { get; set; }
    }

    public class BuscadorDeTareas
    {
        private readonly HashSet<string> _ingresadas = new HashSet<string>();
        private readonly HashSet<string> _cerradas = new HashSet<string>(); 
        private readonly HashSet<string> _pendientes = new HashSet<string>(); 


        public BuscadorDeTareas(ITareasIngresadasModelRepository ingresadas, ITareasCerradasModelRepository cerradas, ITareasPendientesModelRepository pendientes)
        {
            foreach (var tarea in ingresadas.SelectAll())
            {
                var idi = tarea.TareaId;
                if (!_ingresadas.Contains(idi))
                {
                    _ingresadas.Add(idi);
                }
               
            }

            foreach (var tarea in cerradas.SelectAll())
            {
                var idi = tarea.TareaId;
                if (!_cerradas.Contains(idi))
                {
                    _cerradas.Add(idi);
                }

            }

            foreach (var tarea in pendientes.SelectAll())
            {
                var idi = tarea.TareaId;
                if (!_pendientes.Contains(idi))
                {
                    _pendientes.Add(idi);
                }

            }
        }

        public bool Ingresada(string tareaId) => _ingresadas.Contains(tareaId);
        public bool Cerradas(string tareaId) => _cerradas.Contains(tareaId);
        public bool Pendientes(string tareaId) => _pendientes.Contains(tareaId);
    }

    public abstract class TareasViewModel : INotifyPropertyChanged
    {

        //---------------------------------------------------------------------
        // Campos
        //---------------------------------------------------------------------
        private readonly CustomCollectionView<TareaViewModel> _customCollectionView;
        private TareasCerradasAnalisisViewModel _tareasCerradasAnalisisViewModel;
        private readonly List<TareaViewModel> _ordenes = new List<TareaViewModel>();
        private TareaViewModel _selectedItem;

        private readonly ITareaModelRepository<ITarea> _tareasModelRepository;
        private readonly TareaAnalizadaModelRepository _tareaAnalizadaModelRepository;


        private ICollectionView _itemsSource;
        private readonly EditViewModelFactory _editarOrdenViewModelFactory;

        //---------------------------------------------------------------------
        // Constructores
        //---------------------------------------------------------------------
        protected TareasViewModel(ITareaModelRepository<ITarea> tareasModelRepository, BuscadorDeTareas b)
        {

            MaxWidth = new WrappingViewModelProperty<string>(() => WindowWidth?.Value);

            _tareasModelRepository = tareasModelRepository;


            _editarOrdenViewModelFactory = new EditViewModelFactory(new IncludePropertyAttributeReflector(), new CommandAttributeReflector(), tareasModelRepository);


            foreach (var model in _tareasModelRepository.SelectAll())
            {
                var id = model.TareaId;
                var viewModel = new TareaViewModel(model, b.Ingresada(id), b.Cerradas(id), b.Pendientes(id));
                
                //viewModel.EstaIngresadaProperty.Value = b.Ingresada(id);
                //viewModel.EstaCerradaProperty.Value = b.Cerradas(id);
                //viewModel.EstaPendienteProperty.Value = b.Pendientes(id);
                _ordenes.Add(viewModel);
            }

            _tareasCerradasAnalisisViewModel = new TareasCerradasAnalisisViewModel(tareasModelRepository.SelectAll());

            _customCollectionView = new CustomCollectionView<TareaViewModel>(_ordenes);
            GroupService = new GroupService(ItemsSource);

            SummaryViewModel = new NewSummaryViewModel()
            {
                CantidadTotal = _customCollectionView.AllCount,
                CantidadEnVistaActual = _customCollectionView.FilteredCount

            };

            RemoveAllGroupsCommand = new RelayCommand(_removeAllGroupsExecuted, _removeAllGroupsCanExecute);
            RemoveAllFiltersCommand = new RelayCommand(_removeAllFiltersExecuted, _removeAllFiltersCanExecute);
            ShowFilterOptionsCommand = new RelayCommand(_showFilterOptionsExecuted, _showFilterOptionsCanExecute);
            EditarCommand = new RelayCommand(_editarExecuted, _editarCanExecute);




        }


        //---------------------------------------------------------------------
        // Propiedades
        //---------------------------------------------------------------------

        public TareasCerradasAnalisisViewModel TareasCerradasEstadisticasViewModel
        {
            get
            {
                return _tareasCerradasAnalisisViewModel;
            }
            set
            {
                _tareasCerradasAnalisisViewModel = value;
                OnPropertyChanged();
            }
        }
        public IBindableProperty<bool> ShowColumn { get; private set; } = new ReadWriteProperty<bool>(false);
        public IBindableProperty<string> Width { get; private set; } = new ReadWriteProperty<string>("0");
        public IBindableProperty<string> WindowWidth { get; set; } = new ReadWriteProperty<string>("0");
        public IBindableProperty<string> MaxWidth { get; set; }
        public IBindableProperty<bool> ShowFilterOptions { get; private set; } = new ReadWriteProperty<bool>(true);


        public ICollectionView ItemsSource => _customCollectionView.View;

        public TareaViewModel SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (_selectedItem == value) return;
                _selectedItem = value;
                OnPropertyChanged();
            }
        }


        public FilterServiceViewModel<TareaViewModel> FilterServiceViewModel => _customCollectionView.Filter;
        public GroupService GroupService { get; private set; }
        public NewSummaryViewModel SummaryViewModel { get; private set; }




        public IEnumerable<string> OperadoresDeComparacion { get; } = new List<string>() { "<", ">" };
        public IBindableProperty<double> SearchValueProperty { get; set; } = new ReadWriteProperty<double>(0);
        public IBindableProperty<string> OperadorDeComparacionSeleccionado { get; set; } = new ReadWriteProperty<string>("<");

        //---------------------------------------------------------------------
        // Commands
        //---------------------------------------------------------------------
        [Command(nameof(_removeAllGroupsExecuted), nameof(_removeAllGroupsCanExecute))]
        public ICommand RemoveAllGroupsCommand { get; private set; }

        private void _removeAllGroupsExecuted(object o)
        {

            GroupService.RemoveAllGroupsCommand.Execute(o);

        }

        private bool _removeAllGroupsCanExecute(object o)
        {
            return GroupService.RemoveAllGroupsCommand.CanExecute(o);
        }


        public ICommand RemoveAllFiltersCommand { get; private set; }
        private void _removeAllFiltersExecuted(object o)
        {
            FilterServiceViewModel.RemoveAllFilters();
            var ordenesFiltradas = new ObservableCollection<TareaViewModel>();
            foreach (var orden in _ordenes)
            {
                ordenesFiltradas.Add(orden);
            }


            _customCollectionView.RemoveAllFilters();
            SummaryViewModel.CantidadEnVistaActual = SummaryViewModel.CantidadTotal;
            updateCharts();
        }
        private bool _removeAllFiltersCanExecute(object o)
        {
            return _customCollectionView.IsFiltered;
        }

        public ICommand FilterCommand { get; set; }
        private void _filterExecuted(object o)
        {
            string propiedad = "TiempoEnBandejaProperty";
            var s = nameof(_selectedItem.TiempoEnBandejaProperty);
            var operadorDeComparacion = OperadorDeComparacionSeleccionado.Value;
            //var vm = FilterServiceViewModel.GetFilterDialogViewModelForProperty(propiedad);
            var vm = _customCollectionView.Filter.GetFilterDialogViewModelForProperty(propiedad);


            if (vm.ValueType == typeof(double) && (operadorDeComparacion == "<" || operadorDeComparacion == ">"))
            {
                Predicate<double> predicate = d => operadorDeComparacion == "<" ? d < SearchValueProperty.Value : d > SearchValueProperty.Value;
                vm.UnselectAll();
                foreach (var valueViewModel in vm)
                {
                    var value = (double)valueViewModel.Value;
                    if (predicate(value))
                    {
                        valueViewModel.IsSelected = true;
                    }
                }
            }
            //FilterServiceViewModel.ApplyFiltersCommand.Execute(null);
            _customCollectionView.ApplyFilters();
            SummaryViewModel.CantidadEnVistaActual = _customCollectionView.FilteredCount;
            updateCharts();
        }
        private bool _filterCanExecute(object o)
        {
            return true;
        }

        public ICommand ShowFilterOptionsCommand { get; private set; }
        private void _showFilterOptionsExecuted(object o)
        {
            var propertyName = (string)o;
            var view = new FilterOptionsView();
            view.AcceptCommand = new RelayCommand(o1 => _customCollectionView.ApplyFilters(), o1 => true);
            view.GroupCommand = GroupService.GroupCommand;

            var window = new Window
            {
                Content = view,
                HorizontalContentAlignment = HorizontalAlignment.Stretch
            };


            var filterDialogViewModel = FilterServiceViewModel.GetFilterDialogViewModelForProperty(propertyName);
            foreach (var a in filterDialogViewModel)
            {
                a.IsSelected = a.IsCurrentFilter;
            }

            _customCollectionView.SetCurrentViewCountForProperty(propertyName);
            window.DataContext = filterDialogViewModel;

            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            window.WindowStyle = WindowStyle.ToolWindow;
            window.Width = 300;
            window.Height = 600;
            window.ShowDialog();


            SummaryViewModel.CantidadEnVistaActual = _customCollectionView.FilteredCount;

            updateCharts();
        }

        private void updateCharts()
        {
            List<ITarea> universe = new List<ITarea>();
            var e = ItemsSource.GetEnumerator();
            while (e.MoveNext())
            {
                var tareaCerradaViewModel = e.Current as TareaViewModel;
                if (tareaCerradaViewModel != null)
                {
                    universe.Add(tareaCerradaViewModel.Model);
                }
            }
            this.TareasCerradasEstadisticasViewModel = new TareasCerradasAnalisisViewModel(universe);
        }

        private bool _showFilterOptionsCanExecute(object o)
        {
            return true;
        }



        public ICommand EditarCommand { get; private set; }
        private void _editarExecuted(object o)
        {
            var window = new Window();
            //var view = new EditView();
            var view = new TareaCerradaDetalleView();
            window.Content = view;
            window.WindowState = WindowState.Maximized;


            var editarOrdenViewModel = _editarOrdenViewModelFactory.CreateViewModel(_selectedItem.Model.TareaId);
            editarOrdenViewModel.ButtonPressed += (sender, args) =>
            {
                if (args.ButtonType == ButtonType.Accept || args.ButtonType == ButtonType.Cancel)
                {
                    window.Close();
                }
            };
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            window.DataContext = editarOrdenViewModel;
            var re = window.ShowDialog();

            _selectedItem.RaisePropertyChangeForAllProperties();
            FilterServiceViewModel.Refresh();
        }
        private bool _editarCanExecute(object o)
        {
            return true;
        }


        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    //public class TareasCerradasViewModel : INotifyPropertyChanged
    //{


    //    private readonly CustomCollectionView<TareaViewModel> _customCollectionView; 

    //    //---------------------------------------------------------------------
    //    // Campos
    //    //---------------------------------------------------------------------
    //    private TareasCerradasAnalisisViewModel _tareasCerradasAnalisisViewModel;
    //    private readonly List<TareaViewModel> _ordenes = new List<TareaViewModel>();


    //    private TareaViewModel _selectedItem;

    //    private readonly TareasCerradasModelRepository _tareaCerradaModelRepository;
    //    private readonly TareaAnalizadaModelRepository _tareaAnalizadaModelRepository;

      
    //    private ICollectionView _itemsSource;
    //    private readonly EditViewModelFactory _editarOrdenViewModelFactory;

    //    //---------------------------------------------------------------------
    //    // Constructores
    //    //---------------------------------------------------------------------
    //    [InitializeCommandsAspect]
    //    public TareasCerradasViewModel(TareasCerradasModelRepository tareaCerradaModelRepository, TareaAnalizadaModelRepository tareaAnalizadaModelRepository, EditViewModelFactory editarOrdenViewModelFactory, ITareasIngresadasModelRepository ingresadasModelRepo)
    //    {

    //        MaxWidth = new WrappingViewModelProperty<string>(() => WindowWidth?.Value);

    //        _tareaCerradaModelRepository = tareaCerradaModelRepository;
    //        _tareaAnalizadaModelRepository = tareaAnalizadaModelRepository;

        
    //        _editarOrdenViewModelFactory = editarOrdenViewModelFactory;

    //        var ingresadas = ingresadasModelRepo.SelectAll();

    //        foreach (var model in _tareaCerradaModelRepository.SelectAll())
    //        {
    //            bool estaIngresada = ingresadas.Count(i => i.TareaId == model.TareaId) > 0;

    //            var analisis = tareaAnalizadaModelRepository.Contains(model.TareaId) ? tareaAnalizadaModelRepository.SelectByTareaId(model.TareaId) : null;
    //            var viewModel = new TareaViewModel(model);
    //            _ordenes.Add(viewModel);
    //        }

    //        _tareasCerradasAnalisisViewModel = new TareasCerradasAnalisisViewModel(tareaCerradaModelRepository.SelectAll());

    //        _customCollectionView = new CustomCollectionView<TareaViewModel>(_ordenes);
    //        GroupService = new GroupService(ItemsSource);

    //        SummaryViewModel = new NewSummaryViewModel()
    //        {
    //            CantidadTotal = _customCollectionView.AllCount,
    //            CantidadEnVistaActual = _customCollectionView.FilteredCount

    //        };

    //    }


    //    //---------------------------------------------------------------------
    //    // Propiedades
    //    //---------------------------------------------------------------------

    //    public TareasCerradasAnalisisViewModel TareasCerradasEstadisticasViewModel
    //    {
    //        get
    //        {
    //            return _tareasCerradasAnalisisViewModel;
    //        }
    //        set
    //        {
    //            _tareasCerradasAnalisisViewModel = value;
    //            OnPropertyChanged();
    //        }
    //    }
    //    public IBindableProperty<bool> ShowColumn { get; private set; } = new ReadWriteProperty<bool>(false);
    //    public IBindableProperty<string> Width { get; private set; } = new ReadWriteProperty<string>("0");
    //    public IBindableProperty<string> WindowWidth { get; set; } = new ReadWriteProperty<string>("0");
    //    public IBindableProperty<string> MaxWidth { get; set; }
    //    public IBindableProperty<bool> ShowFilterOptions { get; private set; } = new ReadWriteProperty<bool>(true);


    //    public ICollectionView ItemsSource => _customCollectionView.View;

    //    public TareaViewModel SelectedItem
    //    {
    //        get { return _selectedItem; }
    //        set
    //        {
    //            if (_selectedItem == value) return;
    //            _selectedItem = value;
    //            OnPropertyChanged();
    //        }
    //    }


    //    public FilterServiceViewModel<TareaViewModel> FilterServiceViewModel => _customCollectionView.Filter;
    //    public GroupService GroupService { get; private set; }
    //    public NewSummaryViewModel SummaryViewModel { get; private set; }




    //    public IEnumerable<string> OperadoresDeComparacion { get; } = new List<string>() { "<", ">" };
    //    public IBindableProperty<double> SearchValueProperty { get; set; } = new ReadWriteProperty<double>(0);
    //    public IBindableProperty<string> OperadorDeComparacionSeleccionado { get; set; } = new ReadWriteProperty<string>("<");

    //    //---------------------------------------------------------------------
    //    // Commands
    //    //---------------------------------------------------------------------
    //    [Command(nameof(_removeAllGroupsExecuted), nameof(_removeAllGroupsCanExecute))]
    //    public ICommand RemoveAllGroupsCommand { get; private set; }

    //    private void _removeAllGroupsExecuted(object o)
    //    {

    //        GroupService.RemoveAllGroupsCommand.Execute(o);

    //    }

    //    private bool _removeAllGroupsCanExecute(object o)
    //    {
    //        return GroupService.RemoveAllGroupsCommand.CanExecute(o);
    //    }


    //    public ICommand RemoveAllFiltersCommand { get; private set; }
    //    private void _removeAllFiltersExecuted(object o)
    //    {
    //        FilterServiceViewModel.RemoveAllFilters();
    //        var ordenesFiltradas = new ObservableCollection<TareaViewModel>();
    //        foreach (var orden in _ordenes)
    //        {
    //            ordenesFiltradas.Add(orden);
    //        }


    //        _customCollectionView.RemoveAllFilters();
    //        SummaryViewModel.CantidadEnVistaActual = SummaryViewModel.CantidadTotal;
    //        updateCharts();
    //    }
    //    private bool _removeAllFiltersCanExecute(object o)
    //    {
    //        return _customCollectionView.IsFiltered;
    //    }

    //    public ICommand FilterCommand { get; set; }
    //    private void _filterExecuted(object o)
    //    {
    //        string propiedad = "TiempoEnBandejaProperty";
    //        var s = nameof(_selectedItem.TiempoEnBandejaProperty);
    //        var operadorDeComparacion = OperadorDeComparacionSeleccionado.Value;
    //        //var vm = FilterServiceViewModel.GetFilterDialogViewModelForProperty(propiedad);
    //        var vm = _customCollectionView.Filter.GetFilterDialogViewModelForProperty(propiedad);
           

    //        if (vm.ValueType == typeof (double) && (operadorDeComparacion == "<" || operadorDeComparacion == ">"))
    //        {
    //            Predicate<double> predicate = d => operadorDeComparacion == "<" ? d < SearchValueProperty.Value : d > SearchValueProperty.Value;
    //            vm.UnselectAll();
    //            foreach (var valueViewModel in vm)
    //            {
    //                var value = (double)valueViewModel.Value;
    //                if (predicate(value))
    //                {
    //                    valueViewModel.IsSelected = true;
    //                }
    //            }
    //        }
    //        //FilterServiceViewModel.ApplyFiltersCommand.Execute(null);
    //        _customCollectionView.ApplyFilters();
    //        SummaryViewModel.CantidadEnVistaActual = _customCollectionView.FilteredCount;
    //        updateCharts();
    //    }
    //    private bool _filterCanExecute(object o)
    //    {
    //        return true;
    //    }

    //    public ICommand ShowFilterOptionsCommand { get; private set; }
    //    private void _showFilterOptionsExecuted(object o)
    //    {
    //        var propertyName = (string)o;
    //        var view = new FilterOptionsView();
    //        view.AcceptCommand = new RelayCommand(o1 => _customCollectionView.ApplyFilters(), o1 => true);
    //        view.GroupCommand = GroupService.GroupCommand;

    //        var window = new Window
    //        {
    //            Content = view,
    //            HorizontalContentAlignment = HorizontalAlignment.Stretch
    //        };


    //        var filterDialogViewModel = FilterServiceViewModel.GetFilterDialogViewModelForProperty(propertyName);
    //        foreach (var a in filterDialogViewModel)
    //        {
    //            a.IsSelected = BuscadorDeTareas.IsCurrentFilter;
    //        }

    //        _customCollectionView.SetCurrentViewCountForProperty(propertyName);
    //        window.DataContext = filterDialogViewModel;

    //        window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
    //        window.WindowStyle = WindowStyle.ToolWindow;
    //        window.Width = 300;
    //        window.Height = 600;
    //        window.ShowDialog();


    //        SummaryViewModel.CantidadEnVistaActual = _customCollectionView.FilteredCount;

    //        updateCharts();
    //    }

    //    private void updateCharts()
    //    {
    //        List<ITarea> universe = new List<ITarea>();
    //        var e = ItemsSource.GetEnumerator();
    //        while (e.MoveNext())
    //        {
    //            var tareaCerradaViewModel = e.Current as TareaViewModel;
    //            if (tareaCerradaViewModel != null)
    //            {
    //                universe.Add(tareaCerradaViewModel.Model);
    //            }
    //        }
    //        this.TareasCerradasEstadisticasViewModel = new TareasCerradasAnalisisViewModel(universe);
    //    }

    //    private bool _showFilterOptionsCanExecute(object o)
    //    {
    //        return true;
    //    }



    //    public ICommand EditarCommand { get; private set; }
    //    private void _editarExecuted(object o)
    //    {
    //        var window = new Window();
    //        //var view = new EditView();
    //        var view = new TareaCerradaDetalleView();
    //        window.Content = view;
    //        window.WindowState = WindowState.Maximized;


    //        var editarOrdenViewModel = _editarOrdenViewModelFactory.CreateViewModel(_selectedItem.Model.TareaId);
    //        editarOrdenViewModel.ButtonPressed += (sender, args) =>
    //        {
    //            if (args.ButtonType == ButtonType.Accept || args.ButtonType == ButtonType.Cancel)
    //            {
    //                window.Close();
    //            }
    //        };
    //        window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
    //        window.DataContext = editarOrdenViewModel;
    //        var re = window.ShowDialog();

    //        _selectedItem.RaisePropertyChangeForAllProperties();
    //        FilterServiceViewModel.Refresh();
    //    }
    //    private bool _editarCanExecute(object o)
    //    {
    //        return true;
    //    }


    //    //---------------------------------------------------------------------
    //    // INotifyPropertyChanged
    //    //---------------------------------------------------------------------
    //    public event PropertyChangedEventHandler PropertyChanged;

    //    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    //    {
    //        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    //    }
    //}
}



