using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using OxyPlot;
using OxyPlot.Series;

namespace SIFO.DesktopUI.TareasCerradas.Estadisticas
{
    public class PieChartViewModel : INotifyPropertyChanged
    {
        private readonly string _title;
        private PlotModel _plotViewModel;

        public PieChartViewModel(string title)
        {
            _title = title;
        }

        public PieChartViewModel() : this(string.Empty) { }

        public PlotModel SetSeries<T>(IEnumerable<T> universe, Func<T, string> getter)
        {
            var values = universe.Select(getter).Distinct();

            var plotModel = new PlotModel()
            {
                Title = _title
            };
            var piesSeries = new OxyPlot.Series.PieSeries();
            foreach (var value in values)
            {
                var slice = new PieSlice(value, universe.Count(t => getter(t) == value));
                slice.IsExploded = true;
                piesSeries.Slices.Add(slice);
            }
            plotModel.Series.Add(piesSeries);

            _plotViewModel = plotModel;
            return plotModel;
        }


        public PlotModel PlotViewModel
        {
            get { return _plotViewModel; }
        }

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }




    }
}