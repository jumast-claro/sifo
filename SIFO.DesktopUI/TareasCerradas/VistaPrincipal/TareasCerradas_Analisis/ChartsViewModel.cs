﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SIFO.DesktopUI.TareasCerradas.Estadisticas;
using SIFO.DesktopUI.TareasIngresadas;

namespace SIFO.DesktopUI.TareasCerradas.VistaPrincipal.Graficos
{
    public class ChartsViewModel
    {
        public PieChartViewModel PieChartViewModel { get; set; }
        public BarChartViewModel BarChartViewModel { get; set; }
    }

    public class IngresadasChartsViewModel
    {
        public PieChartViewModel PieChartViewModel { get; set; }
        public TareaIngresadaBarChartViewModel BarChartViewModel { get; set; }
    }
}
