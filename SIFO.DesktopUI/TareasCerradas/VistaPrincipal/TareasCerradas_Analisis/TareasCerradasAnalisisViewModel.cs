﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using SIFO.DesktopUI.TareasCerradas.VistaPrincipal.Graficos;
using SIFO.DesktopUI.Utilities;
using SIFO.Model;
using PieSeries = LiveCharts.PieSeries;

namespace SIFO.DesktopUI.TareasCerradas.Estadisticas
{
    public class TareasCerradasAnalisisViewModel : INotifyPropertyChanged
    {
        private readonly IEnumerable<ITarea> _universe;
        //private readonly ITareasCerradasModelRepository _repo;

        public TareasCerradasAnalisisViewModel(IEnumerable<ITarea> universe)
        {
            _universe = universe;
            //_repo = repo;

            var tareasPorPlantaPieChartViewModel = new PieChartViewModel("País - Tareas cerradas por planta");
            tareasPorPlantaPieChartViewModel.SetSeries(_universe, t => t.Planta.ToString());

            var tareasPorZonaPieChartViewModel = new PieChartViewModel("País - Tareas cerradas por zona");
            tareasPorZonaPieChartViewModel.SetSeries(universe, t => t.Zona.ToString());

            var tareasPorInboxPieChartViewModel = new PieChartViewModel("País - Tareas cerradas por inbox");
            tareasPorInboxPieChartViewModel.SetSeries(universe, t => t.Inbox);

            var tareasPorResultadoPieChartViewModel = new PieChartViewModel("País - Tareas por resultado");
            tareasPorResultadoPieChartViewModel.SetSeries(universe, t => t.Estado.ToString());

            var tareasPorTipoDeOrdenPieChartViewModel = new PieChartViewModel("País - Tareas por tipo de orden");
            tareasPorTipoDeOrdenPieChartViewModel.SetSeries(universe, t => t.TipoDeOrden);

            var tareasPorPlantaBarChartViewModel = new BarChartViewModel(universe, t => t.Planta.ToString());
            var tareasPorZonaBarChartViewModel = new BarChartViewModel(universe, t => t.Zona.ToString());
            var tareasPorTipoDeOrdenBarChartViewModel = new BarChartViewModel(universe, t => t.TipoDeOrden);
            var tareasPorResultadoBarChartViewModel = new BarChartViewModel(universe, t => t.Estado.ToString());


            TareasPorPlantaChartsViewModel = new ChartsViewModel()
            {
                PieChartViewModel = tareasPorPlantaPieChartViewModel,
                BarChartViewModel = tareasPorPlantaBarChartViewModel
            };

            TareasPorZonaChartsViewModel = new ChartsViewModel()
            {
                PieChartViewModel = tareasPorZonaPieChartViewModel,
                BarChartViewModel = tareasPorZonaBarChartViewModel
            };

            TareasPorTipoDeOrdenChartsViewModel = new ChartsViewModel()
            {
                PieChartViewModel = tareasPorTipoDeOrdenPieChartViewModel,
                BarChartViewModel = tareasPorTipoDeOrdenBarChartViewModel
            };

            TareasPorResultadoChartsViewModel = new ChartsViewModel()
            {
                PieChartViewModel = tareasPorResultadoPieChartViewModel,
                BarChartViewModel = tareasPorResultadoBarChartViewModel
            };
        }

        //---------------------------------------------------------------------
        // Propiedades
        //---------------------------------------------------------------------
        public ChartsViewModel TareasPorPlantaChartsViewModel { get; set; }
        public ChartsViewModel TareasPorZonaChartsViewModel { get; set; }
        public ChartsViewModel TareasPorTipoDeOrdenChartsViewModel { get; set; }
        public ChartsViewModel TareasPorResultadoChartsViewModel { get; set; }


        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


    }
}
