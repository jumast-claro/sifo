using System;
using System.Collections.Generic;
using System.Linq;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using SIFO.Model;

namespace SIFO.DesktopUI.TareasCerradas.Estadisticas
{
    public class BarChartViewModel
    {
        public BarChartViewModel(IEnumerable<ITarea> universe, Func<ITarea, string> getter)
        {
            var meses = universe.Select(t => t.FechaCierreTarea.Mes).Distinct().OrderBy((i => i)).ToList();


            var valores = universe.Select(t => getter(t)).Distinct().ToList();
            PlotViewModel = new PlotModel();

            foreach (var valor in valores)
            {

                var cs = new ColumnSeries() {LabelFormatString = "{0}"};
                var i = 0;
                foreach (var mes in meses)
                {
                    cs.Items.Add(new ColumnItem{CategoryIndex = i, Value = universe.Count(t => getter(t) == valor && t.FechaCierreTarea.Mes == mes)});
                    i++;
                }
                PlotViewModel.Series.Add(cs);

               
            }


            var cantAxis = new LinearAxis() { Position = AxisPosition.Left, Title = "Cantidad"};

            PlotViewModel.Axes.Add(cantAxis);


        }


        public PlotModel PlotViewModel { get; set; }

    }
}