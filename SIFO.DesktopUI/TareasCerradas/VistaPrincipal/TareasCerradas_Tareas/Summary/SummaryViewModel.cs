﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using SIFO.DesktopUI.Annotations;
using SIFO.Model;

namespace SIFO.DesktopUI.TareasCerradas
{

    public class NewSummaryViewModel : INotifyPropertyChanged
    {

        private int _cantidadTotal;
        private int _cantidadEnVistaActual;


        public int CantidadTotal
        {
            get
            {
                return _cantidadTotal;
            }
            set
            {
                if (value == _cantidadTotal) return;
                _cantidadTotal = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(DisplayString));
            }
        }

        public int CantidadEnVistaActual
        {
            get
            {
                return _cantidadEnVistaActual;
            }
            set
            {
                if (value == _cantidadEnVistaActual) return;
                _cantidadEnVistaActual = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(DisplayString));
            }
        }

        public string DisplayString
        {
            get { return $"Registros = {CantidadEnVistaActual} de {CantidadTotal},"; }
            set
            {
                throw new InvalidOperationException();
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class SummaryViewModel : INotifyPropertyChanged
    {
        private IEnumerable<TareaViewModel> _ordenes;
        private ICollectionView _view;

        private ICollectionView ItemsSource => _view;

        public SummaryViewModel(ICollectionView view)
        {
            _view = view;
            _ordenes = view.SourceCollection as IEnumerable<TareaViewModel>;
            view.CollectionChanged += (sender, args) => notifyChanges();
        }


        private bool _isVisible = true;

        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                _isVisible = value;
                OnPropertyChanged();
            }
        }

        public string CantidadString
        {
            get { return $"Registros = {CantidadEnVistaActual} de {Cantidad},"; }
            set
            {
                throw new InvalidOperationException();
            }
        }


        public int Cantidad => _ordenes.Count();
        public string PrimeraFechaFin => _ordenes.Select(o => o.Model.FechaCierreTarea.Fecha.Value).Min().ToString("dd/MM/yyyy HH:mm:ss");
        public string UltimaFechaFin => _ordenes.Select(o => o.Model.FechaCierreTarea.Fecha.Value).Max().ToString("dd/MM/yyyy HH:mm:ss");
        public int CantidadEnVistaActual
        {
            get { return ItemsSource.Cast<TareaViewModel>().Count(); }
            set
            {
                throw new InvalidOperationException();
            }
        }

        public void notifyChanges()
        {
            OnPropertyChanged(nameof(CantidadEnVistaActual));
            OnPropertyChanged(nameof(CantidadString));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
