﻿using System.Windows;
using System.Windows.Controls;

namespace SIFO.DesktopUI.TareasCerradas.ColumnHeaderTooltips
{
    /// <summary>
    /// Interaction logic for ColumnHeaderToolTipView.xaml
    /// </summary>
    public partial class ColumnHeaderToolTipView : UserControl
    {
        public static DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof(string), typeof(ColumnHeaderToolTipView));

        public ColumnHeaderToolTipView()
        {
            InitializeComponent();
        }

        public string Title
        {
            get { return (string) GetValue(TitleProperty); }
            set
            {
                SetValue(TitleProperty, value);
            }
        }
    }
}
