using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using SIFO.DesktopUI.Annotations;
using SIFO.DesktopUI.Framework;

namespace SIFO.DesktopUI.TareasCerradas.Indicadores
{
    public class OptionsCollection : INotifyPropertyChanged
    {
        private readonly ObservableCollection<Option> _options = new ObservableCollection<Option>();


        public ObservableCollection<Option> Options => _options;

        public OptionsCollection()
        {
            SelectAllCommand = new RelayCommand(_selectAllExecuted, _selectAllCanExecute);
            UnselectAllCommand = new RelayCommand(_unselectAllExecuted, _unselectAllCanExecute);
            ToogleCommand = new RelayCommand(_toogleExecuted, _toogleCanExecute);
        }

        public IEnumerable<Option> SelectedOptions
        {
            get { return Options.Where(option => option.IsSelected); }
        }

        public void AddOption(Option option)
        {
            _options.Add(option);
        }

        public ICommand SelectAllCommand { get; private set; }
        public void _selectAllExecuted(object o)
        {
            foreach (var option in _options)
            {
                option.IsSelectedProperty.Value = true;
            }
        }
        public bool _selectAllCanExecute(object o)
        {
            return true;
        }

        public ICommand UnselectAllCommand { get; private set; }
        public void _unselectAllExecuted(object o)
        {
            foreach (var option in _options)
            {
                option.IsSelectedProperty.Value = false;
            }
        }
        public bool _unselectAllCanExecute(object o)
        {
            return true;
        }

        private bool _selectAll = true;
        public bool SelectAll
        {
            get
            {
                return _selectAll;
            }
            set
            {
                foreach (var option in _options)
                {
                    option.IsSelectedProperty.Value = value;
                }
                _selectAll = value;
                OnPropertyChanged();
            }
        }

        public ICommand ToogleCommand { get; private set; }

        private void _toogleExecuted(object o)
        {
            SelectedOption.IsSelectedProperty.Value = !SelectedOption.IsSelectedProperty.Value;
        }

        private bool _toogleCanExecute(object o) => SelectedOption != null;

        private Option _option;

        public Option SelectedOption
        {
            get { return _option; }
            set
            {
                _option = value;
                
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}