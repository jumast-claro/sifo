﻿using System.Windows;
using System.Windows.Controls;

namespace SIFO.DesktopUI.TareasCerradas.Indicadores
{
    /// <summary>
    /// Interaction logic for OptionsCollectionView.xaml
    /// </summary>
    public partial class OptionsCollectionView : UserControl
    {

        public static readonly DependencyProperty PropertyNameProperty = DependencyProperty.Register("PropertyName", typeof(string), typeof(OptionsCollectionView));

        public OptionsCollectionView()
        {
            InitializeComponent();
        }

        public string PropertyName
        {
            get { return this.GetValue(PropertyNameProperty) as string; }
            set
            {
                this.SetValue(PropertyNameProperty, value);
            }
        }
    }
}
