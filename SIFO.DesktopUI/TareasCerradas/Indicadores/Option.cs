using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.Properties;

namespace SIFO.DesktopUI.TareasCerradas.Indicadores
{
    public class Option
    {

        public Option(bool isSelected, string description)
        {
            IsSelectedProperty = new ReadWriteProperty<bool>(isSelected);
            DescriptionProperty = new ReadOnlyProperty<string>(description);
        }

        public IBindableProperty<bool> IsSelectedProperty { get; private set; }
        public IBindableProperty<string> DescriptionProperty { get; private set; }
        public IBindableProperty<int> CantidadProperty { get; set; }

        public bool IsSelected => IsSelectedProperty.Value;



    }
}