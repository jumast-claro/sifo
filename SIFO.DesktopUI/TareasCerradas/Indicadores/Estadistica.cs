using System.Collections.Generic;
using LiveCharts;
using SIFO.DesktopUI.Utilities.Properties;
using SIFO.Model.Consultas;

namespace SIFO.DesktopUI.TareasCerradas.Indicadores
{
    public class Estadistica 
    {


        public Estadistica()
        {

        }


        public ReadOnlyProperty<string> Ingresadas { get; set; } 
        public ReadOnlyProperty<string> Exito { get; set; }
        public ReadOnlyProperty<string> Fracaso { get; set; }
        public ReadOnlyProperty<string> OnTime { get; set; }
        public ReadOnlyProperty<string> OnTimeAPosteriori { get; set; }
        public bool IsOnTime { get; set; }
        public bool IsOnTimeAPosteriori { get; set; }

        public bool HasValue => Exito.Value != "0";
    }






}