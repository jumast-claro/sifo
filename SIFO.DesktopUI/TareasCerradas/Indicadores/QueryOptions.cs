using System.Collections.Generic;
using System.Linq;
using SIFO.Model;

namespace SIFO.DesktopUI.TareasCerradas.Indicadores
{
    public class QueryOptions
    {


        public QueryOptions() { }

        public QueryOptions(IEnumerable<ITarea>[] universes, Planta planta)
        {
            Planta = planta;

            foreach (var universe in universes)
            {
                foreach (var zona in universe.Select(o => o.Zona).Distinct())
                {
                    if (!Zona.Contains(zona))
                    {
                        Zona.Add(zona);
                    }

                }

                foreach (var producto in universe.Select(o => o.Producto).Distinct())
                {
                    if (!Productos.Contains(producto))
                    {
                        Productos.Add(producto);
                    }
                    
                }

                foreach (var tipoDeOrden in universe.Select(o => o.TipoDeOrden).Distinct())
                {
                    if (!TipoDeOrden.Contains(tipoDeOrden))
                    {
                        TipoDeOrden.Add(tipoDeOrden);
                    }
                   
                }

                foreach (var tipoDeSolucion in universe.Select(o => o.TipoSolucion).Distinct())
                {
                    if (!TipoDeSolucion.Contains(tipoDeSolucion))
                    {
                        TipoDeSolucion.Add(tipoDeSolucion);
                    }
                   
                }

                foreach (var inbox in universe.Select(o => o.Inbox).Distinct())
                {
                    if (!Inbox.Contains(inbox))
                    {
                        Inbox.Add(inbox);
                    }
                   
                }
                foreach (var regionFinal in universe.Select(o => o.RegionFinal).Distinct())
                {
                    if (!RegionFinal.Contains(regionFinal))
                    {
                        RegionFinal.Add(regionFinal);
                    }
                 
                }
                foreach (var cliente in universe.Select(o => o.Cliente).Distinct())
                {
                    if (!Cliente.Contains(cliente))
                    {
                        Cliente.Add(cliente);
                    }
                  
                }

                foreach (var tipoDeMovimiento in universe.Select(o => o.TipoDeMovimiento).Distinct())
                {
                    if (!TipoDeMovimiento.Contains(tipoDeMovimiento))
                    {
                        TipoDeMovimiento.Add(tipoDeMovimiento);
                    }

                }

                foreach (var tecnologia in universe.Select(o => o.Tecnologia).Distinct())
                {
                    if (!Tecnologia.Contains(tecnologia))
                    {
                        Tecnologia.Add(tecnologia);
                    }

                }

                foreach (var tipoDeFO in universe.Select(o => o.TipoDeFibraOptica).Distinct())
                {
                    if (!TipoDeFibraOptica.Contains(tipoDeFO))
                    {
                        TipoDeFibraOptica.Add(tipoDeFO);
                    }

                }
            }
        }


        public QueryOptions(IEnumerable<ITarea> universe, Planta planta)
        {
            Planta = planta;

            //foreach (var zona in universe.Select(o => o.Zona).Distinct())
            //{
            //    Zona.Add(zona);

            //}

            //foreach (var producto in universe.Select(o => o.Producto).Distinct())
            //{
            //    Productos.Add(producto);
            //}

            //foreach (var tipoDeOrden in universe.Select(o => o.TipoDeOrden).Distinct())
            //{
            //    TipoDeOrden.Add(tipoDeOrden);
            //}

            //foreach (var tipoDeSolucion in universe.Select(o => o.TipoSolucion).Distinct())
            //{
            //    TipoDeSolucion.Add(tipoDeSolucion);
            //}

            //foreach (var inbox in universe.Select(o => o.Inbox).Distinct())
            //{
            //    Inbox.Add(inbox);
            //}
            //foreach (var regionFinal in universe.Select(o => o.RegionFinal).Distinct())
            //{
            //    RegionFinal.Add(regionFinal);
            //}
            //foreach (var cliente in universe.Select(o => o.Cliente).Distinct())
            //{
            //    Cliente.Add(cliente);
            //}

            foreach (var zona in universe.Select(o => o.Zona).Distinct())
            {
                if (!Zona.Contains(zona))
                {
                    Zona.Add(zona);
                }

            }

            foreach (var producto in universe.Select(o => o.Producto).Distinct())
            {
                if (!Productos.Contains(producto))
                {
                    Productos.Add(producto);
                }

            }

            foreach (var tipoDeOrden in universe.Select(o => o.TipoDeOrden).Distinct())
            {
                if (!TipoDeOrden.Contains(tipoDeOrden))
                {
                    TipoDeOrden.Add(tipoDeOrden);
                }

            }

            foreach (var tipoDeSolucion in universe.Select(o => o.TipoSolucion).Distinct())
            {
                if (!TipoDeSolucion.Contains(tipoDeSolucion))
                {
                    TipoDeSolucion.Add(tipoDeSolucion);
                }

            }

            foreach (var inbox in universe.Select(o => o.Inbox).Distinct())
            {
                if (!Inbox.Contains(inbox))
                {
                    Inbox.Add(inbox);
                }

            }
            foreach (var regionFinal in universe.Select(o => o.RegionFinal).Distinct())
            {
                if (!RegionFinal.Contains(regionFinal))
                {
                    RegionFinal.Add(regionFinal);
                }

            }
            foreach (var cliente in universe.Select(o => o.Cliente).Distinct())
            {
                if (!Cliente.Contains(cliente))
                {
                    Cliente.Add(cliente);
                }

            }

            foreach (var tipoDeMovimiento in universe.Select(o => o.TipoDeMovimiento).Distinct())
            {
                if (!TipoDeMovimiento.Contains(tipoDeMovimiento))
                {
                    TipoDeMovimiento.Add(tipoDeMovimiento);
                }

            }

            foreach (var tecnologia in universe.Select(o => o.Tecnologia).Distinct())
            {
                if (!Tecnologia.Contains(tecnologia))
                {
                    Tecnologia.Add(tecnologia);
                }

            }

            foreach (var tipoDeFO in universe.Select(o => o.TipoDeFibraOptica).Distinct())
            {
                if (!TipoDeFibraOptica.Contains(tipoDeFO))
                {
                    TipoDeFibraOptica.Add(tipoDeFO);
                }

            }
        }

        public Planta Planta { get; set; }
        public bool IncluirProyectosEspeciales { get; set; } = false;

        public List<Zona> Zona { get; private set; } = new List<Zona>();
        public List<string> Productos { get; private set; } = new List<string>();
        public List<string> TipoDeOrden { get; private set; } = new List<string>();
        public List<string> TipoDeSolucion { get; set; } = new List<string>();
        public List<string> Inbox { get; set; } = new List<string>();
        public List<string> RegionFinal { get; set; } = new List<string>();
        public List<string> Cliente { get; set; } = new List<string>();

        public List<string> TipoDeMovimiento { get; set; }  = new List<string>();
        public List<string> TipoDeFibraOptica { get; set; } = new List<string>();
        public List<string> Tecnologia { get; set; } = new List<string>();
        public int Percentil { get; set; } = 80;
    }
}