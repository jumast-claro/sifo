using System.Linq;
using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.Properties;
using SIFO.Model;

namespace SIFO.DesktopUI.TareasCerradas.Indicadores
{
    public class QueryOptionsViewModel
    {

        public QueryOptionsViewModel(QueryOptions queryOptions)
        {
            PlantaProperty = new ReadOnlyProperty<Planta>(queryOptions.Planta);
            IncluirProyectosEspecialesProperty = new ReadWriteProperty<bool>(queryOptions.IncluirProyectosEspeciales);
            PercentilProperty = new ReadWriteProperty<int>(queryOptions.Percentil);
            var productos = queryOptions.Productos;


            foreach (var zona in queryOptions.Zona.OrderBy(s => s))
            {
                Zona.AddOption(new Option(true, zona.ToString()));
            }

            foreach (var producto in productos.OrderBy(s => s))
            {
                Productos.AddOption(new Option(true, producto));
            }

            foreach (var tipoOrden in queryOptions.TipoDeOrden.OrderBy(s => s))
            {
                TipoDeOrden.AddOption(new Option(true, tipoOrden));
            }

            foreach (var tipoSolucion in queryOptions.TipoDeSolucion.OrderBy(s => s))
            {
                TipoDeSolucion.AddOption(new Option(true, tipoSolucion));
            }

            foreach (var inbox in queryOptions.Inbox.OrderBy(s => s))
            {
                Inbox.AddOption(new Option(true, inbox));
            }

            foreach (var regionFinal in queryOptions.RegionFinal.OrderBy(s => s))
            {
                RegionFinal.AddOption(new Option(true, regionFinal));
            }
            foreach (var cliente in queryOptions.Cliente.OrderBy(s => s))
            {
                Cliente.AddOption(new Option(true, cliente));
            }


            foreach (var tipoMov in queryOptions.TipoDeMovimiento.OrderBy(s => s))
            {
                TipoDeMovimiento.AddOption(new Option(true, tipoMov));
            }
            foreach (var tecnologia in queryOptions.Tecnologia.OrderBy(s => s))
            {
                Tecnologia.AddOption(new Option(true, tecnologia));
            }
            foreach (var tipoDeFO in queryOptions.TipoDeFibraOptica.OrderBy(s => s))
            {
                TipoDeFibraOptica.AddOption(new Option(true, tipoDeFO));
            }

        }

        public IBindableProperty<Planta> PlantaProperty { get; private set; }
        public IBindableProperty<bool> IncluirProyectosEspecialesProperty { get; private set; }


        public OptionsCollection Zona { get; private set; } = new OptionsCollection();
        public OptionsCollection Productos { get; private set; } = new OptionsCollection();
        public OptionsCollection TipoDeOrden { get; private set; } = new OptionsCollection();
        public OptionsCollection TipoDeSolucion { get; private set; } = new OptionsCollection();
        public OptionsCollection Inbox { get; private set; } = new OptionsCollection();
        public OptionsCollection RegionFinal { get; private set; } = new OptionsCollection();
        public OptionsCollection Cliente { get; private set; } = new OptionsCollection();


        public OptionsCollection TipoDeMovimiento { get; private set; } = new OptionsCollection();
        public OptionsCollection Tecnologia { get; private set; } = new OptionsCollection();
        public OptionsCollection TipoDeFibraOptica { get; private set; } = new OptionsCollection();

        public IBindableProperty<int> PercentilProperty { get; private set; }
    }
}