﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SIFO.DesktopUI.TareasCerradas.Indicadores.PlantaExterna
{
    /// <summary>
    /// Interaction logic for PlantaExternaTableView.xaml
    /// </summary>
    public partial class PlantaExternaTableView : UserControl
    {
        public static DependencyProperty RegionProperty = DependencyProperty.Register("Region", typeof(string), typeof(PlantaExternaTableView));
        public static DependencyProperty HeaderBackgroundProperty = DependencyProperty.Register("HeaderBackground", typeof(Brush), typeof(PlantaExternaTableView));

        public PlantaExternaTableView()
        {
            InitializeComponent();
        }

        public string Region
        {
            get { return (string)GetValue(RegionProperty); }
            set
            {
                SetValue(RegionProperty, value);
            }
        }

        public Brush HeaderBackground
        {
            get { return (Brush)GetValue(RegionProperty); }
            set
            {
                SetValue(HeaderBackgroundProperty, value);
            }
        }
    }
}
