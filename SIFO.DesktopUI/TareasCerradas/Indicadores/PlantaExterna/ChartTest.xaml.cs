﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveCharts;

namespace SIFO.DesktopUI.TareasCerradas.Indicadores.PlantaExterna
{
    /// <summary>
    /// Interaction logic for ChartTest.xaml
    /// </summary>
    public partial class ChartTest : UserControl
    {
        public ChartTest()
        {
            InitializeComponent();
            Series = new SeriesCollection();

            //create some series
            var charlesSeries = new PieSeries
            {
                Title = "Charles",
                Values = new ChartValues<double> {10}
            };
            var jamesSeries = new PieSeries
            {
                Title = "James",
                Values = new ChartValues<double> {5}
            };
            var mariaSeries = new PieSeries
            {
                Title = "James",
                Values = new ChartValues<double> {7}
            };

            //add series to SeriesCollection
            Series.Add(charlesSeries);
            Series.Add(jamesSeries);
            Series.Add(mariaSeries);

            //that's it, LiveCharts is ready and listening for your data changes.
            DataContext = this;
        }

        public SeriesCollection Series { get; set; }
    }
}
