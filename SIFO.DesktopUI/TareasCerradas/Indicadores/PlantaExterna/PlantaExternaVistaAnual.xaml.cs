﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SIFO.DesktopUI.TareasCerradas.Indicadores.PlantaExterna
{
    /// <summary>
    /// Interaction logic for PlantaExternaVistaAnual.xaml
    /// </summary>
    public partial class PlantaExternaVistaAnual : UserControl
    {
        public static DependencyProperty RegionProperty = DependencyProperty.Register("Region", typeof(string), typeof(PlantaExternaTableView));
        public static DependencyProperty HeaderBackgroundProperty = DependencyProperty.Register("HeaderBackground", typeof(Brush), typeof(PlantaExternaTableView));

        public PlantaExternaVistaAnual()
        {
            InitializeComponent();
        }

        public string Region
        {
            get { return (string)GetValue(RegionProperty); }
            set
            {
                SetValue(RegionProperty, value);
            }
        }

        public Brush HeaderBackground
        {
            get { return (Brush)GetValue(RegionProperty); }
            set
            {
                SetValue(HeaderBackgroundProperty, value);
            }
        }
    }
}
