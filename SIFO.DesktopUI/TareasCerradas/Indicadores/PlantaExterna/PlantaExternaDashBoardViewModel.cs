﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using SIFO.Model;
using SIFO.Model.Consultas;
using SIFO.Services;

namespace SIFO.DesktopUI.TareasCerradas.Indicadores.PlantaExterna
{
    public class UniversoDeTareas<T> : IEnumerable<T> where T : ITarea
    {
        private readonly IEnumerable<T> _tareasCerradas;

        public UniversoDeTareas(IEnumerable<T> tareasCerradas)
        {
            _tareasCerradas = tareasCerradas;
        }

        public IEnumerable<T> Where(QueryOptions queryOptions)
        {
            //var resultado = _tareasCerradas.Where(tareaCerrada => tareaCerrada.Planta == queryOptions.Planta);
            var resultado = _tareasCerradas;

            var c = resultado.Count();
            if (queryOptions.IncluirProyectosEspeciales == false)
            {
                resultado = resultado.Where(o => !o.EsProyectoEspecial);
                c = resultado.Count();
            }

            resultado = resultado.Where(o => queryOptions.Zona.Contains(o.Zona));
            resultado = resultado.Where(o => queryOptions.Productos.Contains(o.Producto));
            resultado = resultado.Where(o => queryOptions.TipoDeOrden.Contains(o.TipoDeOrden));
            resultado = resultado.Where(o => queryOptions.TipoDeSolucion.Contains(o.TipoSolucion));
            resultado = resultado.Where(o => queryOptions.Inbox.Contains(o.Inbox));
            resultado = resultado.Where(o => queryOptions.RegionFinal.Contains(o.RegionFinal));
            resultado = resultado.Where(o => queryOptions.Cliente.Contains(o.Cliente));
            resultado = resultado.Where(o => queryOptions.TipoDeMovimiento.Contains(o.TipoDeMovimiento));
            resultado = resultado.Where(o => queryOptions.TipoDeFibraOptica.Contains(o.TipoDeFibraOptica));
            resultado = resultado.Where(o => queryOptions.Tecnologia.Contains(o.Tecnologia));
            return resultado;
        }


        public IEnumerator<T> GetEnumerator()
        {
            return _tareasCerradas.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

  
}
