﻿using System.Windows.Controls;

namespace SIFO.DesktopUI.TareasCerradas.Indicadores.PlantaInterna
{
    /// <summary>
    /// Interaction logic for PlantaInternaRecalcularButtonView.xaml
    /// </summary>
    public partial class PlantaInternaRecalcularButtonView : UserControl
    {
        public PlantaInternaRecalcularButtonView()
        {
            InitializeComponent();
        }
    }
}
