﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SIFO.DesktopUI.TareasCerradas.Indicadores.PlantaInterna
{
    /// <summary>
    /// Interaction logic for PlantaInternaTableView.xaml
    /// </summary>
    public partial class PlantaInternaTableView : UserControl
    {
        public static DependencyProperty RegionProperty = DependencyProperty.Register("Region", typeof(string), typeof(PlantaInternaTableView));
        public static DependencyProperty HeaderBackgroundProperty = DependencyProperty.Register("HeaderBackground", typeof(Brush), typeof(PlantaInternaTableView));

        public PlantaInternaTableView()
        {
            InitializeComponent();
        }

        public string Region
        {
            get { return (string)GetValue(RegionProperty); }
            set
            {
                SetValue(RegionProperty, value);
            }
        }

        public Brush HeaderBackground
        {
            get { return (Brush)GetValue(RegionProperty); }
            set
            {
                SetValue(HeaderBackgroundProperty, value);
            }
        }

    }
}
