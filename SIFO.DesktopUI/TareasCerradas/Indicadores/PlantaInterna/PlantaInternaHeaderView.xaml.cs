﻿using System.Windows.Controls;

namespace SIFO.DesktopUI.TareasCerradas.Indicadores.PlantaInterna
{
    /// <summary>
    /// Interaction logic for PlantaInternaHeaderView.xaml
    /// </summary>
    public partial class PlantaInternaHeaderView : UserControl
    {
        public PlantaInternaHeaderView()
        {
            InitializeComponent();
        }
    }
}
