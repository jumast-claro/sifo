﻿using System.Windows.Controls;

namespace SIFO.DesktopUI.TareasCerradas.Indicadores.PlantaInterna
{
    /// <summary>
    /// Interaction logic for IndicadoresPlantaInternaView.xaml
    /// </summary>
    public partial class IndicadoresPlantaInternaView : UserControl
    {
        public IndicadoresPlantaInternaView()
        {
            InitializeComponent();
        }
    }
}
