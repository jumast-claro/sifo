﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NUnit.Framework;
using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.Properties;
using SIFO.DesktopUI.Utilities.Reflection;
using SIFO.Model;

namespace SIFO.DesktopUI.TareasCerradas
{
    public class TareaViewModel
    {
        //---------------------------------------------------------------------
        // Campos
        //---------------------------------------------------------------------
        private readonly ITarea _model;

        //---------------------------------------------------------------------
        // Constructores
        //---------------------------------------------------------------------
        public TareaViewModel(ITarea model, bool ingresada, bool cerrada, bool pendiente)
        {
            _model = model;
            
            _inicializarPropiedades(ingresada, cerrada, pendiente);
            initPropertyValues();
        }
        private void _inicializarPropiedades(bool ingresada, bool cerrada, bool pendiente)
        {


            EstaIngresadaProperty = new ReadOnlyProperty<bool>(ingresada);
            EstaCerradaProperty = new ReadOnlyProperty<bool>(cerrada);
            EstaPendienteProperty = new ReadOnlyProperty<bool>(pendiente);

            TipoDeMovimientoProperty = new ReadOnlyProperty<string>(_model.TipoDeMovimiento);
            TecnologiaBisProperty = new ReadOnlyProperty<string>(_model.Tecnologia);
            TipoDeFibraOpticaProperty = new ReadOnlyProperty<string>(_model.TipoDeFibraOptica);

            PlantaProperty = new WrappingViewModelProperty<string>(() => _model.Planta == Planta.PlantaExterna ? "PE" : (_model.Planta == Planta.PlantaInterna ? "PI" : ""));
            RegionFinalProperty = new WrappingViewModelProperty<string>(() => _model.RegionFinal);
            ZonaProperty = new WrappingViewModelProperty<string>(() => _model.Zona.ToString());
            InboxProperty = new ReadOnlyProperty<string>(_model.Inbox);
            InboxFinalProperty = new ReadOnlyProperty<string>(_model.InboxFinal);
            TecnologiaProperty = new ReadOnlyProperty<string>(_model.TecnologiaBis == TecnologiaEnum.FibraOptica ? "FO" : _model.TecnologiaBis == TecnologiaEnum.Inalambrico ? "Inal." : _model.TecnologiaBis == TecnologiaEnum.NoAplica ? "N/A" : "?");
            ProductoProperty = new WrappingViewModelProperty<string>(() => _model.Producto);
            TipoDeOrdenProperty = new ReadOnlyProperty<string>(_model.TipoDeOrden);

            TipoDeSolucionProperty = new ReadOnlyProperty<string>(_model.TipoSolucion, _model.TipoSolucion == String.Empty ? "?" :  _model.CodigoTipificacion);
            FechaInicioOrdenProperty = new WrappingViewModelProperty<string>(() => _model.FechaInicioOrden.HasValue ? _model.FechaInicioOrden.Value.ToString("dd/MM/yyyy") : "?");
            FechaCompromisoOrdenProperty = new ReadOnlyProperty<DateTime?>(_model.FechaCompromisoOrden, _model.FechaCompromisoOrden.HasValue ? _model.FechaCompromisoOrden.Value.ToString("dd/MM/yyyy") : "");
            FechaRenegociadaProperty = new WrappingViewModelProperty<string>(() => _model.FechaRenegociada.HasValue ? _model.FechaRenegociada.Value.ToString("dd/MM/yyyy") : "");
            FechaInstalacionProperty = new WrappingViewModelProperty<string>(() => _model.FechaDeInstalacion.HasValue ? _model.FechaDeInstalacion.Value.ToString("dd/MM/yyy") : "");


            TiempoMaximoDePermanenciaEnTodasLasBandejasProperty = new WrappingViewModelProperty<int>(() => _model.DiasDeInstalacion ?? 0, (value) => value == 0 ? "?" : value.ToString());
            TiempoMaximoDePermanenciaEnOtrasBandejasProperty = new ReadOnlyProperty<int>(_model.TiempoMaximoPermanenciaOrdenEnOtrasBandejas);

            FechaInicioProperty = new ReadOnlyProperty<DateTime>(_model.FechaInicioTarea, _model.FechaInicioTarea.ToString("dd/MM/yyyy"));
            FechaFinProperty = new ReadOnlyProperty<DateTime>(_model.FechaCierreTarea.Fecha.Value, _model.FechaCierreTarea.IsNull ? "" : _model.FechaCierreTarea.Fecha.Value.ToString(CultureInfo.InvariantCulture));
            TiempoEnBandejaProperty = new ReadOnlyProperty<double>(Math.Round(_model.TiempoEfectivoPermanenciaTareaEnEstaBandeja, 2, MidpointRounding.AwayFromZero), _model.TiempoEfectivoPermanenciaTareaEnEstaBandeja.ToString("F2") + " días");
            EstadoProperty = new ReadOnlyProperty<string>(_model.Estado.ToString());
            TomadoPorProperty = new ReadOnlyProperty<string>(_model.TomadoPor);
            ClienteProperty = new ReadOnlyProperty<string>(_model.Cliente);
            OrdenProperty = new ReadOnlyProperty<long>(_model.Orden);
            NumeroEnlaceProperty = new WrappingViewModelProperty<long>(() => _model.Enlace);
            TareaIdProperty = new ReadOnlyProperty<string>(_model.TareaId);
            IdentificadorProperty = new WrappingViewModelProperty<string>(() => _model.Identificador);

            EsProyectoEspecialProperty = new WrappingViewModelProperty<bool>(() => _model.EsProyectoEspecial, (value) => value ? "Sí" : "No");

            MesDeFinProperty = new ReadOnlyProperty<int>(_model.FechaCierreTarea.Mes);
            IngresoOffTimeProperty = new ReadOnlyProperty<bool>(_model.IngresaOffTime, _model.IngresaOffTime ? "Sí" : "No");
            ZonaSinInfo = new WrappingViewModelProperty<bool>(() => _model.Zona == Zona.SinInfo);
            SinEnlace = new WrappingViewModelProperty<bool>(() => _model.Enlace == 0);


            MaximaFechaDeCierreProperty = new ReadOnlyProperty<DateTime?>(_model.FechaCompromisoTarea, _model.FechaCompromisoTarea.HasValue ? DateTimeService.ToStringRepresentation(_model.FechaCompromisoTarea.Value, "dd/MM/yyyy") : "");

            
            EsAltaPuraProperty = new WrappingViewModelProperty<bool>(() => _model.EsAltaPura, (value) => value ? "Sí" : "No");

            TiempoInterrumpidaEnDiasProperty = new ReadWriteProperty<double>(-1);

            DifIniciosProperty = new ReadOnlyProperty<bool>(!areEqual(_model.FechaInicioOrden, _model.FechaInicioTarea));

        }

        private bool areEqual(DateTime? inicioOrden, DateTime inicioTarea)
        {
            if (!inicioOrden.HasValue)
            {
                return true;
            }

            var anio = inicioOrden.Value.Year == inicioTarea.Year;
            var mes = inicioOrden.Value.Month == inicioTarea.Month;
            var dia = inicioOrden.Value.Day == inicioTarea.Day;
            return anio && mes && dia;
        }

        //---------------------------------------------------------------------
        // Propiedades
        //---------------------------------------------------------------------
        public ITarea Model => _model;

       





        public void RaisePropertyChangeForAllProperties()
        {
            FechaRenegociadaProperty.RaisePropertyChanged();
            FechaInstalacionProperty.RaisePropertyChanged();
            EsProyectoEspecialProperty.RaisePropertyChanged();
            PlantaProperty.RaisePropertyChanged();
            RegionFinalProperty.RaisePropertyChanged();
            ZonaProperty.RaisePropertyChanged();
            InboxProperty.RaisePropertyChanged();
            ProductoProperty.RaisePropertyChanged();
            TipoDeOrdenProperty.RaisePropertyChanged();
            TipoDeSolucionProperty.RaisePropertyChanged();
            FechaInicioOrdenProperty.RaisePropertyChanged();
            FechaCompromisoOrdenProperty.RaisePropertyChanged();
            TiempoMaximoDePermanenciaEnTodasLasBandejasProperty.RaisePropertyChanged();
            TiempoMaximoDePermanenciaEnOtrasBandejasProperty.RaisePropertyChanged();
            FechaInicioProperty.RaisePropertyChanged();
            FechaFinProperty.RaisePropertyChanged();
            TiempoEnBandejaProperty.RaisePropertyChanged();
            EstadoProperty.RaisePropertyChanged();
            TomadoPorProperty.RaisePropertyChanged();
            ClienteProperty.RaisePropertyChanged();
            OrdenProperty.RaisePropertyChanged();
            NumeroEnlaceProperty.RaisePropertyChanged();
            TareaIdProperty.RaisePropertyChanged();
            IdentificadorProperty.RaisePropertyChanged();
            IngresoOffTimeProperty.RaisePropertyChanged();
            ZonaSinInfo.RaisePropertyChanged();
            SinEnlace.RaisePropertyChanged();
            EsAltaPuraProperty.RaisePropertyChanged();
        }

        private void addProperty(string name, IBindableProperty property)
        {
            _propertyValues.Add(name, property);
        }

        private void initPropertyValues()
        {
            addProperty(nameof(TareaIdProperty), TareaIdProperty);
            addProperty(nameof(OrdenProperty), OrdenProperty);
            addProperty(nameof(NumeroEnlaceProperty), NumeroEnlaceProperty);
            addProperty(nameof(MesDeFinProperty),MesDeFinProperty);
            addProperty(nameof(TomadoPorProperty), TomadoPorProperty);
            addProperty(nameof(PlantaProperty),PlantaProperty);
            addProperty(nameof(ZonaProperty), ZonaProperty);
            addProperty(nameof(RegionFinalProperty), RegionFinalProperty);
            addProperty(nameof(InboxProperty), InboxProperty);
            addProperty(nameof(InboxFinalProperty), InboxFinalProperty);
        }


        [Filter("Tipo de Mox.")] public IBindableProperty<string> TipoDeMovimientoProperty { get; set; }
        [Filter("Tecnología")] public IBindableProperty<string> TecnologiaBisProperty { get; set; }
        [Filter("Tipo de FO")] public IBindableProperty<string> TipoDeFibraOpticaProperty { get; set; }


        [Filter("Ingresada?")] public IBindableProperty<bool> EstaIngresadaProperty { get; set; }
        [Filter("Cerrada?")] public IBindableProperty<bool> EstaCerradaProperty { get; set; }
        [Filter("Pendiente?")] public IBindableProperty<bool> EstaPendienteProperty { get; set; }

        [Filter("Tarea ID")]public IBindableProperty<string> TareaIdProperty { get; private set; }
        [Filter("Orden")] public IBindableProperty<long> OrdenProperty { get; private set; }
        [Filter("Enlace")] public IBindableProperty<long> NumeroEnlaceProperty { get; private set; }
        [Filter("Mes de fin")] public IBindableProperty<int> MesDeFinProperty { get; set; }
        [Filter("Tomado por")] public IBindableProperty<string> TomadoPorProperty { get; set; }

        [Filter("Planta")] public IBindableProperty<string> PlantaProperty { get; private set; }
        [Filter("Zona")] public IBindableProperty<string> ZonaProperty { get; private set; }
        [Filter("Region final")] public IBindableProperty<string> RegionFinalProperty { get; private set; }
        [Filter("Inbox")] public IBindableProperty<string> InboxProperty { get; private set; }
        [Filter("Inbox final")] public IBindableProperty<string> InboxFinalProperty { get; set; }
        [Filter("Tecnología")] public IBindableProperty<string> TecnologiaProperty { get; set; }
        [Filter("Estado")] public IBindableProperty<string> EstadoProperty { get; private set; }
        [Filter("Tipo de orden")] public IBindableProperty<string> TipoDeOrdenProperty { get; set; }
        [Filter("Es alta pura?")] public IBindableProperty<bool> EsAltaPuraProperty { get; set; }
        [Filter("Es proyecto especial?")] public IBindableProperty<bool> EsProyectoEspecialProperty { get; private set; }
        [Filter("Fecha inicio orden")] public IBindableProperty<string> FechaInicioOrdenProperty { get; private set; }
        [Filter("Fecha compromiso orden")] public IBindableProperty<DateTime?> FechaCompromisoOrdenProperty { get; set; } 
        [Filter("Fecha renegociada")] public IBindableProperty<string> FechaRenegociadaProperty { get; set; }
        [Filter("Fecha instalación")] public IBindableProperty<string> FechaInstalacionProperty { get; set; }
        [Filter("Tipo de solución")] public IBindableProperty<string> TipoDeSolucionProperty { get; set; } 
        [Filter("Días de instalación")] public IBindableProperty<int> TiempoMaximoDePermanenciaEnTodasLasBandejasProperty { get; set; }
        [Filter("Fecha inicio tarea")] public IBindableProperty<DateTime> FechaInicioProperty { get; private set; }
        [Filter("Fecha cierre tarea")] public IBindableProperty<DateTime> FechaFinProperty { get; private set; }
        [Filter("Tiempo en bandeja")] public IBindableProperty<double> TiempoEnBandejaProperty { get; private set; }
        [Filter("Cliente")] public IBindableProperty<string> ClienteProperty { get; private set; }
        [Filter("Identificador")] public IBindableProperty<string> IdentificadorProperty { get; private set; }
        [Filter("Producto")] public IBindableProperty<string> ProductoProperty { get; set; }
        [Filter("Tiempo máximo de permanencia en otras bandejas")]
        public IBindableProperty<int> TiempoMaximoDePermanenciaEnOtrasBandejasProperty { get; set; }
        [Filter("Ingreso off time?")] public IBindableProperty<bool> IngresoOffTimeProperty { get; set; } 
        [Filter("Zona sin info?")] public IBindableProperty<bool> ZonaSinInfo { get; set; } 
        [Filter("Sin enlace?")] public IBindableProperty<bool> SinEnlace { get; set; } 


        [Filter("Máxima fecha de cierre")]
       public IBindableProperty<DateTime?> MaximaFechaDeCierreProperty { get; set; }
        [Filter("Interrumpida")] public IBindableProperty<double> TiempoInterrumpidaEnDiasProperty { get; set; }

        [Filter("*** Dif. Inicios ***")]
        public IBindableProperty<bool> DifIniciosProperty { get; set; }



        private readonly Dictionary<string, IBindableProperty> _propertyValues = new Dictionary<string, IBindableProperty>();
        public object this[string propertyName]
        {
            get { return _propertyValues[propertyName].Value; }
        }

        public bool HasProperty(string propertyName)
        {
            return _propertyValues.ContainsKey(propertyName);
        }
    }



}

