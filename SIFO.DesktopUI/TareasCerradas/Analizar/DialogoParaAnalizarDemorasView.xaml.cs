﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SIFO.DesktopUI.TareasCerradas.Analizar
{
    /// <summary>
    /// Interaction logic for DialogoParaAnalizarDemorasView.xaml
    /// </summary>
    public partial class DialogoParaAnalizarDemorasView : UserControl
    {

        public DialogoParaAnalizarDemorasView()
        {
            InitializeComponent();
        }


        private void AcceptButton_OnClick(object sender, RoutedEventArgs e)
        {
            //Window.GetWindow(this).Close();
        }

        private void CancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }
    }
}
