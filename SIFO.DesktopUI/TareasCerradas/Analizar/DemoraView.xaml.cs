﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SIFO.DesktopUI.TareasCerradas.Analizar
{
    /// <summary>
    /// Interaction logic for DemoraView.xaml
    /// </summary>
    public partial class DemoraView : UserControl
    {
        public static DependencyProperty DisplayNameProperty = DependencyProperty.Register("DisplayName", typeof(string), typeof(DemoraView));
        public static DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof (int), typeof (DemoraView));


        public DemoraView()
        {
            InitializeComponent();
        }

        public string DisplayName
        {
            get { return (string)GetValue(DisplayNameProperty); }
            set
            {
                SetValue(DisplayNameProperty, value);
            }
        }

        public int Value
        {
            get { return (int)GetValue(ValueProperty); }
            set
            {
                SetValue(ValueProperty, value);
            }
        }


    }
}
