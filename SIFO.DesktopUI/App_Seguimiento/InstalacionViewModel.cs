﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumast.Claro.ObraCivilTendido.Model.Abstract;
using SIFO.DesktopUI.Utilities;

namespace SIFO.DesktopUI.App_Seguimiento
{
    public class InstalacionViewModel
    {
        public InstalacionViewModel(IInstalacionFO instacion)
        {
            ClienteProperty = new ReadWriteProperty<string>(instacion.Cliente);
            DireccionProperty = new ReadWriteProperty<string>(instacion.Direccion);
            PartidoProperty = new ReadWriteProperty<string>(instacion.Partido);
        }

        public IBindableProperty<string> ClienteProperty { get; private set; } 
        public IBindableProperty<string> DireccionProperty { get; private set; } 
        public IBindableProperty<string> PartidoProperty { get; private set; } 
    }
}
