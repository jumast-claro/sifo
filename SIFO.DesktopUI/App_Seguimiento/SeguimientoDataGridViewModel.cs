﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Jumast.Claro.ObraCivilTendido.DataAcces.Abstract;

namespace SIFO.DesktopUI.App_Seguimiento
{
    public class SeguimientoDataGridViewModel : INotifyPropertyChanged
    {
        private readonly IInstalacionesFOExcelRepository _excelRepository;
       

        public SeguimientoDataGridViewModel(IInstalacionesFOExcelRepository excelRepository)
        {
            _excelRepository = excelRepository;

            _excelRepository.Actualizar();
            var instalaciones = _excelRepository.ObtenerTodasLasInstalaciones();
            foreach (var instalacion in instalaciones)
            {
                var instalacionViewModel = new InstalacionViewModel(instalacion);
                _viewModels.Add(instalacionViewModel);
            }
        }

        private ObservableCollection<InstalacionViewModel> _viewModels = new ObservableCollection<InstalacionViewModel>();

        public ObservableCollection<InstalacionViewModel> Instalaciones
        {
            get { return _viewModels; }
            set
            {   if(value == _viewModels) return;
                _viewModels = value;
                OnPropertyChanged();
            }
        }

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
