﻿using Ninject.Modules;
using SIFO.DataAcces.TareasCerradas;
using SIFO.DataAcces.TareasCerradas.Repositories;
using SIFO.DesktopUI.Framework;
using SIFO.Model;
using SIFO.Services;
using System.Configuration;
using System.IO;
using Jumast.Claro.ObraCivilTendido.DataAcces.Abstract;
using Jumast.Claro.ObraCivilTendido.DataAcces.Concrete;
using SIFO.DesktopUI.App_Vantive;
using SIFO.DesktopUI.App_Vantive.SyncfusionDataGrid;
using SIFO.DesktopUI.TareasIngresadas;
using SIFO.Services.DataService;
using Vantive.DataAcces;
using Vantive.DataAcces.Abstractions;
using Vantive.DataAcces.Implementations;
using Vantive.DataService.Abstractions;
using Vantive.DataService.Implementations;

namespace SIFO.DesktopUI.DependencyInjection.Ninject
{
    public class VantiveNinjectModule : NinjectModule
    {

        public override void Load()
        {

            Bind<IInstalacionesFOExcelRepository>().ToConstant(new SeguimientoExcelRepository(@"C:\Users\Jumast\Desktop\Seguimiento Instalaciones FO - JUM.xlsx"));

            Bind<ITareasVantiveModelRepository>().To<TareasVantiveModelRepository>();

            Bind<IVantiveDataRepository>().ToConstant(new VantiveDataRepository(Path.Combine(ConfigurationManager.AppSettings.Get("RUTA"), "produ_Obra Civil Tend AMBA"), "\t", false));
            //Bind<IVantiveDataRepository>().ToConstant(new VantiveDataRepository(@"S:\Implantacion\Servicios Fijos\Servicios Fijos\Reportes\SIFO\data\produ_Obra Civil Tend AMBA", "\t", false));
            Bind<IVantiveDataMapper>().To<VantiveDataMapper>();

            Bind<IExportToExcelCommand>().To<ExportToExcelCommand>();
            Bind<IShowColumnChooserCommand>().To<ChooseColumnsCommand>();
        }
    }

    public class TareasCerradasDataModule : NinjectModule
    {

        public override void Load()
        {
            var path = ConfigurationManager.AppSettings.Get("RUTA");

            var tareasCerradasCsvFilePath = Path.Combine(path, ConfigurationManager.AppSettings.Get("TareasCerradasCsvFilePath"));
            var tareasCerradasCsvSeparator = ConfigurationManager.AppSettings.Get("TareasCerradasCsvSeparator");
            bool tareasCerradasCsvHasFieldsInclosedInQuotes = bool.Parse(ConfigurationManager.AppSettings.Get("TareasCerradasCsvHasFieldsEnclosedInQuotes"));
            Bind<IBusinessObjectDataRepository>().
                ToConstant(new BusinessObjectDataRepository(tareasCerradasCsvFilePath, tareasCerradasCsvSeparator, tareasCerradasCsvHasFieldsInclosedInQuotes))
                .WhenInjectedExactlyInto<TareasCerradasModelRepository>();

            var tareasIngresadasCsvFilePath = Path.Combine(path, ConfigurationManager.AppSettings.Get("TareasIngresadasCsvFilePath"));
            var tareasIngresadasCsvSeparator = ConfigurationManager.AppSettings.Get("TareasIngresadasCsvSeparator");
            bool tareasIngresadasCsvHasFieldsInclosedInQuotes = bool.Parse(ConfigurationManager.AppSettings.Get("TareasIngresadasCsvHasFieldsEnclosedInQuotes"));
            Bind<IBusinessObjectDataRepository>().
                ToConstant(new BusinessObjectDataRepository(tareasIngresadasCsvFilePath, tareasIngresadasCsvSeparator, tareasIngresadasCsvHasFieldsInclosedInQuotes))
                .WhenInjectedExactlyInto<TareasIngresadasModelRepository>();

            var tareasPendientesCsvFilePath = Path.Combine(path,ConfigurationManager.AppSettings.Get("TareasPendientesCsvFilePath"));
            var tareasPendientesCsvSeparator = ConfigurationManager.AppSettings.Get("TareasPendientesCsvSeparator");
            bool tareasPendientesCsvHasFieldsInclosedInQuotes = bool.Parse(ConfigurationManager.AppSettings.Get("TareasPendientesCsvHasFieldsEnclosedInQuotes"));
            Bind<IBusinessObjectDataRepository>().
                ToConstant(new BusinessObjectDataRepository(tareasPendientesCsvFilePath, tareasPendientesCsvSeparator, tareasPendientesCsvHasFieldsInclosedInQuotes))
                .WhenInjectedExactlyInto<TareasPendientesModelRepository>();

            var tareasPendientesInicio2017CsvFilePath = Path.Combine(path, ConfigurationManager.AppSettings.Get("TareasPendientesInicio2017CsvFilePath"));
            var tareasPendientesInicio2017CsvSeparator = ConfigurationManager.AppSettings.Get("TareasPendientesInicio2017CsvSeparator");
            bool tareasPendientesInicio2017CsvHasFieldsInclosedInQuotes = bool.Parse(ConfigurationManager.AppSettings.Get("TareasPendientesInicio2017CsvHasFieldsEnclosedInQuotes"));
            Bind<IBusinessObjectDataRepository>().
                ToConstant(new BusinessObjectDataRepository(tareasPendientesInicio2017CsvFilePath, tareasPendientesInicio2017CsvSeparator, tareasPendientesInicio2017CsvHasFieldsInclosedInQuotes))
                .WhenInjectedExactlyInto<TareasPendientesInicio2017ModelRepository>();



            var tareasAnalizadasCsvFilePath = Path.Combine(path, ConfigurationManager.AppSettings.Get("TareasAnalizadasCsvFilePath"));
            var tareasAnalizadasCsvSeparator = ConfigurationManager.AppSettings.Get("TareasAnalizadasCsvSeparator");
            bool tareasAnalizadasCsvHasFieldsInclosedInQuotes = bool.Parse(ConfigurationManager.AppSettings.Get("TareasAnalizadasCsvHasFieldsEnclosedInQuotes"));
            Bind<ITareasAnalizadasRepository>().ToConstant(new TareasAnalizadasCsvRepository(tareasAnalizadasCsvFilePath, tareasAnalizadasCsvSeparator, tareasAnalizadasCsvHasFieldsInclosedInQuotes));


            Bind<IDataMapper<ITarea, TareaDataTransferObject>>().To<TareaIngresadaDataMapper>().WhenInjectedExactlyInto<TareasIngresadasModelRepository>().InSingletonScope();
            Bind<IDataMapper<ITarea, TareaDataTransferObject>>().To<TareaCerradaDataMapper>().WhenInjectedExactlyInto<TareasCerradasModelRepository>().InSingletonScope();
            Bind<IDataMapper<ITarea, TareaDataTransferObject>>().To<TareaPendienteDataMapper>().WhenInjectedExactlyInto<TareasPendientesModelRepository>().InSingletonScope();
            Bind<IDataMapper<ITarea, TareaDataTransferObject>>().To<TareaPendienteDataMapper>().WhenInjectedExactlyInto<TareasPendientesInicio2017ModelRepository>().InSingletonScope();



            Bind<IEnumMapper<Estado>>().To<TareaCerradaResultadoEnumMapper>().WhenInjectedExactlyInto<TareaCerradaDataMapper>().InSingletonScope();
            Bind<IEnumMapper<Estado>>().To<TareaIngresadaResultadoMapper>().WhenInjectedExactlyInto<TareaIngresadaDataMapper>().InSingletonScope();
            Bind<IEnumMapper<Estado>>().To<TareaPendienteResultadoMapper>().WhenInjectedExactlyInto<TareaPendienteDataMapper>().InSingletonScope();
            Bind<IEnumMapper<Zona>>().To<ZonaEnumMapper>().InSingletonScope();
        }
    }

    public class TareasCerradasModelModule : NinjectModule
    {
        public override void Load()
        {

            Bind<TareasCerradasModelRepository>().To<TareasCerradasModelRepository>().InSingletonScope();

            Bind<ITareasIngresadasModelRepository>().To<TareasIngresadasModelRepository>().InSingletonScope();
            Bind<ITareasCerradasModelRepository>().To<TareasCerradasModelRepository>().InSingletonScope();
            Bind<ITareasPendientesModelRepository>().To<TareasPendientesModelRepository>().InSingletonScope();
            Bind<ITareasPendientesInicio2017ModelRepository>().To<TareasPendientesInicio2017ModelRepository>().InSingletonScope();
            Bind<TareaAnalizadaModelRepository>().To<TareaAnalizadaModelRepository>().InSingletonScope();
        }
    }
}