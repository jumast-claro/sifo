﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Ninject.Modules;
using SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento;
using SIFO.DesktopUI.Annotations;
using SIFO.DesktopUI.Framework;
using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.Properties;
using Vantive.Model.Abstractions;

namespace SIFO.DesktopUI.App_Vantive
{
    public class TareaVantiveViewModel : IEnlaceViewModel
    {

        public TareaVantiveViewModel(ITareaVantive model)
        {
            FechaDeInstalacionProperty = new ReadOnlyProperty<DateTime?>(model.FechaDeInstalacion, model.FechaDeInstalacion.HasValue ? model.FechaDeInstalacion.ToString() : "no tiene");
            ClienteProperty = new ReadOnlyProperty<string>(model.Cliente);
            EstadoProperty = new ReadOnlyProperty<string>(model.Estado);
            FechaDeRecibidoProperty = new ReadOnlyProperty<DateTime>(model.FechaDeRecibido);
            IdentificadorProperty = new ReadOnlyProperty<string>(model.Identificador);
            IdTareaProperty = new ReadOnlyProperty<string>(model.IdTarea);
            LocalidadProperty = new ReadOnlyProperty<string>(model.Localidad);
            MotivoDeInterrupcionProperty = new ReadOnlyProperty<string>(model.MotivoDeInterrupcion);
            MotivoDeOrdenProperty = new ReadOnlyProperty<string>(model.MotivoDeOrden);
            NumeroDeOrdenProperty = new ReadOnlyProperty<string>(model.NumeroDeOrden);
            ProvinciaProperty = new ReadOnlyProperty<string>(model.Provincia);
            TomadoPorProperty = new ReadOnlyProperty<string>(model.TomadoPor);
            UnidadDeNegocioProperty = new ReadOnlyProperty<string>(model.UnidadDeNegocio);

            PendienteBoProperty = new ReadWriteProperty<bool>(false);

            InboxProperty = new ReadOnlyProperty<string>("Obra Civil Tend AMBA");
            PseudoEnlaceProperty = new ReadWriteProperty<string>("");

            VencidaProperty = new ReadOnlyProperty<string>(model.Vencida.ToString());
            DiasParaVencimientoProperty = new ReadOnlyProperty<int?>(model.DiasParaVencimiento);

            DiasDeRecibidoProperty = new ReadOnlyProperty<int>(model.DiasDeRecibido);

            TipificacionProperty = new ReadWriteProperty<string>("");
            DireccionProperty = new ReadWriteProperty<string>("");

            ProductoProperty = new ReadOnlyProperty<string>(model.Producto);

            ContratistaProperty = new ReadWriteProperty<string>("");

            FechaInformeRecibidoProperty = new ReadWriteProperty<DateTime?>(null);
            FechaAsignadaProperty = new ReadWriteProperty<DateTime?>(null);
            DireccionPlanaProperty = new ReadWriteProperty<string>("");

            HipervisorProperty = new ReadWriteProperty<string>("");
            PartidoProperty = new ReadWriteProperty<string>("");
            CompromisoPlanaProperty = new ReadWriteProperty<DateTime?>(null);

            NavigateUriProperty = new ReadWriteProperty<string>("");

            InstalacionIDProperty = new ReadWriteProperty<string>("");

            //NotasProperty = new ReadWriteProperty<string>("notas");

            //Notas = new ObservableCollection<NotaModel>();

            //AgregarNotaCommand = new RelayCommand(_agregarNotaExecuted, o => true);

            Notas = new NotasViewModel();
        }

        public IBindableProperty<DateTime?> FechaDeInstalacionProperty { get; private set; }
        public IBindableProperty<string> ClienteProperty { get; set; }
        public IBindableProperty<string> EstadoProperty { get; private set; }
        public IBindableProperty<DateTime> FechaDeRecibidoProperty { get; private set; }
        public IBindableProperty<string> IdentificadorProperty { get; private set; }
        public IBindableProperty<string> IdTareaProperty { get; private set; }
        public IBindableProperty<string> LocalidadProperty { get; private set; }
        public IBindableProperty<string> MotivoDeInterrupcionProperty { get; private set; }
        public IBindableProperty<string> MotivoDeOrdenProperty { get; private set; }
        public IBindableProperty<string> NumeroDeOrdenProperty { get; private set; }
        public IBindableProperty<string> ProvinciaProperty { get; private set; }
        public IBindableProperty<string> UnidadDeNegocioProperty { get; private set; }
        public IBindableProperty<string> TomadoPorProperty { get; set; }

        public IBindableProperty<bool> PendienteBoProperty { get; private set; }

        public IBindableProperty<string> InboxProperty { get; }
        public IBindableProperty<string> PseudoEnlaceProperty { get; }

        public IBindableProperty<string> VencidaProperty { get; }

        public IBindableProperty<int?> DiasParaVencimientoProperty { get; }

        public IBindableProperty<int> DiasDeRecibidoProperty { get; }

        public IBindableProperty<string> TipificacionProperty { get; set; }

        public IBindableProperty<string> DireccionProperty { get; set; }

        public IBindableProperty<string> ProductoProperty { get; private set; }

        public IBindableProperty<string> ContratistaProperty { get; set; }

        public IBindableProperty<DateTime?> FechaInformeRecibidoProperty { get; set; }
      
       
        public IBindableProperty<string> DireccionPlanaProperty { get; set; }
        public IBindableProperty<string> PartidoProperty { get; set; }
        public IBindableProperty<DateTime?> CompromisoPlanaProperty { get; set; }
        public IBindableProperty<DateTime?> FechaAsignadaProperty { get; set; }
        public IBindableProperty<string> HipervisorProperty { get; set; }
        public IBindableProperty<string> NavigateUriProperty { get; set; }

        public IBindableProperty<string> InstalacionIDProperty { get; set; }

        //public IBindableProperty<string> NotasProperty { get; set; }


        //public ObservableCollection<NotaModel> Notas { get; set; }

        //public ICommand AgregarNotaCommand { get; set; }

        //private void _agregarNotaExecuted(object o)
        //{
        //    Notas.Add(new NotaModel());
        //}

        public NotasViewModel Notas { get; set; }
      

    }

    public class NotasViewModel : INotifyPropertyChanged
    {
        
        private NotaModel _nota;

        public string Enlace { get; set; }

        public NotasViewModel()
        {
            AgregarNotaCommand = new RelayCommand(agregarNotaExecuted, o => true);
            EliminarNotaCommand = new RelayCommand(_eleminarNotaExecuted, _eliminarNotaCanExecute);
        }

        private ObservableCollection<INota> _notas = new ObservableCollection<INota>();
        public ObservableCollection<INota> Notas
        {
            get
            {
                return _notas;                
            }
            set
            {
                _notas = value;
                OnPropertyChanged();
            }
        }

        public NotaModel Nota
        {
            get
            {
                return _nota;
            }
            set
            {
                _nota = value; 
                OnPropertyChanged();
            }
        }

        public ICommand AgregarNotaCommand { get; set; }

        private void agregarNotaExecuted(object o)
        {
            _notas.Insert(0, new NotaModel() {Fecha = DateTime.Now, Enlace = Enlace});
        }

        public ICommand EliminarNotaCommand { get; set; }

        private void _eleminarNotaExecuted(object o)
        {
            _notas.Remove(Nota);
        }

        private bool _eliminarNotaCanExecute(object o)
        {
            return Nota != null;
        }



        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
