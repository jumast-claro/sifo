﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Syncfusion.UI.Xaml.Grid;

namespace SIFO.DesktopUI.App_Vantive.SyncfusionDataGrid
{
    public interface IShowColumnChooserCommand : ICommand
    {
        
    }

    public class ChooseColumnsCommand : IShowColumnChooserCommand
    {
        private ColumnChooser _columnChooser;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {

            var sfDataGrid = parameter as SfDataGrid;
            if(sfDataGrid == null) return;


            if (_columnChooser == null)
            {
                _columnChooser = new ColumnChooser(sfDataGrid);

                _columnChooser.Closed += (sender, args) =>
                {
                    _columnChooser = null;
                };
            }

            var chooserWindow = _columnChooser;
            if (!chooserWindow.IsVisible)
            {
                //chooserWindow = new ColumnChooser(sfDataGrid);
                chooserWindow.Resources.MergedDictionaries.Clear();
                chooserWindow.ClearValue(ColumnChooser.StyleProperty);
                sfDataGrid.GridColumnDragDropController = new GridColumnChooserController(sfDataGrid, chooserWindow);

                chooserWindow.Width = 300;
                chooserWindow.Height = 500;
                chooserWindow.Show();

                chooserWindow.Owner = Application.Current.MainWindow;
            }


        }

        public event EventHandler CanExecuteChanged;
    }
}
