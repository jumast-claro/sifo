﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Win32;
using Syncfusion.UI.Xaml.Grid;
using Syncfusion.UI.Xaml.Grid.Converter;
using Syncfusion.XlsIO;
using Syncfusion.XlsIO.Implementation;
using Syncfusion.XlsIO.Implementation.Collections;

namespace SIFO.DesktopUI.App_Vantive
{

    public interface IExportToExcelCommand : ICommand
    {
        SfDataGrid SfDataGrid { get; set; }
    }

    public class ExportToExcelCommand : FrameworkElement, IExportToExcelCommand
    {
        public static DependencyProperty SfDataGridProperty = DependencyProperty.Register(
            "SfDataGrid", 
            typeof (SfDataGrid), 
            typeof (ExportToExcelCommand), 
            new FrameworkPropertyMetadata(null, 
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public Brush HeaderBackGround { get; set; } = new SolidColorBrush(Colors.Black);
        public Brush HeaderForeground { get; set; } = new SolidColorBrush(Colors.WhiteSmoke);
        public string DefaultBookName { get; set; } = "Libro1";

        public SfDataGrid SfDataGrid
        {
            get { return (SfDataGrid)GetValue(SfDataGridProperty); }
            set
            {
                SetValue(SfDataGridProperty, value);
            }
        }



        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            //var dataGrid = parameter as SfDataGrid;
            var dataGrid = SfDataGrid;
            if (dataGrid == null)
            {
                MessageBox.Show("Null SyncfusionDataGrid");
                return;
            }
            try
            {
                var options = new ExcelExportingOptions();
                options.ExcelVersion = ExcelVersion.Excel2013;
                options.ExportStackedHeaders = true;
                options.ExportingEventHandler = ExportingHandler;
                //options.CellsExportingEventHandler = CustomizeCellExportingHandler;

                var excelEngine = dataGrid.ExportToExcel(dataGrid.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                var range = "A" + (dataGrid.StackedHeaderRows.Count + 1).ToString() + ":" + workBook.Worksheets[0].UsedRange.End.AddressLocal;
                excelEngine.Excel.Workbooks[0].Worksheets[0].AutoFilters.FilterRange = workBook.Worksheets[0].Range[range];

                var ws = workBook.Worksheets[0];
                var rowCount = ws.Rows.Length;

                ws.SetRowHeight(1, 25);
                ws.SetRowHeight(2, 25);
                for (int i = 3; i <= rowCount; i++)
                {
                    ws.SetRowHeight(i, 15);
                }

                SaveFileDialog sfd = new SaveFileDialog
                {
                    FilterIndex = 2,
                    Filter = "Excel 97 to 2003 Files(*.xls)|*.xls|Excel 2007 to 2010 Files(*.xlsx)|*.xlsx",
                    FileName = DefaultBookName
                };

                if (sfd.ShowDialog() == true)
                {
                    using (Stream stream = sfd.OpenFile())
                    {
                        if (sfd.FilterIndex == 1)
                            workBook.Version = ExcelVersion.Excel97to2003;
                        else
                            workBook.Version = ExcelVersion.Excel2010;
                        workBook.SaveAs(stream);
                    }

                    //Message box confirmation to view the created spreadsheet.
                    if (MessageBox.Show("Querés abrir el libro creado?", "Se creó el libro de excel",
                                        MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
                    {
                        //Launching the Excel file using the default Application.[MS Excel Or Free ExcelViewer]
                        System.Diagnostics.Process.Start(sfd.FileName);
                    }
                }
            }
            catch (Exception)
            {

            }
        }


        private void ExportingHandler(object sender, GridExcelExportingEventArgs e)
        {

            if (e.CellType == ExportCellType.StackedHeaderCell)
            {
                e.CellStyle.BackGroundBrush = HeaderBackGround;
                e.CellStyle.ForeGroundBrush = HeaderForeground;
            }

            if (e.CellType == ExportCellType.HeaderCell)
            {
                e.CellStyle.BackGroundBrush = HeaderBackGround;
                e.CellStyle.ForeGroundBrush = HeaderForeground;
            }

            if (e.CellType == ExportCellType.HeaderCell || e.CellType == ExportCellType.StackedHeaderCell)
            {
                e.Style.HorizontalAlignment = ExcelHAlign.HAlignCenter;
                e.Style.VerticalAlignment = ExcelVAlign.VAlignCenter;
                e.Handled = true;
            }

        }

        public event EventHandler CanExecuteChanged;
    }
}
