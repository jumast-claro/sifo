﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Syncfusion.UI.Xaml.Grid;

namespace SIFO.DesktopUI.App_Vantive
{
    /// <summary>
    /// Interaction logic for TareasVantiveSfDataGridView.xaml
    /// </summary>
    public partial class TareasVantiveSfDataGridView : UserControl
    {
        public static DependencyProperty SfDataGridProperty = DependencyProperty.Register("SfDataGrid", typeof(SfDataGrid), typeof(TareasVantiveSfDataGridView));

        public TareasVantiveSfDataGridView()
        {
            InitializeComponent();
            SfDataGrid = this.sfDataGrid;
        }

        public SfDataGrid SfDataGrid
        {
            get { return (SfDataGrid)GetValue(SfDataGridProperty); }
            set
            {
                SetValue(SfDataGridProperty, value);
            }
        }

    }
}
