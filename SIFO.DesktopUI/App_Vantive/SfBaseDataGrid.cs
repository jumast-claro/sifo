﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SIFO.DesktopUI.AnalisisPermanencia.Cumplimiento;
using SIFO.DesktopUI.App_Enlaces;
using SIFO.DesktopUI.App_Tareas;
using SIFO.DesktopUI.Commands;
using SIFO.DesktopUI.RoutedCommands;
using SIFO.Model;
using Syncfusion.Data.Extensions;
using Syncfusion.UI.Xaml.Grid;

namespace SIFO.DesktopUI.App_Vantive
{
    [ContentProperty("Content")]
    public class SfBaseDataGrid : Control
    {
       
        public static  DependencyProperty AjustaAlturaDeDetallesProperty = DependencyProperty.Register("AjustarAlturaDeDetalles", typeof(bool), typeof(SfBaseDataGrid), new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public bool AjustarAlturaDeDetalles
        {
            get { return (bool) GetValue(AjustaAlturaDeDetallesProperty); }
            set
            {
                SetValue(AjustaAlturaDeDetallesProperty, value);
            }
        }


        public static DependencyProperty TareasItemsSourceProperty = DependencyProperty.Register("TareasItemsSource", typeof(object), typeof(SfBaseDataGrid));
        public static readonly DependencyProperty TareaSeleccionadaProperty = DependencyProperty.Register("TareaSeleccionada", typeof(object), typeof(SfBaseDataGrid), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

       

        public static DependencyProperty TareasGanttProperty = DependencyProperty.Register("TareasGantt", typeof(object), typeof(SfBaseDataGrid));
        public static DependencyProperty HighlightedTareasGanttProperty = DependencyProperty.Register("HighlightedTareasGantt", typeof(object), typeof(SfBaseDataGrid));

        public static DependencyProperty EnlaceProperty = DependencyProperty.Register("Enlace", typeof(string), typeof(SfBaseDataGrid), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,EnlaceProperty_PropertyChangedCallback));

        private static void EnlaceProperty_PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

            SfBaseDataGrid me = d as SfBaseDataGrid;

            if (me.AjustarAlturaDeDetalles == false)
            {
                return;
            }

            var height = me.GetTemplateChild("PART_Height") as RowDefinition;
            if (height == null) return;

            var count = (me.TareasItemsSource as IEnumerable).ToList<object>().Count();
            height.Height = new GridLength(110 + 20 * count, GridUnitType.Pixel);
        }

        public string Enlace
        {
            get
            {
                return (string)GetValue(EnlaceProperty);
            }
            set
            {
                SetValue(EnlaceProperty, value);
            }
        }


        public object TareasItemsSource
        {
            get
            {
                return GetValue(TareasItemsSourceProperty);
            }
            set
            {
                SetValue(TareasItemsSourceProperty, value);
            }
        }

        public object TareaSeleccionada
        {
            get { return GetValue(TareaSeleccionadaProperty); }
            set
            {
                SetValue(TareaSeleccionadaProperty, value);
            }
        }

        public object TareasGantt
        {
            get
            {
                return GetValue(TareasGanttProperty);
            }
            set
            {
                SetValue(TareasGanttProperty, value);
            }
        }

        public object HighlightedTareasGantt
        {
            get
            {
                return GetValue(HighlightedTareasGanttProperty);
            }
            set
            {
                SetValue(HighlightedTareasGanttProperty, value);
            }
        }

        public static DependencyProperty MostrarDetalleDeTareaCommandProperty = DependencyProperty.Register("MostrarDetalleDeTareaCommand", typeof (MostrarDetallesDeTareaCommand), typeof (SfBaseDataGrid));

        public MostrarDetallesDeTareaCommand MostrarDetallesDeTareaCommand
        {
            get
            {
                return (MostrarDetallesDeTareaCommand) GetValue(MostrarDetalleDeTareaCommandProperty);
                
            }
            set
            {
                SetValue(MostrarDetalleDeTareaCommandProperty, value);
            }
        }


        public static DependencyProperty ContentProperty = DependencyProperty.Register("Content", typeof(object), typeof(SfBaseDataGrid),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsParentMeasure));

        public static DependencyProperty ExportToExcelCommandProperty = DependencyProperty.Register("ExportToExcelCommand", typeof(ICommand), typeof(SfBaseDataGrid));
        public static DependencyProperty ShowColumnChooserCommandProperty = DependencyProperty.Register("ShowColumnChooserCommand", typeof(ICommand), typeof(SfBaseDataGrid));

        public static DependencyProperty HeaderTextProperty = DependencyProperty.Register("HeaderText", typeof(string), typeof(SfBaseDataGrid));

        public static DependencyProperty ThemeColorProperty = DependencyProperty.Register("ThemeColor", typeof(Brush), typeof(SfBaseDataGrid));

        public static DependencyProperty SfDataGridProperty = DependencyProperty.Register("SfDataGrid", typeof(SfDataGrid), typeof(SfBaseDataGrid), new FrameworkPropertyMetadata(null, _SfDataGridProperty_PropertyChangedCallback));

        private static void _SfDataGridProperty_PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var newValue = e.NewValue as SfDataGrid;
            var me = d as SfBaseDataGrid;
            var command = me.ExportToExcelCommand as IExportToExcelCommand;
            command.SfDataGrid = newValue;
        }

        public static DependencyProperty PaletteWidthProperty = DependencyProperty.Register("PaletteWidth", typeof(GridLength), typeof(SfBaseDataGrid));

        static SfBaseDataGrid()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SfBaseDataGrid), new FrameworkPropertyMetadata(typeof(SfBaseDataGrid)));

            CommandManager.RegisterClassCommandBinding(typeof(SfBaseDataGrid), new CommandBinding(EventCommands.DoubleClick, Executed));

           

        }

        private static void Executed(object sender, ExecutedRoutedEventArgs executedRoutedEventArgs)
        {
            var tarea = executedRoutedEventArgs.Parameter as ITarea;
            //MessageBox.Show(tareaId);

            var window = new Window();
            var view = new DetalleDeTareaView();
            window.Content = view;
            window.WindowState = WindowState.Maximized;

            var viewModel = new DetalleDeTareaViewModel(tarea);
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            window.DataContext = viewModel;
            window.ShowDialog();
        }


        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            var part = GetTemplateChild("PART_Width") as ColumnDefinition;
            if(part == null) return;
            part.Width = new GridLength(0, GridUnitType.Pixel);

            var height = GetTemplateChild("PART_Height") as RowDefinition;
            if (height == null) return;
            height.Height = new GridLength(0, GridUnitType.Pixel);
        }

        public object Content
        {
            get { return GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        public ICommand ExportToExcelCommand
        {
            get { return (ICommand)GetValue(ExportToExcelCommandProperty); }
            set
            {
                SetValue(ExportToExcelCommandProperty, value);
            }
        }

        public ICommand ShowColumnChooserCommand
        {
            get { return (ICommand)GetValue(ShowColumnChooserCommandProperty); }
            set
            {
                SetValue(ShowColumnChooserCommandProperty, value);
            }
        }

        public string HeaderText
        {
            get { return (string) GetValue(HeaderTextProperty); }
            set
            {
                SetValue(HeaderTextProperty, value);
            }
        }

        public Brush ThemeColor
        {
            get { return (Brush)GetValue(ThemeColorProperty); }
            set
            {
                SetValue(ThemeColorProperty, value);
            }
        }

        public SfDataGrid SfDataGrid
        {
            get { return (SfDataGrid)GetValue(SfDataGridProperty); }
            set
            {
                SetValue(SfDataGridProperty, value);
            }
        }

        public GridLength PaletteWidth
        {
            get { return (GridLength)GetValue(PaletteWidthProperty); }
            set
            {
                SetValue(PaletteWidthProperty, value);
            }
        }


    }
}
