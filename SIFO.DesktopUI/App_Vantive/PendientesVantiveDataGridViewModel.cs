using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Windows;
using System.Windows.Input;
using System.Xml;
using System.Xml.Serialization;
using SIFO.DesktopUI.App_Enlaces;
using SIFO.DesktopUI.App_Vantive.SyncfusionDataGrid;
using SIFO.DesktopUI.Framework;
using SIFO.DesktopUI.TareasIngresadas;
using SIFO.DesktopUI.Utilities;
using SIFO.DesktopUI.Utilities.Properties;
using Syncfusion.XlsIO;
using Vantive.DataService.Abstractions;

namespace SIFO.DesktopUI.App_Vantive
{

    public class InstalacionData
    {
        public string ContratistaEnCurso { get; set; }
        public DateTime? FechaDeAsignada { get; set; }
        public DateTime? FechaDeInformeRecibido { get; set; }
        public string Direccion { get; set; }
        public string Partido { get; set; }
        public string Analista { get; set; }
        public DateTime? Compromiso { get; set; }
        public string NavigateUri { get; set; }


    }

    public interface IInstalacionDataProvider
    {
        bool ContieneEnlace(string enlace);
        InstalacionData ObtenerDatosParaEnlace(string enlace);
        void Actualizar();
    }

    public class ExcelInstalacionDataProvider : IInstalacionDataProvider
    {
        private readonly Dictionary<string, InstalacionData> _dic = new Dictionary<string, InstalacionData>();

        private void read()
        {
            ExcelEngine excelEngine = new ExcelEngine();
            IApplication application = excelEngine.Excel;
            IWorkbook wb = application.Workbooks.Open(Path.Combine(ConfigurationManager.AppSettings.Get("RUTA"), ConfigurationManager.AppSettings.Get("PLANA")));
            IWorksheet ws = wb.Worksheets[0];
            var rowCount = ws.Rows.Length;

            for (int i = 3; i <= rowCount; i++)
            {
                var enlacePlana = ws.Range[i, 4].Value;
                if (string.IsNullOrEmpty(enlacePlana)) continue;

                var data = new InstalacionData();

                var contrata = ws.Range[i, 10].Value;
                data.ContratistaEnCurso = contrata;

                var informe = ws.Range[i, 12].Value;
                if (!string.IsNullOrEmpty(informe))
                {
                    try
                    {
                        var fechaInforme = DateTime.ParseExact(informe, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        data.FechaDeInformeRecibido = fechaInforme;
                    }
                    catch (Exception)
                    {

                    }

                }

                var asignada = ws.Range[i, 11].Value;
                if (!string.IsNullOrEmpty(asignada))
                {
                    try
                    {
                        var fechaAsignada = DateTime.ParseExact(asignada, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        data.FechaDeAsignada = fechaAsignada;
                    }
                    catch (Exception)
                    {

                    }
                }



                var d = ws.Range[i, 2];

                var direccion = ws.Range[i, 2].FormulaStringValue;
                data.Direccion = direccion;

                try
                {
                    var formula = ws.Range[i, 2].Value;
                    var navigationUri = formula.Substring(15).Split('"')[0];
                    data.NavigateUri = navigationUri;
                }
                catch (Exception)
                {

                    //throw;
                }


                var partido = ws.Range[i, 3].Value;
                data.Partido = partido;

                var analista = ws.Range[i, 18].Value;
                data.Analista = analista;

                var compromiso = ws.Range[i, 7].Value;
                if (!string.IsNullOrEmpty(compromiso))
                {
                    try
                    {
                        var fechaCompromiso = DateTime.ParseExact(compromiso, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        data.Compromiso = fechaCompromiso;
                    }
                    catch (Exception)
                    {

                    }
                }



                _dic[enlacePlana] = data;
            }
        }

        public ExcelInstalacionDataProvider()
        {
           
           read(); 
        }

        public void Actualizar()
        {
            _dic.Clear();
            read();
        }

        public bool ContieneEnlace(string enlace)
        {
            if (_dic.ContainsKey(enlace))
            {
                return true;
            }
            else
            {
                foreach (var key in _dic.Keys)
                {
                    if (key.Contains(enlace))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public InstalacionData ObtenerDatosParaEnlace(string enlace)
        {
            if (_dic.ContainsKey(enlace))
            {
                return _dic[enlace];
            }
            else
            {
                foreach (var key in _dic.Keys)
                {
                    if (key.Contains(enlace))
                    {
                        return _dic[key];
                    }
                }
            }
            throw new InvalidOperationException();
        }
    }

    public class PendientesVantiveDataGridViewModel : INotifyPropertyChanged
    {
        private readonly List<TareaVantiveViewModel> _allItems = new List<TareaVantiveViewModel>();

        private readonly NotasRepository _notasRepository = new NotasRepository();

        private readonly IInstalacionDataProvider _dataProvider = new ExcelInstalacionDataProvider();

        public PendientesVantiveDataGridViewModel(ITareasVantiveModelRepository repo, ITareasPendientesModelRepository tareasPendientesBoModelRepository, ITareasIngresadasModelRepository tareasIngresadasModelRepository, ITareasCerradasModelRepository tareasCerradasModelRepository)
        {

            var tareasVantive = repo.SelectAll();
            foreach (var model in tareasVantive)
            {
                var viewModel = new TareaVantiveViewModel(model);

                var tareaPendienteBo = tareasPendientesBoModelRepository.SelectByTareaId(model.IdTarea);
                if (tareaPendienteBo != null)
                {
                    viewModel.PendienteBoProperty.Value = true;

                }

                var tarea = tareasIngresadasModelRepository.SelectByTareaId(model.IdTarea);
                if (tarea != null)
                {
                    viewModel.PseudoEnlaceProperty.Value = tarea.PseudoEnlace;
                    viewModel.TipificacionProperty.Value = tarea.TipoSolucion;
                    viewModel.DireccionProperty.Value = tarea.Calle + " " + tarea.Numero;
                }


                _allItems.Add(viewModel);

            }

            DetailsViewModel = new EnlaceDataGridDetailsViewModel(tareasCerradasModelRepository, tareasIngresadasModelRepository);


            var fechaDeBacklog = tareasVantive.Select(t => t.FechaDeRecibido).Max();
            HeaderTextProperty = new ReadOnlyProperty<string>($"Tareas pendietes al {fechaDeBacklog.ToString("dd/MM/yyyy")}");


            try
            {
                readExcel();
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }

            RefreshCommand = new RelayCommand(o => readExcel(), o => true);
            GuardarCommand = new RelayCommand(_guardarNotasExecuted, o => true);
            AgregarNotaCommand = new RelayCommand(_agregarNotaExecute, o => true);

            //readXml();

            foreach (var tarea in _allItems)
            {
                var enlaceTarea = tarea.PseudoEnlaceProperty.Value;
                var notas = _notasRepository.TodasLasNotasParaEnlace(enlaceTarea);
                if (notas != null)
                {
                    tarea.Notas.Enlace = enlaceTarea;
                    foreach (var nota in notas.OrderByDescending(n => n.Fecha))
                    {
                        tarea.Notas.Notas.Add(nota);
                    }
                }
            }


        }

        public ICommand RefreshCommand { get; set; }
        public ICommand GuardarCommand { get; set; }

        public ICommand AgregarNotaCommand { get; set; }

        private void _agregarNotaExecute(object o)
        {
            var seleccionada = SelectedTareaPendiente;
            seleccionada.Notas.Notas.Add(new NotaModel() {Fecha = DateTime.Now, Nota = "nueva nota"});
        }

        private void readExcel()
        {
            _dataProvider.Actualizar();
            foreach (var tarea in _allItems)
            {
                var enlace = tarea.PseudoEnlaceProperty.Value;
                if (_dataProvider.ContieneEnlace(enlace))
                {
                    var data = _dataProvider.ObtenerDatosParaEnlace(enlace);
                    tarea.DireccionPlanaProperty.Value = data.Direccion;
                    tarea.FechaAsignadaProperty.Value = data.FechaDeAsignada;
                    tarea.FechaInformeRecibidoProperty.Value = data.FechaDeInformeRecibido;
                    tarea.ContratistaProperty.Value = data.ContratistaEnCurso;
                    tarea.HipervisorProperty.Value = data.Analista;
                    tarea.PartidoProperty.Value = data.Partido;
                    tarea.CompromisoPlanaProperty.Value = data.Compromiso;
                    tarea.NavigateUriProperty.Value = data.NavigateUri;
                }
            }
        }

        public List<TareaVantiveViewModel> TareasVantive => _allItems;

        private EnlaceDataGridDetailsViewModel _enlaceDataGridDetailsViewModel;
        public EnlaceDataGridDetailsViewModel DetailsViewModel
        {
            get
            {
                return _enlaceDataGridDetailsViewModel;
            }
            private set
            {
                _enlaceDataGridDetailsViewModel = value;

            }
        }

        private TareaVantiveViewModel _selectedTareaPendiente;
        public TareaVantiveViewModel SelectedTareaPendiente
        {
            get { return _selectedTareaPendiente; }
            set
            {
                _selectedTareaPendiente = value;
                DetailsViewModel.Enlace = value;
                OnPropertyChanged();
            }
        }

        public IBindableProperty<string> HeaderTextProperty { get; set; }

        private void _guardarNotasExecuted(object o)
        {

            List<INota> notas = new List<INota>();
            foreach (var tarea in _allItems)
            {
                foreach (var nota in tarea.Notas.Notas)
                {
                    notas.Add(nota);
                }
            }
            _notasRepository.Guardar(notas);
        }

        //private void readXml()
        //{
        //    try
        //    {
        //        var path = @"C:\Users\Jumast\Desktop\SIFO_DATA";

        //        var serializer = new XmlSerializer(typeof(XmlDataCollection));

        //        XmlDataCollection xmlData;
        //        using (var reader = XmlReader.Create(Path.Combine(path, "data" + ".xml")))
        //        {
        //            xmlData = (XmlDataCollection)serializer.Deserialize(reader);
        //        }

        //        foreach (var data in xmlData.Data)
        //        {
        //            var enlace = data.ID;
        //            var tarea = _allItems.Where(t => t.PseudoEnlaceProperty.Value == enlace).FirstOrDefault();
        //            if (tarea != null)
        //            {
        //                tarea.Notas.Notas= new ObservableCollection<NotaModel>(data.Notas);
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        MessageBox.Show(e.Message);
        //    }

          



        //}

        //private void _saveExecuted(object o)
        //{
        //    var path = @"C:\Users\Jumast\Desktop\SIFO_DATA";

        //    try
        //    {
        //        var dataCollection = new XmlDataCollection();
        //        foreach (var tarea in _allItems)
        //        {
        //            var instalacionId = tarea.PseudoEnlaceProperty.Value;
        //            if (!string.IsNullOrEmpty(instalacionId))
        //            {
        //                var notasProperty = tarea.Notas.Notas;
        //                var notas = new List<NotaModel>();

        //                foreach (var nota in notasProperty)
        //                {
        //                    if (!string.IsNullOrEmpty(nota.Nota))
        //                    {
        //                        notas.Add(nota);
        //                    }
                           
        //                }

        //                if (notas.Count > 0)
        //                {
        //                    var xamlData = new XmlData() { ID = instalacionId, Notas = notas };
        //                    dataCollection.Data.Add(xamlData);
        //                }
                       
                        


        //            }
        //        }

        //        var serializer = new XmlSerializer(dataCollection.GetType());
        //        using (var writer = XmlWriter.Create(Path.Combine(path, "data" + ".xml"), new XmlWriterSettings() {Indent = true}))
        //        {
        //            serializer.Serialize(writer, dataCollection);
        //        };
        //    }
        //    catch (Exception e)
        //    {

        //        MessageBox.Show(e.Message);
        //    }

          
        //}



        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


    }

    public class XmlDataCollection
    {
        public List<XmlData> Data { get; set; } = new List<XmlData>();
    }

    public class XmlData
    {
        public string ID = "";
        public List<INota> Notas { get; set; }
        //public string XXX { get; set; } = "dfgdfg";
    }

   
}