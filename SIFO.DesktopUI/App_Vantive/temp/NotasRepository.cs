﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Syncfusion.XlsIO;

namespace SIFO.DesktopUI.App_Vantive
{
    public interface INota
    {
        string Enlace { get; set; }
        DateTime Fecha { get; set; }
        string Nota { get; set; }
    }

    public class NotaModel : INota
    {
        public string Enlace { get; set; }
        public string Nota { get; set; }
        public DateTime Fecha { get; set; }


    }

    public interface INotasRepository
    {
        IEnumerable<INota> TodasLasNotas();
        IEnumerable<INota> TodasLasNotasParaEnlace(string enlace);
    }

    public class NotasRepository : INotasRepository
    {

        private readonly List<INota> _notas = new List<INota>();

        public NotasRepository()
        {

            try
            {
                ExcelEngine excelEngine = new ExcelEngine();
                IApplication application = excelEngine.Excel;
                IWorkbook wb = application.Workbooks.Open(@"C:\Users\Jumast\Desktop\SIFO_DATA\db_notas.xlsx");
                IWorksheet ws = wb.Worksheets[0];
                var rowCount = ws.Rows.Length;

                //ws.Range[1, 1].Value = "Enlace";
                //ws.Range[1, 2].Value = "Fecha";
                //ws.Range[1, 3].Value = "Nota";

                for (int i = 1; i <= rowCount; i++)
                {
                    var enlace = ws.Range[i, 1].Value;
                    var fecha = ws.Range[i, 2].DateTime;
                    var nota = ws.Range[i, 3].Value;

                    var notaModel = new NotaModel()
                    {
                        Enlace = enlace,
                        Fecha = fecha,
                        Nota = nota
                    };

                    _notas.Add(notaModel);
                }
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }

           
        }



        public IEnumerable<INota> TodasLasNotas()
        {
            return _notas; 
        }

        public IEnumerable<INota> TodasLasNotasParaEnlace(string enlace)
        {
            var r  = _notas.Where(n => n.Enlace == enlace);
            return r;
        }

        public void Guardar(IEnumerable<INota> notas)
        {

            try
            {
                ExcelEngine excelEngine = new ExcelEngine();
                IApplication application = excelEngine.Excel;
                IWorkbook wb = application.Workbooks.Open(@"C:\Users\Jumast\Desktop\SIFO_DATA\db_notas.xlsx");
                wb.Worksheets.Create();
                IWorksheet ws = wb.Worksheets[1];
                var rowCount = ws.Rows.Length; 

                var notasNuevas = new List<INota>();

                //int i = 2;
                foreach (var nota in notas)
                {
                    var fecha = nota.Fecha;
                    var enlace = nota.Enlace;

                    var notaRepo = _notas.Where(n => n.Enlace == enlace && n.Fecha == fecha).FirstOrDefault();
                    if (notaRepo != null)
                    {
                        notaRepo.Nota = nota.Nota;
                    }
                    else
                    {
                        _notas.Add(nota);
                    }

                    //for (int j = 2; j <= rowCount; j++)
                    //{
                    //    var enlacePlana = ws.Range[j, 1].Value;
                    //    var fechaPlana = ws.Range[j, 2].DateTime;
                    //    if (enlacePlana == enlace && fechaPlana == fecha)
                    //    {
                    //        ws.Range[j, 3].Value2 = nota.Nota;
                    //        break;
                    //    }
                    //}

                    //notasNuevas.Add(nota);
                }

                int i = 1;
                foreach (var nota in _notas)
                {
                    ws.Range[i, 1].Value = nota.Enlace;
                    ws.Range[i, 2].Value2 = nota.Fecha;
                    ws.Range[i, 3].Value = nota.Nota;
                    i++;
                }


                ws.Range[1, 1, ws.Rows.Length, 3].WrapText = false;

                wb.Worksheets[0].Remove();
                wb.Save();
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }

          

        }
    }
}
