﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using SIFO.DesktopUI.TareasCerradas.Estadisticas;
using SIFO.DesktopUI.TareasCerradas.VistaPrincipal.Graficos;
using SIFO.Model;

namespace SIFO.DesktopUI.TareasIngresadas
{
    public class TareasIngresadasAnalisisViewModel : INotifyPropertyChanged
    {
        private readonly IEnumerable<ITarea> _universe;
        //private readonly ITareasCerradasModelRepository _repo;

        public TareasIngresadasAnalisisViewModel(IEnumerable<ITarea> universe)
        {
            _universe = universe;
            //_repo = repo;

            var tareasPorPlantaPieChartViewModel = new PieChartViewModel("Tareas ingresadas por planta");
            tareasPorPlantaPieChartViewModel.SetSeries(_universe, t => t.Planta.ToString());

            var tareasPorZonaPieChartViewModel = new PieChartViewModel("Tareas ingresadas por zona");
            tareasPorZonaPieChartViewModel.SetSeries(universe, t => t.Zona.ToString());

            var tareasPorInboxPieChartViewModel = new PieChartViewModel("Tareas ingresadas por inbox");
            tareasPorInboxPieChartViewModel.SetSeries(universe, t => t.Inbox);

            var tareasPorTipoDeOrdenPieChartViewModel = new PieChartViewModel("Tareas ingresadas por tipo de orden");
            tareasPorTipoDeOrdenPieChartViewModel.SetSeries(universe, t => t.TipoDeOrden);

            var tareasPorPlantaBarChartViewModel = new TareaIngresadaBarChartViewModel(universe, t => t.Planta.ToString());
            var tareasPorZonaBarChartViewModel = new TareaIngresadaBarChartViewModel(universe, t => t.Zona.ToString());
            var tareasPorTipoDeOrdenBarChartViewModel = new TareaIngresadaBarChartViewModel(universe, t => t.TipoDeOrden);


            TareasPorPlantaChartsViewModel = new IngresadasChartsViewModel()
            {
                PieChartViewModel = tareasPorPlantaPieChartViewModel,
                BarChartViewModel = tareasPorPlantaBarChartViewModel
            };

            TareasPorZonaChartsViewModel = new IngresadasChartsViewModel()
            {
                PieChartViewModel = tareasPorZonaPieChartViewModel,
                BarChartViewModel = tareasPorZonaBarChartViewModel
            };

            TareasPorTipoDeOrdenChartsViewModel = new IngresadasChartsViewModel()
            {
                PieChartViewModel = tareasPorTipoDeOrdenPieChartViewModel,
                BarChartViewModel = tareasPorTipoDeOrdenBarChartViewModel
            };
        }

        //---------------------------------------------------------------------
        // Propiedades
        //---------------------------------------------------------------------
        public IngresadasChartsViewModel TareasPorPlantaChartsViewModel { get; set; }
        public IngresadasChartsViewModel TareasPorZonaChartsViewModel { get; set; }
        public IngresadasChartsViewModel TareasPorTipoDeOrdenChartsViewModel { get; set; }


        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


    }
}
