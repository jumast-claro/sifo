namespace SIFO.DesktopUI.Framework
{
    public interface IViewModelFactory<TViewModel, TModel>
    {
        TViewModel CreateViewModel(TModel model);
    }
}