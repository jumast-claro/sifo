﻿using System;
using System.Windows;
using System.Windows.Input;
using SIFO.DesktopUI.TareasIngresadas;

namespace SIFO.DesktopUI.Commands
{
    public class MostrarDetallesDeTareaCommand : ICommand
    {
        private readonly ITareasIngresadasModelRepository _tareasIngresadasModelRepository;
        private readonly ITareasCerradasModelRepository _taeCerradasModelRepository;


        public MostrarDetallesDeTareaCommand(ITareasIngresadasModelRepository tareasIngresadasModelRepository, ITareasCerradasModelRepository taeCerradasModelRepository)
        {
            _tareasIngresadasModelRepository = tareasIngresadasModelRepository;
            _taeCerradasModelRepository = taeCerradasModelRepository;
        }

        public bool CanExecute(object parameter)
        {
            var tareaId = parameter as string;
            if (_tareasIngresadasModelRepository.SelectByTareaId(tareaId) != null)
            {
                return true;
            }
            return false;
        }

        public void Execute(object parameter)
        {
            var tareaId = parameter as string;
            MessageBox.Show("ID Tarea: " + tareaId);
        }

        public event EventHandler CanExecuteChanged;
    }
}
