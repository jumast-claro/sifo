﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SIFO.DesktopUI.Commands
{
    public static class EventCommands
    {
        private static readonly RoutedUICommand _doubleClickCommand = new RoutedUICommand("DoubleClick", "DoubleClickCommand", typeof(RoutedCommands.Commands));

        public static RoutedUICommand DoubleClick => _doubleClickCommand;

        static EventCommands()
        {
        }
    }
}
