﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumast.Claro.BusinessObject.Tareas.DataAcces.Abstract;
using Syncfusion.XlsIO;

namespace Jumast.Claro.BusinessObject.Tareas.DataAcces.Concrete.Excel
{
    public class TareasBusinessObjectExcelReader
    {
        private const int REGION_FINAL = 1;
        private const int ZONA = 2;
        private const int INBOX = 3;
        private const int INBOX_FINAL = 4;
        private const int TIPO_DE_MOVIMIENTO = 5;
        private const int TECNOLOGIA = 6;
        private const int TIPO_FIBRA_OPTICA = 7;
        private const int PRODUCTO = 8;
        private const int TIPO_DE_ORDEN = 9;
        private const int TIPO_DE_SOLUCION = 10;

        private const int FECHA_DE_COMPROMISO = 12;
        private const int FECHA_RENEGOCIADA = 13;
        private const int FECHA_INICIO_TAREA = 14;
        private const int FECHA_FIN_TAREA = 15;

        private const int RESULTADO = 17;
        private const int TOMADO_POR = 18;
        private const int CLIENTE = 19;
        private const int ORDEN = 20;
        private const int ENLACE = 21;
        private const int TAREA_ID = 22;
        private const int IDENTIFICADOR = 23;
        private const int ULTIMA_NOTA_FECHA = 24;
        private const int ULTIMA_NOTA_AUTOR = 25;
        private const int ULTIMA_NOTA = 26;
        private const int WORKFLOW_FECHA_INICIO = 27;
        private const int WORKFLOW_FECHA_FIN = 28;
        private const int PROVINCIA = 29;
        private const int PARTIDO = 30;
        private const int LOCALIDAD = 31;
        private const int CALLE = 32;
        private const int NUMERO = 33;

        //---------------------------------------------------------------------
        // Campos
        //---------------------------------------------------------------------
        private readonly IWorksheet _worksheet;
        private readonly ITareaBusinessObjectMapper _mapper = new TareaBusinessObjectMapper();
        private IEnumerable<ITareaBusinessObjectDataModel> _data;
      


        //---------------------------------------------------------------------
        // Constructor
        //---------------------------------------------------------------------
        public TareasBusinessObjectExcelReader(string workbookFullPathWithExtension, int worksheetNumber)
        {
            ExcelEngine excelEngine = new ExcelEngine();
            IApplication application = excelEngine.Excel;
            IWorkbook wb = application.Workbooks.Open(workbookFullPathWithExtension);
            IWorksheet ws = wb.Worksheets[worksheetNumber - 1];
            _worksheet = ws;

            var name = ws.Name;
        }

        //---------------------------------------------------------------------
        // Propiedades
        //---------------------------------------------------------------------
        public int FirstRowWithData { get; set; } = 2;

        //---------------------------------------------------------------------
        // Métodos públicos
        //---------------------------------------------------------------------
        public IEnumerable<ITareaBusinessObjectDataModel> SelectAll()
        {
            if (_data == null)
            {
                _data = readExcelFile();
            }
            return _data;
        }
        private IEnumerable<ITareaBusinessObjectDataModel> readExcelFile()
        {
            var list = new List<ITareaBusinessObjectDataModel>();
            var rowCount = _worksheet.Rows.Length;
            for (var i = FirstRowWithData; i <= rowCount; i++)
            {
                var dataTransferObject = readRow(i);
                var dataModel = _mapper.MapDataTransferObjectToDataModel(dataTransferObject);
                list.Add(dataModel);
            }
            return list;
        }
        private ITareaBusinessObjectDataTransferObject readRow(int rowNumber)
        {
            var dataTransferObject = new TareaBusinessObjectDataTransferObject()
            {
                RegionFinal = getStringValue(rowNumber, REGION_FINAL),
                Zona = getStringValue(rowNumber, ZONA),
                Inbox = getStringValue(rowNumber, INBOX),
                InboxFinal = getStringValue(rowNumber, INBOX_FINAL),
                TipoMovimiento = getStringValue(rowNumber, TIPO_DE_MOVIMIENTO),
                Tecnologia = getStringValue(rowNumber, TECNOLOGIA),
                TipoFibraOptica = getStringValue(rowNumber, TIPO_FIBRA_OPTICA),
                Producto = getStringValue(rowNumber, PRODUCTO),
                TipoDeOrden = getStringValue(rowNumber, TIPO_DE_ORDEN),
                TipoDeSolucion = getStringValue(rowNumber, TIPO_DE_SOLUCION),
                FechaCompromisoOrden = getStringValue(rowNumber, FECHA_DE_COMPROMISO),
                FechaRenegociada = getStringValue(rowNumber, FECHA_RENEGOCIADA),
                FechaInicioTarea = getStringValue(rowNumber, FECHA_INICIO_TAREA),
                FechaFinTarea = getStringValue(rowNumber, FECHA_FIN_TAREA),
                Resultado = getStringValue(rowNumber, RESULTADO),
                TomadoPor = getStringValue(rowNumber, TOMADO_POR),
                Cliente = getStringValue(rowNumber, CLIENTE),
                Orden = getStringValue(rowNumber, ORDEN),
                Enlace = getStringValue(rowNumber, ENLACE),
                TareaId = getStringValue(rowNumber, TAREA_ID),
                Identificador = getStringValue(rowNumber, IDENTIFICADOR),
                UltimaNotaFecha = getStringValue(rowNumber, ULTIMA_NOTA_FECHA),
                UltimaNotaAutor = getStringValue(rowNumber, ULTIMA_NOTA_AUTOR),
                UltimaNota = getStringValue(rowNumber, ULTIMA_NOTA),
                WorkflowFechaInicio = getStringValue(rowNumber, WORKFLOW_FECHA_INICIO),
                WorkflowFechaFin = getStringValue(rowNumber, WORKFLOW_FECHA_FIN),
                Provincia = getStringValue(rowNumber, PROVINCIA),
                Partido = getStringValue(rowNumber, PARTIDO),
                Localidad = getStringValue(rowNumber, LOCALIDAD),
                Calle = getStringValue(rowNumber, CALLE),
                Numero = getStringValue(rowNumber, NUMERO),
            };

            return dataTransferObject;
        }
        private string getStringValue(int row, int column)
        {
            return _worksheet.Range[row, column].DisplayText;
        }



    }
}
