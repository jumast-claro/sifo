using System.Collections.Generic;

namespace Jumast.Claro.ObraCivilTendido.Notas
{
    public interface INotasRepository
    {
        IEnumerable<INota> TodasLasNotas();
        IEnumerable<INota> TodasLasNotasParaEnlace(string enlace);
        void EliminarNota(INota nota);
        void AgregarNota(INota nota);
        void Guardar();
    }
}