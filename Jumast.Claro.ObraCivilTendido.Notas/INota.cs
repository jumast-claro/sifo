using System;

namespace Jumast.Claro.ObraCivilTendido.Notas
{
    public interface INota
    {
        string Enlace { get; set; }
        DateTime Fecha { get; set; }
        string Nota { get; set; }
    }
}