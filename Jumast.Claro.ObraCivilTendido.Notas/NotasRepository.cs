﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using Syncfusion.XlsIO;

namespace Jumast.Claro.ObraCivilTendido.Notas
{
    public class NotasRepository : INotasRepository
    {
        private readonly string _rutaCompletaConExtension;

        private readonly List<INota> _notas = new List<INota>();

        public void AgregarNota(INota nota)
        {
            _notas.Add(nota);
        }

        public void EliminarNota(INota nota)
        {
            _notas.Remove(nota);
        }

        public NotasRepository(string rutaCompletaConExtension)
        {
            _rutaCompletaConExtension = rutaCompletaConExtension;

            ExcelEngine excelEngine = new ExcelEngine();
            IApplication application = excelEngine.Excel;
            //IWorkbook wb = application.Workbooks.Open(@"C:\Users\Jumast\Desktop\SIFO_DATA\db_notas.xlsx");
            IWorkbook wb = application.Workbooks.Open(rutaCompletaConExtension);
            IWorksheet ws = wb.Worksheets[0];
            var rowCount = ws.Rows.Length;

            //ws.Range[1, 1].Value = "Enlace";
            //ws.Range[1, 2].Value = "Fecha";
            //ws.Range[1, 3].Value = "Nota";

            for (int i = 1; i <= rowCount; i++)
            {
                var enlace = ws.Range[i, 1].Value;
                var fecha = ws.Range[i, 2].DateTime;
                var nota = ws.Range[i, 3].Value;

                var notaModel = new NotaModel()
                {
                    Enlace = enlace,
                    Fecha = fecha,
                    Nota = nota
                };

                _notas.Add(notaModel);
            }


            foreach (var nota in _notas)
            {
                var textoNota = nota.Nota;
                if (!string.IsNullOrEmpty(textoNota))
                {
                    var primeraLinea = textoNota.Split('\n')[0];
                    try
                    {

                        var fecha = DateTime.ParseExact(primeraLinea, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                        nota.Fecha = fecha;
                    }
                    catch (Exception)
                    {
                        
                    }
                }
             
            }
        }


        public IEnumerable<INota> TodasLasNotas()
        {
            return _notas;
        }

        public IEnumerable<INota> TodasLasNotasParaEnlace(string enlace)
        {
            var r = _notas.Where(n => n.Enlace == enlace);
            return r;
        }

        public void Guardar()
        {

            ExcelEngine excelEngine = new ExcelEngine();
            IApplication application = excelEngine.Excel;
            IWorkbook wb = application.Workbooks.Open(_rutaCompletaConExtension);
            wb.Worksheets.Create();
            IWorksheet ws = wb.Worksheets[1];
            var rowCount = ws.Rows.Length;

            var notasNuevas = new List<INota>();

            //int i = 2;
            foreach (var nota in _notas)
            {
                var fecha = nota.Fecha;
                var enlace = nota.Enlace;

                var notaRepo = _notas.Where(n => n.Enlace == enlace && n.Fecha == fecha).FirstOrDefault();
                if (notaRepo != null)
                {
                    notaRepo.Nota = nota.Nota;
                }
                else
                {
                    _notas.Add(nota);
                }
            }

            int i = 1;
            foreach (var nota in _notas)
            {
                ws.Range[i, 1].Value = nota.Enlace;
                ws.Range[i, 2].Value2 = nota.Fecha;
                ws.Range[i, 3].Value = nota.Nota;
                i++;
            }


            ws.Range[1, 1, ws.Rows.Length, 3].WrapText = false;

            wb.Worksheets[0].Remove();
            wb.Save();

        }
    }
}