﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumast.Claro.BusinessObject.Abstract;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Claro.BusinessObject.Mvvm
{
    public class FechaDeInicioProperty 
    {
        private readonly IFechaDeInicio _fechaDeInicio;

        public FechaDeInicioProperty(IFechaDeInicio fechaDeInicio)
        {
            _fechaDeInicio = fechaDeInicio;
        }

        public DateTime Fecha => _fechaDeInicio.Fecha;
        public int TiempoTranscurridoEnDias => _fechaDeInicio.TiempoTranscurridoEnDias;
        public string Texto => _fechaDeInicio.Fecha.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
    }
}
