﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumast.Claro.ObraCivilTendido.Mvvm;
using Jumast.Claro.ObraCivilTendido.Mvvm.InstalacionesSeguimiento;
using Jumast.Claro.Vantive.Mvvm;

namespace Jumast.Claro.ObraCivilTendido.DesktopUI
{
    public class MainWindowViewModel
    {
        private readonly SeguimientoDataGridViewModel _seguimientoDataGridViewModel;
        private readonly TareasVantiveViewModel _tareasVantiveViewModel;
        private readonly TerminadasDataGridViewModel _terminadasViewModel;

        public MainWindowViewModel(SeguimientoDataGridViewModel seguimientoDataGridViewModel, TareasVantiveViewModel tareasVantiveViewModel, TerminadasDataGridViewModel terminadasViewModel)
        {
            _seguimientoDataGridViewModel = seguimientoDataGridViewModel;
            _tareasVantiveViewModel = tareasVantiveViewModel;
            _terminadasViewModel = terminadasViewModel;
        }

        public SeguimientoDataGridViewModel SeguimientoDataGridViewModel => _seguimientoDataGridViewModel;
        public TareasVantiveViewModel TareasVantiveViewModel => _tareasVantiveViewModel;

        public TerminadasDataGridViewModel TerminadasViewModel => _terminadasViewModel;
    }
}
