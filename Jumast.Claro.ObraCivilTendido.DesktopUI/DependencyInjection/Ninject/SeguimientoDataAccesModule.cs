﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumast.Claro.ObraCivilTendido.DataAcces.Abstract;
using Jumast.Claro.ObraCivilTendido.DataAcces.Concrete;
using Jumast.Claro.ObraCivilTendido.Mvvm;
using Jumast.Claro.ObraCivilTendido.Mvvm.InstalacionesSeguimiento;
using Jumast.Claro.ObraCivilTendido.Notas;
using Ninject.Modules;

namespace Jumast.Claro.ObraCivilTendido.DesktopUI.DependencyInjection.Ninject
{
    public class SeguimientoDataAccesModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IInstalacionesFOExcelRepository>().ToConstant(new SeguimientoExcelRepository(@"C:\Users\Jumast\Desktop\Seguimiento Instalaciones FO - JUM.xlsx")).WhenInjectedExactlyInto<SeguimientoDataGridViewModel>();
            Bind<IInstalacionesFOExcelRepository>().ToConstant(new TerminadasExcelRepository(@"C:\Users\Jumast\Desktop\Seguimiento Instalaciones FO - JUM.xlsx")).WhenInjectedExactlyInto<TerminadasDataGridViewModel>();
            Bind<INotasRepository>().ToConstant(new NotasRepository(@"C:\Users\Jumast\Desktop\SIFO_DATA\db_notas.xlsx"));
        }
    }
}
