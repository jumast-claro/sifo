﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumast.Claro.Vantive.DataAcces.Abstract;
using Jumast.Claro.Vantive.Model.Abstract;
using Jumast.Claro.Vantive.Model.Concrete;

namespace Jumast.Claro.ObraCivilTendido.DesktopUI.TareasVantive
{
    public class TareasVantiveModelRepository : ITareasVantiveModelRepository
    {
        private readonly ITareasVantiveDataRepository _dataRepository;

        public TareasVantiveModelRepository(ITareasVantiveDataRepository dataRepository)
        {
            _dataRepository = dataRepository;
        }

        public IEnumerable<ITareaVantive> SelectAll()
        {
            var result = new List<ITareaVantive>();
            var data = _dataRepository.SelectAll();

            foreach (var dataModel in data)
            {
                ITareaVantive tareaVantive = new TareaVantive(dataModel.FechaDeRecibido.Value, dataModel.FechaDeInstalacion,dataModel.Identificador);
                tareaVantive.Inbox = dataModel.Inbox;
                //tareaVantive.FechaInstalacion = dataModel.FechaDeInstalacion;
                tareaVantive.Identificador = dataModel.Identificador;
                tareaVantive.NumeroDeOrden = dataModel.ID;
                tareaVantive.Cliente = dataModel.Cliente;
                tareaVantive.Localidad = dataModel.Localidad;
                tareaVantive.Provincia = dataModel.Provincia;
                tareaVantive.TomadoPor = dataModel.TomadoPor;
                tareaVantive.Estado = dataModel.Estado;
                tareaVantive.MotivoInterrupcion = dataModel.MotivoInterrupcion;
                tareaVantive.MotivoOrden = dataModel.MotivoOrden;
                //tareaVantive.FechaDeRecibido = dataModel.FechaDeRecibido.Value;
                tareaVantive.Ejecutivo = dataModel.Ejecutivo;
                tareaVantive.UnidadDeNegocio = dataModel.UnidadDeNegocio;
                tareaVantive.IdTarea = dataModel.IdObjeto;
                tareaVantive.Clase = dataModel.Clase;
                result.Add(tareaVantive);
            }

            return result;
        }
    }
}
