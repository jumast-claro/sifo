﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Jumast.Claro.ObraCivilTendido.DesktopUI.DependencyInjection.Ninject;
using Ninject.Modules;

namespace Jumast.Claro.ObraCivilTendido.DesktopUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {

            try
            {
                INinjectModule[] ninjectModules = { new SeguimientoDataAccesModule(), new TareasVantiveNinjectModule(),  };
                NinjectInjector ninjectInjector = new NinjectInjector(ninjectModules);
                var mainWindowViewModel = ninjectInjector.Resolve<MainWindowViewModel>();
                this.DataContext = mainWindowViewModel;
                InitializeComponent();
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
                Application.Current.Shutdown();
            }

           
        }
    }
}
