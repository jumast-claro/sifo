﻿using System.Collections.Generic;

namespace SIFO.DesktopUI.TareasIngresadas
{
    public interface ITareaModelRepository<T>
    {
        T SelectByTareaId(string tareaId);
        IEnumerable<T> SelectAll();
        void SaveAll();
    }
}