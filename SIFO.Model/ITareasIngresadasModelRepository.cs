﻿using SIFO.Model;

namespace SIFO.DesktopUI.TareasIngresadas
{
    public interface ITareasIngresadasModelRepository : ITareaModelRepository<ITarea>
    {
    }
}