﻿using System;

namespace SIFO.Model
{
    public class OrdenAnalizada
    {

        public OrdenAnalizada(string tareaId)
        {
            if (string.IsNullOrEmpty(tareaId))
            {
                throw new ArgumentException();
            }

            TareaId = tareaId;
        }

        public string TareaId{ get; private set; }
        public int Vantive_MalCargadaLaDireccion { get; set; }
        public int Vantive_FaltaDeContacto { get; set; }
        public int Vantive_MalTipificada { get; set; }
        public int Factibilidad_MalFactibilizado { get; set; }
        public int Factibilidad_CambioEnLasCondiciones { get; set; }
        public int Relevamiento_ProblemaDeContacto { get; set; }
        public int Relevamiento_ProblemaDeIngreso { get; set; }
        public int Relevamiento_SitioEnConstruccion { get; set; }
        public int InicioDePermisos_FaltaDeAutorizacionDelCliente { get; set; }
        public int InicioDePermisos_FaltaDeAutorizacionPorMotivosInternos { get; set; }
        public int ObraCivil_SitioEnConstruccion { get; set; }
        public int ObraCivil_ProblemaDeIngreso { get; set; }
        public int FaltaIngenieria { get; set; }
        public int FaltaAsignacion { get; set; }
        public int Tendido_SitioEnConstruccion { get; set; }
        public int Tendido_ProblemaDeIngreso { get; set; }
        public int Tendido_FaltaDeMateriales { get; set; }
        public int Odf_SitioEnConstruccion { get; set; }
        public int Odf_ProblemaDeIngreso { get; set; }
        public int Odf_FaltaDeMateriales { get; set; }
        public int SuspensionDeTareasAPedidoDelPM { get; set; }
        public int Fusiones_SoloInterior { get; set; }
        public int FreezingDeRed { get; set; }
        public int AdecuacionDeRed { get; set; }
        public int RadioBase_ProblemaDeIngreso { get; set; }
        public int Permiso_DemoraExtraordinaria { get; set; }

        public int Total => Vantive_MalCargadaLaDireccion +
                            Vantive_FaltaDeContacto +
                            Vantive_MalTipificada +
                            Factibilidad_MalFactibilizado +
                            Factibilidad_CambioEnLasCondiciones +
                            Relevamiento_ProblemaDeContacto +
                            Relevamiento_ProblemaDeIngreso +
                            Relevamiento_SitioEnConstruccion +
                            InicioDePermisos_FaltaDeAutorizacionDelCliente +
                            InicioDePermisos_FaltaDeAutorizacionPorMotivosInternos +
                            ObraCivil_SitioEnConstruccion +
                            ObraCivil_ProblemaDeIngreso +
                            FaltaIngenieria +
                            FaltaAsignacion +
                            Tendido_SitioEnConstruccion +
                            Tendido_ProblemaDeIngreso +
                            Tendido_FaltaDeMateriales +
                            Odf_SitioEnConstruccion +
                            Odf_ProblemaDeIngreso +
                            Odf_FaltaDeMateriales +
                            SuspensionDeTareasAPedidoDelPM +
                            Fusiones_SoloInterior +
                            FreezingDeRed +
                            AdecuacionDeRed +
                            RadioBase_ProblemaDeIngreso +
                            Permiso_DemoraExtraordinaria;
    }

}
