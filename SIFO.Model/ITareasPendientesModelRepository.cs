using SIFO.Model;

namespace SIFO.DesktopUI.TareasIngresadas
{
    public interface ITareasPendientesModelRepository : ITareaModelRepository<ITarea>
    {
    }

    public interface ITareasPendientesInicio2017ModelRepository : ITareaModelRepository<ITarea>
    {
        
    }
}