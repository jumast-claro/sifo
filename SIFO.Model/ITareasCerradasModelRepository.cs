﻿using SIFO.Model;

namespace SIFO.DesktopUI.TareasIngresadas
{
    public interface ITareasCerradasModelRepository : ITareaModelRepository<ITarea>
    {
        
    }
}