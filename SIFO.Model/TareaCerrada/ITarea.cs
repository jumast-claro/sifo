using System;

namespace SIFO.Model
{

    public class FechaDeCompromiso : IFecha
    {
        private readonly DateTime _fecha;

        public FechaDeCompromiso(DateTime fecha)
        {
            _fecha = fecha;
        }

        public DateTime? Fecha => _fecha;
        public bool IsNull => false;
        public int Mes => Fecha.Value.Month;
    }

    public class NullFechaDeCompromiso : IFecha
    {
        public DateTime? Fecha => null;
        public bool IsNull => true;
        public int Mes => 0;
    }

    public interface IFecha
    {
        DateTime? Fecha { get; }
        bool IsNull { get; }
        int Mes { get; }
       
    }


    public class NullFechaDeCierre : IFecha
    {
        public DateTime? Fecha => null;
        public bool IsNull => true;
        public int Mes => 0;
    }

    public class FechaDeCierre : IFecha
    {
        private readonly DateTime _fechaDeCierre;

        public FechaDeCierre(DateTime fechaDeCierre)
        {
            _fechaDeCierre = fechaDeCierre;
        }

        public DateTime? Fecha => _fechaDeCierre;
        public int Mes => _fechaDeCierre.Month;
        public bool IsNull => false;
    }

    public interface ITarea
    {
        bool TieneObraCivil { get; set; }
        double TiempoEnOtrasBandejas { get; set; }

        IFecha FechaCierreTarea { get; }

        double TiempoEfectivoPermanenciaTareaEnEstaBandeja { get; }
        bool OnTimeInbox { get; }
        bool OnTimeGeneral { get; }

        // Leidas
        Zona Zona { get; set; }
        string Inbox { get; }
        string InboxFinal { get; set; }
        string TipoDeOrden { get; }
        string TipoDeMovimiento { get; set; }
        string Tecnologia { get; set; }
        string TipoDeFibraOptica { get; set; }
        string Producto { get; set; }
        string TipoSolucion { get; }
        DateTime? FechaInicioOrden { get; set; }
        DateTime? FechaCompromisoOrden { get; set; }
        DateTime? FechaRenegociada { get; set; }
        DateTime FechaInicioTarea { get; }
        DateTime? FechaCierreTaraeAnterior { get; set; }
        Estado Estado { get; set; }
        string TomadoPor { get; set; }
        string Cliente { get; set; }
        long Orden { get; set; }
        long Enlace { get; set; }
        string TareaId { get; set; }
        string Identificador { get; set; }
        DateTime UltimaNotaFecha { get; set; }
        string UltimaNotaAutor { get; set; }
        string UltimaNota { get; set; }
        DateTime? FechaInicioWorkflow { get; set; }
        DateTime? FechaFinWorkflow { get; set; }
        string Provincia { get; }
        string Partido { get; set; }
        string Localidad { get; }
        string Calle { get; set; }
        string Numero { get; set; }

        // Calculadas
        string CodigoTipificacion { get; }
        int? DiasDeInstalacion { get; }
        string PseudoEnlace { get; set; }
        bool EsAltaPura { get; }
        bool EsPlantaExterna { get; }
        bool EsPlantaInterna { get; }
        bool EsProyectoEspecial { get; }
        DateTime? FechaDeInstalacion { get; }
        bool IngresaOffTime { get; }
        Planta Planta { get; }
        string RegionFinal { get; set; }
        TecnologiaEnum TecnologiaBis { get; }
        int TiempoMaximoPermanenciaOrdenEnOtrasBandejas { get; }
        DateTime? FechaCompromisoTarea { get; }
    }
}