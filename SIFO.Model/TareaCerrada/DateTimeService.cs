using System;
using System.Globalization;

namespace SIFO.Model
{
    public class DateTimeService
    {

        public static DateTime MAX = new DateTime(9999, 12, 31);
        public static DateTime MIN = new DateTime(0001, 1, 1);

        public static bool IsMax(DateTime dateTime)
        {
            return DateTime.Compare(dateTime, MAX) == 0;
        }

        public static bool IsMin(DateTime dateTime)
        {
            return DateTime.Compare(dateTime, MIN) == 0;
        }


        public static bool IsNotMax(DateTime dateTime)
        {
            return !IsMax(dateTime);
        }

        public static bool AreEqual(DateTime date1, DateTime date2)
        {

            var ans = date1.Ticks == date2.Ticks;
            if (!ans)
            {
                var x = 3;
            }
            return ans;
        }

        public static string ToStringRepresentation(DateTime dateTime, string format, CultureInfo cultureInfo)
        {
            if (IsMax(dateTime) || IsMin(dateTime))
            {
                return "-";
            }
            return dateTime.ToString(format, cultureInfo);
        }

        public static string ToStringRepresentation(DateTime dateTime, string format)
        {
            if (IsMax(dateTime) || IsMin(dateTime))
            {
                return "-";
            }
            return dateTime.ToString(format, CultureInfo.InvariantCulture);
        }

    }
}