﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIFO.Model
{
    public class Interrupcion
    {
        public Interrupcion(string motivo, double tiempoEnDias)
        {
            Motivo = motivo;
            TiempoEnDias = tiempoEnDias;
        }

        public string Motivo { get; }
        public double TiempoEnDias { get; }
    }

    public class Interrupciones
    {
        private readonly List<Interrupcion> _interrupciones = new List<Interrupcion>();

        public void Add(Interrupcion interrupcion)
        {
           _interrupciones.Add(interrupcion); 
        }

        public double TotalEnDias
        {
            get { return _interrupciones.Sum(interrupcion => interrupcion.TiempoEnDias); }
        }
    }
}
