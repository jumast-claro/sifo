﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIFO.Model
{
    public abstract class Tarea : ITarea
    {
        public bool TieneObraCivil { get; set; } = false;
        public double TiempoEnOtrasBandejas { get; set; } = 0;

        private readonly IFecha _fechaDeCierre;

        public IFecha FechaCierreTarea => _fechaDeCierre;

        public double TiempoEfectivoPermanenciaTareaEnEstaBandeja
        {
            get
            {
                if (FechaCierreTarea.IsNull)
                {
                    return (DateTime.Today - FechaInicioTarea).TotalDays;
                }
                return (FechaCierreTarea.Fecha.Value - FechaInicioTarea).TotalDays;
            }
        }


        public abstract bool OnTimeInbox { get; }

        public abstract bool OnTimeGeneral { get; }





        // --------------------------------------------------------------------
        // Campos
        // --------------------------------------------------------------------
        private readonly IInbox _inbox;
        private readonly ProyectoEspecialStrategy _proyectoEspecialStrategy;
        private readonly TecnologiaStrategy _tecnologiaStrategy;
        private readonly string _tareaId;
        private readonly IOrden _orden;

        private string _cliente;

        // --------------------------------------------------------------------
        // Constructores
        // --------------------------------------------------------------------
        public Tarea(IOrden orden, IInbox inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea)
        {
            _orden = orden;
            FechaInicioTarea = fechaInicioTarea;
            //FechaCierreTarea = fechaCierreTarea;

            _proyectoEspecialStrategy = new ProyectoEspecialStrategy();
            _tecnologiaStrategy = new TecnologiaStrategy();
            _inbox = inbox;

            //_fechaDeCierre = new FechaDeCierre(fechaCierreTarea);
            _fechaDeCierre = fechaCierreTarea;
        }


        // --------------------------------------------------------------------
        // Propiedades
        // --------------------------------------------------------------------
        public Estado Estado { get; set; }
        public string TareaId { get; set; } = "";
        public Zona Zona { get; set; }
        public string Inbox => _inbox.Nombre;
        public string TipoDeOrden => _orden.TipoDeOrden;
        public string TipoDeMovimiento { get; set; }
        public string Tecnologia { get; set; }
        public string TipoDeFibraOptica { get; set; }
        public bool EsAltaPura => _orden.EsAltaPura;
        public DateTime FechaInicioTarea { get; }
        //public DateTime FechaCierreTarea { get; }
        public DateTime? FechaCierreTaraeAnterior { get; set; }
        public Planta Planta => _inbox.Planta;
       
        public string TomadoPor { get; set; } = "";

        public string Cliente
        {
            get
            {
                return _cliente;
            }
            set { _cliente = value; }
        }

        public bool EsPlantaExterna => Planta == Planta.PlantaExterna;
        public bool EsPlantaInterna => Planta == Planta.PlantaInterna;
        public bool EsProyectoEspecial => _proyectoEspecialStrategy.EsProyectoEspecial(Cliente); 
        public TecnologiaEnum TecnologiaBis => _tecnologiaStrategy.Tencnologia(InboxFinal); 


        #region IOrden
        public long Orden
        {
            get { return _orden.NumeroOrden; }
            set { _orden.NumeroOrden = value; }
        }

        public long Enlace
        {
            get { return _orden.NumeroEnlace; }
            set { _orden.NumeroEnlace = value; }
        }

        public string PseudoEnlace 
        {
            get { return _orden.PseudoEnlace; }
            set { _orden.PseudoEnlace = value; }
        }

        public DateTime? FechaInicioOrden
        {
            get { return _orden.FechaInicio; }
            set { _orden.FechaInicio = value; }
        }
        public DateTime? FechaCompromisoOrden
        {
            get { return _orden.FechaCompromiso; }
            set { _orden.FechaCompromiso = value; }
        }
        public DateTime? FechaRenegociada
        {
            get { return _orden.FechaRenegociada; }
            set { _orden.FechaRenegociada = value; }
        }
        public DateTime? FechaDeInstalacion => _orden.FechaDeInstalacion;

        public int? DiasDeInstalacion => _orden.DiasDeInstalacion;

        public string Provincia { get; }
        public string Partido { get; set; }
        public string Localidad { get; }
        public string Calle { get; set; }
        public string Numero { get; set; }
        public string UltimaNota { get; set; }
        public DateTime? FechaInicioWorkflow { get; set; }
        public DateTime? FechaFinWorkflow { get; set; }
        public string UltimaNotaAutor { get; set; }
        public DateTime UltimaNotaFecha { get; set; }
        #endregion

        #region InfoTarea
        public string RegionFinal { get; set; } = ""; 
        public string InboxFinal { get; set; } = ""; 
        public string Producto { get; set; } = ""; 
        public string TipoSolucion => _orden.TipoDeSolucion.Descripcion; 
        public string CodigoTipificacion => _orden.TipoDeSolucion.Codigo; 
        public string Identificador { get; set; }
        #endregion

        public bool IngresaOffTime => FechaInicioTarea > FechaCompromisoOrden;

     
       

        /// <summary>
        /// Devuelve el tiempo, en días, que estuvo la tarea en la bandeja.
        /// </summary>

        public double TiempoPermanenciaAnterior
        {
            get
            {
                if (FechaInicioOrden.HasValue && FechaCierreTaraeAnterior.HasValue)
                {
                    var ans = (FechaCierreTaraeAnterior.Value - FechaInicioOrden.Value).TotalDays;
                    return ans;
                }
                return 0;
            }
        }


        public int TiempoMaximoPermanenciaOrdenEnOtrasBandejas
        {
            get
            {
                switch (Planta)
                {
                    case Planta.PlantaInterna:
                        return 6;
                    case Planta.PlantaExterna:
                        return 14;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(Planta), Planta, null);
                }
            }

        }

        public DateTime? FechaCompromisoTarea
        {
            get
            {
                if (!FechaDeInstalacion.HasValue)
                {
                    return null;
                }
                return FechaDeInstalacion.Value.AddDays(-1*TiempoMaximoPermanenciaOrdenEnOtrasBandejas);
            }
        }

    }
}
