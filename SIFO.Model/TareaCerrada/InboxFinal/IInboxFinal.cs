﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIFO.Model
{
    interface IInboxFinal
    {
        TecnologiaEnum Tecnologia { get; } 
    }

    public class InstalacionesFO_Final : IInboxFinal
    {
        public TecnologiaEnum Tecnologia => TecnologiaEnum.FibraOptica;
    }

    public class InstalacionesFoGpon : IInboxFinal
    {
        public TecnologiaEnum Tecnologia => TecnologiaEnum.FibraOptica;
    }

    public class InstalacionesFoGponTigre : IInboxFinal
    {
        public TecnologiaEnum Tecnologia => TecnologiaEnum.FibraOptica;
    }

}
