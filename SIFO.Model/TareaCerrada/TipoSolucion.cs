using System;
using System.Collections.Generic;

namespace SIFO.Model
{

    public interface ITipoSolucion
    {
        string Codigo { get; }
        string Descripcion { get; }
        bool DefineDiasDeInstalacion { get;  }
        int? DiasDeInstalacion { get; }
    }

    public class TipoA : ITipoSolucion
    {
        public string Codigo => "A";
        public string Descripcion => "A - Edificio no acometido <200 mts (sin cruces de calle) (60 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 60;
    }

    public class TipoB : ITipoSolucion
    {
        public string Codigo => "B";
        public string Descripcion => "B - Edificio on net no acometido (45 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 45;
    }

    public class TipoC : ITipoSolucion
    {
        public string Codigo => "C";
        public string Descripcion => "C - Edificio Acometido (35 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 35;
    }

    public class TipoCViejo : ITipoSolucion
    {
        public string Codigo => "C";
        public string Descripcion => "C - Edificio acometido sin clientes en servicio (30 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 30;
    }

    public class TipoD : ITipoSolucion
    {
        public string Codigo => "D";
        public string Descripcion => "D - Edificio acometido con servicio en otro cliente (25 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 25;
    }

    public class TipoE : ITipoSolucion
    {
        public string Codigo => "E";
        public string Descripcion => "E - Edificio acometido con servicio en el mismo cliente (20 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 20;
    }

    public class TipoF : ITipoSolucion
    {
        public string Codigo => "F";
        public string Descripcion => "F - LMDS (con LV y BW OK) (60 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 60;
    }

    public class TipoJ : ITipoSolucion
    {
        public string Codigo => "J";
        public string Descripcion => "J - Ultima Milla TASA (70 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 70;
    }

    public class TipoK : ITipoSolucion
    {
        public string Codigo => "K";
        public string Descripcion => "K - Ultima Milla TECO (70 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 70;
    }

    public class TipoL : ITipoSolucion
    {
        public string Codigo => "L";
        public string Descripcion => "L - Producto CGIP (65 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 65;
    }

    public class TipoN : ITipoSolucion
    {
        public string Codigo => "N";
        public string Descripcion => "N - Wimax (20 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 20;
    }

    public class TipoP : ITipoSolucion
    {
        public string Codigo => "P";
        public string Descripcion => "P - Producto Trama Ip (65 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 65;
    }

    public class TipoQ : ITipoSolucion
    {
        public string Codigo => "Q";
        public string Descripcion => "Q - Producto L�neas Telefon�a Telmex (7 D�as + Ultima milla)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 7;
    }

    public class TipoQBis : ITipoSolucion
    {
        public string Codigo => "Q";
        public string Descripcion => "Q - Producto L�neas Telefon�a Telmex (7 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 7;
    }

    public class TipoS : ITipoSolucion
    {
        public string Codigo => "S";
        public string Descripcion => "S - G-PON A�reo (30 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 30;
    }

    public class TipoT : ITipoSolucion
    {
        public string Codigo => "T";
        public string Descripcion => "T - Movil (30 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 30;
    }

    public class TipoU : ITipoSolucion
    {
        public string Codigo => "U";
        public string Descripcion => "U - LMDS-NG (con LV y BW OK) (30 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 30;
    }

    public class TipoWZ : ITipoSolucion
    {
        public string Codigo => "WZ";
        public string Descripcion => "WZ";
        public bool DefineDiasDeInstalacion => false;
        public int? DiasDeInstalacion => null;
    }

    public class TipoY : ITipoSolucion
    {
        public string Codigo => "Y";
        public string Descripcion => "Y - Radio PaP sin infraestructura adicional (90 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 90;
    }

    public class TipoZ : ITipoSolucion
    {
        public string Codigo => "Z";
        public string Descripcion => "Z - Producto No Estandar ( D�as)";
        public bool DefineDiasDeInstalacion => false;
        public int? DiasDeInstalacion => null;
    }

    public class TipoZD : ITipoSolucion
    {
        public string Codigo => "ZD";
        public string Descripcion => "ZD - SIN USO (35 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 35;
    }

    public class TipoZE : ITipoSolucion
    {
        public string Codigo => "ZE";
        public string Descripcion => "ZE - SIN USO (35 D�as)";
        public bool DefineDiasDeInstalacion => true;
        public int? DiasDeInstalacion => 35;
    }

    public class SinTipoSolucion : ITipoSolucion
    {
        public string Codigo => "";
        public string Descripcion => "";
        public bool DefineDiasDeInstalacion => false;
        public int? DiasDeInstalacion => null;
    }

    public class OtroTipoDeSolucion : ITipoSolucion
    {
        public string Codigo => "Otro";
        public string Descripcion => "";
        public bool DefineDiasDeInstalacion => false;
        public int? DiasDeInstalacion => null;
    }

    public class TipoSolucionFactory
    {
        public ITipoSolucion CrearTipoDeSolucion(string descripcion)
        {
            if (descripcion == "")
            {
                return new SinTipoSolucion();
            }


            var primeraLetra = descripcion[0];
            switch (primeraLetra)
            {
                case 'A':
                    return new TipoA();
                case 'B':
                    return new TipoB();
                case 'C':
                    if (descripcion.Contains("Edificio Acometido"))
                    {
                        return new TipoC();
                    }
                    if (descripcion.Contains("Edificio acometido sin clientes en servicio"))
                    {
                        return new TipoCViejo();
                    }
                    break;
                case 'D':
                    return new TipoD();
                case 'E':
                    return new TipoE();
                case 'F':
                    return new TipoF();
                case 'J':
                    return new TipoJ();
                case 'K':
                    return new TipoK();
                case 'L':
                    return new TipoL();
                case 'N':
                    return new TipoN();
                case 'P':
                    return new TipoP();
                case 'Q':
                    return new TipoQ();
                case 'S':
                    return new TipoS();
                case 'T':
                    return new TipoT();
                case 'U':
                    return new TipoU();
                case 'W':
                    if (descripcion == "WZ")
                    {
                        return new TipoWZ();
                    }
                    break;
                case 'Y':
                    return new TipoY();
                case 'Z':
                    if (descripcion.Contains("Z - Producto No Estandar"))
                    {
                        return new TipoZ();
                    }
                    if (descripcion.Contains("ZD - SIN USO"))
                    {
                        return new TipoZD();
                    }
                    if (descripcion.Contains("ZE - SIN USO"))
                    {
                        return new TipoZE();
                    }
                    break;
                default:
                    return new OtroTipoDeSolucion();
            }

            throw new ArgumentException("El c�digo de la tipificaci�n es incorrecto.");
        }
    }
}