﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIFO.Model
{

    public class TareaFactory
    {

        public ITarea Create(string inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea, DateTime? fechaInicioOrden, DateTime? fechaCompromisoOrden, string tipificacion, long enlace, string identificador, string tipoDeOrden, string partido)
        {
            var tipoSolucion = new TipoSolucionFactory().CrearTipoDeSolucion(tipificacion);

            // inicio
            DateTime? temp_fechaInicioOrden;
            if (fechaInicioOrden.HasValue)
            {
                temp_fechaInicioOrden = fechaInicioOrden;
            }
            else
            {
                temp_fechaInicioOrden = inferirFechaInicioOrden(fechaInicioTarea, fechaCompromisoOrden, tipoSolucion);
            }

            // compromiso
            //DateTime temp_fechaCompromisoOrden = inferirFechaCompromisoOrden(fechaCompromisoOrden);
            DateTime? temp_fechaCompromisoOrden = fechaCompromisoOrden;

            if (fechaCompromisoOrden == null)
            {
                var x = 5;
            }

            // enlace
            long temp_enlace;
            if (enlace != 0 && enlace.ToString().Length == 7)
            {
                temp_enlace = enlace;
            }
            else
            {
                temp_enlace = inferirEnlace(identificador);
            }

            var temp_tipoDeOrden = new TipoDeOrdenFactory().CrearTipoDeOrden(tipoDeOrden);

            var tmp_inbox = new InboxFactory().CrearInbox(inbox);

            IOrden orden = new Orden(0, temp_enlace, temp_fechaInicioOrden, temp_fechaCompromisoOrden, tipoSolucion, temp_tipoDeOrden);


            var theInbox = inbox;

            switch (theInbox)
            {
                case "ABM":
                    return new TareaABM(orden, tmp_inbox, fechaInicioTarea, fechaCierreTarea) {Identificador = identificador, Partido = partido};
                case "Administracion FO":
                    return new TareaAdministracionFo(orden, tmp_inbox, fechaInicioTarea, fechaCierreTarea) { Identificador = identificador, Partido = partido };
                case "Gestión de Ingresos":
                    return new TareaGestionIngresos(orden, tmp_inbox, fechaInicioTarea, fechaCierreTarea) { Identificador = identificador, Partido = partido };
                case "Ingenieria de FO":
                    return new TareaIngenieriaDeFo(orden, tmp_inbox, fechaInicioTarea, fechaCierreTarea) { Identificador = identificador, Partido = partido };
                case "Inserciones AMBA":
                    return new TareaInsercionesAmba(orden, tmp_inbox, fechaInicioTarea, fechaCierreTarea) { Identificador = identificador, Partido = partido };
                case "Inserciones Interior":
                    return new TareaInsercionesInterior(orden, tmp_inbox, fechaInicioTarea, fechaCierreTarea) { Identificador = identificador, Partido = partido };
                case "Instalacion GPON":
                    return new TareaInstalacionGpon(orden, tmp_inbox, fechaInicioTarea, fechaCierreTarea) { Identificador = identificador, Partido = partido };
                case "Instalaciones FO":
                    return new TareaInstalacionesFo(orden, tmp_inbox, fechaInicioTarea, fechaCierreTarea) { Identificador = identificador, Partido = partido };
                case "Instalaciones satel.":
                    return new TareaInstalacionesSatel(orden, tmp_inbox, fechaInicioTarea, fechaCierreTarea) { Identificador = identificador, Partido = partido };
                case "Obra Civil Tend AMBA":
                    return new TareaObraCivilTendAmba(orden, tmp_inbox, fechaInicioTarea, fechaCierreTarea) { Identificador = identificador, Partido = partido };
                case "Obra Civil Tend INTE":
                    return new TareaObraCivilTendInterior(orden, tmp_inbox, fechaInicioTarea, fechaCierreTarea) { Identificador = identificador, Partido = partido };
                case "Operac. - Start Up":
                    return new TareaOperacStartUp(orden, tmp_inbox, fechaInicioTarea, fechaCierreTarea) { Identificador = identificador, Partido = partido };
                case "Ops - Instalaciones":
                    return new TareaOpsInstalaciones(orden, tmp_inbox, fechaInicioTarea, fechaCierreTarea) { Identificador = identificador, Partido = partido };
                case "Permisos Municipales":
                    return new TareaPermisosMunicipales(orden, tmp_inbox, fechaInicioTarea, fechaCierreTarea) { Identificador = identificador, Partido = partido };
                case "Radiobases":
                    return new TareaRadioBases(orden, tmp_inbox, fechaInicioTarea, fechaCierreTarea) { Identificador = identificador, Partido = partido };
                case "Start UP - FO":
                    return new TareaStartUpFo(orden, tmp_inbox, fechaInicioTarea, fechaCierreTarea) { Identificador = identificador, Partido = partido };
                default:
                    throw new ArgumentException();
            }


            //var tareaCerrada = new TareaCerrada(orden, tmp_inbox, fechaInicioTarea, fechaCierreTarea) { Identificador = identificador };
            //tareaCerrada.Partido = partido;
            //return tareaCerrada;
        }

        /// <summary>
        /// Infiere la fecha de inicio de la orden, a partir de la fecha de inicio de la tarea y la fecha de compromiso
        /// de la orden. Puede ser que la orden no tenga fecha de compromiso; en ese caso, no se puede inferir la fecha 
        /// de inicio. 
        /// Si la fecha de inicio de la tarea es menor que la fecha de compromiso de la orden, se asume que la fecha de
        /// inicio de la orden es igual a la fecha de inicio de la tarea.Es decir, si la tarea no ingresa vencida, se
        /// asume que es la primera tarea asociada a la orden.
        /// </summary>
        private DateTime? inferirFechaInicioOrden(DateTime fechaInicioTarea, DateTime? fechaCompromisoOrden, ITipoSolucion tipoDeSolucion)
        {
            return null;

            //if (fechaCompromisoOrden == null)
            //{
            //    return null;
            //}
            //if (tipoDeSolucion.DiasDeInstalacion.HasValue)
            //{

            //    var posibleFechaDeInicio = fechaCompromisoOrden.Value.AddDays(-1 * tipoDeSolucion.DiasDeInstalacion.Value);
            //    if (posibleFechaDeInicio <= fechaInicioTarea)
            //    {
            //        return posibleFechaDeInicio;
            //    }
            //    return null;
            //}

            //if (fechaInicioTarea < fechaCompromisoOrden && (fechaCompromisoOrden.Value - fechaInicioTarea).Days >= 60)
            //{
            //    return fechaInicioTarea;
            //}
            //return null;
        }

        /// <summary>
        /// Si se pasa por parámetro un fecha, devuelve esa fecha, si no, asume
        /// que lo orden no tiene fecha de compromiso y devuelve DateTime.Max.
        /// </summary>
        //private DateTime inferirFechaCompromisoOrden(DateTime? fechaCompromisoOrden)
        //{
        //    if (fechaCompromisoOrden != null)
        //    {
        //        return (DateTime)fechaCompromisoOrden;
        //    }
        //    else
        //    {
        //        return DateTimeService.MAX;
        //    }
        //}

        /// <summary>
        /// Infiere el enlace a partir del identificador.
        /// </summary>
        private long inferirEnlace(string identificador)
        {
            if (identificador == "")
            {
                return 0;
            }
            else
            {
                var indiceDosPuntos = identificador.IndexOf(':');
                if (indiceDosPuntos < 0) return 0;

                var indicePrimerDigito = indiceDosPuntos + 2;
                var indicePrimerEspacio = identificador.IndexOf(' ', indicePrimerDigito);
                var longitudEnlace = indicePrimerEspacio - indicePrimerDigito;

                if (longitudEnlace == 7)
                {
                    var posibleEnlace = identificador.Substring(indicePrimerDigito, longitudEnlace + 1);
                    var enlace = int.Parse(posibleEnlace);
                    return enlace;
                }
                else
                {
                    return 0;
                }
            }
        }
    }

    

}
