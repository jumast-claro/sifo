using System;
using System.Collections.Generic;

namespace SIFO.Model
{
    public class ProyectoEspecialStrategy
    {
        private readonly HashSet<string> _proyectosEspecialesSegunCliente = new HashSet<string>()
        {
            "MUNICIPALIDAD DE USHUAIA",
            "SERV ADMIN FINAN DE LA GOBERNA",
            "TELMEX ARGENTINA S.A.",
            "UNIVERSIDAD TECNOLOGICA N."
        };

        public bool EsProyectoEspecial(string cliente)
        {
            return _proyectosEspecialesSegunCliente.Contains(cliente);
        }
    }

    public class TecnologiaStrategy
    {
        public TecnologiaEnum Tencnologia(string inboxFinal)
        {
            switch (inboxFinal)
            {
                case "Instalaciones FO":
                    return TecnologiaEnum.FibraOptica;
                case "Instalaciones FO GPON":
                    return TecnologiaEnum.FibraOptica;
                case "Instalaciones FO GPON Tigre":
                    return TecnologiaEnum.FibraOptica;
                case "Instalaciones satel.":
                    return TecnologiaEnum.Inalambrico;
                case "Operac. - Start Up":
                    return TecnologiaEnum.NoAplica;
                case "Ops - Instalaciones":
                    return TecnologiaEnum.Inalambrico;
                case "Ops - Instalaciones GPON":
                    return TecnologiaEnum.FibraOptica;
                case "Ops - Instalaciones HFC":
                    return TecnologiaEnum.Inalambrico;
                case "Ops - Instalaciones WiMax":
                    return TecnologiaEnum.Inalambrico;
                case "Radiobases":
                    return TecnologiaEnum.Inalambrico;
                case "Start UP - FO":
                    return TecnologiaEnum.NoAplica;
                default:
                    return TecnologiaEnum.NoAplica;
            }
        }
    }
}