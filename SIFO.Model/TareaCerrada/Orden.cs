﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIFO.Model
{

    public interface ITipoDeOrden
    {
        string TipoDeOrden { get; }
        bool EsAltaPura { get; }
    }

    public class Alta : ITipoDeOrden
    {
        public string TipoDeOrden => "Alta";
        public bool EsAltaPura => true;
    }

    public class UgMtOtiSyR : ITipoDeOrden
    {
        public string TipoDeOrden => "UG/MT/OTI/SyR";
        public bool EsAltaPura => false;
    }

    public class Optimizacion : ITipoDeOrden
    {
        public string TipoDeOrden => "Optimización";
        public bool EsAltaPura => false;
    }

    public class TipoDeOrdenFactory
    {
        public ITipoDeOrden CrearTipoDeOrden(string tipoDeOrden)
        {
            switch (tipoDeOrden)
            {
                case "Alta":
                    return new Alta();
                case "UG/MT/OTI/SyR":
                    return new UgMtOtiSyR();
                case "Optimizacion":
                    return new Optimizacion();
                default:
                    throw new ArgumentException();
            }
        }
    }

    public interface IOrden
    {
        long NumeroOrden { get; set; }
        long NumeroEnlace { get; set; }
        string PseudoEnlace { get; set; }
        DateTime? FechaInicio { get; set; }
        DateTime? FechaCompromiso { get; set; }
        DateTime? FechaRenegociada { get; set; }
        DateTime? FechaDeInstalacion { get; }
        int? DiasDeInstalacion { get; }

        ITipoSolucion TipoDeSolucion { get; }

        bool EsAltaPura { get; }
        string TipoDeOrden { get; }

        
    }


    public class Orden : IOrden
    {
        // --------------------------------------------------------------------
        // Campos
        // --------------------------------------------------------------------
        private long _numeroOrden = 0;
        private long _numeroEnlace = 0;
        private DateTime? _fechaInicio;
        private DateTime? _fechaCompromiso;
        private DateTime? _fechaRenegociada;
        private readonly ITipoSolucion _tipoDeSolucion;
        private readonly ITipoDeOrden _tipoDeOrden;

        private string _pseudoEnlace;

        private int? diasDeInstalacionSegunFechas
        {
            get
            {
                if (FechaInicio.HasValue && FechaDeInstalacion.HasValue)
                {
                    return (FechaDeInstalacion.Value - FechaInicio.Value).Days;
                }
                return null;
            }
        }

        // --------------------------------------------------------------------
        // Constructor
        // --------------------------------------------------------------------
        public Orden(long numeroOrden, long numeroEnlace, DateTime? fechaInicio, DateTime? fechaCompromiso, ITipoSolucion tipoDeSolucion, ITipoDeOrden tipoDeOrden)
        {
            _numeroOrden = numeroOrden;
            _numeroEnlace = numeroEnlace;
            _fechaInicio = fechaInicio;
            _fechaCompromiso = fechaCompromiso;
            _tipoDeSolucion = tipoDeSolucion;
            _tipoDeOrden = tipoDeOrden;
            //_pseudoEnlace = pseudoEnlace;
        }

        // --------------------------------------------------------------------
        // Propiedades
        // --------------------------------------------------------------------

        public int? DiasDeInstalacion
        {
            get
            {
                // La fecha de renegociada tiene prioridad sobre la tipifiación.
                if (FechaRenegociada.HasValue)
                {
                    return diasDeInstalacionSegunFechas;
                }

                if (_tipoDeSolucion.DefineDiasDeInstalacion)
                {
                    return _tipoDeSolucion.DiasDeInstalacion;
                }
                if (diasDeInstalacionSegunFechas.HasValue)
                {
                    return diasDeInstalacionSegunFechas.Value;
                }
                return null;
            }
        }

        public DateTime? FechaDeInstalacion
        {
            get
            {
                if (FechaRenegociada.HasValue)
                {
                    return FechaRenegociada.Value;
                }
                if (FechaCompromiso.HasValue)
                {
                    return FechaCompromiso;
                }
                return null;
            }
        }

        public DateTime? FechaRenegociada
        {
            get { return _fechaRenegociada; }
            set { _fechaRenegociada = value; }
        }

        public DateTime? FechaCompromiso
        {
            get { return _fechaCompromiso; }
            set { _fechaCompromiso = value; }
        }

        public DateTime? FechaInicio
        {
            get { return _fechaInicio; }
            set { _fechaInicio = value; }
        }

        public long NumeroOrden
        {
            get { return _numeroOrden; }
            set { _numeroOrden = value; }
        }

        public long NumeroEnlace
        {
            get { return _numeroEnlace; }
            set { _numeroEnlace = value; }
        }

        public string PseudoEnlace
        {
            get { return _pseudoEnlace; }
            set { _pseudoEnlace = value; }
        }


        public bool EsAltaPura => _tipoDeOrden.EsAltaPura;
        public string TipoDeOrden => _tipoDeOrden.TipoDeOrden;

        public ITipoSolucion TipoDeSolucion => _tipoDeSolucion;

    }
}
