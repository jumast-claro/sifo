﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using SIFO.DataAcces.TareasCerradas.Repositories;
using SIFO.DesktopUI.TareasIngresadas;
using SIFO.Model;

namespace SIFO.Model
{

    public class TareaABM : Tarea
    {
        public TareaABM(IOrden orden, IInbox inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea) : base(orden, inbox, fechaInicioTarea, fechaCierreTarea)
        {
            
        }
        public override bool OnTimeInbox => TiempoEfectivoPermanenciaTareaEnEstaBandeja <= 6;
        public override bool OnTimeGeneral => false;

    }

    public class TareaAdministracionFo : Tarea
    {
        public TareaAdministracionFo(IOrden orden, IInbox inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea) : base(orden, inbox, fechaInicioTarea, fechaCierreTarea)
        {
        }

        public override bool OnTimeInbox => TiempoEfectivoPermanenciaTareaEnEstaBandeja <= 5;
        public override bool OnTimeGeneral => false;
    }

    public class TareaGestionIngresos : Tarea
    {
        public TareaGestionIngresos(IOrden orden, IInbox inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea) : base(orden, inbox, fechaInicioTarea, fechaCierreTarea)
        {
        }

        public override bool OnTimeInbox => TiempoEfectivoPermanenciaTareaEnEstaBandeja <= 20;
        public override bool OnTimeGeneral => false;
    }

    public class TareaIngenieriaDeFo : Tarea
    {
        public TareaIngenieriaDeFo(IOrden orden, IInbox inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea) : base(orden, inbox, fechaInicioTarea, fechaCierreTarea)
        {
        }

        public override bool OnTimeInbox => TiempoEfectivoPermanenciaTareaEnEstaBandeja <= 3;
        public override bool OnTimeGeneral => false;
    }

    public class TareaInsercionesAmba : Tarea
    {

      

        public TareaInsercionesAmba(IOrden orden, IInbox inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea) : base(orden, inbox, fechaInicioTarea, fechaCierreTarea)
        {

        }

        public override bool OnTimeInbox => TiempoEfectivoPermanenciaTareaEnEstaBandeja <= 5;
        public override bool OnTimeGeneral => false;
    }

    public class TareaInsercionesInterior : Tarea
    {
        public TareaInsercionesInterior(IOrden orden, IInbox inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea) : base(orden, inbox, fechaInicioTarea, fechaCierreTarea)
        {
        }

        public override bool OnTimeInbox => TiempoEfectivoPermanenciaTareaEnEstaBandeja <= 5;
        public override bool OnTimeGeneral => false;
    }

    public class TareaInstalacionGpon : Tarea
    {
        public TareaInstalacionGpon(IOrden orden, IInbox inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea) : base(orden, inbox, fechaInicioTarea, fechaCierreTarea)
        {
        }

        public override bool OnTimeInbox => TiempoEfectivoPermanenciaTareaEnEstaBandeja <= 7;
        public override bool OnTimeGeneral => false;
    }


    public class TareaInstalacionesFo : Tarea
    {
        public TareaInstalacionesFo(IOrden orden, IInbox inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea) : base(orden, inbox, fechaInicioTarea, fechaCierreTarea)
        {
        }

        public override bool OnTimeInbox => false;
        public override bool OnTimeGeneral => false;
    }


    public class TareaInstalacionesSatel : Tarea
    {
        public TareaInstalacionesSatel(IOrden orden, IInbox inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea) : base(orden, inbox, fechaInicioTarea, fechaCierreTarea)
        {
        }

        public override bool OnTimeInbox => TiempoEfectivoPermanenciaTareaEnEstaBandeja <= 60;
        public override bool OnTimeGeneral => false;
    }


    public class TareaObraCivilTendAmba : Tarea
    {

        public TareaObraCivilTendAmba(IOrden orden, IInbox inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea) : base(orden, inbox, fechaInicioTarea, fechaCierreTarea)
        {
        }

        //public override bool OnTimeInbox => TiempoEfectivoPermanenciaTareaEnEstaBandeja <= 21;

        public override bool OnTimeInbox
        {
            get
            {
                double permanenciaReal = TiempoEfectivoPermanenciaTareaEnEstaBandeja - TiempoEnOtrasBandejas;
                double permanenciaPermitida = 21;
                if (TieneObraCivil)
                {
                    permanenciaPermitida += 15;
                }

                return permanenciaReal <= permanenciaPermitida;
            }
        }
        public override bool OnTimeGeneral => false;
    }

    public class TareaObraCivilTendInterior : Tarea
    {
        public TareaObraCivilTendInterior(IOrden orden, IInbox inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea) : base(orden, inbox, fechaInicioTarea, fechaCierreTarea)
        {
        }

        public override bool OnTimeInbox
        {
            get
            {
                double permanenciaReal = TiempoEfectivoPermanenciaTareaEnEstaBandeja - TiempoEnOtrasBandejas;
                double permanenciaPermitida = 21;
                if (TieneObraCivil)
                {
                    permanenciaPermitida += 15;
                }

                return permanenciaReal <= permanenciaPermitida;
            }
        }
        public override bool OnTimeGeneral => false;
    }


    public class TareaStartUpFo : Tarea
    {
        public TareaStartUpFo(IOrden orden, IInbox inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea) : base(orden, inbox, fechaInicioTarea, fechaCierreTarea)
        {
        }

        public override bool OnTimeInbox => TiempoEfectivoPermanenciaTareaEnEstaBandeja <= 8;
        public override bool OnTimeGeneral => false;
    }

    public class TareaOperacStartUp : Tarea
    {
        public TareaOperacStartUp(IOrden orden, IInbox inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea) : base(orden, inbox, fechaInicioTarea, fechaCierreTarea)
        {
        }

        public override bool OnTimeInbox => TiempoEfectivoPermanenciaTareaEnEstaBandeja <= 8;
        public override bool OnTimeGeneral => false;
    }

    public class TareaOpsInstalaciones : Tarea
    {
        public TareaOpsInstalaciones(IOrden orden, IInbox inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea) : base(orden, inbox, fechaInicioTarea, fechaCierreTarea)
        {
        }

        public override bool OnTimeInbox => TiempoEfectivoPermanenciaTareaEnEstaBandeja <= 30;
        public override bool OnTimeGeneral => false;
    }


    public class TareaPermisosMunicipales : Tarea
    {
        public TareaPermisosMunicipales(IOrden orden, IInbox inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea) : base(orden, inbox, fechaInicioTarea, fechaCierreTarea)
        {
        }

        public override bool OnTimeInbox
        {
            get
            {
                //if (base.Partido == "CAPITAL FEDERAL" || Partido == "CAPITAL")
                //{
                //    return TiempoEfectivoPermanenciaTareaEnEstaBandeja <= 21;
                //}
                //return TiempoEfectivoPermanenciaTareaEnEstaBandeja <= 120;
                var fechaInicioOrden = base.FechaInicioOrden;
                var fechaInstalacion = base.FechaDeInstalacion;

                if (fechaInicioOrden.HasValue && fechaInstalacion.HasValue)
                {
                    var diasDeInstalacion = (fechaInstalacion.Value - fechaInicioOrden.Value).Days;
                    var diasParaPermisos = diasDeInstalacion - 36 - 14;
                    if (TiempoEfectivoPermanenciaTareaEnEstaBandeja <= diasParaPermisos)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
        }

        public override bool OnTimeGeneral => false;

    }


    public class TareaRadioBases : Tarea
    {
        public TareaRadioBases(IOrden orden, IInbox inbox, DateTime fechaInicioTarea, IFecha fechaCierreTarea) : base(orden, inbox, fechaInicioTarea, fechaCierreTarea)
        {
        }

        public override bool OnTimeInbox => TiempoEfectivoPermanenciaTareaEnEstaBandeja <= 60;
        public override bool OnTimeGeneral => false;
    }


   

    

    

   

   

    

   

  

  

   

   

  

   

}