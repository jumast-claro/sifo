using System;
using System.Collections.Generic;

namespace SIFO.Model
{

    public interface IInbox
    {
        string Nombre { get; }
        Planta Planta { get; }
    }

    public class InboxFactory
    {
        public IInbox CrearInbox(string nombreInbox)
        {
            switch (nombreInbox)
            {
                case "ABM":
                    return new ABM();
                case "Instalaciones FO":
                    return new InstalacionesFo();
                case "Instalaciones satel.":
                    return new InstalacionesSatel();
                case "Ops - Instalaciones":
                    return new OpsInstalaciones();
                case "Radiobases":
                    return new RadioBases();
                case "Start UP - FO":
                    return new StartUpFo();
                case "Operac. - Start Up":
                    return new OperacStartUp();
                case "Inserciones Interior":
                    return new InsercionesInterior();
                case "Instalacion GPON":
                    return new InstalacionGpon();
                case "Obra Civil Tend AMBA":
                    return new ObraCivilTendAmba();
                case "Obra Civil Tend INTE":
                    return new ObraCivilTendInterior();
                case "Permisos Municipales":
                    return new PermisosMunicipales();
                case "Administracion FO":
                    return new AdministracionFo();
                case "Gesti�n de Ingresos":
                    return new GestionIngresos();
                case "Ingenieria de FO":
                    return new IngenieriaDeFo();
                case "Inserciones AMBA":
                    return new InsercionesAmba();
                default:
                    throw new ArgumentException();
            }
        }
    }


    public class ABM : IInbox
    {
        public string Nombre => "ABM";
        public Planta Planta => Planta.PlantaInterna;
    }

    public class InstalacionesFo : IInbox
    {
        public string Nombre => "Instalaciones FO";
        public Planta Planta => Planta.PlantaExterna;
    }

    public class InstalacionesSatel : IInbox
    {
        public string Nombre => "Instalaciones satel.";
        public Planta Planta => Planta.PlantaExterna;
    }

    public class OpsInstalaciones : IInbox
    {
        public string Nombre => "Ops - Instalaciones";
        public Planta Planta => Planta.PlantaExterna;
    }

    public class RadioBases : IInbox
    {
        public string Nombre => "Radiobases";
        public Planta Planta => Planta.PlantaExterna;
    }

    public class StartUpFo : IInbox
    {
        public string Nombre => "Start UP - FO";
        public Planta Planta => Planta.PlantaInterna;
    }

    public class OperacStartUp : IInbox
    {
        public string Nombre => "Operac. - Start Up";
        public Planta Planta => Planta.PlantaInterna;
    }

    public class InsercionesInterior : IInbox
    {
        public string Nombre => "Inserciones Interior";
        public Planta Planta => Planta.PlantaExterna;
    }

    public class InstalacionGpon : IInbox
    {
        public string Nombre => "Instalacion GPON";
        public Planta Planta => Planta.PlantaInterna;
    }

    public class ObraCivilTendAmba : IInbox
    {
        public string Nombre => "Obra Civil Tend AMBA";
        public Planta Planta => Planta.PlantaExterna;
    }

    public class ObraCivilTendInterior : IInbox
    {
        public string Nombre => "Obra Civil Tend INTE";
        public Planta Planta => Planta.PlantaExterna;
    }

    public class PermisosMunicipales : IInbox
    {
        public string Nombre => "Permisos Municipales";
        public Planta Planta => Planta.PlantaExterna;
    }

    public class AdministracionFo : IInbox
    {
        public string Nombre => "Administracion FO";
        public Planta Planta => Planta.PlantaExterna;
    }

    public class GestionIngresos : IInbox
    {
        public string Nombre => "Gestion de Ingresos";
        public Planta Planta => Planta.PlantaExterna;
    }

    public class IngenieriaDeFo : IInbox
    {
        public string Nombre => "Ingenieria de FO";
        public Planta Planta => Planta.PlantaExterna;
    }

    public class InsercionesAmba : IInbox
    {
        public string Nombre => "Inserciones AMBA";
        public Planta Planta => Planta.PlantaExterna;
    }

}