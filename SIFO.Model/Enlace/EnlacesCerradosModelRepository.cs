using System.Collections.Generic;
using System.Linq;
using SIFO.DesktopUI.TareasIngresadas;

namespace SIFO.Model
{
    public class EnlacesCerradosModelRepository
    {
        //---------------------------------------------------------------------
        // Private
        //---------------------------------------------------------------------
        private readonly ITareasCerradasModelRepository _tareasCerradasModelRepository;
        private readonly List<IEnlaceCerrado> _list = new List<IEnlaceCerrado>();


        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------
        public EnlacesCerradosModelRepository(ITareasCerradasModelRepository tareasCerradasModelRepository)
        {
            _tareasCerradasModelRepository = tareasCerradasModelRepository;


            var factory = new PseudoTareaFactory();

            var cerradasExito = tareasCerradasModelRepository.SelectAll();
                //.Where(t => t.Estado == Estado.Exito);

            var cerradasPermisos = tareasCerradasModelRepository.SelectAll()
                .Where(t => t.Inbox == "Permisos Municipales");


            foreach (var tarea in cerradasExito)
            {


                var inbox = tarea.Inbox;
                var mes = tarea.FechaCierreTarea.Mes;
                var pseudoEnlace = tarea.PseudoEnlace;

                var pseudoTarea = _list
                    .Where(pt => pt.PseudoEnlace == pseudoEnlace)
                    .Where(pt => pt.Inbox == inbox)
                    .FirstOrDefault(pt => pt.MesCierre == mes);

                if (pseudoTarea == null)
                {
                    pseudoTarea = factory.CreatePseudoTarea(inbox);
                    pseudoTarea.PseudoEnlace = pseudoEnlace;
                    pseudoTarea.Orden = tarea.Orden;
                    pseudoTarea.MesCierre = mes;
                    pseudoTarea.Inbox = inbox;

                    pseudoTarea.TipoDeMovimiento = tarea.TipoDeMovimiento;
                    pseudoTarea.Tecnologia = tarea.Tecnologia;
                    pseudoTarea.TipoDeFibraOptica = tarea.TipoDeFibraOptica;

                    pseudoTarea.Producto = tarea.Producto;
                    pseudoTarea.TipoDeOrden = tarea.TipoDeOrden;
                    pseudoTarea.TipoDeSolucion = tarea.TipoSolucion;
                    pseudoTarea.Cliente = tarea.Cliente;
                    pseudoTarea.Identificador = tarea.Identificador;

                    pseudoTarea.WorkFlowFechaInicio = tarea.FechaInicioWorkflow;
                    pseudoTarea.WorkFlowFechaFin = tarea.FechaFinWorkflow;
                    pseudoTarea.Zona = tarea.Zona.ToString();

                    pseudoTarea.Partido = tarea.Partido;

                    pseudoTarea.TomadoPor = tarea.TomadoPor;

                    var calle = tarea.Calle;
                    var numero = tarea.Numero;

                    pseudoTarea.Direccion = calle + " " + numero;


                    //bool tieneFechaDeCompromiso = tarea.FechaCompromisoOrden.HasValue;
                    bool tieneFechaDeCompromiso = tarea.FechaDeInstalacion.HasValue;
                    IFecha fechaDeCompromiso;
                    if (tieneFechaDeCompromiso)
                    {
                        fechaDeCompromiso = new FechaDeCompromiso(tarea.FechaDeInstalacion.Value);
                    }
                    else
                    {
                        fechaDeCompromiso = new NullFechaDeCompromiso();
                    }
                    pseudoTarea.FechaDeCompromiso = fechaDeCompromiso;

                    _list.Add(pseudoTarea);


                    pseudoTarea.FechaDeCierre = tarea.FechaCierreTarea;
                    pseudoTarea.Resultado = tarea.Estado;
                   
                }

                if (tarea.FechaCierreTarea.Fecha > pseudoTarea.FechaDeCierre.Fecha)
                {
                    pseudoTarea.FechaDeCierre = tarea.FechaCierreTarea;
                    pseudoTarea.Resultado = tarea.Estado;
                }
              


                pseudoTarea.TiempoEnBandejaBruto += tarea.TiempoEfectivoPermanenciaTareaEnEstaBandeja;
                pseudoTarea.CanTareas += 1;

                if (inbox == "Obra Civil Tend INTE" || inbox == "Obra Civil Tend AMBA")
                {
                    var cerradasPermisosParaEnlace = cerradasPermisos.Where(t => t.PseudoEnlace == pseudoEnlace);

                    foreach (var cerradaPermisos in cerradasPermisosParaEnlace)
                    {
                        var inicioP = cerradaPermisos.FechaInicioTarea;
                        var finP = cerradaPermisos.FechaCierreTarea.Fecha;
                        if (inicioP > tarea.FechaInicioTarea && finP < tarea.FechaCierreTarea.Fecha)
                        {
                            pseudoTarea.TiempoEnOtrasBandejas += cerradaPermisos.TiempoEfectivoPermanenciaTareaEnEstaBandeja;
                        }

                    }

                }




            }

            // acumular
            foreach (var pseudoTarea in _list.OrderByDescending(pt => pt.MesCierre))
            {
                var inbox = pseudoTarea.Inbox;
                var mesDeCierre = pseudoTarea.MesCierre;

                var mesesAnteriores = _list
                    .Where(e => e.PseudoEnlace == pseudoTarea.PseudoEnlace)
                    .Where(e => e.Inbox == inbox)
                    .Where(e => e.MesCierre < mesDeCierre);

                foreach (var e in mesesAnteriores)
                {
                    pseudoTarea.TiempoEnBandejaBruto += e.TiempoEnBandejaBruto;
                    pseudoTarea.TiempoEnOtrasBandejas += e.TiempoEnOtrasBandejas;
                }

                // restar permisos
                //if (inbox == "Obra Civil Tend INTE" || inbox == "Obra Civil Tend AMBA")
                //{
                //    var cp = cerradasPermisos
                //        .Where(e => e.PseudoEnlace == pseudoTarea.PseudoEnlace)
                //        //.Where(e => e.Inbox == "Permisos Municipales")
                //        .Where(e => e.FechaCierreTarea.Mes < pseudoTarea.MesCierre);

                //    foreach (var p in cp)
                //    {
                //        if (!mesesAnteriores.Any(e => e.MesCierre == p.FechaCierreTarea.Mes))
                //        {
                //            pseudoTarea.TiempoEnOtrasBandejas += p.TiempoEfectivoPermanenciaTareaEnEstaBandeja;
                //        }
                //    }


                //}

            }


        }

        public IEnumerable<IEnlaceCerrado> SelectAll() => _list;
    }




}