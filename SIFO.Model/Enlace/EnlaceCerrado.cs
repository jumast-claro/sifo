using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace SIFO.Model
{
    public interface IEnlace
    {
        string PseudoEnlace { get; set; }
        long Orden { get; set; }
        string Inbox { get; set; }
        int CanTareas { get; set; }

        string TipoDeMovimiento { get; set; }
        string Tecnologia { get; set; }
        string TipoDeFibraOptica { get; set; }

        string Producto { get; set; }
        string TipoDeOrden { get; set; }
        string TipoDeSolucion { get; set; }
        string Cliente { get; set; }
        string Identificador { get; set; }

        DateTime FechaDeInicio { get; set; }

        int Mes { get; }
        int Annio { get; }

        DateTime? WorkFlowFechaInicio { get; set; }
        DateTime? WorkFlowFechaFin { get; set; }

        string Zona { get; set; }

        string Partido { get; set; }
        string Direccion { get; set; }
    }


    public class EnlacePendiente : IEnlace
    {
        private readonly int _annio;
        private readonly int _mes;

        public EnlacePendiente(int annio, int mes)
        {
            _annio = annio;
            _mes = mes;
        }

        public DateTime? WorkFlowFechaInicio { get; set; }
        public DateTime? WorkFlowFechaFin { get; set; }

        public string PseudoEnlace { get; set; }
        public long Orden { get; set; }
        public string Inbox { get; set; }
        public int CanTareas { get; set; } = 0;

        public string TipoDeMovimiento { get; set; } = "";
        public string Tecnologia { get; set; } = "";
        public string TipoDeFibraOptica { get; set; } = "";

        public string Producto { get; set; }
        public string TipoDeOrden { get; set; }
        public string TipoDeSolucion { get; set; }
        public string Cliente { get; set; }
        public string Identificador { get; set; }
        public DateTime FechaDeInicio { get; set; }
        public int Mes => _mes;
        public int Annio => _annio;

        public string Zona { get; set; }

        public string Partido { get; set; }

        public string Direccion { get; set; }


    }

    public class Enlace : IEnlace
    {
        public string Zona { get; set; }
        public DateTime? WorkFlowFechaInicio { get; set; }
        public DateTime? WorkFlowFechaFin { get; set; }

        public string PseudoEnlace { get; set; }
        public long Orden { get; set; }
        public string Inbox { get; set; }
        public int CanTareas { get; set; } = 0;

        public string TipoDeMovimiento { get; set; } = "";
        public string Tecnologia { get; set; } = "";
        public string TipoDeFibraOptica { get; set; } = "";

        public string Producto { get; set; }
        public string TipoDeOrden { get; set; }
        public string TipoDeSolucion { get; set; }
        public string Cliente { get; set; }
        public string Identificador { get; set; }

        //public int MesDeInicio { get; } = 0;
        //public int AnnioDeInicio { get; } = 0;

        public DateTime FechaDeInicio { get; set; }

        public virtual int Mes => FechaDeInicio.Month;
        public virtual int Annio => FechaDeInicio.Year;

        public string Partido { get; set; }

        public string TomadoPor { get; set; }

        public string Direccion { get; set; }


    }

    public class EnlaceCerrado : Enlace
    {


        public override int Mes => FechaDeCierre.Mes;
        public override int Annio => FechaDeCierre.Fecha.Value.Year;

        public int AnnioDeCierre => FechaDeCierre.Fecha.Value.Year;

        protected int _maximaPermanenciaParaOnTime;
        protected int _diasEnBandejasRestantes;

        public double TiempoEnBandejaBruto { get; set; } = 0;
        public double TiempoEnOtrasBandejas { get; set; } = 0;
        public double TiempoEnBandejaNeto => TiempoEnBandejaBruto - TiempoEnOtrasBandejas;
        public int MesCierre { get; set; } = -1;


        public Estado Resultado { get; set; }

        public IFecha FechaDeCierre { get; set; }


        private List<ITarea> _tareas = new List<ITarea>();

        public virtual OnTimeEnlace OnTime
        {
            get
            {
                if (Resultado == Estado.Fracaso)
                {
                    return OnTimeEnlace.NoAplica;
                }
                return TiempoEnBandejaNeto <= _maximaPermanenciaParaOnTime ? OnTimeEnlace.Si : OnTimeEnlace.No;
            }
        }

        public IFecha FechaDeCompromiso { get; set; }

        public virtual OnTimeEnlace OnTimeGeneral
        {
            get
            {
                if (Resultado == Estado.Fracaso || FechaDeCompromiso.IsNull)
                {
                    return OnTimeEnlace.NoAplica;
                }
                else
                {


                    var difCierreEnDias = (FechaDeCompromiso.Fecha.Value - FechaDeCierre.Fecha.Value).TotalDays;

                    if (difCierreEnDias >= _diasEnBandejasRestantes)
                    {
                        return OnTimeEnlace.Si;
                    }
                    else
                    {
                        return OnTimeEnlace.No;
                    }


                }
            }
        }

        public virtual int TiempoMaximoPermitidoEnBandeja => _maximaPermanenciaParaOnTime;
    }

    public class EnlaceCerradoABM : EnlaceCerrado, IEnlaceCerrado
    {
        public EnlaceCerradoABM()
        {
            _maximaPermanenciaParaOnTime = 6;
        }
    }

    public class EnlaceCerradoAdministracionFo : EnlaceCerrado, IEnlaceCerrado
    {
        public EnlaceCerradoAdministracionFo()
        {
            _maximaPermanenciaParaOnTime = 5;
        }


        public override OnTimeEnlace OnTimeGeneral => OnTimeEnlace.Indefinido;
    }

    public class EnlaceCerradoGestionDeIngresos : EnlaceCerrado, IEnlaceCerrado
    {
        //public bool OnTime => TiempoEnBandejaNeto <= 20;
        public EnlaceCerradoGestionDeIngresos()
        {
            _maximaPermanenciaParaOnTime = 20;
        }

        public override OnTimeEnlace OnTimeGeneral => OnTimeEnlace.Indefinido;
    }

    public class EnlaceCerradoIngenieriaDeFo : EnlaceCerrado, IEnlaceCerrado
    {
        //public bool OnTime => TiempoEnBandejaNeto <= 3;o
        public EnlaceCerradoIngenieriaDeFo()
        {
            _maximaPermanenciaParaOnTime = 3;
        }

        public override OnTimeEnlace OnTimeGeneral => OnTimeEnlace.Indefinido;
    }

    public class EnlaceCerradoInsercionesAmba : EnlaceCerrado, IEnlaceCerrado
    {
        //public bool OnTime => TiempoEnBandejaNeto <= 5;
        public EnlaceCerradoInsercionesAmba()
        {
            _maximaPermanenciaParaOnTime = 5;
            _diasEnBandejasRestantes = 8;
        }


        //public OnTimeEnlace OnTimeGeneral
        //{
        //    get
        //    {
        //        if (FechaDeCompromiso.IsNull)
        //        {
        //            return OnTimeEnlace.NoAplica;
        //        }
        //        else
        //        {
        //            var diasEnBandejasRestantes = 8;
        //            var difCierreEnDias = (FechaDeCompromiso.Fecha - FechaDeCierre.Fecha).TotalDays;

        //            if (difCierreEnDias <= diasEnBandejasRestantes)
        //            {
        //                return OnTimeEnlace.Si;
        //            }
        //            else
        //            {
        //                return OnTimeEnlace.No;
        //            }


        //        }
        //    }
        //}
    }

    public class EnlaceCerradoInsercionesInterior : EnlaceCerrado, IEnlaceCerrado
    {
        //public bool OnTime => TiempoEnBandejaNeto <= 5;
        public EnlaceCerradoInsercionesInterior()
        {
            _maximaPermanenciaParaOnTime = 5;
            _diasEnBandejasRestantes = 8;
        }
    }

    public class EnlaceCerradoIntalacionGpon : EnlaceCerrado, IEnlaceCerrado
    {
        //public bool OnTime => TiempoEnBandejaNeto <= 7;
        public EnlaceCerradoIntalacionGpon()
        {
            _maximaPermanenciaParaOnTime = 7;
            _diasEnBandejasRestantes = 0;
        }
    }

    public class EnlaceCerradoInstalacionesFo : EnlaceCerrado, IEnlaceCerrado
    {

        public EnlaceCerradoInstalacionesFo()
        {
            _diasEnBandejasRestantes = 13;
        }

        //public bool OnTime => false;
        public override OnTimeEnlace OnTime
        {
            get
            {
                if (Resultado == Estado.Fracaso)
                {
                    return OnTimeEnlace.NoAplica;
                }
                return OnTimeEnlace.Indefinido;
            }
        }
    }

    public class EnlaceCerradoIntalacionesSatel : EnlaceCerrado, IEnlaceCerrado
    {
        //public bool OnTime => TiempoEnBandejaNeto <= 60;
        public EnlaceCerradoIntalacionesSatel()
        {
            _maximaPermanenciaParaOnTime = 60;
        }

        public override OnTimeEnlace OnTimeGeneral => OnTimeEnlace.Indefinido;
    }

    public class EnlaceCerradoObraCivilTendAmba : EnlaceCerrado, IEnlaceCerrado
    {



        public EnlaceCerradoObraCivilTendAmba()
        {
            _diasEnBandejasRestantes = 13;
        }
        //public bool OnTime
        //{
        //    get
        //    {
        //        if (TiempoEnOtrasBandejas > 0)
        //        {
        //            return TiempoEnBandejaNeto <= 36;
        //        }
        //        else
        //        {
        //            return TiempoEnBandejaNeto <= 21;
        //        }
        //    }
        //}
        public override OnTimeEnlace OnTime
        {
            get
            {
                if (Resultado == Estado.Exito)
                {
                    if (TiempoEnOtrasBandejas > 0)
                    {
                        return TiempoEnBandejaNeto <= 36 ? OnTimeEnlace.Si : OnTimeEnlace.No;
                    }
                    else
                    {
                        return TiempoEnBandejaNeto <= 21 ? OnTimeEnlace.Si : OnTimeEnlace.No;
                    }
                }
                else
                {
                    return OnTimeEnlace.NoAplica;
                }
            }
        }

        public override int TiempoMaximoPermitidoEnBandeja => TiempoEnOtrasBandejas > 0 ? 36 : 21;
    }

    public class EnlaceCerradoObraCivilTendInte : EnlaceCerrado, IEnlaceCerrado
    {
        public EnlaceCerradoObraCivilTendInte()
        {
            _diasEnBandejasRestantes = 13;
        }

        //public bool OnTime
        //{
        //    get
        //    {
        //        if (TiempoEnOtrasBandejas > 0)
        //        {
        //            return TiempoEnBandejaNeto <= 36;
        //        }
        //        else
        //        {
        //            return TiempoEnBandejaNeto <= 21;
        //        }
        //    }
        //}
        public override OnTimeEnlace OnTime
        {
            get
            {
                if (Resultado == Estado.Exito)
                {
                    if (TiempoEnOtrasBandejas > 0)
                    {
                        return TiempoEnBandejaNeto <= 36 ? OnTimeEnlace.Si : OnTimeEnlace.No;
                    }
                    else
                    {
                        return TiempoEnBandejaNeto <= 21 ? OnTimeEnlace.Si : OnTimeEnlace.No;
                    }
                }
                else
                {
                    return OnTimeEnlace.NoAplica;
                }
            }
        }

        public override int TiempoMaximoPermitidoEnBandeja => TiempoEnOtrasBandejas > 0 ? 36 : 21;
    }

    public class EnlaceCerradoStartUpFo : EnlaceCerrado, IEnlaceCerrado
    {
        //public bool OnTime => TiempoEnBandejaNeto <= 8;
        public EnlaceCerradoStartUpFo()
        {
            _maximaPermanenciaParaOnTime = 8;
            _diasEnBandejasRestantes = 0;
        }
    }

    public class EnlaceCerradoOperacStartUp : EnlaceCerrado, IEnlaceCerrado
    {
        //public bool OnTime => TiempoEnBandejaNeto <= 8;
        public EnlaceCerradoOperacStartUp()
        {
            _maximaPermanenciaParaOnTime = 8;
            _diasEnBandejasRestantes = 0;
        }
    }

    public class EnlaceCerradoOpsInstalaciones : EnlaceCerrado, IEnlaceCerrado
    {
        //public bool OnTime => TiempoEnBandejaNeto <= 30;
        public EnlaceCerradoOpsInstalaciones()
        {
            _maximaPermanenciaParaOnTime = 30;
            _diasEnBandejasRestantes = 0;
        }


    }

    public class EnlaceCerradoPermisosMunicipales : EnlaceCerrado, IEnlaceCerrado
    {
        private readonly IDiasDePermisos _diasDePermisos;
        //public bool OnTime => TiempoEnBandejaNeto <= 21;
        //public EnlaceCerradoPermisosMunicipales()
        //{
        //    _maximaPermanenciaParaOnTime = 21;
        //}

        //private readonly int _maximo = 0;

        public override int TiempoMaximoPermitidoEnBandeja
        {
            get
            {
                var strPartido = this.Partido;
                var partido = new PartidoMapper().Map(strPartido);
                if (partido == AMBA.desconocido || partido == AMBA.otro)
                {
                    return 0;
                }

                if (partido == AMBA.CapitalFederal)
                {
                    return 36;
                }
                else
                {
                    var dias = _diasDePermisos.ObtenerDiasDePermisos(partido, this.FechaDeInicio.Year, this.FechaDeInicio.Month);
                    return dias;
                }
            }
        }

        public EnlaceCerradoPermisosMunicipales(IDiasDePermisos diasDePermisos)
        {
            _diasDePermisos = diasDePermisos;
            _diasEnBandejasRestantes = 36 + 13;

          
        }

        public override OnTimeEnlace OnTime
        {
            get
            {

                if (Resultado == Estado.Exito)
                {


                    var strPartido = this.Partido;
                    var partido = new PartidoMapper().Map(strPartido);

                    if (partido == AMBA.desconocido || partido == AMBA.otro)
                    {
                        return OnTimeEnlace.Indefinido;
                    }

                    if (partido == AMBA.CapitalFederal)
                    {
                        if (TiempoEnBandejaBruto <= 21)
                        {
                            return OnTimeEnlace.Si;
                        }
                        if (TiempoEnBandejaBruto > 36)
                        {
                            return OnTimeEnlace.No;
                        }
                        return OnTimeEnlace.Indefinido;
                    }

                    var dias = _diasDePermisos.ObtenerDiasDePermisos(partido, this.FechaDeInicio.Year, this.FechaDeInicio.Month);

                    if (TiempoEnBandejaNeto <= dias)
                    {
                        return OnTimeEnlace.Si;
                    }
                    else
                    {
                        return OnTimeEnlace.No;
                    }



                    //if (TiempoEnBandejaNeto <= 21)
                    //{
                    //    return OnTimeEnlace.Si;
                    //}
                    //if (TiempoEnBandejaNeto > 130)
                    //{
                    //    return OnTimeEnlace.No;
                    //}
                    //return OnTimeEnlace.Indefinido;
                }
                else
                {
                    return OnTimeEnlace.NoAplica;
                }
            }
        }
    }

    public class EnlaceCerradoRadioBases : EnlaceCerrado, IEnlaceCerrado
    {
        //public bool OnTime => TiempoEnBandejaNeto <= 60;
        public EnlaceCerradoRadioBases()
        {
            _maximaPermanenciaParaOnTime = 60;
        }

        public override OnTimeEnlace OnTimeGeneral => OnTimeEnlace.Indefinido;
    }

    public interface IDiasDePermisos
    {
        int ObtenerDiasDePermisos(AMBA partido, int annioInicioOrden, int mesInicioOrden);
    }

    public class InMemoryDiasDePermisos : IDiasDePermisos
    {
        public int ObtenerDiasDePermisos(AMBA partido, int annioInicioOrden, int mesInicioOrden)
        {
            var dias = 0;

            switch (partido)
            {
                case AMBA.AlmiranteBrown:
                    dias = 45;
                    break;
                case AMBA.Avellaneda:
                    dias = 72;
                    break;
                case AMBA.Berazategui:
                    dias = 91;
                    break;
                case AMBA.Campana:
                    dias = 142;
                    break;
                case AMBA.Escobar:
                    dias = 66;
                    break;
                case AMBA.EstebanEcheverria:
                    dias = 81;
                    break;
                case AMBA.Ezeiza:
                    dias = 66;
                    break;
                case AMBA.FlorencioVarela:
                    dias = 180;
                    break;
                case AMBA.GeneralRodriguez:
                    dias = 66;
                    break;
                case AMBA.GeneralSanMartin:
                    dias = 71;
                    break;
                case AMBA.Hurlingham:
                    dias = 141;
                    break;
                case AMBA.Ituzaingo:
                    dias = 123;
                    break;
                case AMBA.JoseCPaz:
                    dias = 51;
                    break;
                case AMBA.LaPlata:
                    dias = 81;
                    break;
                case AMBA.Lanus:
                    dias = 51;
                    break;
                case AMBA.LomasDeZamora:
                    dias = 64;
                    break;
                case AMBA.Lujan:
                    dias = 110;
                    break;
                case AMBA.MalvinasArgentinas:
                    dias = 154;
                    break;
                case AMBA.Merlo:
                    dias = 0;
                    break;
                case AMBA.Moreno:
                    dias = 111;
                    break;
                case AMBA.Moron:
                    dias = 290;
                    break;
                case AMBA.Pilar:
                    dias = 183;
                    break;
                case AMBA.SanFernando:
                    dias = 121;
                    break;
                case AMBA.SanIsidro:
                    dias = 200;
                    break;
                case AMBA.SanMiguel:
                    dias = 111;
                    break;
                case AMBA.SanVicente:
                    break;
                case AMBA.Tigre:
                    dias = 75;
                    break;
                case AMBA.TresDeFebrero:
                    dias = 66;
                    break;
                case AMBA.VicenteLopez:
                    dias = 86;
                    break;
                case AMBA.Zarate:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(partido), partido, null);
            }

            return dias;
        }
    }


    public interface IPartidoMapper
    {
        AMBA Map(string partido);
    }

    public class PartidoMapper : IPartidoMapper
    {
        public AMBA Map(string partido)
        {
            switch (partido)
            {
                case "":
                    return AMBA.desconocido;
                case "9 DE JULIO":
                    return AMBA.NueveDeJulio;
                case "ALMIRANTE BROWN":
                    return AMBA.AlmiranteBrown;
                case "AVELLANEDA":
                    return AMBA.Avellaneda;
                case "BERAZATEGUI":
                    return AMBA.Berazategui;
                case "CAMPANA":
                    return AMBA.Campana;
                case "CAPITAL FEDERAL":
                    return AMBA.CapitalFederal;
                case "ESCOBAR":
                    return AMBA.Escobar;
                case "ESTEBAN ECHEVERRIA":
                    return AMBA.EstebanEcheverria;
                case "FLORENCIO VARELA":
                    return AMBA.FlorencioVarela;
                case "GENERAL SAN MARTIN":
                    return AMBA.GeneralSanMartin;
                case "ITUZAINGO":
                    return AMBA.Ituzaingo;
                case "JOSE CLEMENTE PAZ":
                    return AMBA.JoseCPaz;
                case "LA MATANZA":
                    return AMBA.LaMatanza;
                case "LA PLATA":
                    return AMBA.LaPlata;
                case "LANUS":
                    return AMBA.Lanus;
                case "LOMAS DE ZAMORA":
                    return AMBA.LomasDeZamora;
                case "LUJAN":
                    return AMBA.Lujan;
                case "MALVINAS ARGENTINAS":
                    return AMBA.MalvinasArgentinas;
                case "MERLO":
                    return AMBA.Merlo;
                case "MORENO":
                    return AMBA.Moreno;
                case "MORON":
                    return AMBA.Moron;
                case "PILAR":
                    return AMBA.Pilar;
                case "SAN FERNANDO":
                    return AMBA.SanFernando;
                case "SAN ISIDRO":
                    return AMBA.SanIsidro;
                case "SAN MARTIN":
                    return AMBA.GeneralSanMartin;
                case "SAN MIGUEL":
                    return AMBA.SanMiguel;
                case "SAN VICENTE":
                    return AMBA.SanVicente;
                case "TIGRE":
                    return AMBA.Tigre;
                case "TRES DE FEBRERO":
                    return AMBA.TresDeFebrero;
                case "VICENTE LOPEZ":
                    return AMBA.TresDeFebrero;
                case "ZARATE":
                    return AMBA.Zarate;
                default:
                    return AMBA.otro;
            }
        }
    }

    public enum AMBA
    {
        CapitalFederal,
        AlmiranteBrown,
        Avellaneda,
        Berisso,
        Berazategui,
        Campana,
        Canuelas,
        Ensenada,
        EstebanEcheverria,
        Escobar,
        Ezeiza,
        FlorencioVarela,
        GeneralRodriguez,
        GeneralSanMartin,
        Hurlingham,
        Ituzaingo,
        JoseCPaz,
        LaMatanza,
        LaPlata,
        Lanus,
        LomasDeZamora,
        Lujan,
        MalvinasArgentinas,
        MarcosPaz,
        Merlo,
        Moreno,
        Moron,
        NueveDeJulio,
        Pilar,
        PresidentePeron,
        Quilmes,
        SanFernando,
        SanIsidro,
        SanMiguel,
        SanVicente,
        Tigre,
        TresDeFebrero,
        VicenteLopez,
        Zarate,
        desconocido,
        otro
    }
}