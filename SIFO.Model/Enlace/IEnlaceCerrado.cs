namespace SIFO.Model
{
    public interface IEnlaceCerrado : IEnlace
    {
        //string PseudoEnlace { get; set; }
        //long Orden { get; set; }
        //string Inbox { get; set; }
        double TiempoEnBandejaBruto { get; set; }
        double TiempoEnOtrasBandejas { get; set; }
        double TiempoEnBandejaNeto { get; }
        int MesCierre { get; set; }
        //int CanTareas { get; set; }
        OnTimeEnlace OnTime { get; }

        //string TipoDeMovimiento { get; set; }
        //string Tecnologia { get; set; }
        //string TipoDeFibraOptica { get; set; }

        //string Producto { get; set; }
        //string TipoDeOrden { get; set; }
        //string TipoDeSolucion { get; set; }
        //string Cliente { get; set; }
        //string Identificador { get; set; }


        Estado Resultado { get; set; }

        IFecha FechaDeCierre { get; set; }

        IFecha FechaDeCompromiso { get; set; }
        OnTimeEnlace OnTimeGeneral { get; }

        int AnnioDeCierre { get; }

        int TiempoMaximoPermitidoEnBandeja { get; }

        string TomadoPor { get; set; }

    }

    public enum OnTimeEnlace
    {
        Si,
        No,
        Indefinido,
        NoAplica,
    }

}