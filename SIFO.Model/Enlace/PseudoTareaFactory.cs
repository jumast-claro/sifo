using System;

namespace SIFO.Model
{
    public class PseudoTareaFactory
    {
        public IEnlaceCerrado CreatePseudoTarea(string inbox)
        {
            //var inbox = pseudoTarea.Inbox;

            IEnlaceCerrado result;
            switch (inbox)
            {
                case "ABM":
                    result = new EnlaceCerradoABM();
                    break;
                case "Administracion FO":
                    result = new EnlaceCerradoAdministracionFo();
                    break;
                case "Gestion de Ingresos":
                    result = new EnlaceCerradoGestionDeIngresos();
                    break;
                case "Ingenieria de FO":
                    result = new EnlaceCerradoIngenieriaDeFo();
                    break;
                case "Inserciones AMBA":
                    result = new EnlaceCerradoInsercionesAmba();
                    break;
                case "Inserciones Interior":
                    result = new EnlaceCerradoInsercionesInterior();
                    break;
                case "Instalacion GPON":
                    result = new EnlaceCerradoIntalacionGpon();
                    break;
                case "Instalaciones FO":
                    result = new EnlaceCerradoInstalacionesFo();
                    break;
                case "Instalaciones satel.":
                    result = new EnlaceCerradoIntalacionesSatel();
                    break;
                case "Obra Civil Tend AMBA":
                    result = new EnlaceCerradoObraCivilTendAmba();
                    break;
                case "Obra Civil Tend INTE":
                    result = new EnlaceCerradoObraCivilTendInte();
                    break;
                case "Operac. - Start Up":
                    result =  new EnlaceCerradoOperacStartUp();
                    break;
                case "Ops - Instalaciones":
                    result = new EnlaceCerradoOpsInstalaciones();
                    break;
                case "Permisos Municipales":
                    result = new EnlaceCerradoPermisosMunicipales(new InMemoryDiasDePermisos());
                    break;
                case "Radiobases":
                    result = new EnlaceCerradoRadioBases();
                    break;
                case "Start UP - FO":
                    result = new EnlaceCerradoStartUpFo();
                    break;
                default:
                    throw new ArgumentException();
            }

            return result;
        }
    }
}