﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using SIFO.DesktopUI.TareasIngresadas;

namespace SIFO.Model
{
    public class EnlacesPendientesInicio2017ModelRepository
    {
        //---------------------------------------------------------------------
        // Private
        //---------------------------------------------------------------------
        private readonly List<EnlacePendiente> _list = new List<EnlacePendiente>();

        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------
        public EnlacesPendientesInicio2017ModelRepository(ITareasPendientesInicio2017ModelRepository tareasPendientesModelRepository)
        {
            var tareasPendientes = tareasPendientesModelRepository.SelectAll();

            foreach (var tareaPendiente in tareasPendientes)
            {
                var inbox = tareaPendiente.Inbox;
                var enlace = tareaPendiente.PseudoEnlace;

                var enlaceModel = _list
                    .Where(e => e.PseudoEnlace == enlace)
                    .Where(e => e.Inbox == inbox).FirstOrDefault();

                if (enlaceModel == null)
                {
                    enlaceModel = new EnlacePendiente(2017,1);
                    enlaceModel.PseudoEnlace = enlace;
                    enlaceModel.Orden = tareaPendiente.Orden;
                    enlaceModel.Inbox = tareaPendiente.Inbox;

                    enlaceModel.TipoDeMovimiento = tareaPendiente.TipoDeMovimiento;
                    enlaceModel.Tecnologia = tareaPendiente.Tecnologia;
                    enlaceModel.TipoDeFibraOptica = tareaPendiente.TipoDeFibraOptica;

                    enlaceModel.Producto = tareaPendiente.Producto;
                    enlaceModel.TipoDeOrden = tareaPendiente.TipoDeOrden;
                    enlaceModel.TipoDeSolucion = tareaPendiente.TipoSolucion;
                    enlaceModel.Cliente = tareaPendiente.Cliente;
                    enlaceModel.Identificador = tareaPendiente.Identificador;

                    enlaceModel.FechaDeInicio = tareaPendiente.FechaInicioTarea;
                    enlaceModel.Zona = tareaPendiente.Zona.ToString();
                    _list.Add(enlaceModel);

                }
            }

            
        }

        public IEnumerable<IEnlace> SelectAll()
        {
            return _list;
        }
    }

    public class EnlacesIngreadosModelRepository
    {

        //---------------------------------------------------------------------
        // Private
        //---------------------------------------------------------------------
        private readonly List<Enlace> _list = new List<Enlace>(); 

        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------
        public EnlacesIngreadosModelRepository(ITareasIngresadasModelRepository tareasIngresadasModelRepository)
        {
            var tareaIngresadas = tareasIngresadasModelRepository.SelectAll();

            foreach (var tareaIngresada in tareaIngresadas)
            {
                var inbox = tareaIngresada.Inbox;
                var mesDeIngreso = tareaIngresada.FechaInicioTarea.Month;
                var pseudoEnlace = tareaIngresada.PseudoEnlace;

                var enlace = _list
                    .Where(e => e.PseudoEnlace == pseudoEnlace)
                    .Where(e => e.Inbox == inbox)
                    .FirstOrDefault(e => e.Mes == mesDeIngreso);



                if (enlace == null)
                {
                    enlace = new Enlace();
                    enlace.PseudoEnlace = pseudoEnlace;
                    enlace.Orden = tareaIngresada.Orden;
                    enlace.Inbox = tareaIngresada.Inbox;

                    enlace.TipoDeMovimiento = tareaIngresada.TipoDeMovimiento;
                    enlace.Tecnologia = tareaIngresada.Tecnologia;
                    enlace.TipoDeFibraOptica = tareaIngresada.TipoDeFibraOptica;

                    enlace.Producto = tareaIngresada.Producto;
                    enlace.TipoDeOrden = tareaIngresada.TipoDeOrden;
                    enlace.TipoDeSolucion = tareaIngresada.TipoSolucion;
                    enlace.Cliente = tareaIngresada.Cliente;
                    enlace.Identificador = tareaIngresada.Identificador;

                    enlace.FechaDeInicio = tareaIngresada.FechaInicioTarea;

                    enlace.WorkFlowFechaInicio = tareaIngresada.FechaInicioWorkflow;
                    enlace.WorkFlowFechaFin = tareaIngresada.FechaFinWorkflow;
                    enlace.Zona = tareaIngresada.Zona.ToString();

                    _list.Add(enlace);
                }
                else
                {
                    if (tareaIngresada.FechaInicioTarea < enlace.FechaDeInicio)
                    {
                        enlace.FechaDeInicio = tareaIngresada.FechaInicioTarea;
                    }
                }

            }

        }

        public IEnumerable<Enlace> SelectAll()
        {
            return _list;
        }

    }
}
