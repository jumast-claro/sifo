﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumast.Claro.BusinessObject.Tareas.DataAcces.Concrete.Csv;
using Jumast.Claro.BusinessObject.Tareas.DataAcces.Concrete.Excel;

namespace SIFO.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var excelRepo = new TareasBusinessObjectExcelReader(@"C:\Users\Jumast\Desktop\BO - 16-01-2017.xlsx", 2);
            var list1 = excelRepo.SelectAll();
            var x = 1;

            var csvRepo = new TareasBusinessObjectCsvReader(@"C:\Users\Jumast\Desktop\produ_TareasCerradas.csv");
            var list2 = csvRepo.SelectAll();
            var y = 1;
        }
    }
}
