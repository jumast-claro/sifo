﻿using System.Windows;
using System.Windows.Controls;
using Syncfusion.UI.Xaml.Grid;

namespace Jumast.Claro.Vantive.Mvvm
{
    /// <summary>
    /// Interaction logic for TareasVantiveSfDataGridView.xaml
    /// </summary>
    public partial class TareasVantiveSfDataGridView : UserControl
    {
        public static DependencyProperty SfDataGridProperty = DependencyProperty.Register("SfDataGrid", typeof(SfDataGrid), typeof(TareasVantiveSfDataGridView));
        public static DependencyProperty TareasVativeProperty = DependencyProperty.Register("TareasVantive", typeof(object), typeof(TareasVantiveSfDataGridView));
        public static DependencyProperty TareaSeleccionadaProperty = DependencyProperty.Register("TareaSeleccionada", typeof(object), typeof(TareasVantiveSfDataGridView));

        public TareasVantiveSfDataGridView()
        {
            InitializeComponent();
            SfDataGrid = this.sfDataGrid;
        }

        public SfDataGrid SfDataGrid
        {
            get { return (SfDataGrid)GetValue(SfDataGridProperty); }
            set
            {
                SetValue(SfDataGridProperty, value);
            }
        }

        public object TareasVantive
        {
            get { return GetValue(TareasVativeProperty); }
            set
            {
                SetValue(TareasVativeProperty, value);
            }
        }

        public object TareaSeleccionada
        {
            get { return GetValue(TareaSeleccionadaProperty); }
            set
            {
                SetValue(TareaSeleccionadaProperty, value);
            }
        }


    }
}
