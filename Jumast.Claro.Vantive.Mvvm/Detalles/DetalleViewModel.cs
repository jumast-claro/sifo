﻿using Jumast.Claro.Vantive.Model.Abstract;
using Jumast.Wpf.Mvvm.BindableProperties;

namespace Jumast.Claro.Vantive.Mvvm
{
    public class DetalleViewModel : TareaVantiveViewModel
    {
        public DetalleViewModel(ITareaVantive model) : base(model)
        {
            HeaderProperty = new ReadOnlyProperty<string>($"{model.Cliente} ({model.Localidad}-{model.Provincia})_Orden:{model.NumeroDeOrden}_Enl:{model.Enlace}");
        }


        public IBindableProperty<string> HeaderProperty { get; set; } 
    }
}
