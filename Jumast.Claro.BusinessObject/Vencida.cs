﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumast.Claro.BusinessObject
{
    public enum Vencida
    {
        Si,
        No,
        NoAplica
    }
}
