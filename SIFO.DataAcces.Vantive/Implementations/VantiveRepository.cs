﻿using SIFO.DataAcces.TareasCerradas;
using SIFO.DataAcces.Vantive.Abstractions;

namespace SIFO.DataAcces.Vantive.Implementations
{
    public class VantiveRepository : CsvRepository<IVantiveDataTransferObject>
    {
        public VantiveRepository(string path, string delimiter, bool hasFieldsEnclosedInQuotes) : base(path, delimiter, hasFieldsEnclosedInQuotes)
        {
        }
    }
}