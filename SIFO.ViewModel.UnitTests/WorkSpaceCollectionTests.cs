﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Internal;
using NUnit.Framework;
using SIFO.DesktopUI.Utilities;

namespace SIFO.ViewModel.UnitTests
{
    [TestFixture]
    public class WorkSpaceCollectionTests
    {

        [Test]
        public void AddWorkspace_NullParameter_ThrowsException()
        {
            // arrange
            var workspaceCollection = new WorkspaceCollection(new FakeDataTemplateSelector());
            var expectedExceptionType = typeof (ArgumentException);

            // act - assert
            var expectedException = Assert.Throws(expectedExceptionType, () => workspaceCollection.AddWorkspace(null));
        }

        [Test]
        public void AddWorkspace_SetsSelectedWorkspace()
        {
            // arrange
            var workspaceCollection = new WorkspaceCollection(new FakeDataTemplateSelector());
            const string WORKSPACE = "5";

            // act
            workspaceCollection.AddWorkspace(WORKSPACE);

            // assert
            Assert.AreEqual(WORKSPACE, workspaceCollection.SelectedWorkspace);
        }

        [Test]
        public void RemoveWorkspace_NullParameter_ThrosException()
        {
            // arrange
            var workspaceCollection = new WorkspaceCollection(new FakeDataTemplateSelector());
            var expectedExceptionType = typeof(ArgumentException);

            // act - assert
            var expectedException = Assert.Throws(expectedExceptionType, () => workspaceCollection.RemoveWorkspace(null));
        }

        [Test]
        public void RemoveWorkspace_WorkspaceNotInCollecion_ThrosException()
        {
            // arrange
            var expectedExceptionType = typeof(ArgumentException);
            var workspaceCollection = new WorkspaceCollection(new FakeDataTemplateSelector());
            const string INCLUDED_WORKSPACE = "Included";
            const string NOT_INCLUDED_WORKSPACE = "NotIncluded";
            workspaceCollection.AddWorkspace(INCLUDED_WORKSPACE);

            // act - assert
            var expectedException = Assert.Throws(expectedExceptionType, () => workspaceCollection.RemoveWorkspace(NOT_INCLUDED_WORKSPACE));
        }

        [Test]
        public void RemoveWorkspace_SetsDefaultWorkspaceAsSelectedWorkspace()
        {
            // arrange
            var workspaceCollection = new WorkspaceCollection(new FakeDataTemplateSelector());
            const string WORKSPACE = "5";
            const string DEFAULT_WORKSPACE = "Default";
            workspaceCollection.AddWorkspace(WORKSPACE);
            workspaceCollection.DefaultWorkspace = DEFAULT_WORKSPACE;

            // act - assert
            Assert.AreSame(WORKSPACE, workspaceCollection.SelectedWorkspace);
            workspaceCollection.RemoveWorkspace(WORKSPACE);
            Assert.AreSame(DEFAULT_WORKSPACE, workspaceCollection.SelectedWorkspace);
        }
    }
}
