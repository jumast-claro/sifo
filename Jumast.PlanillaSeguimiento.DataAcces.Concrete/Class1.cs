﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumast.PlanillaSeguimiento.DataAcces
{
    public class InstalacionData
    {
        public string ContratistaEnCurso { get; set; }
        public DateTime? FechaDeAsignada { get; set; }
        public DateTime? FechaDeInformeRecibido { get; set; }
        public string Direccion { get; set; }
        public string Partido { get; set; }
        public string Analista { get; set; }
        public DateTime? Compromiso { get; set; }
        public string NavigateUri { get; set; }


    }

    public interface IInstalacionDataProvider
    {
        bool ContieneEnlace(string enlace);
        InstalacionData ObtenerDatosParaEnlace(string enlace);
        void Actualizar();
    }

    public class ExcelInstalacionDataProvider : IInstalacionDataProvider
    {
        private readonly Dictionary<string, InstalacionData> _dic = new Dictionary<string, InstalacionData>();

        private void read()
        {
            ExcelEngine excelEngine = new ExcelEngine();
            IApplication application = excelEngine.Excel;
            IWorkbook wb = application.Workbooks.Open(Path.Combine(ConfigurationManager.AppSettings.Get("RUTA"), ConfigurationManager.AppSettings.Get("PLANA")));
            IWorksheet ws = wb.Worksheets[0];
            var rowCount = ws.Rows.Length;

            for (int i = 3; i <= rowCount; i++)
            {
                var enlacePlana = ws.Range[i, 4].Value;
                if (string.IsNullOrEmpty(enlacePlana)) continue;

                var data = new InstalacionData();

                var contrata = ws.Range[i, 10].Value;
                data.ContratistaEnCurso = contrata;

                var informe = ws.Range[i, 12].Value;
                if (!string.IsNullOrEmpty(informe))
                {
                    try
                    {
                        var fechaInforme = DateTime.ParseExact(informe, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        data.FechaDeInformeRecibido = fechaInforme;
                    }
                    catch (Exception)
                    {

                    }

                }

                var asignada = ws.Range[i, 11].Value;
                if (!string.IsNullOrEmpty(asignada))
                {
                    try
                    {
                        var fechaAsignada = DateTime.ParseExact(asignada, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        data.FechaDeAsignada = fechaAsignada;
                    }
                    catch (Exception)
                    {

                    }
                }



                var d = ws.Range[i, 2];

                var direccion = ws.Range[i, 2].FormulaStringValue;
                data.Direccion = direccion;

                try
                {
                    var formula = ws.Range[i, 2].Value;
                    var navigationUri = formula.Substring(15).Split('"')[0];
                    data.NavigateUri = navigationUri;
                }
                catch (Exception)
                {

                    //throw;
                }


                var partido = ws.Range[i, 3].Value;
                data.Partido = partido;

                var analista = ws.Range[i, 18].Value;
                data.Analista = analista;

                var compromiso = ws.Range[i, 7].Value;
                if (!string.IsNullOrEmpty(compromiso))
                {
                    try
                    {
                        var fechaCompromiso = DateTime.ParseExact(compromiso, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        data.Compromiso = fechaCompromiso;
                    }
                    catch (Exception)
                    {

                    }
                }



                _dic[enlacePlana] = data;
            }
        }

        public ExcelInstalacionDataProvider()
        {

            read();
        }

        public void Actualizar()
        {
            _dic.Clear();
            read();
        }

        public bool ContieneEnlace(string enlace)
        {
            if (_dic.ContainsKey(enlace))
            {
                return true;
            }
            else
            {
                foreach (var key in _dic.Keys)
                {
                    if (key.Contains(enlace))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public InstalacionData ObtenerDatosParaEnlace(string enlace)
        {
            if (_dic.ContainsKey(enlace))
            {
                return _dic[enlace];
            }
            else
            {
                foreach (var key in _dic.Keys)
                {
                    if (key.Contains(enlace))
                    {
                        return _dic[key];
                    }
                }
            }
            throw new InvalidOperationException();
        }
    }
}
