﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Internal;
using NUnit.Framework;

namespace SIFO.Model.UnitTests.TareaCerradaTests
{
    [TestFixture]
    public class General
    {

        private ITareaCerrada crearTareaCerrada(string inbox)
        {
            var fechaInicioTarea = new DateTime(05/01/2016);
            var fechaFinTarea = new DateTime(07/03/2016); 
            var f = new Model.TareaCerradaFactory();
            var tareaCerrada = f.Create(inbox, fechaInicioTarea, fechaFinTarea, null, null, "A", 0, "", "Alta");
            return tareaCerrada;
        }

        [Test]
        public void Constructor_InboxInvalido_LanzaExcepcion()
        {
            Assert.Throws(Is.InstanceOf(typeof (ArgumentException)), delegate { new Model.TareaCerradaFactory().Create("InboxInvalido", DateTime.MinValue, DateTime.MaxValue, null, null, "A", 0, "", "Alta"); });
        }

        [TestCase("Instalaciones FO", Planta.PlantaExterna)]
        [TestCase("Instalaciones satel.", Planta.PlantaExterna)]
        [TestCase("Ops - Instalaciones", Planta.PlantaExterna)]
        [TestCase("Radiobases", Planta.PlantaExterna)]
        [TestCase("Start UP - FO", Planta.PlantaInterna)]
        [TestCase("Operac. - Start Up", Planta.PlantaInterna)]
        public void Planta_DevuelveValorCorrecto(string inbox, Planta planta)
        {
            // arrange
            var orden = crearTareaCerrada(inbox);

            // act
            var plantaEsperada = orden.Planta;

            // 
            Assert.AreEqual(planta, plantaEsperada);
        }

        [Test]
        public void EsProyectoEspecial_DevuelveValorCorrecto()
        {
            // arrange
            var orden = crearTareaCerrada("Instalaciones FO");

            orden.Cliente = "MUNICIPALIDAD DE USHUAIA";
            Assert.IsTrue(orden.EsProyectoEspecial);

            orden.Cliente = "SERV ADMIN FINAN DE LA GOBERNA";
            Assert.IsTrue(orden.EsProyectoEspecial);

            orden.Cliente = "TELMEX ARGENTINA S.A.";
            Assert.IsTrue(orden.EsProyectoEspecial);

            orden.Cliente = "UNIVERSIDAD TECNOLOGICA N.";
            Assert.IsTrue(orden.EsProyectoEspecial);

            orden.Cliente = "OTRO CLIENTE";
            Assert.IsFalse(orden.EsProyectoEspecial);
        }


      

        [Test]
        public void FechaCompromisoOrden()
        {
            // arrange
            var inbox = "Instalaciones FO";
            var fechaInicioTarea = new DateTime(2016, 2,18);
            var fechaFinTarea = new DateTime(2016, 2,19); ;
            DateTime? fechaCompromisoOrden = null;
            var tareaCerrada = new Model.TareaCerradaFactory().Create(inbox, fechaInicioTarea, fechaFinTarea, null, fechaCompromisoOrden, "A", 0, "", "Alta");
            var fechaCompromisoOrdenEsperado = DateTimeService.MAX;

            //var fechaInicioOrden = new DateTime(2016, 2, 9);
            //tareaCerrada.FechaInicioOrden = fechaInicioOrden;

            // act
            var fechaCompromisoOrdenObtenido = tareaCerrada.FechaCompromisoOrden;

            // assert
            Assert.AreEqual(fechaCompromisoOrdenEsperado, fechaCompromisoOrdenObtenido);
        }


        [Test]
        public void Test_Enlace()
        {
            // arrange
            var enlace = 3296348;
            var fechaInicioTarea = new DateTime(2016, 3, 7);
            var fechaFinTarea = new DateTime(2016, 3, 7); ;
            var fechaCompromisoOrden = new DateTime(2016, 3, 7);
            var tareaCerrada = new Model.TareaCerradaFactory().Create("Instalaciones FO", fechaInicioTarea, fechaFinTarea, null, fechaCompromisoOrden, "A", enlace, "", "Alta");

            // act
            var enlaceObtenido = tareaCerrada.Enlace;

            //assert
            Assert.AreEqual(enlace, enlaceObtenido);
        }

        [TestCase("", 0)] 
        [TestCase("0", 0)]
        [TestCase("(RPV Multiservicios) Modificacion Tecnica - fila 2", 0)]
        [TestCase("(RPV Multiservicios) Trama IP - fila 1: 1152999700", 0)]
        [TestCase("Local - fila 1: 1152836300 - Telmex Argentina (local 30/100)", 0)]
        [TestCase("Cambio Ancho de Banda RPV - fila 1: 3296348 - Telmex Argentina (Baja enlaces 2224070-2226046-2226055  // 1024 Kbps)", 3296348)]
        [TestCase("(Central Gerenciada IP (2008)) RPV Multiservicios - fila 14: 4817155 - Telmex Argentina (RPV CGIP rempl 4050415)", 4817155)]
        public void Test_Identificador(string identificador, int enlaceEsperado)
        {
            // arrange
            var fechaInicioTarea = new DateTime(2016, 3, 7);
            var fechaFinTarea = new DateTime(2016, 3, 7); ;
            var fechaCompromisoOrden = new DateTime(2016, 3, 7);
            var tareaCerrada = new Model.TareaCerradaFactory().Create("Instalaciones FO", fechaInicioTarea, fechaFinTarea, null, fechaCompromisoOrden, "A", 0, identificador, "Alta");

            // act
            var enlaceObtenido = tareaCerrada.Enlace;

            //assert
            Assert.AreEqual(enlaceEsperado, enlaceObtenido);
        }
    }
}