﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SIFO.Model.UnitTests.TareaCerradaTests
{
    public class DiasDeInstalacionTests
    {
        [Test]
        public void DiasDeInstalacion_OrdenTipoA_Devuelve60()
        {
            // arrange
            var constructorParameters = new ConstructorParameters()
            {
                Cliente = "OBRA SOCIAL ACEROS PARANA",
                Zona = Zona.Amba,
                Inbox = "Instalaciones FO",
                FechaInicioTarea = DateTimeParser.ParseExactDateTime("07/03/2014 14:00:34"),
                FechaFinTarea = DateTimeParser.ParseExactDateTime("24/02/2016 12:47:38"),
                FechaInicioOrden = null,
                FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("27/07/2014"),
                TipoSolucion = "A - Edificio no acometido <200 mts (sin cruces de calle) (60 Días)",
                Enlace = 4364235,
                Identificador = "Internet Dedicado - fila 1: 4364235 - Telmex Argentina (ID 512)",
                Estado = Estado.Exito,
                Demoras = null
            };
            var tareaCerrada = new TareaCerradaFactory().CreateInstance(constructorParameters);

            // act
            var diasDeInstalacionObtenidos = tareaCerrada.DiasDeInstalacion;

            // assert
            Assert.AreEqual(60, diasDeInstalacionObtenidos);
        }

        [Test]
        public void DiasDeInstalacion_OrdenTipoB_Devuelve45()
        {
            // arrange
            var constructorParameters = new ConstructorParameters()
            {
                Cliente = "CORREO OFICIAL DE LA REP.ARG.S.A.",
                Zona = Zona.Amba,
                Inbox = "Instalaciones FO",
                FechaInicioTarea = DateTimeParser.ParseExactShortDate("10/07/2015"),
                FechaFinTarea = DateTimeParser.ParseExactShortDate("26/02/2016"),
                FechaInicioOrden = null,
                FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("24/08/2015"),
                TipoSolucion = "B - Edificio on net no acometido (45 Días)",
                Enlace = 4502166,
                Identificador = "RPV Multiservicios - fila 1: 4502166 - Telmex Argentina (Mudanza enlace 3410872)",
                Estado = Estado.Exito,
                Demoras = null
            };
            var tareaCerrada = new TareaCerradaFactory().CreateInstance(constructorParameters);

            // act
            var diasDeInstalacionObtenidos = tareaCerrada.DiasDeInstalacion;

            // assert
            Assert.AreEqual(45, diasDeInstalacionObtenidos);
        }

        [Test]
        public void DiasDeInstalacion_OrdenTipoZConFechaInicioOrden_DevuelveCantidadDeDiasEntreFechaDeInicioYFechaDeCompromisoDeLaOrden()
        {
            // arrange
            var fechaInicioTarea = DateTimeParser.ParseExactShortDate("12/11/2015");
            var fechaCompromisoOrden = DateTimeParser.ParseExactShortDate("21/12/2015");

            var constructorParameters = new ConstructorParameters()
            {
                Cliente = "DIFFUPAR SA",
                Zona = Zona.Amba,
                Inbox = "Instalaciones FO",
                FechaInicioTarea = fechaInicioTarea,
                FechaFinTarea = DateTimeParser.ParseExactShortDate("18/01/2016"),
                FechaInicioOrden = null,
                FechaCompromisoOrden = fechaCompromisoOrden,
                TipoSolucion = "Z - Producto No Estandar ( Días)",
                Enlace = 4694734,
                Identificador = "",
                Estado = Estado.Exito,
                Demoras = null
            };
            var tareaCerrada = new TareaCerradaFactory().CreateInstance(constructorParameters);
            var diasDeInstalacionEsperados = (fechaCompromisoOrden - fechaInicioTarea).Days;

            // act
            var diasDeInstalacionObtenidos = tareaCerrada.DiasDeInstalacion;

            // assert
            Assert.AreEqual(diasDeInstalacionEsperados, diasDeInstalacionObtenidos);
        }

        [Test]
        public void DiasDeInstalacion_OrdenTipoZSinFechaInicioOrden_DevuelveNull()
        {
            // arrange
            var fechaInicioTarea = DateTimeParser.ParseExactDateTime("19/02/2016 16:18:04");
            var fechaCompromisoOrden = DateTimeParser.ParseExactShortDate("07/10/2015");

            var constructorParameters = new ConstructorParameters()
            {
                Cliente = "MUNICIPALIDAD DE TIGRE",
                Zona = Zona.Amba,
                Inbox = "Instalaciones FO",
                FechaInicioTarea = fechaInicioTarea,
                FechaFinTarea = DateTimeParser.ParseExactDateTime("15/03/2016 17:02:46"),
                FechaInicioOrden = null,
                FechaCompromisoOrden = fechaCompromisoOrden,
                TipoSolucion = "Z - Producto No Estandar ( Días)",
                Enlace = 4567180,
                Identificador = "",
                Estado = Estado.Exito,
                Demoras = null
            };
            var tareaCerrada = new TareaCerradaFactory().CreateInstance(constructorParameters);
            int? diasDeInstalacionEsperados = null;

            // act
            var diasDeInstalacionObtenidos = tareaCerrada.DiasDeInstalacion;

            // assert
            Assert.AreEqual(diasDeInstalacionEsperados, diasDeInstalacionObtenidos);
        }

        [Test]
        public void DiasDeInstalacion_OrdenTipoASinFechaInicioOrdenYConFechaRenegociada_Devuelve60()
        {
            // arrange
            var fechaRenegociada = DateTimeParser.ParseExactShortDate("10/08/2014");

            var constructorParameters = new ConstructorParameters()
            {
                Cliente = "OBRA SOCIAL ACEROS PARANA",
                Zona = Zona.Amba,
                Inbox = "Instalaciones FO",
                FechaInicioTarea = DateTimeParser.ParseExactDateTime("07/03/2014 14:00:34"),
                FechaFinTarea = DateTimeParser.ParseExactDateTime("24/02/2016 12:47:38"),
                FechaInicioOrden = null,
                FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("27/07/2014"),
                TipoSolucion = "A - Edificio no acometido <200 mts (sin cruces de calle) (60 Días)",
                Enlace = 4364235,
                Identificador = "Internet Dedicado - fila 1: 4364235 - Telmex Argentina (ID 512)",
                Estado = Estado.Exito,
                Demoras = null
            };
            var tareaCerrada = new TareaCerradaFactory().CreateInstance(constructorParameters);
            tareaCerrada.FechaRenegociada = fechaRenegociada;

            var diasDeInstalacionEsperados = 60;

            // act
            var diasDeInstalacionObtenidos = tareaCerrada.DiasDeInstalacion;

            // assert
            Assert.AreEqual(diasDeInstalacionEsperados, diasDeInstalacionObtenidos);
        }

        [Test]
        public void DiasDeInstalacion_OrdenTipoBConFechaInicioOrdenYConFechaRenegociada_DevuelveCantidadDeDiasEntreFechaDeInicioYFechaRegociacionDeLaOrden()
        {
            // arrange
            var fechaRenegociada = DateTimeParser.ParseExactShortDate("02/03/2016");

            var constructorParameters = new ConstructorParameters()
            {
                Cliente = "BANCO SANTANDER RIO S.A.",
                Zona = Zona.Amba,
                Inbox = "Instalaciones FO",
                FechaInicioTarea = DateTimeParser.ParseExactDateTime("23/10/2015 10:50:20"),
                FechaFinTarea = DateTimeParser.ParseExactDateTime("18/01/2016 11:53:08"),
                FechaInicioOrden = null,
                FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("07/12/2015"),
                TipoSolucion = "B - Edificio on net no acometido (45 Días)",
                Enlace = 4666178,
                Identificador = "",
                Estado = Estado.Exito,
                Demoras = null
            };
            var tareaCerrada = new TareaCerradaFactory().CreateInstance(constructorParameters);
            tareaCerrada.FechaRenegociada = fechaRenegociada;

            //var diasDeInstalacionEsperados = (fechaRenegociada - fechaInicioOrden).Days;
            var diasDeInstalacionEsperados = (fechaRenegociada - tareaCerrada.FechaInicioOrden.Value).Days;

            // act
            var diasDeInstalacionObtenidos = tareaCerrada.DiasDeInstalacion;

            // assert
            Assert.AreEqual(diasDeInstalacionEsperados, diasDeInstalacionObtenidos);
        }

        [Test]
        public void DiasDeInstalacion_OrdenTipoZConFechaInicioOrdenYFechaRenegociada_DevuelveCantidadDeDiasEntreFechaDeInicioYFechaRegociacionDeLaOrden()
        {
            // arrange
            var fechaInicioTarea = DateTimeParser.ParseExactShortDate("12/11/2015");
            var fechaRenegociada = DateTimeParser.ParseExactShortDate("01/12/2016");

            var constructorParameters = new ConstructorParameters()
            {
                Cliente = "DIFFUPAR SA",
                Zona = Zona.Amba,
                Inbox = "Instalaciones FO",
                FechaInicioTarea = fechaInicioTarea,
                FechaFinTarea = DateTimeParser.ParseExactShortDate("18/01/2016"),
                FechaInicioOrden = null,
                FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("21/12/2015"),
                TipoSolucion = "Z - Producto No Estandar ( Días)",
                Enlace = 4694734,
                Identificador = "",
                Estado = Estado.Exito,
                Demoras = null
            };
            var tareaCerrada = new TareaCerradaFactory().CreateInstance(constructorParameters);
            tareaCerrada.FechaRenegociada = fechaRenegociada;

            var diasDeInstalacionEsperados = (fechaRenegociada - fechaInicioTarea).Days;

            // act
            var diasDeInstalacionObtenidos = tareaCerrada.DiasDeInstalacion;

            // assert
            Assert.AreEqual(diasDeInstalacionEsperados, diasDeInstalacionObtenidos);
        }
    }
}
