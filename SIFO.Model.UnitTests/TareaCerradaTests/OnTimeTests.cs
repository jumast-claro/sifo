﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SIFO.Model.UnitTests.TareaCerradaTests
{
    public class OnTimeTests
    {
        public class TestCaseData
        {
            public ConstructorParameters ConstructorParameters { get; set; }
            public OnTime OnTimeAPrioriEsperado { get; set; }
            public OnTime OnTimeAPosterioriEsperado { get; set; }

        }

        private static readonly TestCaseData[] _testCaseData = new[]
{
            new TestCaseData()
            {
                ConstructorParameters = new ConstructorParameters()
                {
                    Cliente = "CORREO OFICIAL DE LA REP.ARG.S.A.",
                    Zona = Zona.Amba,
                    Inbox = "Instalaciones FO",
                    FechaInicioTarea = DateTimeParser.ParseExactShortDate("10/07/2015"),
                    FechaFinTarea = DateTimeParser.ParseExactShortDate("26/02/2016"),
                    FechaInicioOrden = null,
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("24/08/2015"),
                    TipoSolucion = "B - Edificio on net no acometido (45 Días)",
                    Enlace = 4502166,
                    Identificador = "RPV Multiservicios - fila 1: 4502166 - Telmex Argentina (Mudanza enlace 3410872)",
                    Estado = Estado.Exito,
                    Demoras = 0
                },
                OnTimeAPrioriEsperado = OnTime.No,
                OnTimeAPosterioriEsperado = OnTime.No
            },
            new TestCaseData()
            {
                ConstructorParameters = new ConstructorParameters()
                {
                    Cliente = "CORREO OFICIAL DE LA REP.ARG.S.A.",
                    Zona = Zona.Amba,
                    Inbox = "Instalaciones FO",
                    FechaInicioTarea = DateTimeParser.ParseExactShortDate("10/07/2015"),
                    FechaFinTarea = DateTimeParser.ParseExactShortDate("26/02/2016"),
                   FechaInicioOrden = null,
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("24/08/2015"),
                    TipoSolucion = "B - Edificio on net no acometido (45 Días)",
                    Enlace = 4502166,
                    Identificador = "RPV Multiservicios - fila 1: 4502166 - Telmex Argentina (Mudanza enlace 3410872)",
                    Estado = Estado.Exito,
                    Demoras = 20
                },
                OnTimeAPrioriEsperado = OnTime.No,
                OnTimeAPosterioriEsperado = OnTime.No
            },
            new TestCaseData()
            {
                ConstructorParameters = new ConstructorParameters()
                {
                    Cliente = "CORREO OFICIAL DE LA REP.ARG.S.A.",
                    Zona = Zona.Amba,
                    Inbox = "Instalaciones FO",
                    FechaInicioTarea = DateTimeParser.ParseExactShortDate("10/07/2015"),
                    FechaFinTarea = DateTimeParser.ParseExactShortDate("26/02/2016"),
                  FechaInicioOrden = null,
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("24/08/2015"),
                    TipoSolucion = "B - Edificio on net no acometido (45 Días)",
                    Enlace = 4502166,
                    Identificador = "RPV Multiservicios - fila 1: 4502166 - Telmex Argentina (Mudanza enlace 3410872)",
                    Estado = Estado.Exito,
                    Demoras = 205
                },
                OnTimeAPrioriEsperado = OnTime.No,
                OnTimeAPosterioriEsperado = OnTime.Si
            },
            new TestCaseData()
            {
                ConstructorParameters = new ConstructorParameters()
                {
                    Cliente = "CORREO OFICIAL DE LA REP.ARG.S.A.",
                    Zona = Zona.Amba,
                    Inbox = "Instalaciones FO",
                    FechaInicioTarea = DateTimeParser.ParseExactShortDate("10/07/2015"),
                    FechaFinTarea = DateTimeParser.ParseExactShortDate("26/02/2016"),
                  FechaInicioOrden = null,
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("24/08/2015"),
                    TipoSolucion = "B - Edificio on net no acometido (45 Días)",
                    Enlace = 4502166,
                    Identificador = "RPV Multiservicios - fila 1: 4502166 - Telmex Argentina (Mudanza enlace 3410872)",
                    Estado = Estado.Exito,
                    Demoras = null
                },
                OnTimeAPrioriEsperado = OnTime.No,
                OnTimeAPosterioriEsperado = OnTime.FaltanDatos
            },
            new TestCaseData()
            {
                ConstructorParameters = new ConstructorParameters()
                {
                    Cliente = "DIFFUPAR SA",
                    Zona = Zona.Amba,
                    Inbox = "Instalaciones FO",
                    FechaInicioTarea = DateTimeParser.ParseExactShortDate("12/11/2015"),
                    FechaFinTarea = DateTimeParser.ParseExactShortDate("18/01/2016"),
                   FechaInicioOrden = null,
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("21/12/2015"),
                    TipoSolucion = "Z - Producto No Estandar ( Días)",
                    Enlace = 4694734,
                    Identificador = "",
                    Estado = Estado.Exito,
                    Demoras = null
                },
                OnTimeAPrioriEsperado = OnTime.No,
                OnTimeAPosterioriEsperado = OnTime.FaltanDatos
            },
            new TestCaseData()
            {
                ConstructorParameters = new ConstructorParameters()
                {
                    Cliente = "DIFFUPAR SA",
                    Zona = Zona.Amba,
                    Inbox = "Instalaciones FO",
                    FechaInicioTarea = DateTimeParser.ParseExactShortDate("12/11/2015"),
                    FechaFinTarea = DateTimeParser.ParseExactShortDate("18/01/2016"),
                  FechaInicioOrden = null,
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("21/12/2015"),
                    TipoSolucion = "Z - Producto No Estandar ( Días)",
                    Enlace = 4694734,
                    Identificador = "",
                    Estado = Estado.Exito,
                    Demoras = 5
                },
                OnTimeAPrioriEsperado = OnTime.No,
                OnTimeAPosterioriEsperado = OnTime.No
            },
            new TestCaseData()
            {
                ConstructorParameters = new ConstructorParameters()
                {
                    Cliente = "DIFFUPAR SA",
                    Zona = Zona.Amba,
                    Inbox = "Instalaciones FO",
                    FechaInicioTarea = DateTimeParser.ParseExactShortDate("12/11/2015"),
                    FechaFinTarea = DateTimeParser.ParseExactShortDate("18/01/2016"),
                    FechaInicioOrden = null,
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("21/12/2015"),
                    TipoSolucion = "Z - Producto No Estandar ( Días)",
                    Enlace = 4694734,
                    Identificador = "",
                    Estado = Estado.Exito,
                    Demoras = 45
                },
                OnTimeAPrioriEsperado = OnTime.No,
                OnTimeAPosterioriEsperado = OnTime.Si
            },
        };

        [Test]
        public void test([ValueSource(nameof(_testCaseData))] TestCaseData testCaseData)
        {
            // arrange
            var tareaCerrada = new TareaCerradaFactory().CreateInstance(testCaseData.ConstructorParameters);

            // act
            var onTimeAPrioriObtenido = tareaCerrada.OnTimeAPriori;
            var onTimeAPosterioriObtenido = tareaCerrada.OnTimeAPosteriori;

            // assert
            Assert.AreEqual(testCaseData.OnTimeAPrioriEsperado, onTimeAPrioriObtenido);
            Assert.AreEqual(testCaseData.OnTimeAPosterioriEsperado, onTimeAPosterioriObtenido);
        }

        [Test]
        public void Test_OrdenTipoB()
        {
            var constructorParameters = new ConstructorParameters()
            {
                Cliente = "CORREO OFICIAL DE LA REP.ARG.S.A.",
                Zona = Zona.Amba,
                Inbox = "Instalaciones FO",
                FechaInicioTarea = DateTimeParser.ParseExactShortDate("10/07/2015"),
                FechaFinTarea = DateTimeParser.ParseExactShortDate("26/02/2016"),
                FechaInicioOrden = null,
                FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("24/08/2015"),
                TipoSolucion = "B - Edificio on net no acometido (45 Días)",
                Enlace = 4502166,
                Identificador = "RPV Multiservicios - fila 1: 4502166 - Telmex Argentina (Mudanza enlace 3410872)",
                Estado = Estado.Exito,
                Demoras = null
            };

            var tareaCerrada = new TareaCerradaFactory().CreateInstance(constructorParameters);
            Assert.AreEqual(OnTime.No, tareaCerrada.OnTimeAPriori);

            tareaCerrada.FechaInicioOrden = null;
            Assert.AreEqual(OnTime.No, tareaCerrada.OnTimeAPriori);

            Assert.AreEqual(OnTime.FaltanDatos, tareaCerrada.OnTimeAPosteriori);

            tareaCerrada.Demoras = 0;
            Assert.AreEqual(OnTime.No, tareaCerrada.OnTimeAPosteriori);

            tareaCerrada.Demoras = 30;
            Assert.AreEqual(OnTime.No, tareaCerrada.OnTimeAPosteriori);

            tareaCerrada.Demoras = 65;
            Assert.AreEqual(OnTime.No, tareaCerrada.OnTimeAPosteriori);

            tareaCerrada.Demoras = 210;
            Assert.AreEqual(OnTime.Si, tareaCerrada.OnTimeAPosteriori);
        }

        [Test]
        public void Test_OrdenTipoZ()
        {
            var constructorParameters = new ConstructorParameters()
            {
                Cliente = "DIFFUPAR SA",
                Zona = Zona.Amba,
                Inbox = "Instalaciones FO",
                FechaInicioTarea = DateTimeParser.ParseExactShortDate("12/11/2015"),
                FechaFinTarea = DateTimeParser.ParseExactShortDate("18/01/2016"),
                FechaInicioOrden = null,
                FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("21/12/2015"),
                TipoSolucion = "Z - Producto No Estandar ( Días)",
                Enlace = 4694734,
                Identificador = "",
                Estado = Estado.Exito,
                Demoras = 45
            };

            var tareaCerrada = new TareaCerradaFactory().CreateInstance(constructorParameters);
            Assert.AreEqual(OnTime.No, tareaCerrada.OnTimeAPriori);

            tareaCerrada.FechaInicioOrden = null;
            Assert.AreEqual(OnTime.FaltanDatos, tareaCerrada.OnTimeAPriori);

            tareaCerrada.FechaInicioOrden = DateTimeParser.ParseExactShortDate("12/11/2015");
            Assert.AreEqual(OnTime.No, tareaCerrada.OnTimeAPriori);

            tareaCerrada.Demoras = 30;
            Assert.AreEqual(OnTime.No, tareaCerrada.OnTimeAPosteriori);

            tareaCerrada.Demoras = 65;
            Assert.AreEqual(OnTime.Si, tareaCerrada.OnTimeAPosteriori);
        }
    }
}
