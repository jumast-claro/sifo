﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SIFO.Model.UnitTests.TareaCerradaTests
{
    [TestFixture]
    public class TipoDeSolucionTests
    {

        private readonly TipoSolucionFactory _factory = new TipoSolucionFactory();

        [TestCase("", "")]
        [TestCase("A - Edificio no acometido <200 mts (sin cruces de calle) (60 Días)", "A")]
        [TestCase("B - Edificio on net no acometido (45 Días)", "B")]
        [TestCase("C - Edificio Acometido (35 d¿as) (35 Días)", "C")]
        [TestCase("C - Edificio acometido sin clientes en servicio(30 Días)", "C")]
        [TestCase("D - Edificio acometido con servicio en otro cliente (25 Días)", "D")]
        [TestCase("E - Edificio acometido con servicio en el mismo cliente (20 Días)", "E")]
        [TestCase("F - LMDS (con LV y BW OK) (60 Días)", "F")]
        [TestCase("J - Ultima Milla TASA (70 Días)", "J")]
        [TestCase("L - Producto CGIP (65 Días)", "L")]
        [TestCase("N - Wimax (20 Días)", "N")]
        [TestCase("P - Producto Trama Ip (65 Días)", "P")]
        [TestCase("Q - Producto Líneas Telefonía Telmex (7 Días + Ultima milla)", "Q")]
        [TestCase("Q - Producto Líneas Telefonía Telmex (7 Días)", "Q")]
        [TestCase("S - G-PON Aéreo (30 Días)", "S")]
        [TestCase("T - Movil (30 Días)", "T")]
        [TestCase("U - LMDS-NG (con LV y BW OK) (30 Días)", "U")]
        [TestCase("WZ", "WZ")]
        [TestCase("Y - Radio PaP sin infraestructura adicional (90 Días)", "Y")]
        [TestCase("Z - Producto No Estandar ( Días)", "Z")]
        [TestCase("ZD - SIN USO (35 Días)", "ZD")]
        public void Test_Codigo(string descripcion, string codigoEsperado)
        {
            // arrange
            var tipoDeSolucion = _factory.CrearTipoDeSolucion(descripcion);

            // act
            var codigoObtenido = tipoDeSolucion.Codigo;

            // assert 
            Assert.AreEqual(codigoObtenido, codigoEsperado);
        }

        [TestCase("", false)]
        [TestCase("A - Edificio no acometido <200 mts (sin cruces de calle) (60 Días)", true)]
        [TestCase("B - Edificio on net no acometido (45 Días)", true)]
        [TestCase("C - Edificio Acometido (35 d¿as) (35 Días)", true)]
        [TestCase("C - Edificio acometido sin clientes en servicio(30 Días)", true)]
        [TestCase("D - Edificio acometido con servicio en otro cliente (25 Días)", true)]
        [TestCase("E - Edificio acometido con servicio en el mismo cliente (20 Días)", true)]
        [TestCase("F - LMDS (con LV y BW OK) (60 Días)", true)]
        [TestCase("J - Ultima Milla TASA (70 Días)", true)]
        [TestCase("L - Producto CGIP (65 Días)", true)]
        [TestCase("N - Wimax (20 Días)", true)]
        [TestCase("P - Producto Trama Ip (65 Días)", true)]
        [TestCase("Q - Producto Líneas Telefonía Telmex (7 Días + Ultima milla)", true)]
        [TestCase("Q - Producto Líneas Telefonía Telmex (7 Días)", true)]
        [TestCase("S - G-PON Aéreo (30 Días)", true)]
        [TestCase("T - Movil (30 Días)", true)]
        [TestCase("U - LMDS-NG (con LV y BW OK) (30 Días)", true)]
        [TestCase("WZ", false)]
        [TestCase("Y - Radio PaP sin infraestructura adicional (90 Días)", true)]
        [TestCase("Z - Producto No Estandar ( Días)", false)]
        [TestCase("ZD - SIN USO (35 Días)", true)]
        public void Test_DefineDiasDeInstalacion(string descripcion, bool defineDiasDeInstalacionEsperado)
        {
            // arrange
            var tipoDeSolucion = _factory.CrearTipoDeSolucion(descripcion);

            // act
            var defineDiasDeInstalacionObtenido = tipoDeSolucion.DefineDiasDeInstalacion;

            // assert 
            Assert.AreEqual(defineDiasDeInstalacionObtenido, defineDiasDeInstalacionEsperado);
        }

        [TestCase("", null)]
        [TestCase("A - Edificio no acometido <200 mts (sin cruces de calle) (60 Días)", 60)]
        [TestCase("B - Edificio on net no acometido (45 Días)", 45)]
        [TestCase("C - Edificio Acometido (35 d¿as) (35 Días)", 35)]
        [TestCase("C - Edificio acometido sin clientes en servicio(30 Días)", 30)]
        [TestCase("D - Edificio acometido con servicio en otro cliente (25 Días)", 25)]
        [TestCase("E - Edificio acometido con servicio en el mismo cliente (20 Días)", 20)]
        [TestCase("F - LMDS (con LV y BW OK) (60 Días)", 60)]
        [TestCase("J - Ultima Milla TASA (70 Días)", 70)]
        [TestCase("L - Producto CGIP (65 Días)", 65)]
        [TestCase("N - Wimax (20 Días)", 20)]
        [TestCase("P - Producto Trama Ip (65 Días)", 65)]
        [TestCase("Q - Producto Líneas Telefonía Telmex (7 Días + Ultima milla)", 7)]
        [TestCase("Q - Producto Líneas Telefonía Telmex (7 Días)", 7)]
        [TestCase("S - G-PON Aéreo (30 Días)", 30)]
        [TestCase("T - Movil (30 Días)", 30)]
        [TestCase("U - LMDS-NG (con LV y BW OK) (30 Días)", 30)]
        [TestCase("WZ", null)]
        [TestCase("Y - Radio PaP sin infraestructura adicional (90 Días)", 90)]
        [TestCase("Z - Producto No Estandar ( Días)", null)]
        [TestCase("ZD - SIN USO (35 Días)", 35)]
        public void Test_DiasDeInstalacion(string descripcion, int? diasDeInstalacionEsperado)
        {
            // arrange
            var factory = new TipoSolucionFactory();
            var tipoDeSolucion = _factory.CrearTipoDeSolucion(descripcion);

            // act
            var diasDeInstalacionObtenido = tipoDeSolucion.DiasDeInstalacion;

            // assert 
            Assert.AreEqual(diasDeInstalacionObtenido, diasDeInstalacionEsperado);
        }

        [TestCase("", "", null)]
        [TestCase("A - Edificio no acometido <200 mts...", "A", 60)]
        [TestCase("B - Edificio on net no acometido...", "B", 45)]
        [TestCase("C - Edificio Acometido", "C", 35)]
        [TestCase("C - Edificio acometido sin clientes en servicio...","C", 30)]
        [TestCase("D - Edificio acometido con servicio en otro cliente...", "D", 25)]
        [TestCase("E - Edificio acometido con servicio en el mismo cliente...", "E", 20)]
        [TestCase("F - LMDS...", "F", 60)]
        [TestCase("J - Ultima Milla TASA...", "J", 70)]
        [TestCase("L - Producto CGIP...", "L", 65)]
        [TestCase("N - Wimax...", "N", 20)]
        [TestCase("P - Producto Trama Ip...", "P", 65)]
        [TestCase("Q - Producto Líneas...", "Q", 7)]
        [TestCase("S - G-PON....", "S", 30)]
        [TestCase("T - Movil....", "T", 30)]
        [TestCase("U - LMDS-NG...", "U", 30)]
        [TestCase("WZ", "WZ", null)]
        [TestCase("Y - Radio PaP sin infraestructura adicional...", "Y", 90)]
        [TestCase("Z - Producto No Estandar...", "Z", null)]
        [TestCase("ZD - SIN USO...", "ZD", 35)]
        public void TipoDeSolucion_CadenaIncompleta(string descripcion, string codigoEsperado, int? diasDeInstalacionEsperado)
        {
            // arrange
            var tipoDeSolucion = _factory.CrearTipoDeSolucion(descripcion);

            // act
            string codigoObtenido = tipoDeSolucion.Codigo;
            int? diasDeInstalacionObtenido = tipoDeSolucion.DiasDeInstalacion;

            //assert
            Assert.AreEqual(codigoObtenido, codigoEsperado);
            Assert.AreEqual(diasDeInstalacionObtenido, diasDeInstalacionEsperado);
        }

        [Test]
        public void Constructor_DescripcionInvalida_LanzaExcepcion()
        {
            Assert.Throws(Is.InstanceOf(typeof (ArgumentException)), delegate { _factory.CrearTipoDeSolucion("a"); });
        }


    }


}
