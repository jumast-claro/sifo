﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace SIFO.Model.UnitTests.TareaCerradaTests
{
    public class FechaInicioOrdenTests
    {
        public class ConstructorParameters
        {
            public string Inbox { get; set; }
            public DateTime FechaInicioTarea { get; set; }
            public DateTime FechaFinTarea { get; set; }
            public DateTime? FechaCompromisoOrden { get; set; }
            public string TipoSolucion { get; set; }
            public long Enlace { get; set; }
            public string Identificador { get; set; }
            public string Cliente { get; set; }
            public string TipoDeOrden { get; set; }
        }

        public class TestCaseData
        {
            public FechaInicioOrdenTests.ConstructorParameters ConstructorParameters { get; set; }
            public DateTime? FechaInicioOrdenEsperada { get; set; }

        }

        public class TareaCerradaFactory
        {
            public ITareaCerrada CreateInstance(ConstructorParameters constructorParameters)
            {
                return new Model.TareaCerradaFactory().Create(constructorParameters.Inbox, constructorParameters.FechaInicioTarea, constructorParameters.FechaFinTarea, null, constructorParameters.FechaCompromisoOrden, constructorParameters.TipoSolucion, constructorParameters.Enlace, constructorParameters.Identificador, constructorParameters.TipoDeOrden);
            }
        }

        private static readonly TestCaseData[] _testCaseData = new[]
        {
            // FechaCompromisoOrden == null && TipoSolucion define dias de instalación.
            new TestCaseData()
            {
                ConstructorParameters = new ConstructorParameters()
                {
                    Inbox = "Instalaciones FO",
                    FechaInicioTarea = new DateTime(2015, 9, 29),
                    FechaFinTarea = new DateTime(2016, 2, 10),
                    FechaCompromisoOrden = null,
                    TipoSolucion = "A - Edificio no acometido <200 mts (sin cruces de calle) (60 Días)",
                    Enlace = 4600190,
                    Identificador = "Internet Dedicado - fila 49: 4600190 - Telmex Argentina (ID 8mb)",
                    Cliente = "Claro"
                },
                FechaInicioOrdenEsperada = null
            },
            // FechaCompromisoOrden == null TipoSolucion no define dias de instalación.
            new TestCaseData()
            {
                ConstructorParameters = new ConstructorParameters()
                {
                    Inbox = "Instalaciones FO",
                    FechaInicioTarea = new DateTime(2015, 12, 30),
                    FechaFinTarea = new DateTime(2016, 3, 9),
                    FechaCompromisoOrden = null,
                    TipoSolucion = "Z - Producto No Estandar ( Días)",
                    Enlace = 4760706,
                    Identificador = "",
                      Cliente = "Claro"
                },
                FechaInicioOrdenEsperada = null
            },
            // FechaCompromisoOrden != null && TipoSolucion define dias de instalación.
            new TestCaseData() 
            {
                ConstructorParameters = new ConstructorParameters()
                {
                    Inbox = "Instalaciones FO",
                    FechaInicioTarea = new DateTime(2015, 9, 29),
                    FechaFinTarea = new DateTime(2016, 2, 10),
                    FechaCompromisoOrden = new DateTime(2015, 11, 13),
                    TipoSolucion = "A - Edificio no acometido <200 mts (sin cruces de calle) (60 Días)",
                    Enlace = 4600190,
                    Identificador = "Internet Dedicado - fila 49: 4600190 - Telmex Argentina (ID 8mb)",
                    Cliente = "Claro"
                },
                 FechaInicioOrdenEsperada = new DateTime(2015,11,13).AddDays(-1*60)
            },
            // TipoSolucion no define dias de instalación && FechaInicioTarea < FechaCompromisoOrden.
            new TestCaseData() 
            {
                ConstructorParameters = new ConstructorParameters()
                {
                    Inbox = "Instalaciones FO",
                    FechaInicioTarea = new DateTime(2015, 12, 15),
                    FechaFinTarea = new DateTime(2016, 3, 21),
                    FechaCompromisoOrden = new DateTime(2016, 2, 23),
                    TipoSolucion = "Z - Producto No Estandar ( Días)",
                    Enlace = 1152937800,
                    Identificador = "0",
                    Cliente = "Claro"
                },
                FechaInicioOrdenEsperada = new DateTime(2015,12,15)
            },
            // TipoSolucion no define dias de instalación && FechaInicioTarea > FechaCompromisoOrden.
            new TestCaseData() 
            {
                ConstructorParameters = new ConstructorParameters()
                {
                    Inbox = "Start UP - FO",
                    FechaInicioTarea = new DateTime(2016, 2, 18),
                    FechaFinTarea = new DateTime(2016, 2, 19),
                    FechaCompromisoOrden = new DateTime(2015, 12, 25),
                    TipoSolucion = "Z - Producto No Estandar ( Días)",
                    Enlace = 4721660,
                    Identificador = "RPV Multiservicios - fila 16: 4721660 - Telmex Argentina (rpv lite 5mb)",
                    Cliente = "Claro"
                },
                FechaInicioOrdenEsperada = null,
            },
            // TipoSolucion no define dias de instalación && FechaInicioTarea == FechaCompromisoOrden
            new TestCaseData() 
            {
                ConstructorParameters = new ConstructorParameters()
                {
                    Inbox = "Start UP - FO",
                    FechaInicioTarea = new DateTime(2016, 2, 18),
                    FechaFinTarea = new DateTime(2016, 2, 19),
                    FechaCompromisoOrden = new DateTime(2016, 2, 18),
                    TipoSolucion = "Z - Producto No Estandar ( Días)",
                    Enlace = 4721660,
                    Identificador = "RPV Multiservicios - fila 16: 4721660 - Telmex Argentina (rpv lite 5mb)",
                    Cliente = "Claro"
                },
                FechaInicioOrdenEsperada = null,
            }
        };

        [Test]
        public void Test_FechaInicioOrden_InmediatamenteDespuesDeLaContruccion([ValueSource(nameof(_testCaseData))] TestCaseData testCaseData)
        {
            // arrange
            var tareaCerrada = new TareaCerradaFactory().CreateInstance(testCaseData.ConstructorParameters);

            // act
            var fechaInicioOrdenObtenida = tareaCerrada.FechaInicioOrden;

            // assert
            Assert.AreEqual(testCaseData.FechaInicioOrdenEsperada, fechaInicioOrdenObtenida);

        }

        [Test]
        public void FechaInicioOrden_SobreescribeValorInferidoEnLaConstruccion()
        {
            // arrange
            var testCaseData = new TestCaseData()
            {
                ConstructorParameters = new ConstructorParameters()
                {
                    Inbox = "Instalaciones FO",
                    FechaInicioTarea = new DateTime(2015, 9, 29),
                    FechaFinTarea = new DateTime(2016, 2, 10),
                    FechaCompromisoOrden = null,
                    TipoSolucion = "A - Edificio no acometido <200 mts (sin cruces de calle) (60 Días)",
                    Enlace = 4600190,
                    Identificador = "Internet Dedicado - fila 49: 4600190 - Telmex Argentina (ID 8mb)",
                    Cliente = "Claro"
                },
                FechaInicioOrdenEsperada = null
            };
            var fechaInicioOrdenEsperado = new DateTime(2015,9,25);

            // act
            var tareaCerrada = new TareaCerradaFactory().CreateInstance(testCaseData.ConstructorParameters);
            tareaCerrada.FechaInicioOrden = fechaInicioOrdenEsperado;

            // act
            var fechaInicioOrdenObtenido = tareaCerrada.FechaInicioOrden;

            // assert
            Assert.AreEqual(fechaInicioOrdenEsperado, fechaInicioOrdenObtenido);
        }
    }
}
