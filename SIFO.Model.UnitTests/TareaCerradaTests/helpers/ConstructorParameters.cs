﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIFO.Model.UnitTests.TareaCerradaTests
{
    public class ConstructorParameters
    {
        public string Cliente { get; set; }
        public Zona Zona { get; set; }
        public string Inbox { get; set; }
        public DateTime FechaInicioTarea { get; set; }
        public DateTime FechaFinTarea { get; set; }
        public DateTime? FechaInicioOrden { get; set; }
        public DateTime? FechaCompromisoOrden { get; set; }
        public string TipoSolucion { get; set; }
        public long Enlace { get; set; }
        public string Identificador { get; set; }
        public Estado Estado { get; set; }
        public int? Demoras { get; set; }
        public string TipoDeOrden { get; set; }
    }
}
