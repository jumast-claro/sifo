﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIFO.Model.UnitTests.TareaCerradaTests
{
    public class TareaCerradaFactory
    {
        public ITareaCerrada CreateInstance(ConstructorParameters constructorParameters)
        {
            var tareaCerrada = new Model.TareaCerradaFactory().Create(constructorParameters.Inbox, constructorParameters.FechaInicioTarea, constructorParameters.FechaFinTarea, constructorParameters.FechaInicioOrden, constructorParameters.FechaCompromisoOrden, constructorParameters.TipoSolucion, constructorParameters.Enlace, constructorParameters.Identificador, constructorParameters.TipoDeOrden);

            tareaCerrada.Cliente = constructorParameters.Cliente;
            tareaCerrada.Zona = constructorParameters.Zona;
            tareaCerrada.Estado = constructorParameters.Estado;
            tareaCerrada.Demoras = constructorParameters.Demoras;
            return tareaCerrada;
        }
    }
}
