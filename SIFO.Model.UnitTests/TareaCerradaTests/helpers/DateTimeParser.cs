﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIFO.Model.UnitTests.TareaCerradaTests
{
    public static class DateTimeParser
    {
        public static DateTime ParseExactShortDate(string stringValue)
        {
            return DateTime.ParseExact(stringValue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }

        public static DateTime ParseExactDateTime(string stringValue)
        {
            return DateTime.ParseExact(stringValue, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
        }

        public static DateTime? parseNullableDateTime(string stringValue)
        {
            if (stringValue == "") return null;
            return DateTime.ParseExact(stringValue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
    }
}
