﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework.Internal;
using NUnit.Framework;

namespace SIFO.Model.UnitTests.TareaCerradaTests
{
    public class OnTimeTestsBis
    {
        public class TestParameters
        {
            public int NumeroOrden { get; set; }
            public int NumeroEnlace { get; set; }
            public DateTime? FechaInicioOrden { get; set; }
            public DateTime  FechaCompromisoOrden { get; set; }
            public ITipoSolucion TipoDeSolucion { get; set; }
            public ITipoDeOrden TipoDeOrden { get; set; }
            public IInbox Inbox { get; set; }
            public DateTime FechaInicioTarea { get; set; }
            public DateTime FechaCierreTarea { get; set; }
        }

        public class TestCaseData
        {
            public TestParameters TestParameters { get; set; }
            public OnTime OnTimeAPrioriEsperado { get; set; }
        }

        private static readonly TestCaseData[] _testCaseData = 
        {
            new TestCaseData()
            {
                TestParameters = new TestParameters()
                {
                    NumeroOrden = 31527953,
                    NumeroEnlace = 4502166,
                    FechaInicioOrden = null,
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("24/08/2015"),
                    TipoDeSolucion = new TipoB(),
                    TipoDeOrden = new Alta(),
                    Inbox = new InstalacionesFo(),
                    FechaInicioTarea = DateTimeParser.ParseExactShortDate("10/07/2015"),
                    FechaCierreTarea = DateTimeParser.ParseExactShortDate("26/02/2016")
                },
                OnTimeAPrioriEsperado = OnTime.No,
            },
            new TestCaseData()
            {
                TestParameters = new TestParameters()
                {
                    NumeroOrden = 31527953,
                    NumeroEnlace = 4502166,
                    FechaInicioOrden = null,
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("24/08/2015"),
                    TipoDeSolucion = new TipoB(),
                    TipoDeOrden = new Alta(),
                    Inbox = new InstalacionesFo(),
                    FechaInicioTarea = DateTimeParser.ParseExactShortDate("08/04/2016"),
                    FechaCierreTarea = DateTimeParser.ParseExactShortDate("11/04/2016")
                },
                OnTimeAPrioriEsperado = OnTime.Si,
            },
            new TestCaseData()
            {
                TestParameters = new TestParameters()
                {
                    NumeroOrden = 33446354,
                    NumeroEnlace = 4694734,
                    FechaInicioOrden = null,
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("21/12/2015"),
                    TipoDeSolucion = new TipoZ(),
                    TipoDeOrden = new Alta(),
                    Inbox = new InstalacionesFo(),
                    FechaInicioTarea = DateTimeParser.ParseExactShortDate("12/11/2015"),
                    FechaCierreTarea = DateTimeParser.ParseExactShortDate("21/12/2015")
                },
                OnTimeAPrioriEsperado = OnTime.FaltanDatos,
            },
            new TestCaseData()
            {
                TestParameters = new TestParameters()
                {
                    NumeroOrden = 32395748,
                    NumeroEnlace = 4591258,
                    FechaInicioOrden = null,
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("29/01/2016"),
                    TipoDeSolucion = new TipoZ(),
                    TipoDeOrden = new Alta(),
                    Inbox = new InstalacionesFo(),
                    FechaInicioTarea = DateTimeParser.ParseExactShortDate("07/01/2016"),
                    FechaCierreTarea = DateTimeParser.ParseExactShortDate("18/01/2016")
                },
                OnTimeAPrioriEsperado = OnTime.FaltanDatos,
            },
        };

        [Test]
        public void Test([ValueSource((nameof(_testCaseData)))] TestCaseData testCaseData)
        {
            // arrange
            var parameters = testCaseData.TestParameters;
            IOrden orden = new Orden(parameters.NumeroOrden, parameters.NumeroEnlace, parameters.FechaInicioOrden, parameters.FechaCompromisoOrden, parameters.TipoDeSolucion, parameters.TipoDeOrden);
            ITareaCerrada tareaCerrada = new TareaCerrada(orden, parameters.Inbox, parameters.FechaInicioTarea, parameters.FechaCierreTarea);

            // act
            var onTimeAPrioriEsperado = testCaseData.OnTimeAPrioriEsperado;
            var onTimeAPrioriObtenido = tareaCerrada.OnTimeAPriori;

            // assert
            Assert.AreEqual(onTimeAPrioriEsperado, onTimeAPrioriObtenido);
        }
    }
}
