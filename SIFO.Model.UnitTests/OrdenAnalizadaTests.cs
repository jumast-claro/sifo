﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Internal;
using NUnit.Framework;

namespace SIFO.Model.UnitTests
{
    [TestFixture]
    public class OrdenAnalizadaTests
    {

        [Test]
        public void Constructor_TareaIdEsNull_ThrowsException()
        {
            // arrange
            const string TAREA_ID = null;
            var expectedExceptionType = typeof(ArgumentException);

            // act
            var expectedExcepcion = Assert.Throws(expectedExceptionType, delegate { new OrdenAnalizada(TAREA_ID); });

            // assert
            Assert.AreEqual(expectedExcepcion.GetType(), expectedExceptionType);
        }

        [Test]
        public void Constructor_TareaIdEsCadenaVacia_ThrowsException()
        {
            // arrange
            const string TAREA_ID = "";
            var expectedExceptionType = typeof(ArgumentException);

            // act
            var expectedExcepcion = Assert.Throws(expectedExceptionType, delegate { new OrdenAnalizada(TAREA_ID); });

            // assert
            Assert.AreEqual(expectedExcepcion.GetType(), expectedExceptionType);
        }

        [Test]
        public void TareaId_DespuesDeLaConstruccion_DevuelveValorCorrecto()
        {
            // arrange
            const string TAREA_ID = "TareaId123";
            var ordenAnalizada = new OrdenAnalizada(TAREA_ID);
            const string VALOR_ESPERADO = TAREA_ID;

            // act
            var valorObtenido = ordenAnalizada.TareaId;

            // assert
            Assert.AreEqual(VALOR_ESPERADO, valorObtenido);
        }


        [Test]
        public void Total_DespuesDeLaConstruccion_EsCero()
        {
            // arrange
            const string TAREA_ID = "TareaId";
            var ordenAnalizada = new OrdenAnalizada(TAREA_ID);
            const int VALOR_ESPERADO = 0;

            // act
            var valorObtenido = ordenAnalizada.Total;

            // assert
            Assert.AreEqual(VALOR_ESPERADO, valorObtenido);
        }

        [Test]
        public void Total_DevuelveValorCorrecto()
        {
            // arrange
            const string TAREA_ID = "TareaId";
            var ordenAnalizada = new OrdenAnalizada(TAREA_ID);
            const int FALTA_INGENIERIA = 15;
            const int FALTA_ASIGNACION = 4;
            ordenAnalizada.FaltaIngenieria = FALTA_INGENIERIA;
            ordenAnalizada.FaltaAsignacion = FALTA_ASIGNACION;
            const int VALOR_ESPERADO = FALTA_INGENIERIA + FALTA_ASIGNACION;

            // act
            var valorObtenido = ordenAnalizada.Total;

            // assert
            Assert.AreEqual(VALOR_ESPERADO, valorObtenido);
        }


    }
}
