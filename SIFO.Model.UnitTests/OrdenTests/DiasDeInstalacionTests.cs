﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using SIFO.Model.UnitTests.TareaCerradaTests;

namespace SIFO.Model.UnitTests.OrdenTests
{
    public class DiasDeInstalacionTests
    {
        public class TestParameters
        {
            public long NumeroDeOrden { get; set; }
            public long NumeroDeEnlace { get; set; }
            public DateTime? FechaInicioOrden { get; set; }
            public DateTime? FechaCompromisoOrden { get; set; }
            public ITipoSolucion TipoDeSolucion { get; set; }
            public ITipoDeOrden TipoDeOrden { get; set; }
            public DateTime? FechaRenegociada { get; set; }
        }

        public class TestCaseData
        {
            public TestParameters TestParameters { get; set; }
            public int? DiasDeInstalacionesEsperados { get; set; }
        }

        private static readonly TestCaseData[] _testCaseData =
        {
            new TestCaseData()
            {
                // Orden tipo B | Fechas de inicio y compromiso coherentes con la tipificación | Sin fecha renegociada.
                TestParameters = new TestParameters()
                {
                    NumeroDeOrden = 0,
                    NumeroDeEnlace = 0,
                    FechaInicioOrden = DateTimeParser.ParseExactShortDate("28/11/2014"),
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("12/01/2015"),
                    TipoDeSolucion = new TipoB(),
                    TipoDeOrden = Substitute.For<ITipoDeOrden>(),
                    FechaRenegociada = null,

                },
                DiasDeInstalacionesEsperados = 45
            },
            // Orden tipo B | Fechas de inicio y compromiso coherentes con la tipificación | Con fecha renegociada.
            new TestCaseData()
            {
                TestParameters = new TestParameters()
                {
                    NumeroDeOrden = 0,
                    NumeroDeEnlace = 0,
                    FechaInicioOrden = DateTimeParser.ParseExactShortDate("28/11/2014"),
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("12/01/2015"),
                    TipoDeSolucion = new TipoB(),
                    TipoDeOrden = Substitute.For<ITipoDeOrden>(),
                    FechaRenegociada = DateTimeParser.ParseExactShortDate("25/01/2015"),

                },
                DiasDeInstalacionesEsperados = 58
            },
            // Orden tipo Z | Fechas de inicio y compromiso | Sin fecha renegociada.
            new TestCaseData()
            {
                TestParameters = new TestParameters()
                {
                    NumeroDeOrden = 0,
                    NumeroDeEnlace = 0,
                    FechaInicioOrden = DateTimeParser.ParseExactShortDate("16/07/2015"),
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("13/11/2015"),
                    TipoDeSolucion = new TipoZ(),
                    TipoDeOrden = Substitute.For<ITipoDeOrden>(),
                    FechaRenegociada = null,

                },
                DiasDeInstalacionesEsperados = 120
            },
            // Orden tipo Z | Fechas de inicio y compromiso | Sin fecha renegociada.
            new TestCaseData()
            {
                TestParameters = new TestParameters()
                {
                    NumeroDeOrden = 0,
                    NumeroDeEnlace = 0,
                    FechaInicioOrden = DateTimeParser.ParseExactShortDate("16/07/2015"),
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("13/11/2015"),
                    TipoDeSolucion = new TipoZ(),
                    TipoDeOrden = Substitute.For<ITipoDeOrden>(),
                    FechaRenegociada = DateTimeParser.ParseExactShortDate("27/11/2015"),

                },
                DiasDeInstalacionesEsperados = 134
            },
            new TestCaseData()
            {
                // Orden tipo A | Sin fecha de inicio y con fecha de compromiso | Sin fecha renegociada.
                TestParameters = new TestParameters()
                {
                    NumeroDeOrden = 0,
                    NumeroDeEnlace = 0,
                    FechaInicioOrden = null,
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("12/01/2015"),
                    TipoDeSolucion = new TipoA(),
                    TipoDeOrden = Substitute.For<ITipoDeOrden>(),
                    FechaRenegociada = null,

                },
                DiasDeInstalacionesEsperados = 60
            },
             new TestCaseData()
            {
                // Orden tipo A | Sin fecha de inicio y con fecha de compromiso | Con fecha renegociada.
                TestParameters = new TestParameters()
                {
                    NumeroDeOrden = 0,
                    NumeroDeEnlace = 0,
                    FechaInicioOrden = null,
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("12/01/2015"),
                    TipoDeSolucion = new TipoA(),
                    TipoDeOrden = Substitute.For<ITipoDeOrden>(),
                    FechaRenegociada = DateTimeParser.ParseExactShortDate("20/01/2015"),

                },
                DiasDeInstalacionesEsperados = null
            },
            // Orden tipo Z | Sin fecha de inicio y con fecha de compromiso | Sin fecha renegociada.
            new TestCaseData()
            {
                TestParameters = new TestParameters()
                {
                    NumeroDeOrden = 0,
                    NumeroDeEnlace = 0,
                    FechaInicioOrden = null,
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("13/11/2015"),
                    TipoDeSolucion = new TipoZ(),
                    TipoDeOrden = Substitute.For<ITipoDeOrden>(),
                    FechaRenegociada = null

                },
                DiasDeInstalacionesEsperados = null
            },
            // Orden tipo Z | Sin fecha de inicio y con fecha de compromiso | Con fecha renegociada.
            new TestCaseData()
            {
                TestParameters = new TestParameters()
                {
                    NumeroDeOrden = 0,
                    NumeroDeEnlace = 0,
                    FechaInicioOrden = null,
                    FechaCompromisoOrden = DateTimeParser.ParseExactShortDate("13/11/2015"),
                    TipoDeSolucion = new TipoZ(),
                    TipoDeOrden = Substitute.For<ITipoDeOrden>(),
                    FechaRenegociada = DateTimeParser.ParseExactShortDate("27/11/2015"),

                },
                DiasDeInstalacionesEsperados = null
            },

        };

        [Test]
        public void Test_DiasDeInstalacion([ValueSource(nameof(_testCaseData))] TestCaseData testCaseData)
        {
            // arrange
            var p = testCaseData.TestParameters;

            var orden = new Orden(p.NumeroDeOrden, p.NumeroDeEnlace, p.FechaInicioOrden, p.FechaCompromisoOrden, p.TipoDeSolucion, p.TipoDeOrden)
            {
                FechaRenegociada = p.FechaRenegociada
            };

            var diasDeInstalacionEsperados = testCaseData.DiasDeInstalacionesEsperados;

            // act
            var diasDeInstalacionObtenidos = orden.DiasDeInstalacion;


            // assert
            Assert.AreEqual(diasDeInstalacionEsperados, diasDeInstalacionObtenidos);
        }
    }
}
