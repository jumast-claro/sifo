﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SIFO.Model.UnitTests
{
    [TestFixture]
    public class TipoDeSolucionTests
    {
        [TestCase("A - Edificio no acometido <200 mts (sin cruces de calle) (60 Días)", 60)]
        [TestCase("B - Edificio on net no acometido (45 Días)", 45)]
        [TestCase("C - Edificio Acometido (35 d¿as) (35 Días)", 35)]
        [TestCase("C - Edificio acometido sin clientes en servicio(30 Días)", 30)]
        [TestCase("D - Edificio acometido con servicio en otro cliente (25 Días)", 25)]
        [TestCase("E - Edificio acometido con servicio en el mismo cliente (20 Días)", 20)]
        [TestCase("F - LMDS (con LV y BW OK) (60 Días)", 60)]
        [TestCase("J - Ultima Milla TASA (70 Días)", 70)]
        [TestCase("L - Producto CGIP (65 Días)", 65)]
        [TestCase("N - Wimax (20 Días)", 20)]
        [TestCase("Q - Producto Líneas Telefonía Telmex (7 Días + Ultima milla)", 7)]
        [TestCase("Q - Producto Líneas Telefonía Telmex (7 Días)", 7)]
        [TestCase("S - G-PON Aéreo (30 Días)", 30)]
        [TestCase("T - Movil (30 Días)", 30)]
        [TestCase("U - LMDS-NG (con LV y BW OK) (30 Días)", 30)]
        [TestCase("WZ", null)]
        [TestCase("Y - Radio PaP sin infraestructura adicional (90 Días)", 90)]
        [TestCase("Z - Producto No Estandar ( Días)", null)]
        [TestCase("ZD - SIN USO (35 Días)", 35)]
        public void Test_DiasDeInstalacion(string identificador, int? diasDeInstalacionEsperados)
        {
            // arrange
            var tipoDeSolucion = new TipoSolucionFactory().CrearTipoDeSolucion(identificador);

            //act
            var diasDeInstalacionObtenidos = tipoDeSolucion.DiasDeInstalacion;

            // assert
            Assert.AreEqual(diasDeInstalacionEsperados, diasDeInstalacionObtenidos);
        }

        [TestCase("A - Edificio no acometido <200 mts (sin cruces de calle) (60 Días)", "A")]
        [TestCase("B - Edificio on net no acometido (45 Días)", "B")]
        [TestCase("C - Edificio Acometido (35 d¿as) (35 Días)", "C")]
        [TestCase("C - Edificio acometido sin clientes en servicio(30 Días)", "C")]
        [TestCase("D - Edificio acometido con servicio en otro cliente (25 Días)", "D")]
        [TestCase("E - Edificio acometido con servicio en el mismo cliente (20 Días)", "E")]
        [TestCase("F - LMDS (con LV y BW OK) (60 Días)", "F")]
        [TestCase("J - Ultima Milla TASA (70 Días)", "J")]
        [TestCase("L - Producto CGIP (65 Días)", "L")]
        [TestCase("N - Wimax (20 Días)", "N")]
        [TestCase("Q - Producto Líneas Telefonía Telmex (7 Días + Ultima milla)", "Q")]
        [TestCase("Q - Producto Líneas Telefonía Telmex (7 Días)","Q")]
        [TestCase("S - G-PON Aéreo (30 Días)", "S")]
        [TestCase("T - Movil (30 Días)", "T")]
        [TestCase("U - LMDS-NG (con LV y BW OK) (30 Días)", "U")]
        [TestCase("WZ", "WZ")]
        [TestCase("Y - Radio PaP sin infraestructura adicional (90 Días)", "Y")]
        [TestCase("Z - Producto No Estandar ( Días)", "Z")]
        [TestCase("ZD - SIN USO (35 Días)", "ZD")]
        public void Test_Codigo(string identificador, string codigoEsperado)
        {
            // arrange
            var tipoDeSolucion = new TipoSolucionFactory().CrearTipoDeSolucion(identificador);

            //act
            var codigoObtenido = tipoDeSolucion.Codigo;

            // assert
            Assert.AreEqual(codigoEsperado, codigoObtenido);
        }

    }
}
