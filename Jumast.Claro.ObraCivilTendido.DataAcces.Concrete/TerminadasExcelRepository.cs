using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Jumast.Claro.ObraCivilTendido.DataAcces.Abstract;
using Jumast.Claro.ObraCivilTendido.Model.Abstract;
using Jumast.Claro.ObraCivilTendido.Model.Concrete;
using Syncfusion.XlsIO;

namespace Jumast.Claro.ObraCivilTendido.DataAcces.Concrete
{
    public class TerminadasExcelRepository : IInstalacionesFOExcelRepository
    {
        private readonly string _rutaCompletaConExtension;
        private const int PRIMERA_FILA_CON_DATOS = 4;

        private const int CLIENTE = 1;
        private const int DIRECCION = 2;
        private const int PARTIDO = 3;
        private const int ENLACE = 4;
        private const int ORDEN = 5;
        private const int FECHA_DE_RECIBIDO = 6;
        private const int FECHA_DE_COMPROMISO_VANTIVE = 7;
        private const int TIPO_DE_ORDEN = 8;
        private const int TECNOLOGIA = 9;
        private const int CONTRATISTA_EN_CURSO = 10;
        private const int FECHA_DE_ASIGNADO = 11;
        private const int FECHA_DE_INFORME_RECIBIDO = 12;
        private const int METROS_DE_OBRA_CIVIL = 13;
        private const int METROS_DE_TENDIDO = 14;
        private const int VALOR_RELEVADO = 15;
        private const int DOCUMENTADO_WEBGIS = 16;
        private const int FECHA_DE_CIERRE = 17;
        private const int HIPERVISOR = 18;

        private const int ESTADO = 20;
        private const int OBSERVACIONES = 21;

        private const int HOJA_TERMINADAS = 1;
        private const int HOJA_FRACADAS = 2;

        private const string FORMATO_FECHA = "dd/MM/yyyy";


        private readonly Dictionary<string, IInstalacionFO> _instalacionesPorEnlace = new Dictionary<string, IInstalacionFO>();
        private readonly List<IInstalacionFO> _todasLasInstalaciones = new List<IInstalacionFO>();


        public TerminadasExcelRepository(string rutaCompletaConExtension)
        {
            _rutaCompletaConExtension = rutaCompletaConExtension;
        }

        //private async Task<IEnumerable<IInstalacionFO>> leerHojaAsync(int hoja, string estado)
        //{
        //    var resultado = await leerHoja(hoja, estado);
        //    return resultado;
        //}


        private IEnumerable<IInstalacionFO> leerHoja(int hoja, string estado)
        {

            var resultado = new List<IInstalacionFO>();

            ExcelEngine excelEngine = new ExcelEngine();
            IApplication application = excelEngine.Excel;
            IWorkbook wb = application.Workbooks.Open(_rutaCompletaConExtension);
            IWorksheet ws = wb.Worksheets[hoja];
            var rowCount = ws.Rows.Length;


            for (var i = PRIMERA_FILA_CON_DATOS; i <= rowCount; i++)
            {
                var enlace = ws.Range[i, ENLACE].Value;
                var cliente = ws.Range[i, CLIENTE].Value;
                if (string.IsNullOrEmpty(cliente)) continue;

                var direccion = ws.Range[i, DIRECCION].FormulaStringValue;
                var partido = ws.Range[i, PARTIDO].Value;
                var orden = ws.Range[i, ORDEN].Value;
                var fechaDeRecibido = obtenerFecha(ws, i, FECHA_DE_RECIBIDO);
                var fechaDeCompromisoVantive = obtenerFecha(ws, i, FECHA_DE_COMPROMISO_VANTIVE);
                var tipoDeOrden = ws.Range[i, TIPO_DE_ORDEN].Value;
                var tecnologia = ws.Range[i, TECNOLOGIA].Value;
                var contratistaEnCurso = ws.Range[i, CONTRATISTA_EN_CURSO].Value;
                var fechaDeAsignado = obtenerFecha(ws, i, FECHA_DE_ASIGNADO);
                var fechaDeInformeRecibido = obtenerFecha(ws, i, FECHA_DE_INFORME_RECIBIDO);
                var metrosDeObraCivil = obtenerNumero(ws, i, METROS_DE_OBRA_CIVIL);
                var metrosDeTendido = obtenerNumero(ws, i, METROS_DE_TENDIDO);
                var valorRelevado = obtenerNumero(ws, i, VALOR_RELEVADO);
                var documentadoWebGis = ws.Range[i, DOCUMENTADO_WEBGIS].Value;
                var fechaDeCierre = obtenerFecha(ws, i, FECHA_DE_CIERRE);
                //var hipervisor = ws.Range[i, HIPERVISOR].Value;

                //var estado = ws.Range[i, OBSERVACIONES].Value;
                var observaciones = ws.Range[i, ESTADO].Value;

                var hyperlink = obtenerHyperlink(ws.Range[i, DIRECCION].Formula);

                IInstalacionFO instalacionFo = new InstalacionFO()
                {
                    Cliente = cliente,
                    Direccion = direccion,
                    Partido = partido,
                    Enlace = enlace,
                    Orden = orden,
                    FechaDeRecibido = fechaDeRecibido,
                    FechaDeCompromisoVantive = fechaDeCompromisoVantive,
                    TipoDeOrden = tipoDeOrden,
                    Tecnologia = tecnologia,
                    ContratistaEnCurso = contratistaEnCurso,
                    FechaDeAsignado = fechaDeAsignado,
                    FechaDeInformeRecibido = fechaDeInformeRecibido,
                    MetrosDeObraCivil = metrosDeObraCivil,
                    MetrosDeTendido = metrosDeTendido,
                    ValorRelevado = valorRelevado,
                    DocumentadoWebGis = documentadoWebGis,
                    FechaDeCierre = fechaDeCierre,
                    Hipervisor = "",

                    Estado = estado,
                    Observaciones = "",
                    Link = hyperlink
                };
                resultado.Add(instalacionFo);
            }
            return resultado;
        }

        public virtual void Actualizar()
        {
            _todasLasInstalaciones.Clear();
            _instalacionesPorEnlace.Clear();


            var terminadas = leerHoja(HOJA_TERMINADAS, "Terminada");
            var fracasadas = leerHoja(HOJA_FRACADAS, "Fracasada");

            foreach (var instalacion in terminadas)
            {
                _todasLasInstalaciones.Add(instalacion);
                var enlace = instalacion.Enlace;
                if (!string.IsNullOrEmpty(enlace))
                {
                    _instalacionesPorEnlace[enlace] = instalacion;
                }

            }

            foreach (var instalacion in fracasadas)
            {
                _todasLasInstalaciones.Add(instalacion);

                var enlace = instalacion.Enlace;
                if (!string.IsNullOrEmpty(enlace))
                {
                    _instalacionesPorEnlace[enlace] = instalacion;
                }
            }
        }

        public IEnumerable<IInstalacionFO> ObtenerTodasLasInstalaciones()
        {
            return _todasLasInstalaciones;
        }

        public IInstalacionFO ObtenerInstalacionParaEnlace(string enlace)
        {
            return _instalacionesPorEnlace.ContainsKey(enlace) ? _instalacionesPorEnlace[enlace] : _todasLasInstalaciones.FirstOrDefault(instalacion => instalacion.Enlace.Contains(enlace));
        }


        private double? obtenerNumero(IWorksheet worksheet, int fila, int columna)
        {
            var valor = worksheet.Range[fila, columna].Value;
            if (string.IsNullOrEmpty(valor))
            {
                return null;
            }
            else
            {
                if (worksheet.Range[fila, columna].HasNumber)
                {
                    return worksheet.Range[fila, columna].Number;
                }
                else
                {
                    return null;
                }
            }
        }
        private DateTime? obtenerFecha(IWorksheet worksheet, int fila, int columna)
        {
            var valor = worksheet.Range[fila, columna].Value;
            if (string.IsNullOrEmpty(valor))
            {
                return null;
            }
            else
            {
                try
                {
                    return DateTime.ParseExact(valor, FORMATO_FECHA, CultureInfo.InvariantCulture);
                }
                catch (Exception)
                {

                    return null;
                }
            }

        }


        private string obtenerHyperlink(string hyperlinkFormula)
        {
            try
            {
                var x = hyperlinkFormula.Substring(12).Split('"')[0];
                return x;
            }
            catch (Exception)
            {

                return hyperlinkFormula;
            }

        }
    }
}